<?php

/**
 * Remove valores de um array pelas suas chaves. Ex:
 * 
 *      array_remove_keys($user,array('name','id'));
 * 
 * @param array $array Uma array com as chaves e valores
 * @param array $keys Uma array contendo as chaves
 * @return array Retorna a nova array
 */
function array_remove_keys($array, $keys) {
    foreach ($keys as $key) {
        unset($array[$key]);
    }
    return $array;
}

/**
 * Converte uma string com XML para Objeto
 * @param String $xml O xml em string
 * @param array $tags [Opcional] Serve para converter as tags do xml em array com os dados mesmo
 * @param array $index [Opcional] O index da de quais tags continuar
 * @return array
 */
function xml_to_array($xml, $tags = array(), &$index = 0) {
    if (isNotEmpty($xml)) {
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $xml, $tags);
        xml_parser_free($parser);
    }
    $result = array();
    $size = count($tags);
    for (; $index < $size; $index++) {
        $tag = $tags[$index];
        $key = $tag['tag'];
        if ($tag['type'] == "open") {
            $index++;
            $result[$key] = xml_to_array("", $tags, $index);
        } else if ($tag['type'] == "complete") {
            if (array_key_exists($key, $result)) {
                if (is_array($result[$key])) {
                    array_push($result[$key], $tag["value"]);
                } else {
                    $result[$key] = array($result[$key], $tag["value"]);
                }
            } else {
                $result[$key] = $tag["value"];
            }
        } else if ($tag['type'] == "close") {
            return $result;
        }
    }
    return $result;
}

/**
 * Ordena um array
 * @param array $array A array
 * @param string $on O campo que deseja usar para ordenar
 * @param int $order Tipo de ordenação (SORT_ASC,SORT_DESC)
 * @return array A array ordenada
 */
function array_sort($array, $on, $order = SORT_ASC) {
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

/**
 * Função para agilizar o require de views
 * Ex de uso:
 *      requireView("main"); // VIEWS/main/MainView.php
 *      requireView("postOne","post"); // VIEWS/post/postOne/postOneView.php
 *  
 * 
 * @param string $view Nome da view
 * @param string $path [opcional] 
 * @return boolean Se a view existe 
 *      true = existe
 *      false = não existe
 */
function requireView($view, $path = "") {
    $file = new File($view . "View.php", VIEWS . $path . DS . $view);
    if ($file->getValidFile()->exists()) {
        require_once $file . "";
        return true;
    } else {
        $fileRoot = new File($view . ".php", VIEWS . $path);
        if ($fileRoot->getValidFile()->exists()) {
            require_once $fileRoot;
            return true;
        }
        if (DEBUG_PROJECT) {
            throw new FileDontFoundException($file, "Não foi possível requisitar tal view.");
        }
        return false;
    }
}

/**
 * Muda o nome da chave
 * @param array $array
 * @param String $oldKey
 * @param String $newKey
 */
function change_array_key($array, $oldKey, $newKey) {
    $new = array();
    foreach ($array as $key => $value) {
        if ($key == $oldKey) {
            $k = $newKey;
        } else {
            $k = $key;
        }
        $new[$k] = $value;
    }
    return $new;
}

/**
 *
 * Gera um texto com letras e números aleatórios
 * @param int $length Tamanho do texto gerado
 * @param String $alphabet Que letras e números usar para gerar
 * @return type
 */
function randText($length = 10, $alphabet = 'abcdefghijklmnopqrstuvxwyzABCDEFGHIJKLMNOPQRSTUVXZW1234567890') {
    $key = "";
    for ($i = 0; $i < $length; $i++) {
        $rand = rand(0, strlen($alphabet));
        $key.=substr($alphabet, $rand, 1);
    }
    return $key;
}

/**
 * Remover um valor de um array
 * @param Array $array
 * @param mix $value
 * @return array
 */
function array_remove_value($array, $value) {
    $keys = array_keys($array, $value);
    $is_int = false;
    for ($i = count($keys) - 1; $i >= 0; $i--) {
        $key = $keys[$i];
        if (is_int($key)) {
            $is_int = true;
        }
        unset($array[$key]);
    }
    if ($is_int) {
        $result = array();
        foreach ($array as $key => $value) {
            $result[$key] = $value;
        }
    } else {
        $result = $array;
    }
    return $result;
}

/**
 * Retorna o valor de uma variavel de um objeto, se não encontrar essa variavel
 *  retorna um valor default;
 * @param Object $object Objeto do qual deseja extrair a variavel
 * @param String $variableName Nome da variavel que deseja obter seu valor
 * @param mix $default Valor que deseja retorna se não encontrar a variavel
 * @return type
 */
function object_return_valid($object, $variableName, $default = null) {
    $value = @$object->{$variableName};
    if (isset($value)) {
        return $value;
    } else {
        return $default;
    }
}

/**
 * Converte um objeto para array
 */
function object_to_array($data) {
    if (is_array($data) || is_object($data)) {
        $result = array();
        foreach ($data as $key => $value) {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}

/**
 * Retorna um valor da variavel
 * @param array $array Array do qual deseja extrair o valor
 * @param String $key
 * @param mix $default
 * @return mix
 */
function array_return_valid($array, $key, $default = null) {
    if (array_key_exists($key, $array)) {
        return $array[$key];
    } else {
        return $default;
    }
}

/**
 *
 * Exemplo:
 *  return_valid(valor1,valor2,valor3,valorn...);
 * @return mix Retorna o primeiro valor que achar válido.
 *      Se não achar nenhum valor válido retorna null.
 */
function return_valid() {
    $args = func_get_args();
    foreach ($args as $arg)
        if (isset($arg))
            return $arg;
    return null;
}

/**
 * Remove partes de um string. 
 * Ex: str_remove(" henrique","luiz henrique") -> Saída: "luiz"
 * 
 * @param String $remove Parte que deseja remove.
 * @param String $string Toda a string
 */
function str_remove($remove, $string) {
    return str_replace($remove, "", $string);
}

/**
 * Substitui caracteres especiais para o código correspondente para html
 * 
 * @param string|object $string
 * @return string
 */
function toPT($object) {
    if (is_object($object)) {
        if (method_exists($object, "toPT")) {
            return $object->toPT();
        }
    }
    $string = $object;
    $string = str_replace('ã', '&atilde;', $string);
    $string = str_replace('ç', '&ccedil;', $string);
    $string = str_replace('é', '&eacute;', $string);
    $string = str_replace('á', '&aacute;', $string);
    $string = str_replace('í', '&iacute;', $string);
    $string = str_replace('ó', '&oacute;', $string);
    $string = str_replace('ú', '&uacute;', $string);
    $string = str_replace("É", "&Eacute;", $string);
    $string = str_replace("â", "&acirc;", $string);
    $string = str_replace("ê", "&ecirc;", $string);
    return $string;
}

/**
 * Substitui < > pelos respectivos em html
 * @param String $string
 * @return String
 */
function adaptTags($string) {
    $string = str_replace("<", "&lt;", $string);
    $string = str_replace(">", "&gt;", $string);
    return $string;
}

/**
 * Verifica se é um inteiro. (Verifica tbm se uma string tem conteúdo inteiro)
 * @param mixed $value
 */
function isInt($value) {
    return is_numeric($value) && is_int($value + 0);
}

/**
 * Retorna se é a string está vazia
 * 
 * @param String $string
 * @return boolean
 */
function isEmpty($string) {
    try {
        if ($string instanceof stdClass){
            return false;
        }
        if (is_array($string)) {
            return count($string) <= 0;
        }
        return is_null($string) || $string === '' || (strlen($string . "") === 0);
    } catch (Exception $e) {
        Log::getInstance()->addException($e);
    }
}

/**
 * Verifica se a string está não está vazio
 * @param string $string A string que deseja verificar
 * @return boolean true = não está vazio <br> false = está vazio
 */
function isNotEmpty($string) {
    return !isEmpty($string);
}

/**
 * Copia um texto por suas partes pelo lado esquerda
 * Ex:
 * strcopy_l("Muito bom (muito mesmo), (bom) ...","(",")"); Saída->muito mesmo
 * strcopy_l("Muito bom (muito mesmo), (bom) ...","(",")",0,1);  Saída->muito mesmo)
 * @param string $string A string que deseja copiar
 * @param string $inicio A parte de inicio da string
 * @param string $fim A parte do fim da string
 * @param int $adicao_no_inicio Corta uma parte do inicio
 * @param int $adicao_no_fim Aumentar na parte final
 * 
 */
function strcopy_l($string, $inicio = "", $fim = "", $adicao_no_inicio = 0, $adicao_no_fim = 0) {
    if ($adicao_no_inicio === true) {
        $adicao_no_inicio = -strlen($inicio);
    }
    if ($adicao_no_fim === true) {
        $adicao_no_fim = -strlen($fim);
    }
    if (isNotEmpty($inicio)) {
        $pos = strpos($string, $inicio);
        if ($pos !== false) {
            $string = substr($string, $pos + $adicao_no_inicio + strlen($inicio));
        }
    }
    if (isNotEmpty($fim)) {
        $pos = strpos($string, $fim);
        if ($pos !== false) {
            $string = substr($string, 0, $pos + $adicao_no_fim);
        }
    }
    return $string;
}

/**
 * Copia um texto por suas partes pelo lado direito
 * Ex:
 * strcopy_l("Muito bom (muito mesmo), (bom) ...","(",")"); Saída->bom
 * strcopy_l("Muito bom (muito mesmo), (bom) ...","(",")",1,1);  Saída->bom)
 * @param string $string A string que deseja copiar
 * @param string $inicio A parte de inicio da string
 * @param string $fim A parte do fim da string
 * @param int $adicao_no_inicio Corta uma parte do inicio
 * @param int $adicao_no_final Aumentar na parte final
 * 
 */
function strcopy_r($string, $inicio, $fim = "", $adicao_no_inicio = 0, $adicao_no_final = 0) {
    $pos = strrpos($string, $fim);
    if ($pos != 0) {
        $string = substr($string, 0, $pos + $adicao_no_final);
    }
    $pos = strrpos($string, $inicio);
    if ($pos != 0) {
        $string = substr($string, $pos + $adicao_no_inicio);
    }
    return $string;
}

/**
 * Copia um texto por suas partes pelo lado esquerda e direito
 * Ex:
 * strcopy_l("Muito bom (muito mesmo), (bom) ...","(",")"); Saída->muito mesmo), (bom
 * strcopy_l("Muito bom (muito mesmo), (bom) ...","(",")",1,1);  Saída->uito mesmo), (bom)
 * @param string $string A string que deseja copiar
 * @param string $inicio A parte de inicio da string
 * @param string $fim A parte do fim da string
 * @param int $adicao_no_inicio Corta uma parte do inicio
 * @param int $adicao_no_final Aumentar na parte final
 * 
 */
function strcopy_lr($string, $inicio, $fim = "", $adicao_no_inicio = 0, $adicao_no_final = 0) {
    $pos = strpos($string, $inicio);
    if ($pos != 0) {
        $string = substr($string, $pos + $adicao_no_inicio);
    }
    $pos = strrpos($string, $fim);
    if ($pos != 0) {
        $string = substr($string, 0, $pos + $adicao_no_fim);
    }
    return $string;
}

/**
 * Obtém todo o link corrente
 * @return string O link em si
 */
function get_full_path() {
    $s = &$_SERVER;
    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;
    $sp = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
    $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    $uri = $protocol . '://' . $host . $s['REQUEST_URI'];
    $segments = explode('?', $uri, 2);
    $url = $segments[0];
    return $url;
}

/**
 * Função própria de encode json adaptada para caso 
 *  a função padrão (json_encode) não consiga funcionar.
 * @param Array $array
 * @return String
 */
function json_encode_yc($array) {
    if (($json = json_encode($array))) {
        return $json;
    }
    $json = '';
    foreach ($array as $key => $value) {
        if (isNotEmpty($json)) {
            $json.=",";
        }
        $json.="\"$key\":\"$value\"";
    }
    return "{" . $json . "}";
}
