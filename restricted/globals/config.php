<?php

date_default_timezone_set("America/Sao_Paulo");

/**
 * Constantes para debugar
 */
define('DEBUG_PROJECT', true);

/**
 * Constantes para diretórios
 */
define('DS', DIRECTORY_SEPARATOR);
define('RESTRICTED', File::toParentFile(__DIR__) . DS);
define('ROOT', File::toParentFile(RESTRICTED) . DS);
define('CLASSES', RESTRICTED . 'classes' . DS);
define('MODELS', RESTRICTED . 'models' . DS);
define('GLOBALS', RESTRICTED . 'globals' . DS);
define('VIEWS', RESTRICTED . "views" . DS);
define('FRAMEWORKS', RESTRICTED . "frameworks" . DS);
define('CONTROLLERS', RESTRICTED . "controllers" . DS);
define('API',RESTRICTED."api".DS);
define('DOCUMENTATION',RESTRICTED."documentation".DS);

/**
 * Constantes para arquivos
 */
define('FILE_DATABASE',DOCUMENTATION.'Banco.sql');

/**
 * Constantes para urls
 */
$URL_ROOTS = array('', 'YouCurte'); //URLs que podem ser raiz (necessário para o site/API funcionar corretamente)
define('URL_ROOT', 1);
define('URL_CSS', new URL("css", URL_ROOT) . ""); //URL para os css
define('URL_JAVASCRIPT', new URL("js", URL_ROOT) . ""); //URL para os javascripts
define('URL_ERROR_404',new URL("errors/Page404",URL_ROOT).""); //Para páginas não encontradas
define('URL_ERROR_POST_DONT_FOUND', URL_ERROR_404); //Para posts não encontrados
define("URL_LOGGED_IN",new URL("errors/permissionError/in",URL_ROOT).""); //URL para página de erro (usuário deve estar logado)
define("URL_LOGGED_OUT",new URL("errors/permissionError/out",URL_ROOT).""); //URL para página de erro (usuário não deve estar logado)
//define('URL_LOGIN',new URL("login",URL_ROOT));

/**
 * Configuração do banco de dados
 */
if (!strstr(URL::getRootFromURL(), "youcurte.com.br")) { //Verifica se está versão local (localhost)
    define('DB_HOST', "127.0.0.1"); //endereço do banco
    define('DB_USER', "root"); //usuário do banco
    define('DB_PASSWORD', "");  //senha do banco
    define('DB_NAME', "YouCurte"); //nome do banco YouCurte
    define('IS_LOCAL',true);
} else { 
    //Defines os dados para a versão online
    define('DB_HOST', "mysql.hostinger.com.br"); //endereço do banco 
    define('DB_USER', "u288815344_you"); //usuário do banco
    define('DB_PASSWORD', "LAFdU9gfq9");  //senha do banco
    define('DB_NAME', "u288815344_you"); //nome do banco YouCurte
    define('IS_LOCAL',false);
}
define('DB_TYPE', "mysql"); //engine //nome do banco
define('DB_FORMAT_DATE', 'Y-m-d'); //formato de data que o banco trabalha
define('DB_FORMAT_DATETIME', 'Y-m-d H:i:s'); //formato de data e hora que o banco trabalha

/**
 * Constantes para pesquisas
 */
define('COUNT_MAX_RETURN', 10); //Retornar no máximo tal quantidade

/**
 * Constantes para envio de e-mail
 */
define('EMAIL_PASSWORD', 'secretlife007'); //senha do e-mail
define('EMAIL_FROM', 'youcurte@outlook.com');
define('EMAIL_FROM_NAME', 'YouCurte');
define('EMAIL_PORT', 587);
define('EMAIL_HOST', 'smtp.live.com');
define("EMAIL_FOR_MAIL",true);

/**
 * Constantes de Login 
 * (se necessário consultar documentação da class Date Interval)
 */
define('LOGIN_INTERVAL_MIN', 'P1D');
/*
 * Usado quando não foi pedido para manter logado 
 * 'PT1D' mantém o registro por 1 DIA
 */
define('LOGIN_INTERVAL_MAX', 'P30D');
/*
 * Usado quando pedido para manter conectado
 * 'PT30' mantém por 30 Dias
 */

/**
 * Constantes para cookies
 */
define('COOKIE_USER_AUTH', 'auth');
define('COOKIE_TIME_LOGGED_IN', 60 * 60 * 24 * 366);  //definida como um ano
//tempo de ficar logado com cookie

/**
 *  Constantes para usuários
 */
define('USER_IMAGE_DEFAULT', new URL("/img/defaultUser.png", URL_ROOT) . "");
//caminho para a imagem padrão de perfil
define('USER_SEX_MALE', 'male');
 //definição usada no banco para indicar sexo masculino
define('USER_SEX_FEMALE', 'female');
 //definição usada no banco para indicar sexo femininoo
define('USER_PASSWORD_MIN_LENGTH',6);
//tamanho minino para senha
define('USER_PASSWORD_MAX_LENGTH',12);
//tamanho máximo para a senha
define('USER_PASSWORD_DEFAULT','Asdf1234');
 //senha default -- não usada para nada ainda
$USER_SEX_ARRAY = array(USER_SEX_MALE, USER_SEX_FEMALE);
//tipos de sexos;
$USER_STATE_ARRAY = array("PR", "RS", "SC");
//estados que aceita
uksort($USER_STATE_ARRAY, 'strcasecmp');
//ordena por ordem alfabetica os estados
$USER_INTEREST_ARRAY = array("Amizades", "Trovas", "Contatos");
//tipos de interesses
uksort($USER_INTEREST_ARRAY, 'strcasecmp');
//ordenas os interesses por ordem alfabetica

/*
 * Constantes para log
 */
define("LOG_FILE", new File(RESTRICTED . "Log.txt"));
//Caminho para o arquivo de log

/*
 * Constantes para album
 */
define("ALBUM_IMAGE_DEFAULT", USER_IMAGE_DEFAULT); //Imagem para usar como capa para o álbum
define("ALBUM_NAME_DEFAULT", "Default"); //Nome para o álbum default
define("ALBUM_ID_DEFAULT",0); //Id para o álbum default

/*
 * Constantes para imagens
 */
define("IMAGE_URL_PATH", new URL("/img/user/", URL_ROOT) . "");
//URL do diretório de imagens
define("IMAGE_FILE_PATH", new File("/img/user/", ROOT) . "");
//Caminho do diretório de imagens
define("IMAGE_FILE_SIZE_MAX", 2 * 1024 * 1024);
//tamanho maximo de uma imagem
define("IMAGE_TEMP_LONG","PT1H"); //definada como uma hora
//quanto tempo uma imagem temporária deve existir
define("IMAGE_DONT_FOUND",new URL("/img/Image404.png",URL_ROOT)."");
define("COUNT_RETURN_IMAGES",9); //quantidade máxima de imagens a ser retornadas
define("IMAGE_DIRECTORY_TEMP",new File("/img/tmp/",ROOT)."");
/*
 * Constantes para post
 * 
 */
define("POST_TEXT_SIZE_MAX", 150);
//Quantos caracteres um post pode ter
define("POST_NUMBER_MOST_TRI", 5); 
//Quantos posts mais tri exibir na home


/**
 * Constantes para Depoimentos
 */
define("DEPOSITION_TEXT_SIZE_MAX",POST_TEXT_SIZE_MAX); 
//quantidades máxima de caracteres para depoimentos

/**
 * Constantes para hashtags
 */
define("HASHTAGS_ALPHABET","#abcdefghijklmnopqrstuvxwyzáéíóúàèìòù1234567890çãõïÿüñ_");
//Caracteres válidos numa hashtag