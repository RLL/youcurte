<?php

require_once MODELS . "DepositionDAO.php";

/**
 * Description of DepositionAPI
 *
 * @author YouCurte
 */
class DepositionAPI {

    /**
     * 31	CRIAR DEPOIMENTO
      Método: POST
      URL: http://youcurte.ml/api/user/deposition

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function post(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("user", "text"));
            $api->loadUserLoggedIn($data);
            $user = UserDAO::getInstance()->getUser($data->getField("user"));
            $text = $data->getField("text");
            if ($user === null) {
                return $api->returnError("Não foi encontrado o usuário!");
            }
            if ($user->getId()==$api->getUserLoggedIn()->getId()){
                return $api->returnError("Um usuário não pode fazer depoimento para si mesmo!");
            }
            $deposition = new Deposition();
            $deposition->setUserIdFrom($api->getUserLoggedIn()->getId());
            $deposition->setUserIdTo($user->getId());
            $deposition->setText($text);
            if (DepositionDAO::getInstance()->addDeposition($deposition)) {
                return $api->returnOk($deposition);
            } else {
                return $api->returnError("Não possível salvar o depoimento!");
            }
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 32	EDITAR DEPOIMENTO
      Método: PUT
      URL: http://youcurte.ml/api/user/deposition

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function put(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("depositionId", "text"));
            $user = $api->loadUserLoggedIn($data);
            $depositionId = $data->getField("depositionId");
            $text = $data->getField("text");
            if ($user === null) {
                return $api->returnError("Não foi encontrado o usuário!");
            }
            $deposition = DepositionDAO::getInstance()->getDeposition($depositionId);
            if ($deposition === null) {
                return $api->returnError("Depoimento não encontrado!");
            }
            if ($deposition->getUserIdFrom() !== $user->getId()) {
                return $api->returnError("Sem permissão para editar o depoimento!");
            }
            $deposition->setText($text);
            if (DepositionDAO::getInstance()->updateDeposition($deposition)) {
                return $api->returnOk($deposition);
            } else {
                return $api->returnError("Não possível salvar o depoimento!");
            }
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 34	VISUALIZAR/LISTAR DEPOIMENTOS
      Método: GET
      URL: http://youcurte.ml/api/user/deposition/list?user=[user]&count=[count]&offset=[offset]
      http://youcurte.ml/api/user/deposition/list/[user]/[count]/[offset]

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return String
     */
    public function get_list(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("user", "count", "offset");
            $user = $api->getUser($data);
            if ($user === null) {
                return $api->returnError("Usuário não encontrado!");
            }
            $data->prepareOffsetAndCount();
            $count = $data->getField("count");
            $offset = $data->getField("offset");
            $depositions = DepositionDAO::getInstance()->listDepositions($user->getId(), $count, $offset);
            $result = array();
            foreach ($depositions as $deposition) {
                $result[] = $api->returnDeposition($deposition);
            }
            return $api->returnOk($result);
        } catch (Exception $e) {
            return $api->returnError($e);
        }
    }

    /**
     * 34	DELETAR DEPOIMENTO
      Método: DELETE
      URL: http://youcurte.ml/api/user/deposition/

     * 
     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return String
     */
    public function delete(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("depositionId"));
            $user = $api->loadUserLoggedIn($data);
            $depositionId = $data->getField("depositionId");
            $deposition = DepositionDAO::getInstance()->getDeposition($depositionId);
            if ($deposition === null) {
                return $api->returnError("Depoimento não encontrado!");
            }
            if ($deposition->getUserIdFrom() != $user->getId() && $deposition->getUserIdTo() != $user->getId()) {
                return $api->returnError("Sem permissão para deletar o depoimento!");
            }
            if (DepositionDAO::getInstance()->deleteDeposition($deposition)) {
                return $api->returnOk("Deletado com sucesso!");
            } else {
                return $api->returnError("Por album motivo desconhecido não foi possível deletar o depoimento!");
            }
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

}