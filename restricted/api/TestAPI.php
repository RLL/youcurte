<?php

/**
 * Description of TEst
 *
 * @author YouCurte
 */
class TestAPI {

    //put your code here

    public function get_(APIJsonYouCurte $api, DataInput $data) {
        return $api->returnOk($data->getPath() . "");
    }

    public function post_(APIJsonYouCurte $api, DataInput $data) {
        return $api->returnOk($data->getField("value")."");
    }

    public function put_data(APIJsonYouCurte $api, DataInput $data) {
        return $api->returnOk($data->getPath()."");
    }

    public function x_test_(APIJsonYouCurte $api, DataInput $data) {
        return $api->returnOk($data->getPath()."");
    }

    public function delete(APIJsonYouCurte $api, DataInput $data) {
        if ($data->getField("database") === "YouCurte" && IS_LOCAL) {
            $db = new DBConnection(DB_TYPE . ":charset=utf8;host=" . DB_HOST);
            try {
                $db->exec("drop database YouCurte");
            } catch (Exception $e) {
                
            }
            $file = fopen(new File(FILE_DATABASE) . "", "r");
            $sql = "";
            while (!feof($file)) {
                $sql .= fgets($file, 4096);
            }
            foreach (explode(";", "$sql") as $query) {
                try{
                $db->exec($query);
                }catch(Exception $e){
                    
                }
            }
            fclose($file);
            $images = new File(IMAGE_FILE_PATH);
            foreach ($images->listFiles() as $file){
                $file->deleteFile();
            }
            return $api->returnOk("Deletado!");
        } else if ($data->getField("database") === "YouCurte") {
            return $api->returnOk("Não deletado!");
        }
        return $api->returnError("Dados inválido!");
    }

}