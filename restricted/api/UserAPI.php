<?php

/**
 * Classe responsavel por controlar funções sobre usuários na API
 *
 * @author YouCurte
 */
class UserAPI {
    /*
     * Método: POST
     * URL: youcurte.com.br/api/user
     * JSON:
     * {"name":"Luiz","lastName":"Silva","email":"luiz@silva.com",
     *      "password":"1234","telephone":"97598057","city":"montenegro",
     *      "state":"RS","birthDate":"25/12/1995"}
     * 
     * @param DataInput $data
     * @return String
     */

    public function post(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(User::listFieldsNeed());
            $user = new User($data->getDecodedInput());
            $userDAO = UserDAO::getInstance();
            $userDAO->validatesUser($user, true, true);
            $userDAO->addUser($user);
            return $api->returnOk("Novo usuário cadastrado.");
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }

    /**
     * youcurte.com.br/api/login
     * 
     * {"email":"luiz@silva.com","password":"1234","userAgent":"YouCurte"
     *      ,"expiration":"22/04/2017"}
     * 
     * @param DataInput $data
     * @return String
     */
    public function post_login(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("email", "password", "userAgent"));
            $ldao = LoginDAO::getInstance();
            $udao = UserDAO::getInstance();
            $user = $udao->searchUsersByEmail($data->getField("email"));
            if (!$user || $user->getPassword() != $data->getField("password")) {
                return $api->returnError("Talvez o e-mail e a senha não correspondem!");
            }
            $login = new Login();
            $login->generateAuth();
            $login->setExpirationTime($data->getField("expirationTime"));
            $login->setNative(false);
            $login->setUser($user);
            $login->setUserAgent($data->getField("userAgent"));
            $ldao->addLogin($login, true);
            return $api->returnOk(Row::rowToArray($login, array('userId', 'native')));
        } catch (Exception $ex) {
            return $api->returnError($ex->getMessage());
        }
    }

    /**
     * youcurte.com.br/api/user/logout
     * youcurte.com.br/api/user/logout/:auth
     *  
     *  {"auth":"..."}
     *   
     * @param DataInput $data
     */
    public function delete_login_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("auth");
            $data->checkFields(array("auth"));
            $auth = $data->getField("auth");
            if (isEmpty(trim($auth))) {
                return $api->returnError("auth inválido!");
            }
            LoginDAO::getInstance()->deleteLogin($auth);
            return $api->returnOk("Logout realizado com sucesso.");
        } catch (Exception $ex) {
            return $api->returnError($ex->getMessage());
        }
    }

    /**
     * youcurte.com.br/api/user/logout/all
     * youcurte.com.br/api/user/logout/all/:auth
     *  
     *  {"auth":"..."}
     *   
     * @param DataInput $data
     */
    public function delete_login_all_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("auth");
            $api->loadUserLoggedIn($data);
            $login = LoginDAO::getInstance();
            $user = $login->getUserLoggedIn();
            $login->deleteAllLogins($user);
            return $api->returnOk("Logout realizado com sucesso.");
        } catch (Exception $ex) {
            return $api->returnError($ex->getMessage());
        }
    }

    /**
     * Método: GET
     * URL: youcurte.com.br/api/user
     * Exemplo de uso:
     *      http://youcurte.com.br/api/user/?auth=...
     *      http://youcurte.com.br/api/user/12321?auth=...
     *      http://youcurte.com.br/api/user/auth=...
     *      http://youcurte.com.br/api/user/fernando@hotmail.com?auth=...
     * @param DataInput $data
     * @return String
     */
    public function get_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("user");
            if ($data->hasField("auth")) {
                $userLogged = $api->loadUserLoggedIn($data);
            } else if (!$data->hasField("user")) {
                return $api->returnError("É necessário informar um usuário ou"
                                . " chave de login. Verifique a documentação.");
            }
            if ($data->hasField("user")) {
                if (!isInt($data->getField("user"))) {
                    $data->setField("auth", $data->getField("user"));
                    $user = $api->loadUserLoggedIn($data);
                } else {
                    $user = UserDAO::getInstance()->getUser($data->getField("user"));
                }
                if (is_null($user)) {
                    return $api->returnError("Usuário solicitado é inválido!");
                }
            } else {
                $user = $userLogged;
            }
            return $api->returnOk($user);
        } catch (Exception $ex) {
            return $api->returnError($ex->getMessage());
        }
    }

    /**
     * 
     * Método: POST
     * youcurte.com.br/api/user/follow
     * {"auth":"...","user":"luiz@silva.com"}
     * 
     * @param DataInput $data
     * @return String
     */
    public function post_follow_(APIJsonYouCurte $api, DataInput $data) {
        try {
            require_once MODELS . "FollowDAO.php";
            $api->loadUserLoggedIn($data);
            $data->checkFields(array("user"));
            $user = $data->getField("user");
            $userLogged = LoginDAO::getInstance()->getUserLoggedIn();
            $userFollow = UserDAO::getInstance()->getUser($user);
            if (is_null($userFollow)) {
                throw new Exception(
                "O usuário informado que deseja seguir é inválido.");
            }
            $follow = new Follow();
            $follow->setUserId($userLogged->getId());
            $follow->setUserIdFollow($userFollow->getId());
            FollowDAO::getInstance()->addFollow($follow);
            return $api->returnOk("Operação realizada com sucesso!");
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }

    /**
     * 
     * Método: DELETE
     * youcurte.com.br/api/user/follow
     * {"auth":"...","user":"luiz@silva.com"}
     * 
     * @param DataInput $data
     * @return String
     */
    public function delete_follow(APIJsonYouCurte $api, DataInput $data) {
        try {
            require_once MODELS . "FollowDAO.php";
            $api->loadUserLoggedIn($data);
            $data->checkFields(array("user"));
            $user = $data->getField("user");
            $userLogged = LoginDAO::getInstance()->getUserLoggedIn();
            $userFollow = UserDAO::getInstance()->getUser($user);
            if (is_null($userFollow)) {
                throw new Exception("O usuário '$user' informado é inválido.");
            }
            $follow = new Follow();
            $follow->setUserId($userLogged->getId());
            $follow->setUserIdFollow($userFollow->getId());
            FollowDAO::getInstance()->deleteFollow($follow);
            return $api->returnOk("Operação realizada com sucesso!");
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }

    /*
     * Método: PUT
     * URL: youcurte.com.br/api/user/auth
     * JSON:
     * {"name":"Luiz","lastName":"Silva","email":"luiz@silva.com",
     *      "password":"1234","telephone":"97598057","city":"montenegro",
     *      "state":"RS","birthDate":"25/12/1995"}
     * 
     * @param DataInput $data
     * @return String
     */

    public function put_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("auth");
            $user = $api->loadUserLoggedIn($data);
            $userDAO = UserDAO::getInstance();
            if ($data->hasField("password")) {
                $data->checkFields(array("newPassword"));
                $password = $data->getField("password");
                $newPassword = $data->getField("newPassword");
                if ($password != $user->getPassword()) {
                    throw new Exception("A senha não confere com o"
                    . " usuário logado.");
                }
                $data->setField("password", $newPassword);
            }
            $user->setImage($api->getImage($data, $user->getImage()));
            $data->setField("image", "");
            foreach (User::listFields() as $field) {
                if ($data->hasField($field)) {
                    $user->set($field, $data->getField($field));
                }
            }
            $userDAO->validatesUser($user);
            $userDAO->updateUser($user);
            return $api->returnOk("Dados atualizados.");
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }

    /**
     * Retorna dados dessa pessoa
      youcurte.com.br/api/user/search?name
     * youcurte.com.br/api/user/search?name=Luiz+Henrique&offset=0&&count=10
     * youcurte.com.br/api/user/LuizHenrique

      Pesquisar usuários com o nome Luiz
      youcurte.com.br/api/user/search/Luiz
     */
    public function get_search_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("name", "offset", "count", "auth");
            $data->prepareOffsetAndCount();
            if ($data->hasField("auth")) {
                $api->loadUserLoggedIn($data);
            }
            $name = $data->getField("name");
            $offset = $data->getField("offset");
            $count = $data->getField("count");
            $users = UserDAO::getInstance()->searchUsersByName($name, $count, $offset);
            $result = array();
            foreach ($users->getData() as $user) {
                $result[] = $api->returnUser($user);
            }
            $data = array("count" => $users->getCount(), "users" => $result);
            return $api->returnOk($data);
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }

    public function get_related(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("user", "count", "offset");
            $data->prepareOffsetAndCount();
            $user = $api->getUser($data);
            if ($user === null) {
                return $api->returnError("Usuário inválido!");
            }
            $users = FollowDAO::getInstance()->listRelatedUsers($user);
            $result = array();
            foreach ($users as $user) {
                $result[] = $api->returnUser($user);
            }
            return $api->returnOk($result);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 35	EDITAR ATRIBUTOS DEUM USUÁRIO
      Método: POST/PUT
      URL: http://youcurte.ml/api/user/attributes

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function post_attributes(APIJsonYouCurte $api, DataInput $data) {
        try {
            require_once MODELS . "AttributesDAO.php";
            $data->checkFields(array("user"));
            $user = $api->getUser($data);
            $userLogged = $api->loadUserLoggedIn($data);
            if ($user === null) {
                return $api->returnError("Usuário não encontrado!");
            }
            if ($userLogged->getId() == $user->getId()) {
                return $api->returnError("Usuários não podem dar notas para si mesmos!");
            }
            $attributesDAO = AttributesDAO::getInstance();
            $attributes = $attributesDAO->getAttributes($userLogged->getId(), $user->getId());
            $fields = array("sexy", "cool", "reliable");
            foreach ($fields as $field) {
                if ($data->hasField($field)) {
                    $attributes->set($field, $data->getField($field));
                }
            }
            $attributesDAO->validateAttributes($attributes, true);
            if ($attributesDAO->saveAttributes($attributes)) {
                return $api->returnOk(UserDAO::getInstance()->getUser($user->getId()));
            } else {
                return $api->returnError("Por algum motivo não foi possível salvar os atributos!");
            }
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Alias de post_attributes
     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function put_attributes(APIJsonYouCurte $api, DataInput $data) {
        $this->post_attributes($api, $data);
    }

    /**
     * 36	PEGAR OS ATRIBUTOS QUE ENVIOU PARAUM USUÁRIO
      Método: GET
      URL: http://youcurte.ml/api/user/attributes?user=[user]&auth=auth

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function get_attributes(APIJsonYouCurte $api, DataInput $data) {
        try {
            require_once MODELS . "AttributesDAO.php";
            $data->checkFields(array("user"));
            $user = $api->getUser($data);
            $userLogged = $api->loadUserLoggedIn($data);
            if ($user === null) {
                return $api->returnError("Usuário não encontrado!");
            }
            $attributesDAO = AttributesDAO::getInstance();
            if ($userLogged->getId() == $user->getId()) {
                return $api->returnError("Usuários não dão notas para si mesmos, logo não é possível realizar essa ação!");
            } else {
                $attributes = $attributesDAO->getAttributes($userLogged->getId(), $user->getId());
            }
            return $api->returnOk($attributes);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 37	ESQUECEU A SENHA?
      Método: POST
      URL: http://youcurte.ml/api/user/forgetpassword

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function post_forgotpassword(APIJsonYouCurte $api, DataInput $data) {
        try{
            $data->checkFields("email");
            require_once CONTROLLERS."EmailController.php";
            EmailController::getInstance()->sendEmailForgotPassword($data);
            return $api->returnOk("Foi enviado um e-mail com as intruções para redefinir sua senha!");
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }
    
    
    public function x_test(APIJsonYouCurte $api,DataInput $data){
            var_dump($data->getInput()->getContents());
            echo "\r\nFim\r\n";
            exit();
    }

}