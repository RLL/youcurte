<?php

/**
 * Description of AlbumAPI
 *
 * @author YouCurte
 */
class AlbumAPI extends Singleton {

    /**
     * Enviar uma imagem 
     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function post_image(APIJsonYouCurte $api, DataInput $data) {
        try {
            $image = $api->sendImage($data);
            return $api->returnOk($image);
        } catch (Exception $ex) {
            return $api->returnError($ex->getMessage());
        }
    }

    /**
     * Obter dados de uma imagem
     * 
     * Método: GET
      URL: http://youcurte.com.br/api/album/image/[image]
      Exemplo de dados a serem enviados:
      http://youcurte.com.br/api/album/image/12321
      HTTP://youcurte.com.br/api/album/image/aFEdawq.png
      Campos:
      “image” – link,id ou nome da imagem de uma imagem;

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function get_image(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("image", "count", "offset");
            $data->prepareOffsetAndCount();
            if (!$data->hasField("image")) {
                $api->returnError("Não foi informado nenhum paramêtro para pesquisar sobre a imagem");
            }
            if (isInt($data->getField("image"))) {
                $result = ImageDAO::getInstance()->getImage($data->getField("image"));
                if ($result != null) {
                    return $api->returnOk($result);
                } else {
                    return $api->returnError("O id da imagem não foi encontrado!");
                }
            }
            $images = ImageDAO::getInstance()->
                            search("name like :name", array("name" => '%' . $data->getField("name") . '%'), $data->getField("count"), $data->getField("offset"))->getData();
            $result = array();
            foreach ($images as $img) {
                $result[] = $api->returnImage($img);
            }
            return $api->returnOk($result);
        } catch (Exception $ex) {
            return $api->returnError($ex->getMessage());
        }
    }

    /**
     * Função  para definir o campo images a partir de image.
     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function defineImages(APIJsonYouCurte $api, DataInput $data) {
        if ($data->hasField("image")) {
            if ($data->hasField("images")) {
                $data->setField("images", array_merge($data->getField("images"), array($data->getField("image"))));
            } else {
                $data->setField("images", array($data->getField("image")));
            }
        }
        if ($data->hasField("images")) {
            if (is_string($data->getField("images"))) {
                $data->setField("images", array($data->getField("images")));
            }
        }
        if (!$data->hasField("image") && !$data->hasField("images")) {
            throw new RuntimeException("É necessário informar o ID de uma imagem ou uma lista de IDs. (Consulte a documentação)!");
        }
    }

    /**
     * Deletar uma imagem
     * 
      Método: DELETE
      URL: http://youcurte.com.br/api/album/image


     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function delete_image(APIJsonYouCurte $api, DataInput $data) {
        try {
            $user = $api->loadUserLoggedIn($data);
            $this->defineImages($api, $data);
            $result = array();
            foreach ($data->getField("images") as $id) {
                $error = "";
                $image = ImageDAO::getInstance()->getImage($id);
                if ($image === null) {
                    $error = "Imagem não encontrada!";
                } else if ($image->getUserId() != $user->getId()) {
                    $error = "Sem permissão para deletar a imagem! O usuário logado é o dono da imagem?";
                } else if (!ImageDao::getInstance()->deleteImage($image)) {
                    $error = "Por album motivo estranho não foi possível deletar a imagem!";
                }
                if (isEmpty($error)) {
                    $result[] = array("image" => $image->getId(), "link" => $image->getUrl() . "");
                } else {
                    $result[] = array("error" => $error, "image" => $api->returnImage($image));
                }
            }
            return $api->returnOk($result);
        } catch (Exception $e) {
            return $api->returnError($e);
        }
    }

    /**
     * Atualizar os dados de uma imagem
     * 
     * Método: GET
      Método: DELETE
      URL: http://youcurte.com.br/api/album/image


     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function put_image(APIJsonYouCurte $api, DataInput $data) {
        try {
            $imageDAO = ImageDAO::getInstance();
            $user = $api->loadUserLoggedIn($data);
            $this->defineImages($api, $data);
            $result = array();
            foreach ($data->getField("images") as $id) {
                $image = $imageDAO->getImage($id);
                $error = '';
                if ($image === null) {
                    $error = "Imagem não encontrada!";
                } else if ($image->getUserId() != $user->getId()) {
                    $error = "Sem permissão para editar a imagem! O usuário logado é o dono da imagem?";
                }
                if (isEmpty($error)) {
                    if ($data->hasField("name")) {
                        $image->setName($data->getField("name"));
                    }
                    if ($data->hasField("album")) {
                        $album = $api->getAlbum($data);
                        $image->setAlbumId($album->getId());
                    }
                    $imageDAO->validateImage($image, true);
                    $imageDAO->update($image);
                    if (!$imageDAO->wasLastExecutionSuccessful()) {
                        $error = "Por album motivo estranho não foi possível editar a imagem!";
                    }
                }
                if (!isEmpty($error)) {
                    $result[] = array("error" => $error, "image" => $api->returnImage($image));
                }
            }
            return $api->returnOk($result);
        } catch (Exception $e) {
            return $api->returnError($e);
        }
    }

    /**
     * 11	CRIAR ÁLBUM
      Método: POST
      URL: http://youcurte.com.br/api/album

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function post(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("album"));
            $user = $api->loadUserLoggedIn($data);
            $album = new Album();
            $album->setName($data->getField("album"));
            $image = $api->getImage($data, false);
            if ($image) {
                $album->setImage($image);
            }
            $album->setUserId($user->getId());
            AlbumDAO::getInstance()->addAlbum($album);
            return $api->returnOk($album);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 12	EDITAR ÀLBUM
      Método: POST
      URL: http://youcurte.com.br/api/album

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function put(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("albumId"));
            $user = $api->loadUserLoggedIn($data);
            $album = AlbumDAO::getInstance()->getAlbum($data->getField("albumId"));
            if ($album === null) {
                return $api->returnError("Álbum não encontrado!");
            }
            if ($album->getUserId() != $user->getId()) {
                return $api->returnError("Sem permissão para deletar o álbum!");
            }
            if ($data->hasField("album")) {
                $album->setName($data->getField("album"));
            }
            $image = $api->getImage($data, false);
            if ($image) {
                $album->setImage($image);
            }
            AlbumDAO::getInstance()->updateAlbum($album);
            return $api->returnOk($album);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Deletar album
     * Método: DELETE
      URL: http://youcurte.com.br/api/album
      Exemplo de dados a serem enviados:
      12	EDITAR ÀLBUM
      Método: POST
      URL: http://youcurte.com.br/api/album

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function delete(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("albumId"));
            $user = $api->loadUserLoggedIn($data);
            $album = AlbumDAO::getInstance()->getAlbum($data->getField("albumId"));
            if ($album === null) {
                return $api->returnError("Álbum não encontrado!");
            }
            if ($album->getUserId() != $user->getId()) {
                return $api->returnError("Sem permissão para deletar o álbum!");
            }
            if ($data->hasField("album")) {
                $album->setName($data->getField("album"));
            }
            if (AlbumDAO::getInstance()->deleteAlbum($album)) {
                return $api->returnOk($album);
            } else {
                return $api->returnError("Algo inesperado aconteceu e não foi possível deletar o álbum!");
            }
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Obter dados de um album
     * Método: GET
      URL: http://youcurte.com.br/api/album/[album]

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function get(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("album", "user");
            $data->checkFields(array("album", "user"));
            $user = UserDAO::getInstance()->getUser($data->getField("user"));
            if ($user === null) {
                $user = LoginDAO::getInstance()->getUser($data->getField("user"));
                if ($user === null) {
                    return $api->returnError("Usuário não encontrado!");
                }
            }
            $album = AlbumDAO::getInstance()->getAlbum($data->getField("album"), $user);
            if ($album === null) {
                return $api->returnError("Álbum não encontrado!");
            }
            return $api->returnOk($album);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Listar imagens de um álbum
     * Método: GET
      URL: http://youcurte.com.br/api/album/[album]/[user]/[count]/[offset]

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function get_image_list_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("album", "user");
            $data->checkFields(array("album", "user"));
            $data->prepareOffsetAndCount();
            $user = $api->getUser($data);
            $album = AlbumDAO::getInstance()->getAlbum($data->getField("album"), $user);
            if ($album === null) {
                return $api->returnError("Álbum não encontrado!");
            }
            $count = $data->getField("count");
            $offset = $data->getField("offset");
            $result = array();
            $images = ImageDAO::getInstance()->listImages($album->getId(), $user->getId(), $count, $offset);
            foreach ($images as $image) {
                $result[] = $api->returnImage($image);
            }
            return $api->returnOk($result);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Lista albuns
     * Método: GET
      URL: http://youcurte.com.br/api/album/list/[user]/[count]/[offset]

     */
    public function get_list_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("user","count","offset");
            $data->checkFields(array("user"));
            $data->prepareOffsetAndCount();
            $user = $api->getUser($data);
            $albums = AlbumDAO::getInstance()->listAlbums($user);
            $result = array();
            foreach ($albums as  $album){
                $result[] = $api->returnAlbum($album);
            }
            return $api->returnOk($result);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Retorna a instância principal de AlbumAPI
     * @return AlbumAPI
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}