<?php

require_once MODELS . 'PostDAO.php';

/**
 * Description of Post
 *
 * @author YouCurte
 */
class PostAPI {

    /**
     * Preenche o post com os dados de localização e image,
     * @param Post $post
     */
    public function fillPost(APIJsonYouCurte $api, DataInput $data, Post $post) {
        $image = $api->getImage($data, false);
        if ($image) {
            $post->setImage($image);
            $post->setHasImage(true);
        } else if ($post->getImageId() == null) {
            $post->setHasImage(false);
        }
        if ($data->hasField("location")) {
            $post->setLatitude($data->getField("location")["lat"]);
            $post->setLongitude($data->getField("location")["lng"]);
            if (isEmpty($post->getLocal())) {
                PostDAO::getInstance()->getLocal($post);
            }
        }
    }

    /**
     * Postar
     * Método: POST
      URL: http://youcurte.ml/api/post

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return String O JSON Resultante
     */
    public function post(APIJsonYouCurte $api, DataInput $data) {
        try {
            $postDAO = PostDAO::getInstance();
            $api->loadUserLoggedIn($data);
            $user = LoginDAO::getInstance()->getUserLoggedIn();
            $post = new Post($data->getDecodedInput());
            $this->fillPost($api, $data, $post);
            $postDAO->validatePost($post);
            $postDAO->addPost($post);
            return $api->returnOk($post);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 22	VISUALIZAR POSTAGEM
      Método: GET
      URL: http://youcurte.ml/api/post/:id?auth=:auth
      Exemplo de dados a serem enviados:
      http://youcurte.ml/api/post/123?auth=asdf312asdf

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function get(APIJsonYouCurte $api, DataInput $data) {
        try {
            require_once MODELS . "PostHistoryDAO.php";
            $postDAO = PostDAO::getInstance();
            $data->declareFields("postId", "auth");
            $data->checkFields(array("postId"));
            if ($data->hasField("auth")) {
                $user = $api->loadUserLoggedIn($data);
            }
            $post = $postDAO->getInstance()->getPost($data->getField("postId"));
            if ($post === null) {
                return $api->returnError("Post não encontrado!");
            }
            if ($api->getUserLoggedIn() !== null) {
                $history = new PostHistory();
                $history->setPostId($post->getId());
                $history->setUserId($api->getUserLoggedIn()->getId());
                PostHistoryDAO::getInstance()->savePostHistory($history);
            }
            $result = $api->returnPost($post);
            $visitors = PostHistoryDAO::getInstance()->listPostsHistories(null, $post->getId());
            if (isNotEmpty($visitors)) {
                $users = array();
                foreach ($visitors as $visitor) {
                    $user = UserDAO::getInstance()->getUser($visitor->getUserId());
                    $userArray = array();
                    $userArray["name"] = $user->getName();
                    $userArray["lastName"] = $user->getLastName();
                    $userArray["image"] = $user->getImage() . "";
                    $userArray["userId"] = $user->getId();
                    $users[] = $userArray;
                }
                $result["visitors"] = $users;
            }
            if (isNotEmpty($post->getLocal())) {
                $location = array();
                $posts = $postDAO->listPostsFromLocal($post->getLocal());
                foreach ($posts as $post) {
                    $user = UserDAO::getInstance()->getUser($post->getUserId());
                    $userArray = array();
                    $userArray["name"] = $user->getName();
                    $userArray["lastName"] = $user->getLastName();
                    $userArray["userId"] = $user->getId();
                    $location[] = array("postId" => $post->getId(), "user" => $userArray);
                }
                $result["same_location"] = $location;
            }
            return $api->returnOk($result);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 18	EDITAR POSTAGEM
      Método: PUT
      URL: http://youcurte.ml/api/post

     */
    public function put(APIJsonYouCurte $api, DataInput $data) {
        try {
            require_once MODELS . "PostHistoryDAO.php";
            $fields = array("text", "local");
            $postDAO = PostDAO::getInstance();
            $data->declareFields("postId", "auth");
            $data->checkFields(array("postId"));
            $user = $api->loadUserLoggedIn($data);
            $post = $postDAO->getInstance()->getPost($data->getField("postId"));
            if ($post === null) {
                return $api->returnError("Post não encontrado!");
            }
            foreach ($fields as $field) {
                if ($data->hasField($field)) {
                    $post->set($field, $data->getField($field));
                }
            }
            $this->fillPost($api, $data, $post);
            $postDAO->validatePost($post);
            if (($error = $postDAO->updatePost($post)) === false) {
                return $api->returnOk($post);
            } else {
                return $api->returnError($error);
            }
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Delete post
     * Método: DELETE
     * youcurte.com.br/api/post
     * 
     * @param DataInput $data
     * @return String
     */
    public function delete_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $postDAO = PostDAO::getInstance();
            $api->loadUserLoggedIn($data);
            $data->declareFields("postId", "auth");
            $data->checkFields(array("postId"));
            $id = $data->getField("postId");
            $deleted = $postDAO->deletePost($id);
            if ($deleted === false) {
                return $api->returnOk("Deletado com sucesso!");
            } else {
                return $api->returnError("Não foi possível deletar a imagem. " . $deleted);
            }
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }

    /**
     * 20 OPINAR OU EDITAR UMA OPNIÃO SOBRE POSTAGEM
     * Método: POST/PUT (tanto faz qual usar)
      URL: http://youcurte.ml/api/post/opinion

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function post_opinion(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields(array("auth", "postId", "tri", "bah"));
            $api->loadUserLoggedIn($data);
            $opinionDAO = OpinionDAO::getInstance();
            $post = PostDAO::getInstance()->getPost($data->getField("postId"));
            if ($post === null) {
                return $api->returnError("Post não encontrado!");
            }
            $opinion = new Opinion($data->getDecodedInput());
            $opinion->setUserId($api->getUserLoggedIn()->getId());
            $opinionDAO->saveOpinion($opinion);
            return $api->returnOk($post);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * Alias de post_opinion
     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function put_opinion(APIJsonYouCurte $api, DataInput $data) {
        return $this->post_opinion($api, $data);
    }

    /**
     * LISTAR POSTAGENS DE UM USUáRIO
     * Método: GET
     * URL: http://youcurte.com.br/api/post/list?user=[user]
     * http://youcurte.com.br/api/post/list[user]
     * Exemplo de dados a serem enviados:
     * http://youcurte.com.br/api/post/list?user=1
     * 
     * 
     * @param DataInput $data
     * @return String
     */
    public function get_list_(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("user", "count", "offset");
            $data->checkFields(array("user"));
            $user = $api->getUser($data);
            if ($user===null){
                return $api->returnError("Usuário não é válido!");
            }
            $data->prepareOffsetAndCount();
            $count = $data->getField("count");
            $offset = $data->getField("offset");
            $result = PostDAO::getInstance()->listPosts($user, $count, $offset);
            $posts = array();
            foreach ($result as $post) {
                $posts[] = $api->returnPost($post);
            }
            return $api->returnOk($posts);
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }

    /**
     * TimeLine
     * Método: GET
     *  URL: http://youcurte.com.br/api/post/timeline?auth=[auth]&offset=[offset]&count=[count]
     * 	http://youcurte.com.br/api/post/timeline/[auth]/[count]/[offset]
     *
     */
    public function get_timeline_(APIJsonYouCurte $api,DataInput $data) {
        try {
            $data->declareFields("auth", "count", "offset");
            $user = $api->loadUserLoggedIn($data);
            $data->prepareOffsetAndCount();
            $offset = $data->getField("offset");
            $count = $data->getField("count");
            $result = PostDAO::getInstance()
                    ->getTimeline($user, $count, $offset);
            $posts = array();
            foreach ($result as $post) {
                $posts[] = $api->returnPost($post);
            }
            return $api->returnOk($posts);
        } catch (Exception $e) {
            return $api->returnError($e->getMessage());
        }
    }
    
    public function get_search(APIJsonYouCurte $api, DataInput $data){
        try{
            if ($data->hasField("auth")){
                $api->loadUserLoggedIn($data);
            }
            $data->prepareOffsetAndCount();
            $count = $data->getField("count");
            $offset = $data->getField("offset");
            $search = $data->getField("search");
            $posts = PostDAO::getInstance()->searchPostsForHashtags($search, $count, $offset);
            $result = array();
            $result["posts"] = array();
            foreach ($posts->getData() as $post){
                $array = $api->returnPost($post);
                $user = $api->returnUser(UserDAO::getInstance()->getUser($post->getUserId()));
                $array["user"] = $user;
                $result["posts"][] = $array;
            }
            $result["found"] = $posts->getCount();
            return $api->returnOk($result);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

}