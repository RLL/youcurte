<?php

require_once MODELS . "NotificationDAO.php";

/**
 * Description of NotificationAPI
 *
 * @author YouCurte
 */
class NotificationAPI {

    /**
     * 29	OBTER NOTIFICAÇÕES
      Método: GET
      URL: http://youcurte.ml/api/notification/[auth]
      http://youcurte.com.br/api/notification/?auth=[auth]

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     */
    public function get(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->declareFields("auth", "count", "offset");
            $user = $api->loadUserLoggedIn($data);
            $data->prepareOffsetAndCount();
            $count = $data->getField("count");
            $offset = $data->getField("offset");
            $notifications = NotificationDAO::getInstance()->listNotifications($user, $count, $offset);
            $result = array();
            foreach ($notifications as $not) {
                $result[] = $api->returnNotification($not);
            }
            return $api->returnOk($result);
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

    /**
     * 30	EDITAR NOTIFICAÇÕES
      Método: PUT
      URL: http://youcurte.ml/api/notification
      Exemplo de dados a serem enviados:

     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return type
     */
    public function put(APIJsonYouCurte $api, DataInput $data) {
        try {
            $data->checkFields("notificationId", "read");
            $user = $api->loadUserLoggedIn($data);
            $id = $data->getField("notificationId");
            $notification = NotificationDAO::getInstance()->getNotification($id);
            if ($notification === null) {
                return $api->returnError("A notificação não foi encontrada!");
            }
            if ($notification->getUserId() != $user->getId()) {
                return $api->returnError("Sem permissão para editar a notificação!");
            }
            $read = $data->getField("read");
            $read = $read=="1"||$read=="true";
            $notification->setAlreadyRead($read);
            if (NotificationDAO::getInstance()->updateNotification($notification)) {
                return $api->returnOk($notification);
            } else {
                return $api->returnError("Não foi possível modificar a notificação!");
            }
        } catch (Exception $ex) {
            return $api->returnError($ex);
        }
    }

}