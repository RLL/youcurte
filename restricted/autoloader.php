<?php
//Carrega as classes principais
require_once 'globals' . DIRECTORY_SEPARATOR . 'functions.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'File.php';
require_once 'classes' . DIRECTORY_SEPARATOR . 'URL.php';
require_once 'restricted' . DIRECTORY_SEPARATOR . 'globals' . DIRECTORY_SEPARATOR . 'config.php';
require_once CLASSES . "Singleton.php";
require_once CLASSES . 'Log.php';

//Verifica se está debugando o projeto
if (DEBUG_PROJECT) {
    //Define exibir erros e logs na tela
    ini_set('display_errors', 1);
    ini_set('display_startup_erros', 1);
    error_reporting(E_ALL);
}

require_once CONTROLLERS."UserController.php";
require_once CONTROLLERS."ImageController.php";