<?php

require_once 'Path.php';

/**
 * Classe para trabalhar com arquivos
 *
 * @author YouCurte
 */
class File {

    /**
     * Arquivo
     * @var String
     */
    private $file;

    /**
     * Instância um objeto do tipo File (arquivo)
     * @param string $file [optional] O caminho completo ou nome do arquivo
     * @param type $dir [optional] O caminho do diretório do arquivo
     */
    public function __construct($file = "", $dir = "") {
        if (isNotEmpty($dir)) {
            $file = $dir . DS . $file;
        }
        if ($file instanceof File) {
            $this->file = $file->getPath();
        } else {
            $this->file = $file;
        }
    }

    /**
     * Verifica e adapta o caminho do arquivo para ficar válido. 
     *  (Conserta as letras maiusculas e minusculas
     * @return File Returna a própria instancia
     * 
     */
    public function getValidFile() {
        if ($this->exists() || !$this->hasParent()) {
            return $this;
        }
        $parent = $this->getParentFile();
        if (!$parent->exists()) {
            $parent->validateFile();
        }
        if ($parent->exists()) {
            foreach ($parent->listFiles() as $item) {
                if (strtolower($this) == strtolower($item)) {
                    $this->file = $item;
                    break;
                }
            }
        }
        return $this;
    }

    /**
     * Alias de getValidFile()
     * return File
     */
    public function validateFile() {
        return $this->getValidFile();
    }

    /**
     * Pega o caminho do arquivo
     * @return Path
     */
    public function getPath() {
        $url = $this->file;
        return new Path($url);
    }

    /**
     * Se tem pasta/diretório ou não
     * @return boolean
     */
    public function hasParent() {
        $path = $this->getPath();
        if (isEmpty(trim($path))) {
            return false;
        }
        return !strstr($path, DS) || (strpos($path, DS) != strlen($path) - 1);
    }

    /**
     * Retorna o pai(caminho) do arquivo atual
     * @return String
     */
    public function getParentPath() {
        $file = $this->getPath();
        if ($this->hasParent()) {
            while (strrpos($file, DS) == strlen($file) - 1) {
                $file = substr($file, 0, strlen($file) - 1);
            }
            $rpos = strrpos($file, DS);
            $file = substr($file, 0, $rpos);
        } else {
            $file = "";
        }
        return new Path($file);
    }

    /**
     * Retorna um objeto File do diretório do arquivo corrente
     * @return \File
     */
    public function getParentFile() {
        return new File($this->getParentPath());
    }

    /**
     * Converte um objeto File para Path
     * @param String|File $file
     * @return Path
     */
    public static function toPath($file) {
        $f = new File($file);
        return $f->getPath();
    }

    /**
     * Converte-se para URL. Ex:
     *      $file = new File("img/luiz.jpg");
     *      $file->toURL(); //Saída: new URL("http://(raiz)/img/luiz.jpg")
     * @return \URL
     */
    public function toURL() {
        if (strstr($this . "", RESTRICTED)) {
            echo "sim";
            return new URL($this);
        } else {
            $name = str_replace(ROOT, "", $this . "");
            return new URL($name, URL_ROOT);
        }
    }

    /**
     * Pega e retorna o Path do diretório de um arquivo
     * @param String|File $file O arquivo do qual quer pegar o diretório
     * @return Path O path do diretório
     */
    public static function toParentPath($file) {
        $f = new File($file);
        return $f->getParentPath();
    }

    /**
     * Pega e retorna um objeto de File com o diretório do arquivo informado
     * @param string|File $file Arquivo do qual deseja pegar o diretório
     * @return File O objeto File do diretório
     */
    public static function toParentFile($file) {
        $f = new File($file);
        return $f->getParentFile();
    }

    /**
     * Retorna o nome do arquivo
     */
    public function getName() {
        if ($this->hasParent()) {
            $name = $this->getPath()->getLastPart();
            return $name;
        } else {
            return $this->file;
        }
    }

    /**
     * Retorna o nome do arquivo, sem o diretório (alias de getName())
     */
    public function getFilename() {
        return $this->getName();
    }

    /**
     * Retorna o nome do arquivo sem extensão
     * @return type
     */
    public function getNameWithoutExt() {
        return strcopy_r($this->getName(), '', '.');
    }

    /**
     * Verifica se existe o arquivo
     * @return type
     */
    public function exists() {
        return file_exists($this);
    }

    /**
     * Retorna o caminho completo do arquivo
     * @return String
     */
    public function __toString() {
        $result1 = $this->getPath();
        $result2 = str_replace("\\", DS, $result1);
        $result = str_replace("/", DS, $result2);
        return (String) $result;
    }

    /**
     * Lista os arquivos que tem no diretório
     * @return array Array de file - Lista de arquivos
     */
    public function listFiles() {
        $result = array();
        if (($handle = opendir($this))) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $file = new File($entry, $this);
                    $result[] = $file;
                }
            }
            closedir($handle);
        }
        return $result;
    }

    /**
     * Copia o arquivo para um destino (precisa do caminho inteiro)
     * @param mixed $destination O destino 
     * @return bool True = sucesso em copiar
     */
    public function copyTo($destination) {
        $to = new File($destination);
        return copy($this, $to->getValidFile());
    }

    /**
     * Move o arquivo para um destino (precisa do caminho inteiro)
     * @param mixed $destination
     * @return \File Retorna o novo arquivo
     */
    public function moveTo($destination) {
        $to = new File($destination);
        $this->copyTo($to->getValidFile());
        $this->deleteFile();
        return $to;
    }

    /**
     * Deleta o arquivo atual
     * @return bool Se deletou com sucesso o arquivo
     *  true = sucesso
     */
    public function deleteFile() {
        if (!$this->exists()) {
            return false;
        }
        $result = unlink($this);
        if ($result||!$this->exists()){
            return true;
        }
        return $result;
    }

    /*
     * @return String Retorna a extensão do arquivo;
     */
    public function getFileExtension() {
        return strcopy_r($this->getName(), ".");
    }

}

class FileDontFoundException extends Exception {

    public function __construct($file, $message = "",$code = null,$previous=null) {
        parent::__construct("O arquivo '$file' não foi encontrado.  " . $message
                ,$code,$previous);
    }

}
