<?php

/**
 * Classe para gerenciar URLs
 *
 * @author YouCurte
 */
class URL {

    private static $root;
    private $path;

    /**
     * Método construtor
     * @param Mix $url Aceita os mesmos tipos que o $parent
     * @param Mix $parent 
     *  Valores aceitos: String, URL, Path, se duvidar File também.
     *      Também aceita a constante URL_ROOT
     *  Se o parent não for informado vai pegar o url atual
     *      
     */
    public function __construct($url = "", $parent = "") {
        if ($parent === URL_ROOT) {
            $parent = URL::getRootFromURL();
        }
        if ((stripos($parent . "", "http") === 0)&&(stripos($url . "", "http"))===0) {
            $url = new Path($url);
            $url = $url->copy(2);
        }
        if ($url instanceof URL) {
            if (isNotEmpty($parent)) {
                $parent = new Path($parent);
                $url = $parent->union($url->getPath());
                $parent = "";
            } else {
                $url = $url->getPath() . "";
            }
        } else if (isEmpty($parent)) {
            if (strpos($url, "http") !== 0) {
                $parent = get_full_path();
            }
        }
        if (is_null($this->path)) {
            $this->path = new Path($url, $parent);
        }
    }

    /**
     * Retorna o path do URL (tipo uma conversão)
     * @return Path
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Retorna a raiz do URL. Ex:
     *      $url = new URL("http://www.youcurte.com.br");
     *      print $url->getRoot(); Imprime na tela www.youcurte.com.br
     * @global string $URL_ROOTS Possíveis raizes para url
     * @return \URL
     */
    public function getRoot() {
        if ($this->getPath()->getCountOfParts() == 2) {
            return $this;
        } else if (self::hasRoot($this)) {
            return new URL($this->copy(0, 2));
        }
        return new URL($this->getPath()->copy(0, 1));
    }

    /**
     * Converte o URL para arquivo (não implemetando/funciona para as views)
     * @return \File
     */
    public function toFile() {
        $path = substr($this, strlen($this->getRoot()));
        $file = new File($path, ROOT);
        $file->getValidFile();
        return $file;
    }

    /**
     * Compara o URL com outro URL
     * @param URL $url
     * @return boolean true = igual <br> false = desigual
     */
    public function equals($url) {
        $str = new URL($url);
        return ("$str" == "$this");
    }

    /**
     * Converte o objeto URL para String novamente sem erros
     * @return String
     */
    public function __toString() {
        $str = $this->path . "";
        $root = $this->getPath()->getPart(1);
        $rootURL = URL::getRootFromURL()->getPath()->getPart(1);
        $resultRoot = $root;
        $str = $this->getPath()->getFirstPart() . "//" . $resultRoot
                . "/" . $this->getPath()->copy(2);
        return str_replace(DS, "/", $str);
    }

    /**
     * Copia partes do url e retorna em outro objeto do tipo URL
     * @param int $start inicia em 0
     * @param int $end [opcional]inicia em 0
     * @return \URL
     */
    public function copy($start, $end = null) {
        return new URL($this->getPath()->copy($start, $end));
    }

    /**
     * Retorna o URL atual (escrito no navegador)
     * @return \URL
     */
    public static function getFromURL() {
        return new URL();
    }

    /**
     * Retorna a raiz do URL atual
     * @return URL
     */
    public static function getRootFromURL() {
        if (isset(self::$root)) {
            return self::$root;
        } else {
            $url = self::getFromURL();
            self::$root = $url->getRoot();
            return self::$root;
        }
    }

    /**
     * Verifica se o link atual é a igual a raiz. Ex:
     *      //URL atual = http://youcurte.com.br
     *      isCurrentRoot() retorna true
     *      //URL atual = http://youcurte.com.br/profile
     *      isCurrentRoot() retorna false
     * 
     */
    public static function isCurrentRoot() {
        $obj = self::getRootFromURL();
        return $obj->equals(self::getFromURL());
    }

    /**
     * Verifica se a URL tem um dos dominios do site
     * @param URL $url
     */
    public static function hasRoot(URL $url) {
        global $URL_ROOTS;
        foreach ($URL_ROOTS as $value) {
            if (isNotEmpty($value)) {
                if (strpos($url->getPath()->copy(2), new Path($value) . "") === 0) {
                    return true;
                }
            }
        }
        return false;
    }

}
