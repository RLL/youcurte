<?php

/**
 * Classe de log
 *
 * @author YouCurte
 */
class Log extends Singleton {

    private $file;
    
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Adiciona uma linha no arquivo de log
     * @param String $line
     * @param boolean $open [opcional] Se é para abrir o arquivo
     */
    public function addLine($line, $open = true) {
        if ($open) {
            $this->openFile();
        }
        $datetime = new DateTimeYC();
        fwrite($this->file, $datetime->toPT() . ": " . $line."\r\n");
        if ($open) {
            $this->closeFile();
        }
    }

    /**
     * Adicionar um log da exception no arquivo de log
     * @param Exception $exception
     */
    public function addException(Exception $exception) {
        $this->openFile();
        $this->addLine("-------------------------",false);
        $this->addLine($exception->getMessage(),false);
        $this->addLine($exception->getTraceAsString(),false);
        $this->addLine("-------------------------",false);
        $this->closeFile();
    }

    /**
     * Realizar a descarga no arquivo de log
     */
    public function flush() {
        fflush($this->file);
    }

    /**
     * Abre o arquivo de log
     */
    public function openFile() {
        $this->file = fopen(LOG_FILE, "a");
    }

    /**
     * Fecha o arquivo de log
     */
    public function closeFile() {
        fclose($this->file);
    }

    /**
     * Obtém a instância principal de Log
     * @return Log
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}