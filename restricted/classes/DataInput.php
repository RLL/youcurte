<?php

require_once "Input.php";

/**
 * Classe para lidar com os dados de entrada
 *
 * @author YouCurte
 */
class DataInput {

    protected $path, $contents, $code, $array, $isCoded, $input;

    /**
     * Metódo contrutor da classe. 
     *  Se não informado um dos parametros pegua os valores default (..::getInstance())
     * @param Path $path [opcional]O path do link
     * @param string $contents [opcional]O conteúdo da input
     * @param Input $input [opcional]Um objeto da classe Input para pegar os parametros/campos
     *    
     */
    public function __construct($path = null, $contents = null, Input $input = null) {
        if ($path == null) {
            $path = Path::getFromUrl();
        }
        if ($contents == null) {
            $contents = Input::getInstance()->getContents();
        }
        $this->array = array();
        $this->path = new Path($path);
        $this->contents = $contents;
        $this->code = null;
        $this->input = $input;
        $this->setCoded(true);
        if (is_null($this->input)) {
            $this->input = Input::getInstance();
        }
    }

    /**
     * Retorna o path da url
     * @return Path
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Define o path da url
     * @param type $path
     */
    public function setPath($path) {
        $this->path = $path;
    }

    /**
     * O conteúdo enviado
     * @return type
     */
    public function getContents() {
        return $this->contents;
    }

    /**
     * Verifica se tem um conteúdo válido
     * @return boolean
     */
    public function hasContents() {
        return isNotEmpty(trim($this->getContents()));
    }

    /**
     * Se tem um determinado campo. Ex de como funciona:
     *     isset($_GET[$field])
     *     isset($_POST[$field])
     * @param String $field
     * @return boolean 
     *      true = existe
     *      false = não existe
     */
    public function hasField($field) {
        return isNotEmpty($this->getField($field));
    }

    /**
     * Se os dados estão codificados como XML e JSON
     *  
     * @return boolean
     *      true = Está codificado
     *      false = não está codificado
     */
    public function isCoded() {
        return $this->isCoded;
    }

    /**
     * Define se o conteúdo está codificado como XML ou JSON
     * @param type $value
     */
    public function setCoded($value) {
        $this->isCoded = $value;
    }

    /**
     * Retorna o conteúdo decodificado
     * @return Object
     * @throws Exception
     */
    public function getDecodedInput() {
        if (is_null($this->code)) {
            $content = trim($this->getContents());
            $id = substr($content, 0, 1);
            $obj = $error = '';
            if (isEmpty($content)) {
                return $obj;
            }
            if ($id === "{") {
                $obj = json_decode($content);
                $error = "Código JSON inválido.";
            } else if ($id === "<") {
                $obj = xml_to_array($content);
                $error = "Código XML inválido.";
            }
            if (!isset($obj) || $obj === false) {
                throw new Exception($error);
            }
            if ($id === "<") {
                if (!array_key_exists("YouCurte", $obj)){
                    throw new RuntimeException("Coloque o xml dentro da tag <YouCurte></YouCurte>!");
                }
                $this->code = $obj["YouCurte"];
            } else {
                $this->code = $obj;
            }
        }
        return $this->code;
    }

    /**
     * Retorna o valor de um campo. Ex:
     *      $input->getParameter($field)
     * @param String $field Nome do campo
     * @return String Retorna o valor do campo.
     *      Se não achar o campo retorna null
     */
    public function getField($field) {
        if (array_key_exists($field, $this->array)) {
            return $this->array[$field];
        }
        $input = Input::getInstance();
        if ($input->hasParameter($field)) {
            return $input->getParameter($field);
        } else if (isEmpty($this->getContents()) || !$this->isCoded()) {
            return null;
        } else if (is_array($this->getDecodedInput())){
            return array_return_valid($this->getDecodedInput(),$field);
        }
        $obj = object_return_valid($this->getDecodedInput(), $field);
        if ($obj instanceof stdClass){
            return object_to_array($obj);
        }
        return $obj;
    }

    /**
     * Define um valor para um campo, substituindo o valor existente.
     *  Não substitui o valor do objeto Input que usa para ler os campos.
     * @param string $field O nome do campo
     * @param mixed $value O valor do campo
     */
    public function setField($field, $value) {
        $this->array[$field] = $value;
    }

    /**
     * Declara campos. Maneira rápida para declarar campos
     * Ex de uso:
     *  
     *  declareFields(array("user"=>1,"auth"=>2)); 
     *      -- atribui esses valores aos campos apenas se não eles não tiverem valores.
     *      "user"=>1 e "auth"=>2 //definições resultantes
     * 
     *  declareFields("auth","user",...);
     *      -- define para esses campos valores relacionados ao path informado.
     *      path = "1234/Luiz" //valor da variavel path
     *      "auth"=>"1234" //definições resultantes
     *      "user"=>"Luiz"
     * 
     *  declareFields(array("user"=>1,"auth"));
     *      --intercala as funções
     *      path = "1234/luiz"
     *      "user"=>1 //definições resultantes
     *      "auth"=>"1234"
     * 
     */
    public function declareFields($param) {
        $countPath = $this->path->getCountOfParts();
        $fields = func_get_args();
        $count = func_num_args();
        if ($count > 1) {
            $param = $fields;
        } else if (!is_array($param)) {
            $param = array($param);
        }
        $i = 0;
        foreach ($param as $key => $field) {
            if (is_string($key) && !$this->hasField($key)) {
                $this->setField($key, $field);
            } else if ($i < $countPath) {
                $this->setField($field, $this->getPath()->getPart($i++));
            }
        }
    }

    /**
     * checkFields(array("name","age");
     * checkFields("name","age");
     * Verifica os campos informados.
     * @return Exception
     * @throws WithoutDataException
     * @throws DataDontFoundException
     */

    /**
     * Verifica se os campos informados tem algum valor válido.
     * 
     * Ex de uso:
     *  checkFields(array('name','age'),array('nome','idade'));
     *  Saída: 'Os seguintes campos são necessários: nome.'
     *  checkFields(array('name','age'),array('nome','idade'),'b');
     *  Saída: 'Os seguintes campos são necessários: <b>nome</b>.';
     * 
     * @return mix Retorna uma mensagem se tiverem campos faltando.
     *  Retorna false se tiver tudo ok.
     */
    public function checkFields($fields = null, $labels = null, $tag = "") {
        if (!is_array($fields)){
            $fields = array($fields);
        }
        if (is_null($labels)) {
            $labels = $fields;
        }
        $emptys = array();
        foreach ($fields as $key => $field) {
            if (isEmpty($this->getField($field))) {
                $emptys[] = $labels[$key];
            }
        }
        if (count($emptys) == 0) {
            return false;
        }
        $msg = "";
        foreach ($emptys as $field) {
            if (isNotEmpty($msg)) {
                $msg.=",";
            }
            if (isEmpty($tag)) {
                $msg.=$field;
            } else {
                $msg.="<$tag>$field</$tag>";
            }
        }
        $resultMsg = "Os seguintes campos são necessários: $msg.";
        if ($this->isCoded()) {
            throw new DataDontFoundException($msg);
        }
        return $resultMsg;
    }

    /**
     * Verifica os campos offset e count, se eles não forem definidos seus valores
     *  são setados para count = count_max_return e offset = 0;
     */
    public function prepareOffsetAndCount() {
        if ($this->hasField("count")) {
            //$count = $data->getField("count");
        } else {
            $this->setField("count", COUNT_MAX_RETURN);
        }
        if ($this->hasField("page") && !$this->hasField("offset")) {
            $this->setField("offset", $this->getField("count") * $this->getField("page"));
        } else if (!$this->hasField("offset")) {
            $this->setField("page", "0");
        }
        if ($this->hasField("offset")) {
            //$offset = $data->getField("offset");
        } else {
            $this->setField("offset", 0);
        }
    }

    /**
     * Retorna o objeto Input que pega os valores dos campos
     * @return Input
     */
    function getInput() {
        return $this->input;
    }

    /**
     * Define o objeto Input de onde será pego os valores de campos
     * @param Input $input
     */
    function setInput($input) {
        $this->input = $input;
    }

}

class DataDontFoundException extends Exception {

    public function __construct($fields, $code = null, $previous = null) {
        parent::__construct("Os seguintes dados são necessários: " . $fields . ".", $code, $previous);
        $this->code = $code;
    }

}

class WithoutDataException extends Exception {

    public function __construct($msg = "", $code = null, $previous = null) {
        parent::__construct("Nenhum um dado(conteúdo) informado. " . $msg, $code, $previous);
    }

}
