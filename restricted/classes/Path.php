<?php

/**
 * Classe para lidar com paths
 *
 * @author YouCurte
 */
class Path {

    protected $path;
    private $last_index, $parts;

    /**
     * Método construtor
     * @param string|path|array $path O path/caminho
     * @param string|path|array $parent O path pai
     * @return Path
     */
    public function __construct($path = NULL, $parent = "") {
        if (!isset($path)) {
            $path = filter_input(INPUT_GET, "path");
        } else if (is_array($path)){
            if (is_array($parent)){
                $path = array_merge($path,$parent);
            }
            $this->parts = $path;
            return;
        }
        $path1 = $this->fix($path, $parent);
        $path2 = str_replace("\\", DS, $path1);
        $path3 = str_replace("/", DS, $path2);
        $this->path = $path3;
        $parts = array();
        for ($i = 0; strstr($path3, DS);) {
            $part = strcopy_l($path3, "", DS);
            if (!empty($part) || $part === 0 || $part === "0") {
                $parts[$i++] = $part;
            }
            $path3 = strcopy_l($path3, DS);
        }
        if (isNotEmpty($path3)) {
            $parts[$i++] = $path3;
        }
        $this->last_index = $i - 1;
        $this->parts = $parts;
    }

    /**
     * Une o path e o parent e conserta qualquer incoerência
     * @param string $path
     * @param string $parent
     * @return string Retorna o path com o parent sem erros
     */
    private function fix($path, $parent) {
        if (is_null($parent)) {
            $parent = '';
        }
        if (is_null($path)){
            return NULL;
        }
        if (isNotEmpty($parent)){
            $path = $parent . DS . $path;
        }
        $origin = $path;
        $url1 = str_replace("\\", DS, $path);
        $url2 = str_replace("/", DS, $url1);
        //$inicio = substr($url2, 0, 1) === DS;
        while (strstr($url2, DS . DS)) {
            $url2 = str_replace(DS . DS, DS, $url2);
        }
        /* if ($inicio) {
          $url = DS . $url;
          } */
        //$url = str_replace(":\\", ':\\\\', $url);
        if (substr($origin, 0, 2) === '//') {
            $url2 = '/' . $url2;
        }
        return $url2;
    }

    /**
     * Retorna o index da última parte
     * @return int
     */
    public function getLastIndex() {
        return $this->last_index;
    }

    /**
     * Retorna a quantidade de partes
     * @return int
     */
    public function getCountOfParts() {
        return $this->getLastIndex() + 1;
    }

    /**
     * Retorna a quantidade de partes (alias: getCountOfParts())
     * @return int
     */
    public function countParts() {
        return $this->getCountOfParts();
    }

    /**
     * Verifica se tem uma parte com tal conteúdo
     * @param string $part
     * @return boolean true = tem <br> false = não tem
     */
    public function hasPart($part) {
        return array_search($part, $this->parts);
    }

    /**
     * Verifica se tem uma parte com tal index
     * @param int $index O index que deseja verificar
     * @return boolean true = tem <br> false = não tem
     */
    public function hasPartIndex($index) {
        return $index >= 0 && $index <= $this->getLastIndex();
    }

    /**
     * Retorna uma parte do conteúdo
     * @param int $index O index da parte
     * @return string O conteúdo da parte
     */
    public function getPart($index) {
        if ($index < 0 || (count($this->parts) === 0)){
            return '';
        }
        return $this->parts[$index];
    }

    /**
     * Retorna uma lista das partes
     * @return array
     */
    public function listParts() {
        return $this->parts;
    }

    /**
     * Converte o path em string (Retorna o caminho no formato string,
     *  sem erros)
     * @return string
     */
    public function __toString() {
        return $this->path;
    }

    /**
     * Retorna a si mesmo
     * @return \Path
     */
    public function getPath() {
        return $this;
    }

    /**
     * Retorna o conteúdo da primeira parte
     * @return string
     */
    public function getFirstPart() {
        return $this->getPart(0);
    }

    /**
     * Retorna o conteúdo da última parte
     * @return string
     */
    public function getLastPart() {
        return $this->getPart($this->getLastIndex());
    }

    /**
     * Copia partes desse path e retorna um path com essas partes
     * @param int $index_inicial (valor entre 0 e o total-1)
     * @param int $index_final (valor etnre 0 e o total-1)
     * @return Path 
     */
    public function copy($index_inicial, $index_final = null) {
        $result = "";
        if (is_null($index_final)) {
            $index_final = $this->getCountOfParts() - 1;
        }
        for ($i = $index_inicial; $i <= $index_final; $i++) {
            $result = new Path($this->getPart($i), $result);
        }
        return new Path($result);
    }

    /**
     * Retorna por exemplo:
     *      se o link for: http://youcurte.com.br/api/user
     *      retorna: api/user
     * @return \Path
     */
    public static function getFromURL() {
        return new Path();
    }

    /**
     * Converter para URL e retorna o resultado
     * @return \URL
     */
    public function toURL() {
        return new URL($this);
    }

    /**
     * Verifica se o path está vazio (Seu conteúdo é vazio)
     * @return boolean
     *      true = está vazio <br> false = não está vazio
     */
    public function isEmpty() {
        return $this->countParts() == 0;
    }

    /**
     * Uni um path com outro sem repetições.
     * Ex:
     *  new Path("luiz/henrique")->union(new Path("luiz/henrique/silva"),true)
     *  Saída: "luiz/henrique/silva"
     *  new Path("luiz/henrique")->union(new Path("henrique/silva"),false)
     * Saída: "luiz/henrique/silva"
     * @param Path $more O path que quer ir unir
     * @param boolean $all Verifica todo o path
     * @return Path
     */
    public function union(Path $more, $all = true) {
        foreach ($this->parts as $part){
            if ($part==$more->getPart(0)){
                $all = true;
                $more = $more->copy(1);
            } else if ($all){
                break;
            }
        }
        return $this->join($more);
    }

    /**
     * Junta um path com outro
     * @param Path $arg
     * @return Path
     */
    public function join(Path $arg) {
        return new Path($arg,$this);
    }

    /**
     * Define o valor para uma parte do path
     * @param int $index O index da parte
     * @param String $value O valor da parte
     */
    public function setPart($index,$value){
        $this->parts[$index] = $value;
    }
    
}