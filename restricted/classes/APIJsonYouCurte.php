<?php

require_once CLASSES . 'YouCurte.php';
require_once MODELS . 'UserDAO.php';
require_once MODELS . 'LoginDAO.php';

class APIJsonYouCurte extends YouCurte {

    public function __construct() {
        
    }

    /**
     * Encontra e carrega 
     * @param Path $path
     * @param boolean $print Imprimir na tela os resultados <br/>
     *      true = sim <br/> não
     * @return String
     */
    public function run(Path $path, $print = true) {
        $result = parent::run($path);
        $origin = $path;
        if (!$this->isOk($result)) {
            $error = $result;
            $php = new File($path->getFirstPart() . "API.php", API);
            try {
                if ($php->getValidFile()->exists()) {
                    $path = $path->copy(1);
                    $result = $this->executeMethod($php, $path);
                }
            } catch (Exception $e) {
                Log::getInstance()->addException($e);
            }
            if (!$this->isOk($result) && DEBUG_PROJECT) {
                Log::getInstance()->addLine(json_decode($result)->error . "\r\n" . $error . "  -  " . $origin . "\r\n");
            }
        }
        if ($print) {
            print $result;
        }
        return $result;
    }

    /**
     * Executa um método
     * @param File $php O php que tem a classe
     * @param Path $path O path que contém o nome do método
     * @return String A resposta do método
     */
    public function executeMethod(File $php, Path $path) {
        if (!$php->getValidFile()->exists()) {
            return $this->returnError("O método informado não faz parte da API! Consulte a documentação.");
        }
        require_once $php;
        $reflection = new ReflectionClass($php->getNameWithoutExt());
        $obj = $reflection->newInstance();
        $method = $this->searchMethod($obj, $path);
        if ($method === false) {
            return $this->returnError("Método não encontrado! Consulte a documentação.");
        }
        $subpath = new Path(str_replace("_", DS, $method));
        $data = new DataInput($path->copy($subpath->countParts() - 1));
        return call_user_func_array(array($obj, $method), array($this, $data));
    }

    /**
     * Procura e retorna o nome de um método dentro de uma instância
     * @param Object $obj Uma instância da classe
     * @param Path $path O path que contém o método
     * @param String $method_request [Opcional] Indica se é get,put,delete,post
     * @return String|boolean O nome do método. Retorna <b>false</b> se não encontrado o método.
     */
    public function searchMethod($obj, Path $path, $method_request = "") {
        if (isEmpty($method_request)) {
            $method_request = $this->getRestMethod();
        }
        $separator = "";
        if (!$path->isEmpty()) {
            $separator = "_";
        }
        $method_name = str_replace(DS, "_", $path . "");
        $method1 = $this->existMethod($obj, $method_request . $separator . $method_name);
        $method2 = $this->existMethod($obj, "x" . $separator . $method_name);
        if ($method1) {
            return $method1;
        } else if ($method2) {
            return $method2;
        }
        if ($path->isEmpty()) {
            return false;
        }
        $newPath = $path->copy(0, $path->countParts() - 2);
        return $this->searchMethod($obj, $newPath, $method_request);
    }

    /**
     * Verifica se existe um método que atenda o nome do método informado
     * @param Object obj A intância da classe
     * @param String $method_full_name o nome do método
     * @return boolean|string Retorna o nome do método se existe e retorna 
     *      <b>false</b> se não encontrado.
     */
    public function existMethod($obj, $method_full_name) {
        $method_name = $method_full_name;
        for ($i = 0; $i < 2; $i++) {
            if (method_exists($obj, $method_name)) {
                return $method_name;
            }
            $method_name.="_";
        }
        return false;
    }

    /*
     * Funções básicas da classe
     */

    /**
     * Retorna o usuário logado
     * @return User
     */
    public function getUserLoggedIn() {
        return LoginDAO::getInstance()->getUserLoggedIn();
    }

    /**
     * Pega e retorna usuário (deve ter no $data o campo user)
     * @param DataInput $data
     * @return User
     */
    public function getUser(DataInput $data) {
        $user = UserDAO::getInstance()->getUser($data->getField("user"));
        if ($user === null) {
            $user = LoginDAO::getInstance()->getUser($data->getField("user"));
            if ($user !== null) {
                LoginDAO::getInstance()->setUserLoggedIn($user);
            }
        }
        return $user;
    }

    /**
     * Verifica se alguém está logado
     * @return boolean true = logado <br/> false = não logado
     */
    public function isAnyoneLoggedIn() {
        return LoginDAO::getInstance()->isAnyoneLoggedIn();
    }

    /**
     * Retorna um array preparado para ser reenviado para o cliente,
     *  com os valores do post .
     * @param Post $post
     */
    public function returnPost(Post $post) {
        $array = array();
        $list = array("text", "local", "tri", "bah", "myBah", "myTri",
            "location", "userId");
        PostDAO::getInstance()->preparePost($post);
        foreach ($list as $item) {
            $value = $post->get($item);
            if (isset($value)) {
                $array[$item] = $value;
            }
        }
        $array["time"] = $post->getTime()->toPT();
        $array["postId"] = $post->getId();
        if ($post->getImage() != null) {
            $array = $this->returnImage($post->getImage(), $array);
        }
        $array = change_array_key($array, "myBah", "my_bah");
        $array = change_array_key($array, "myTri", "my_tri");
        if (isNotEmpty($post->getLocal())) {
            $array["local"] = $post->getLocal();
        }
        return $array;
    }

    /**
     * Retorna de forma padronizada para a conversão em JSOn JSON
     * @param Notification $notification
     * @return Array
     */
    public function returnNotification(Notification $notification) {
        $array = array();
        $array["notificationId"] = $notification->getId();
        $array["text"] = $notification->getText();
        $array["link"] = $notification->getLink();
        $userArray = array();
        $user = UserDAO::getInstance()->getUser($notification->getUserIdFrom());
        $userArray["name"] = $user->getName();
        $userArray["lastName"] = $user->getLastName();
        $userArray["image"] = $user->getImage() . "";
        $userArray["id"] = $user->getId();
        $array["read"] = $notification->wasAlreadyRead();
        $array["user"] = $userArray;
        return $array;
    }

    /**
     * Retorna de forma padronizada para a conversão em JSON
     * @param Deposition $deposition
     * @return Array
     */
    public function returnDeposition(Deposition $deposition) {
        $array = array();
        $array["depositionId"] = $deposition->getId();
        $array["text"] = $deposition->getText();
        $userArray = array();
        $user = UserDAO::getInstance()->getUser($deposition->getUserIdFrom());
        $userArray["name"] = $user->getName();
        $userArray["lastName"] = $user->getLastName();
        $userArray["image"] = $user->getImage() . "";
        $userArray["id"] = $user->getId();
        $array["userFrom"] = $userArray;
        $array["userIdTo"] = $deposition->getUserIdTo();
        return $array;
    }

    /**
     * Prepara para retornar dados de uma imagem
     * @param Image|int|string $image A imagem em si ou o seu id o url tbm vale
     * @param type $array Usar esse array para (se não informado cria um novo)
     * @return array Retorna o resultado
     */
    public function returnImage($image, $array = null, $default = IMAGE_DONT_FOUND) {
        $image = ImageDAO::getInstance()->getImage($image);
        if ($image !== null) {
            if (isset($array)) {
                $vetor = $array;
            } else {
                $vetor = array();
                if (isset($image)) {
                    $images = ImageDAO::getInstance()->getImageNeighbors($image->getId(), $image->getAlbumId(), $image->getUserId());
                    if (count($images) == 2) {
                        for ($i = 0; $i < 2; $i++) {
                            if ($images[$i] === null) {
                                $images[$i] = $image;
                            }
                        }

                        $vetor["prevImage"] = $images[0]->getValidUrl() . "";
                        $vetor["nextImage"] = $images[1]->getValidUrl() . "";
                    }
                    $vetor["imageTime"] = $image->getTime()->toDB();
                    $vetor["albumId"] = $image->getAlbumId();
                    $vetor["userId"] = $image->getUserId();
                    $album = AlbumDAO::getInstance()->getAlbum($image->getAlbumId(), $image->getUserId());
                    if ($album != null) {
                        $vetor["album"] = $album->getName();
                    }
                }
            }
        }
        if (!isset($image)) {
            $vetor["image"] = $default;
        } else {
            $vetor["imageId"] = $image->getId();
            $vetor["image"] = $image->getValidUrl() . "";
            $vetor["imageName"] = $image->getName();
        }
        return $vetor;
    }

    /**
     * Retorna um array pronto para ser retornado para o app
     * @param User $user
     * @param boolean $all Retorna todas as informações sem retrição
     * @return Array Retorna o usuário nos dados que devem ser enviados para
     *  aquele q está usando a API.
     */
    public function returnUser(User $user) {
        require_once MODELS . "DepositionDAO.php";
        require_once MODELS . "NotificationDAO.php";
        $array = Row::rowToArray($user);
        unset($array["password"]);
        foreach ($array as $key => $value) {
            if ($value == null) {
                unset($array[$key]);
            }
        }
        $array["related"] = FollowDAO::getInstance()->countRelatedUsers($user);
        $array["depositions"] = DepositionDAO::getInstance()->countDepositions($user);
        $array["status"] = $user->getLabel();
        $array = array_remove_keys($array, array("confirmationCode",
            "confirmationTime", "active"));
        if ($this->isAnyoneLoggedIn() && $this->getUserLoggedIn()->getId() == $user->getId()) {
            $array["notifications"] = NotificationDAO::getInstance()->countNotification($user);
            $array["loggedIn"] = $this->isAnyoneLoggedIn();
        } else {
            if ($this->isAnyoneLoggedIn()) {
                $follow = new Follow();
                $follow->setUserIdFollow($this->getUserLoggedIn()->getId());
                $follow->setUserId($user->getId());
                $array["beingFollow"] = FollowDAO::getInstance()->existsFollow($follow);
                $follow->setUserId($this->getUserLoggedIn()->getId());
                $follow->setUserIdFollow($user->getId());
                $array["following"] = FollowDAO::getInstance()->existsFollow($follow);
            }
        }
        $array = $this->returnImage($user->getImage() . "", $array, USER_IMAGE_DEFAULT);
        $array["sexy"] = $user->getSexy();
        $array["cool"] = $user->getCool();
        $array["reliable"] = $user->getReliable();
        return $array;
    }

    /**
     * Retorna os dados de um usuário
     * @param Album $album
     * @return array
     */
    public function returnAlbum(Album $album) {
        $result = array();
        $result["albumId"] = $album->getId();
        $result["album"] = $album->getName();
        $result["image"] = $album->getImage() . "";
        return $result;
    }

    /**
     * Verifica se foi enviado o auth e se foi pega o usuário logado.
     *  Armazena o usuário logado no LoginDAO
     *      Ex para pegar o usuário logado:
     *          LoginDAO::getInstance()->getUserLoggedIn();
     * @param DataInput $data 
     * @return User
     */
    public function loadUserLoggedIn(DataInput $data) {
        $data->checkFields(array("auth"));
        $auth = $data->getField("auth");
        $loginDAO = LoginDAO::getInstance();
        $user = $loginDAO->getUser($auth);
        if ($user == null) {
            throw new Exception("'auth' não é válido.");
        }
        $loginDAO->setUserLoggedIn($user);
        return $user;
    }

    /**
     * Procura e retorna um álbum, Se não existir cria um ou retorna o default
     * @param DataInput $data
     * @param mixed $user O dono do album
     * @return Album
     */
    public function getAlbum(DataInput $data, $user = null) {
        require_once MODELS . "AlbumDAO.php";
        $this->loadUserLoggedIn($data);
        $albumDAO = AlbumDAO::getInstance();
        $user = UserDAO::getInstance()->getUser($user);
        if (!$data->hasField("album")) {
            return $albumDAO->getDefaultAlbum($user);
        }
        $album = $albumDAO->getAlbum($data->getField("album"));
        if ($album === null && $data->hasField("album")) {
            $album = new Album();
            $album->setName($data->getField("album"));
            $album->setUserId($user->getId());
            $albumDAO->addAlbum($album);
        } else if ($album === null) {
            $album = $albumDAO->getDefaultAlbum($user);
        }
        return $album;
    }

    /**
     * Envia imagens e salva no banco
     * @param APIJsonYouCurte $api
     * @param DataInput $data
     * @return \Image
     */
    public function sendImage(DataInput $data) {
        $this->loadUserLoggedIn($data);
        $data->checkFields(array("image", "format"));
        require_once MODELS . "ImageDAO.php";
        $imageDAO = ImageDAO::getInstance();
        $image = new Image();
        $image->generateURL("." . $data->getField("format"));
        $album = $this->getAlbum($data);
        $image->setAlbumId($album->getId());
        if ($data->hasField("name")) {
            $image->setName($data->getField("name"));
        } else {
            $image->setName("Sem nome");
        }
        $base64 = base64_decode($data->getField("image"));
        $file = fopen($image->getUrl()->toFile(), "wb");
        fwrite($file, $base64);
        fclose($file);
        try {
            $imageDAO->validateImage($image, true);
        } catch (Exception $e) {
            $image->getUrl()->toFile()->deleteFile();
            throw $e;
        }
        ImageDAO::getInstance()->addImage($image);
        return $image;
    }

    /**
     * Retorna a imagem
     * @param DataInput $data
     * @param Image $default Retorna esse valor se não encontrar a imagem. 
     * (Se não tiver imagem default lança um exception)
     * @return Image
     * @throws Exception se não achar a imagem
     */
    public function getImage(DataInput $data, $default = null) {
        $image = $data->getField("image");
        if (is_int($image) || is_numeric($image) || strstr($image, "http")) {
            return ImageDAO::getInstance()->getImage($image);
        } else if ($data->hasField("format")) {
            return $this->sendImage($data);
        } else if ($default !== null) {
            return $default;
        }
        throw new Exception("Imagem inválida!");
    }

    /**
     * Verifica se o resultado está okay (não é de erro e se não está errado)
     * @param JSON $result O json com o resultaod
     * @return boolean true = tudo certo <br/> false = tem algum problema
     */
    public function isOk($result) {
        return strpos($result, '{"result":"ok"') === 0;
    }

    /**
     * Cria um json com o resultado ok
     * @param mixed $result O resultado que quer passar. Pode ser array,string e tal.
     * @return String JSON com o código certo.
     */
    public function returnOk($result) {
        if ($result instanceof User) {
            $result = $this->returnUser($result);
        } else if ($result instanceof Image) {
            $result = $this->returnImage($result);
        } else if ($result instanceof Album) {
            $result = $this->returnAlbum($result);
        } else if ($result instanceof Post) {
            $result = $this->returnPost($result);
        } else if ($result instanceof Notification) {
            $result = $this->returnNotification($result);
        } else if ($result instanceof Deposition) {
            $result = $this->returnDeposition($result);
        } else if ($result instanceof Row) {
            $result = Row::rowToArray($result);
        }
        /* if (is_array($result)) {
          $result = json_encode_yc($result);
          } */
        $array = array("result" => "ok", "data" => $result);
        return json_encode($array);
    }

    /**
     * Cria um json com o resultado error
     * @param String $error A mensagem de erro
     * @return String O json
     */
    public function returnError($error) {
        if ($error instanceof Exception) {
            if (DEBUG_PROJECT) {
                Log::getInstance()->addException($error);
            }
            $error = $error->getMessage();
        }
        $array = array("result" => "error", "error" => trim($error));
        return json_encode_yc($array);
    }

    /**
     * LISTAR POSTAGENS DE UM USUáRIO
     * Método: GET
     * URL: http://youcurte.com.br/api/posts?user=[user]
     * http://youcurte.com.br/api/posts/[user]
     * Exemplo de dados a serem enviados:
     * http://youcurte.com.br/api/posts?user=1
     * 
     * 
     * @param DataInput $data
     * @return String
     */
    public function get_posts_(DataInput $data) {
        try {
            require_once MODELS . "PostDAO.php";
            $data->declareFields("user", "count", "offset");
            $data->checkFields(array("user"));
            $user = $data->getField("user");
            if ($data->hasField("count")) {
                $count = $data->getField("count");
            } else {
                $count = COUNT_MAX_RETURN;
            }
            if ($data->hasField("offset")) {
                $offset = $data->getField("offset");
            } else {
                $offset = 0;
            }
            if (strstr($user, "@") || is_int($user) || is_numeric($user)) {
                $user = $data->getField("user");
            } else {
                $login = LoginDAO::getInstance()->getLogin($user);
                $user = $login->getUser();
            }
            $result = PostDAO::getInstance()->listPosts($user, $count, $offset);
            $posts = array();
            foreach ($result as $post) {
                $posts[] = $this->returnPost($post);
            }
            return $this->returnOk($posts);
        } catch (Exception $e) {
            return $this->returnError($e->getMessage());
        }
    }

    /**
     * TimeLine
     * Método: GET
     *  URL: http://youcurte.com.br/api/timeline?user=[user]&offset=[offset]&count=[count]
     * 	http://youcurte.com.br/api/timeline/[user]/[count]/[offset]
     *
     */
    public function get_timeline_(DataInput $data) {
        try {
            require_once MODELS . "PostDAO.php";
            $data->declareFields("user", "count", "offset");
            $data->checkFields(array("user"));
            $data->prepareOffsetAndCount();
            $user = $data->getField("user");
            $offset = $data->getField("offset");
            $count = $data->getField("count");
            if (strstr($user, "@") || is_int($user) || is_numeric($user)) {
                $user = UserDAO::getInstance()->getUser($user);
            } else {
                $user = LoginDAO::getInstance()->getUser($user);
            }
            $result = PostDAO::getInstance()
                    ->getTimeline($user, $count, $offset);
            $posts = array();
            foreach ($result as $post) {
                $posts[] = $this->returnPost($post);
            }
            return $this->returnOk($posts);
        } catch (Exception $e) {
            return $this->returnError($e->getMessage());
        }
    }

}
