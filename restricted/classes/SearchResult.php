<?php

/**
 * Classe usada para manter os resultados de pequisas
 *
 * @author YouCurte
 */
class SearchResult {

    private $count, $data;

    /**
     * Método construir 
     * @param array|mixed $data Uma array de dados ou algum tipo de dado mesmo
     * @param int $count A quantidade de dados
     */
    public function __construct($data, $count = 0) {
        $this->setData($data);
        $this->setcount($count);
    }

    /**
     * 
     * @return int Quantidade de resultados
     */
    public function getCount() {
        return $this->count;
    }

    /**
     * 
     * @param mix $index Chave do endereço do array 
     *  (se não informado retorna todo o array)
     * @return mix 
     *  Retorna os dados que resultaram da pesquisa (normalmente do tipo Array)
     *  Se o $index é informado retorna o valor daquele index
     * Retorna null se não foi encontrado o index também
     */
    public function getData($index = null) {
        if (is_null($index)) {
            return $this->data;
        } else if (array_key_exists($index, $this->data)) {
            return $this->data[$index];
        } else {
            return null;
        }
    }

    /**
     * Re
     * 
     * @param int $count Quantidade de dados/registros resultantes da pesquisa
     */
    public function setCount($count) {
        $this->count = $count;
    }

    /**
     * 
     * @param Mix $data Seta os dados
     */
    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Verifica se tem algum dado. (Checa pela quantidade getCount()==0)s
     * @return boolean
     *      true = vazio <br> false = não vazio
     */
    public function isEmpty() {
        return $this->getCount() == 0;
    }

}
