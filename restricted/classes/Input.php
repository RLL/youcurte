<?php

/**
 * Classe para trabalhar com o Input (entrada)
 *
 * @author YouCurte
 */
class Input extends Singleton {
    
    private $contents = null;

    public function Input() {
        $this->data = array();
    }
    
    /**
     * Obtém o valor de um parametro
     * @param string $name O nome do parametro
     * @return mixed
     */
    public function __get($name) {
        return $this->getParameter($name);
    }

    /**
     * Retorna o valor do campo enviado de um form por post ou get;
     * 
     * 
     * @param String $name Nome do campo
     * @param int $priority
     *      0 - Método post tem prioridade sobre get
     *      1 - Método get tem prioridade sobre post
     */
    public function getParameter($name, $priority = 0) {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        } else {
            switch ($priority) {
                case 0:
                    $methods = array(INPUT_POST, INPUT_GET);
                    break;
                case 1:
                    $methods = array(INPUT_GET, INPUT_POST);
                    break;
            }
            foreach ($methods as $method) {
                $value = filter_input($method, $name);
                if (isNotEmpty($value)) {
                    return $value;
                }
            }
        }
        return null;
    }

    /**
     * Verifica se tem um parametro
     * @param type $parameter
     * @return type
     */
    public function hasParameter($parameter) {
        return isNotEmpty($this->getParameter($parameter));
    }
    
    /**
     * Verifica se tem tais parametros informados. Se não tiver retorna uma
     *  mensagem de erro
     * @param array|string $parameters Um array com os nomes dos parametros ou 
     *  apenas um nome de um parametro
     * @return string Retorna a mensagem de erro se não retorna vazio('')
     */
    public function needsParameters($parameters) {
        if (!is_array($parameters)) {
            $parameters = array($parameters);
        }
        $params = array();
        foreach ($parameters as $value) {
            if (!$this->hasParameters($value)) {
                $params[] = $value;
            }
        }
        $result = "";
        if (count($params) > 0) {
            foreach ($params as $value) {
                if (isNotEmpty($result)) {
                    $result.=", ";
                }
                $result.="$value";
            }
            $result = "São necessários os seguintes param&ecirc;tros: $result.";
        }
        return $result;
    }
    
    /**
     * Pega o conteúdo enviado na requisição
     * @return String
     */
    public function getContents() {
        if (!is_null($this->contents)){
            return $this->contents;
        }
        $stdin_rsc = fopen("php://input", "r");
        $putdata = "";
        while (!feof($stdin_rsc)){
            $putdata.=fread($stdin_rsc, 1024); 
        }
        fclose($stdin_rsc);
        $this->contents = $putdata;
        return $putdata;
    }

    /**
     * Retorna a instância principal de Input
     * @return Input
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}
