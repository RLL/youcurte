<?php

/**
 * Classe para lidar com where dos selects de bancos
 *
 * @author Luiz
 */
class DBWhere {
    
    private $where,$oldWhere,$fields,$offset,$count;
    
    /**
     * 
     * @param mixed $where O where do select. 
     *      Ex: 'name like "%luiz%"'
     *          array("id"=>1);
     * @param mixed $fields (Opcional)
     *          Os campos para preencher o $where
     * @param int $count (Opcional) A quantidade máxima de registro a retorna
     * @param int $offset (Opcional) A quantidade de registros a ignorar (ñ conta com count)
     */
    public function __construct($where,$fields = null,$count = COUNT_MAX_RETURN,
            $offset = 0) {
        $this->where = $where;
        $this->fields = $fields;
        $this->count = $count;
        $this->offset = $offset;
        $this->format();
    }
    
    /**
     * Formata where e fields
     */
    public function format(){
        if (is_null($this->fields)){
            $this->fields = array();
        }  
        $this->formatWhere();
        $this->formatFields();
    }
    
    /**
     * Formata o where
     */
    private function formatWhere(){
        if (is_array($this->where)){
            $this->oldWhere = $this->where;
            $where = "";
            foreach($this->where as $key=>$value){
                if (isNotEmpty($where)){
                    $where .= " and ";
                }
                if (is_string($key)){
                   $where.="$key=$value";
                } else {
                    $where.="$value=:$value";
                }
            }
            $this->where = $where;
        }
        if (!strstr($this->where,"where")&&isNotEmpty($this->where)){
            $this->where = " where ".$this->where;
        }
        $limit = "limit ".$this->offset.",".$this->count;
        if (!strstr($this->where,$limit)){
            $this->where.=" ".$limit;
        }
    }
    
    /**
     * Formata os campos para o padrão array
     */
    private function formatFields(){
        $fields = array();
        if ($this->fields instanceof Row){
            foreach ($this->fields->listGets() as $get){
                if (strstr($this->where,":$get")){
                    $fields[$get]=$this->fields->get($get);
                }
            }
        } else if (is_array($this->fields)){
            foreach ($this->fields as $key=>$value){
                if (is_string($key)&&strstr($this->where,":$key")){
                    $fields[$key]=$value;
                } else if (!is_string($key)){
                    $fields[$this->oldWhere[$key]]=$value;
                }
            }
        }
        $this->fields = $fields;
    }
    
    /**
     * Atribui os valores aos campos corretos usando $this->fields
     * @param PDOStatement $query A query onde deseja atribuir esses valores
     */
    function bindValues(PDOStatement $query){
        foreach ($this->fields as $key=>$value){
            $query->bindValue($key, $value);
        }
    }
    
    /**
     * Retorna o where.  
     * @return String Se formatado deve ser retornado algo como:
     *  Ex: " where id=:id limit 0,10"
     */
    function getWhere() {
        return $this->where;
    }

    /**
     * Retorna os campos. 
     * @return array O valor formatado é array("campo"=>"valor",...)
     *      
     */
    function getFields() {
        return $this->fields;
    }

    function getOffset() {
        return $this->offset;
    }

    function getCount() {
        return $this->count;
    }

    function setWhere($where) {
        $this->where = $where;
    }

    function setFields($fields) {
        $this->fields = $fields;
    }

    function setOffset($offset) {
        $this->offset = $offset;
    }

    function setCount($count) {
        $this->count = $count;
    }


    
}
