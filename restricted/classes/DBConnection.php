<?php

/**
 * Classe de conexão
 *
 * @author YouCurte
 */
class DBConnection extends PDO {

    protected static $self;

    /**
     * Método construtor da classe de conexão
     * @param string $dsn [opcional] Link para a conexão
     * @param string $username [opcional] Usuário
     * @param string $passwd [opcional] Senha
     * @param string $options [opcional] Opções
     */
    public function __construct($dsn = "", $username = "", $passwd = "", $options = null) {
        if (isEmpty($username)) {
            $username = DB_USER;
        }
        if (isEmpty($passwd)) {
            $passwd = DB_PASSWORD;
        }
        if (isEmpty($dsn)) {
            $dsn = DB_TYPE . ":dbname=" . DB_NAME . ";charset=utf8;host=" . DB_HOST;
        }
        if (is_null($options)) {
            $options = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            );
        }
        
        parent::__construct($dsn, $username, $passwd, $options);
        $this->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
    }

    /**
     * Obter a instância principal de DBConnection
     * @return DBConnection
     */
    public static function getInstance() {
        if (self::$self == null) {
            $rc = new ReflectionClass(get_called_class());
            self::$self = $rc->newInstanceArgs();
        }
        return self::$self;
    }

}