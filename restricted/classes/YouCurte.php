<?php

require_once CLASSES . 'DataInput.php';

/**
 * Classes para gerenciar o direcionamento de views e execução de funções 
 *  por link informado no navegador
 *
 * @author YouCurte
 */
class YouCurte {

    public function __construct() {
        
    }

    /**
     * Executa o código de rodar funções em cima do Path
     * @param Path $path O caminho
     * @return mixed Retorna o resultado obtido
     */
    public function run(Path $path) {
        $kinds = array('get', 'post', 'put', 'delete');
        $result = '';
        try {
            if (!$this->runPrev($this->getData($path)) && count(array_keys($kinds, $this->getRestMethod())) > 0) {
                $finded = false;
                $max = $path->getCountOfParts() - 1;
                for ($i = $max; $i >= 0; $i--) {
                    $methodName = $this->getRestMethod() . '_' . str_replace(DS, "_", $path->copy(0, $i));
                    if (method_exists($this, $methodName . '_') || $i < $max) {
                        $methodName.="_";
                    }
                    if (method_exists($this, $methodName)) {
                        $finded = true;
                        $result = call_user_func(array($this, $methodName), $this->getData($path, $methodName));
                        break;
                    }
                    $methodName = 'x_' . str_replace(DS, "_", $path->copy(0, $i));
                    if (method_exists($this, $methodName . '_') || $i < $max) {
                        $methodName.="_";
                    }
                    if (method_exists($this, $methodName)) {
                        $finded = true;
                        $result = call_user_func(array($this, $methodName), $this->getData($path, $methodName));
                        break;
                    }
                }
                if (!$finded) {
                    if (!$this->runExtra($this->getData($path))) {
                        $result = $this->returnError(("Método não encontrado na API."));
                    }
                }
            } else {
                $result = $this->returnError(("Método '" . $this->getRestMethod() . "' inválido."));
            }
        } catch (Expection $e) {
            $result = $this->returnError("Erro: " . $e->getMessage());
        }
        return $result;
    }

    /**
     * Função destinada a executar antes de executar realmente os comandos de run()
     *      -- run é quem chama essa função
     * @param DataInput $data Entrada de dados
     * @return boolean Retorna um valor boolean indificando se deve continuar a executar o run()
     *      ou não;
     *          true = não continuar executando run() <br>
     *          false = continuar
     */
    public function runPrev(DataInput $data) {
        $data->getPath();
        return false;
    }

    /**
     * Função que é enn  true = Teve sucesso em encontrar uma função<br>
     *   false = não encontrado
     */
    public function runExtra(DataInput $data) {
        $data->getPath();
        return false;
    }

    /**
     * Função para verificar e trata os dados recebidos em caso de erro.
     * 
     * @param mixed $data Os dados a validar
     * @return boolean true = confirmado que deu erro <br>
     *      false = não confirmado (falso positivo)
     */
    public function returnError($data) {
        if ($data instanceof DataInput) {
            $data->getPath();
        }
        return true;
    }

    /**
     * Obtém os dados de entrada a partir do path e do nome da função/método
     * @param Path $path O path
     * @param type $methodName O nome da função ou método
     * @return \DataInput Retorna os dados de entrada resultante
     */
    public function getData(Path $path, $methodName = null) {
        if (is_null($methodName)) {
            return new DataInput($path);
        } else {
            $methodPath = new Path(str_replace("_", DS, $methodName));
            $newPath = $path->copy($methodPath->getCountOfParts() - 1);
        }
        $data = new DataInput($newPath);
        return $data;
    }

    /**
     * Retorna o método de requisição
     *  ('get','post','put','delete')
     * @return string O método
     */
    public function getRestMethod() {
        $r = strtolower(filter_input(INPUT_SERVER, 'REQUEST_METHOD'));
        if (strtolower($r) == "_get") {
            return "get";
        }
        return $r;
    }

    /**
     * Manda uma resposta para redimensionar para outro url/página
     * @param URL|string|int $url O url para qual redimensionar
     */
    public static function redirect($url) {
        if ($url === URL_ROOT) {
            $url = new URL("", URL_ROOT);
        }
        $url1 = new URL($url);
        header("Location: $url1");
        exit();
    }

}
