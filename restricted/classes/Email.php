<?php

require_once FRAMEWORKS . "phpmailer" . DS . "class.phpmailer.php";
require_once FRAMEWORKS . "phpmailer" . DS . "class.smtp.php";
require_once FRAMEWORKS . "phpmailer" . DS . "language" . DS . "phpmailer.lang-pt.php";

/**
 * Classe destinada a trabalhar com envio de e-mails
 *
 * @author YouCurte
 * 
 */
class Email {

    private $from, $to, $fromName, $toName, $message, $subject, $password, $html;

    public function __construct() {
        $this->from = EMAIL_FROM;
        $this->password = EMAIL_PASSWORD;
        $this->fromName = EMAIL_FROM_NAME;
        $this->html = false;
    }

    /**
     * Envia o e-mail
     * @param String $to E-mail para quem deseja enviar [opcional (usará o getFrom() se não fornecido)] 
     * @return boolean|string E-mail enviado com sucesso?
     *  false = enviado com sucesso <br>
     *  true = retorna a mensagem de erro <br>
     * if($this->send()){<br>
        echo "Mailer Error: " . $mail->ErrorInfo;<br>
       }else{<br>
        echo "E-Mail has been sent";<br>
       }<br>
     */
    public function send($to = null, $toName = null) {
        $to = is_null($to) ? $this->getTo() : $to;
        $toName = is_null($toName) ? $this->getToName() : $toName;
        $phpMailer = new PHPMailer();
        //$phpMailer->Debugoutput = 'html';
        //$phpMailer->SMTPDebug = 2;
        $phpMailer->setFrom($this->getFrom(), $this->getFromName());
        $phpMailer->addReplyTo($this->getFrom());
        $phpMailer->Host = EMAIL_HOST;
        $phpMailer->Port = EMAIL_PORT;
        $phpMailer->isSMTP();
        $phpMailer->SMTPAuth = true;
        $phpMailer->SMTPSecure = 'tls';
        $phpMailer->Timeout =  10;
        $phpMailer->Username = $this->getFrom();
        $phpMailer->Password = $this->getPassword();
        $phpMailer->addAddress($to, $toName);
        $phpMailer->isHTML($this->isHTML());
        $phpMailer->Subject = $this->getSubject();
        $phpMailer->Body = $this->getMessage();
        $phpMailer->AltBody = strip_tags($this->getMessage());
        if (EMAIL_FOR_MAIL){
            $phpMailer->isMail();
        }
        if ($phpMailer->send()){
            return $phpMailer->ErrorInfo;
        } else {
            return false;
        }
    }

    /**
     * Prepara o cabeçalho, deixando o formatado
     * @return string
     */
    protected function getHeaders() {
        $headers = "MIME-Version: 1.1\r\n";
        $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
        $headers .= "From: " . $this->getFrom() . "\r\n"; // remetente
        $headers .= "Return-Path: " . $this->getFrom() . "\r\n";
        return $headers;
    }

    /**
     * 
     * @return String Retorna o remetente
     */
    public function getFrom() {
        return $this->from;
    }

    /**
     * 
     * @return String Retorna para quem será enviado o e-mail
     */
    public function getTo() {
        return $this->to;
    }

    /**
     * 
     * @return String Retorna a mensagem que seja envia
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * 
     * @return String Retorna o título da mensagem
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * Define o remetente (se não definido usará o padrão do sistema -> EMAIL_FROM)
     * @param String $from Remetente
     */
    public function setFrom($from) {
        $this->from = $from;
    }

    /**
     * Define o para quem o e-mail será enviado
     * @param type $to Destinatário
     */
    public function setTo($to) {
        $this->to = $to;
    }

    /**
     * Define a mensagem que será enviada.
     * 
     * @param String $message Mensagem
     */
    public function setMessage($message) {
        $this->message = $message;
    }

    /**
     * Define o título/assunto da mensagem
     * @param String $subject Título/assunto da mensagem
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * Retorna o nome do remetente
     * @return String
     */
    public function getFromName() {
        return $this->fromName;
    }

    /**
     * Retorna o nome do destinatpario
     * @return String
     */
    public function getToName() {
        return $this->toName;
    }

    /**
     * Retorna a senha do e-mail
     * @return String
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Define que o conteúdo do e-mail é html
     * @param boolean $isHTML
     */
    public function setIsHTML($isHTML) {
        $this->html = $isHTML;
    }

    /**
     * Define e verifica se o conteúdo do e-mail é html
     * @return boolean
     */
    public function isHTML() {
        return $this->html;
    }

    public function setFromName($fromName) {
        $this->fromName = $fromName;
    }

    public function setToName($toName) {
        $this->toName = $toName;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

}