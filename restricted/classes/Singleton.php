<?php

/**
 * Description of Singleton
 *
 * @author YouCurte
 */
class Singleton {
    
    protected static $self = array();
    
    public function __construct(){
        
    }
    
    /**
     * Cria uma instancia principal
     * @return mixed
     */
    public static function getInstance(){
        $key = get_called_class();
        if (!array_key_exists($key,self::$self)){
            $rc = new ReflectionClass($key);
            self::$self[$key] = $rc->newInstanceArgs();
        }
        return self::$self[$key];
    }
    
    /**
     * Define tal instancia como principal
     * @param mixed $instance
     */
    public static function setInstance($instance){
        self::$self[get_called_class()] = $instance;
    }
    
}