<?php

require_once CLASSES . "APIJsonYouCurte.php";

/**
 * Description of APIXMLYouCurte
 *
 * @author Luiz Henrique
 */
class APIXMLYouCurte extends APIJsonYouCurte {

    /**
     * Executa os processos
     * @param Path $path O path do link
     * @param boolean $print Imprimir na tela
     */
    public function run(Path $path, $print = true) {
        $result = parent::run($path, false);
        $array = json_decode($result, true);
        $xml = $this->arrayToXml($array, '<YouCurte></YouCurte>');
        for ($i=0;strstr($xml,"<$i>");$i++){
            $xml = str_replace("<$i>", "<item>", $xml);
            $xml = str_replace("</$i>", "</item>",$xml);
        }
        if ($print) {
            print $xml;
        }
        return $xml;
    }

    /**
     * Converte array para xml
     * @param array $array O array que deseja converta
     * @param String $rootElement O nome da tag raiz
     * @param XMLElement $xml XML no qual deseja inserir o xml resultante
     * @return String O XML resultante da conversão
     */
    function arrayToXml($array, $rootElement = null, $xml = null) {
        $_xml = $xml;

        if ($_xml === null) {
            $_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<root></root>');
        }

        foreach ($array as $k => $v) {
            if (is_array($v)) { //nested array
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            } else {
                $_xml->addChild($k, $v);
            }
        }
        return $_xml->asXML();
    }

}
