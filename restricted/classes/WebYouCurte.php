<?php

require_once CLASSES . 'YouCurte.php';
require_once VIEWS . 'main' . DS . 'MainView.php';
require_once CONTROLLERS . 'UserController.php';

/**
 * Classe para gerenciar o site
 *      Links com Views
 */
class WebYouCurte extends YouCurte {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Retorna a instância principal de MainView
     * @return MainView
     */
    public function getMainView() {
        return MainView::getInstance();
    }

    /**
     * Executa uma view (SubView)
     * @param String $php O caminho do php
     * @param DataInput $data Os dados de entrada
     * @param View $child [opcional] Uma view filho
     */
    public function executeView($php, DataInput $data, View $child = null) {
        $data->setCoded(false);
        $file = new File($php);
        $file->validateFile();
        require_once $file;
        $this->getMainView()->setData($data);
        forward_static_call_array(array($file->getNameWithoutExt(), "execute")
                , array($this->getMainView(), $child, $data));
    }

    /**
     * Função que gerencia a execução de views
     *      Essa função é executada automáticamente se não encontrado uma função
     *          que represente um link.
     * @param DataInput $data Dados de entrada
     * @return mixed
     */
    public function runExtra(DataInput $data) {
        $data->setCoded(false);
        $path = $data->getPath();
        if (isEmpty(trim($path))) {
            return $this->get_($data);
        }
        if (($php = $this->getViewFromDataInput($data))) {
            return $this->executeView($php, $data);
        }
        return $this->executeView(VIEWS . 'errors/page404/Page404View.php', $data);
    }

    /**
     * Obtém um caminho valido da view apartir dos dados de entrada
     * @param DataInput $data Dados de entrada
     * @param boolean $father [opcional] Verifica se é a primeira execução do método  
     * @return File|boolean Retorna false se não encontrar a view.
     *      Retorna um objeto File com o caminho da view se tudo der certo.
     */
    public function getViewFromDataInput(DataInput $data, $father = true) {
        if ($data->getPath()->isEmpty()) {
            return false;
        }
        $originalPath = $data->getPath();
        $php = $this->getViewFile($data->getPath());
        $nopage = new File('nopage', $php->getParentPath());
        if ($nopage->exists()) {
            return false;
        } else if (!$php->exists()) {
            $path = $data->getPath();
            $data->setPath($path->copy(0, $path->countParts() - 2));
            $result = $this->getViewFromDataInput($data, false);
            if ($result && $father) {
                $path = $originalPath->copy($data->getPath()->countParts());
                $data->setPath($path);
            }
            return $result;
        } else if ($father) {
            $data->setPath(new Path(""));
        }
        return $php;
    }

    /**
     * Retorna o caminho de uma view a partir do Path
     * @param Path $path O path
     * @return File O camiho
     */
    public function getViewFile(Path $path) {
        $name = ucfirst($path->getLastPart()) . "View";
        $file = new File("$path/" . $name . ".php", VIEWS);
        return $file->getValidFile();
    }

    /**
     * Verifica se está logado se não redimensiona para página de erro
     * @param boolean $loggedIn true = verifica se está logado
     *                          false = verifica se não está logado
     */
    public static function checkPrivilege($loggedIn) {
        if (UserController::getInstance()->isAnyoneLoggedIn() != $loggedIn) {
            if ($loggedIn) {
                self::redirect(URL_LOGGED_IN);
            } else {
                self::redirect(URL_LOGGED_OUT);
            }
            exit();
        }
    }

    /*
     * Funções que simulam views/páginas 
     */


    /**
     * Função para fazer logout local
     * youcurte.com.br/logout
     */

    public function get_logout() {
        UserController::getInstance()->logout();
    }

    /**
     * Função para fazer logout de todos os dispositivos
     * youcurte.com.br/logoutAll
     */

    public function get_logoutAll() {
        UserController::getInstance()->logoutAll();
    }

    /**
     * Contra qual página mostrar na home
     *  Leva ao HomeView (com as postagens mais tri e cadastro rápido)
     *  Ou leva ao timeline de um usuário (Se estiver logado)
     * youcurte.com.br
     */
    public function get_(DataInput $data) {
        if (UserController::getInstance()->isAnyoneLoggedIn()) {
            $this->executeView(VIEWS . '/timeline/TimelineView.php', $data);
        } else {
            $this->executeView(VIEWS . '/home/HomeView.php', $data);
        }
    }

    /**
     * Página para mostrar um post único
     * 
     * youcurte.com.br/post
     */
    public function x_post_(DataInput $data) {
        $this->executeView(VIEWS . '/post/singlepost/SinglePostView.php', $data);
    }

}
