<?php

/**
 * Classe para trabalhar com datas e horas
 *
 * @author YouCurte
 */
class DateTimeYC {

    protected $data;
    protected $is_datetime, $valid;

    /**
     * 
     * @param mixed $data A data em questão (String,DateTimeYC,DateTime) Ex:
     *  24/12/2012
     *  12:30:00 25/12/2014 
     * @param String $format [opcional] O formatado da data
     */
    public function __construct($data = null, $format = null) {
        $this->valid = false;
        $this->is_datetime = false;
        if (isEmpty($data)) {
            $this->data = new DateTime();
            $this->is_datetime = true;
            $this->valid = true;
        } else if (is_string($data)) {
            if (isNotEmpty($format)) {
                $this->data = DateTime::createFromFormat($format, $data);
            } else if (strstr($data, "-")) {
                if (strstr($data, " ")) {
                    $this->data = DateTime::createFromFormat(DB_FORMAT_DATETIME, $data);
                    $this->is_datetime = true;
                } else {
                    $this->data = DateTime::createFromFormat(DB_FORMAT_DATE, $data);
                }
            } else {
                if (strstr($data, " ")) {
                    $this->data = DateTime::createFromFormat("H:i:s d/m/Y", $data);
                } else {
                    $this->data = DateTime::createFromFormat("d/m/Y", $data);
                }
            }
            $this->valid = true;
        } else if ($data instanceof DateTime) {
            $this->data = $data;
            $this->is_datetime = true;
            $this->valid = true;
        } else if ($data instanceof DateTimeYC) {
            $this->data = $data->data;
            $this->is_datetime = true;
            $this->valid = true;
        } else {
            $this->data = new DateTime();
            $this->is_datetime = true;
            $this->valid = true;
        }
    }
    
    /**
     * Define se está armazenando apenas data ou hora tbm
     * @param boolean $value true = apenas data| false = data e hora
     * @return DateTimeYC Retorna a si mesmo
     */
    public function setOnlyDate($value){
        $this->is_datetime = !$value;
        return $this;
    }

    /**
     * Converte a data para texto. O formato é o mesmo que usa no banco de dados.
     * @return String REtorna o resultado dessa conversão
     */
    public function __toString() {
        return $this->toDB();
    }

    /**
     * Converte a data para texto em e retorna o mesmo.
     *  O formato para qual é convertido é para o <b>banco de dados</b>.
     * @return String
     */
    public function toDB() {
        if ($this->is_datetime) {
            return $this->format(DB_FORMAT_DATETIME);
        } else {
            return $this->format(DB_FORMAT_DATE);
        }
    }

    /**
     * Verifica se tem apenas informações relacionado a data
     * @return boolean
     */
    public function isOnlyDate() {
        return !($this->is_datetime);
    }

    /**
     * Se tem informações de data e hora e não apenas de data
     * @return boolean
     */
    public function isDateTime() {
        return $this->is_datetime;
    }

    /**
     * Retorna no formato brasileiro (hora:minutos dia/mes/ano)
     * @param boolean seconds Retornar os segundos tbm?
     * @return type
     */
    public function toPT($seconds = false) {
        if ($this->isOnlyDate()) {
            return $this->format('d/m/Y');
        } else if ($seconds) {
            return $this->format('H:i d/m/Y');
        } else {
            return $this->format('H:i:s d/m/Y');
        }
    }

    /**
     * Formata a data em texto para o formato informado.
     * @param String $format O formato para qual deseja converter
     * @return string O resultado 
     */
    public function format($format) {
        if (!$this->valid)
            return '';
        return $this->data->format($format);
    }
    
    /**
     * Retorna em forma númerica a data
     * @return long
     */
    public function toNumber(){
        return $this->data->format('U');
    }

    /**
     * Verifica se esse DateTimeYC é antes do outro($other) DateTimeYC
     * @param DateTimeYC $other Com quem deseja comparar
     * @return boolean
     */
    public function isBefore(DateTimeYC $other) {
        return $this->toNumber()<$other->toNumber();
    }
    
    /**
     * Verifica se essa($this) e a outra($other) data e hora são iguais
     * @param DateTimeYC $other
     * @return boolean
     */
    public function isEqual(DateTimeYC $other){
        return $this->toNumber()==$other->toNumber();
    }
    
    /**
     * Verifica se essa ($this) data e hora passa da outra($other)
     * @param DateTimeYC $other
     * @return boolean
     */
    public function isAfter(DateTimeYC $other){
        return $this->toNumber()>$other->toNumber();
    }

    /**
     * Retorna o DateTime com data e/ou time desse objeto
     * @return DateTime
     */
    public function getDateTime() {
        return $this->data;
    }

    /**
     * Calcula a diferença entre as datas
     * @param mix $dateTime 
     * @return DateInterval
     */
    public function diff($dateTime) {
        $date = new DateTimeYC($dateTime);
        $time = $date->getDateTime();
        return $this->data->diff($time);
    }
    
    /**
     * Soma a data e hora atual com o intervalo informado
     * @param DateInterval $interval Intervalo que seja adicionar 
     *  (pode informar um string tbm ex: 'PT1D')
     */
    public function add($interval){
        if (is_string($interval)){
            $interval = new DateInterval($interval);
        }
        $this->data->add($interval);
    }

    /**
     * Retorna um objeto de DateTimeYC com dados de data e hora atual
     * @return \DateTimeYC
     */
    public static function now() {
        return new DateTimeYC();
    }

}