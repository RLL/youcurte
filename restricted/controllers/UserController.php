<?php

require_once CONTROLLERS . 'Controller.php';
require_once CONTROLLERS . 'ImageController.php';
require_once CONTROLLERS . 'EmailController.php';
require_once MODELS . 'User.php';
require_once MODELS . 'UserDAO.php';
require_once MODELS . 'LoginDAO.php';
require_once MODELS . 'FollowDAO.php';
require_once MODELS . 'NotificationDAO.php';

/**
 * Controlador para as funções de usuário
 *
 * @author YouCurte
 */
class UserController extends Controller {

    public function __construct() {
        parent::__construct(UserDAO::getInstance());
    }

    /**
     * Retorna o usuário logado
     * @return User
     */
    public function getUserLoggedIn() {
        if (LoginDAO::getInstance()->isAnyoneLoggedIn()) {
            return LoginDAO::getInstance()->getUserLoggedIn();
        }
        if (isset($_SESSION["auth"])) {
            $auth = $_SESSION["auth"];
        } else {
            $auth = filter_input(INPUT_COOKIE, "auth");
            $_SESSION["auth"] = $auth;
        }
        if (isNotEmpty($auth)) {
            $user = LoginDAO::getInstance()->getUser($auth);
            if ($user != null) {
                LoginDAO::getInstance()->setUserLoggedIn($user);
                $this->updateCache($user);
            }
            return $user;
        } else {
            return null;
        }
    }

    /**
     * Retorna o id do usuário logado
     * @return int Retorna -1 se não tiver usuário logado
     */
    public function getUserIdLoggedIn() {
        $user = $this->getUserLoggedIn();
        return $user !== null ? $user->getId() : -1;
    }

    /**
     * Atualiza o cache relacionado o número de notificações, relacionados e depoimentos
     * @param User $user De qual usuário pegar os dados para atualizar o cache
     * @param boolean $force Força a atualização do cache sem fazer verificação
     */
    public function updateCache($user = null, $force = false) {
        try{
        $user = $this->getUser($user);
        $notification = NotificationDAO::getInstance()->countNotification($user);
        if ($force || !isset($_SESSION["notification"]) || $_SESSION["notification"] != $notification) {
            require_once MODELS . "DepositionDAO.php";
            $_SESSION["related"] = UserController::getInstance()
                    ->countRelatedUsers($user);
            $_SESSION["notification"] = $notification;
            $_SESSION["deposition"] = DepositionDAO::getInstance()->countDepositions($user);
        }
        }catch(Exception $e){
            Log::getInstance()->addException($e);
        }
    }

    /**
     * Obtém e retorna o objeto/modelo de um usuário
     * @param mixed $param Qualquer parametro que possa identificar o usuário
     * @return User (Retorna null se não encontrado)
     */
    public function getUser($param) {
        return UserDAO::getInstance()->getUser($param);
    }

    /**
     * Atualiza os dados de um usuário no banco de dados
     * @param User $user
     * @return boolean true = sucesso em atualizar <br> false = falha
     * @throws Exception Erro na validação do usuário
     */
    public function updateUser($user, $checkEmail = true) {
        if ($this->getUserLoggedIn() != null && $this->getUserLoggedIn() == $user->getId()) {
            LoginDAO::getInstance()->setUserLoggedIn($user);
        }
        $this->validatesUser($user, $checkEmail, true);
        return $this->getModel()->updateUser($user);
    }

    /**
     * Verifica se tem alguém logado 
     * @return boolean true = tem alguém logado (na sessão) <br> false = não tem ninguém logado (na sessão)
     */
    public function isAnyoneLoggedIn() {
        $this->getUserLoggedIn();
        return LoginDAO::getInstance()->isAnyoneLoggedIn();
    }

    /**
     * Deixar de seguir alguém pela View
     * @param MainView $view
     */
    public function unfollow(MainView $view) {
        $userFollow = $view->getWhoIsBeingVisited()->getId();
        $userLogged = UserController::getInstance()->getUserLoggedIn()->getId();
        $follow = new Follow();
        $follow->setUserId($userLogged);
        $follow->setUserIdFollow($userFollow);
        FollowDAO::getInstance()->deleteFollow($follow);
        $this->updateCache(null, true);
    }

    /**
     * Verificar se o usuário logado está seguindo quem ele está olhando o perfil
     * @param MainView $view
     * @return type
     */
    public function isFollowing(MainView $view) {
        $userFollow = $view->getWhoIsBeingVisited()->getId();
        $userLogged = $this->getUserLoggedIn()->getId();
        $follow = new Follow();
        $follow->setUserId($userLogged);
        $follow->setUserIdFollow($userFollow);
        return FollowDAO::getInstance()->existsFollow($follow);
    }

    /**
     * Ativar a conta (confirmada pelo e-mail)
     * @param DataInput $data
     * @return boolean (true = Conta confirmada) (false = não foi confirmado nenhuma conta)
     */
    public function active(DataInput $data) {
        $user = UserDAO::getInstance()->getUser($data->getField("user"));
        if ($user === null) {
            return false;
        }
        if ($user->getConfirmationCode() == $data->getField("cod")) {
            $user->setConfirmationTime(new DateTimeYC());
            $user->setConfirmationCode(0);
            $user->setActive(true);
            $this->updateUser($user, false);
            return true;
        }
        return false;
    }

    /**
     * Seguir alguém pela view
     * @param MainView $view
     */
    public function follow(MainView $view) {
        if ($this->isFollowing($view)) {
            return;
        }
        $userFollow = UserController::getInstance()->getUser(
                $view->getWhoIsBeingVisited());
        $userLogged = UserController::getInstance()->getUserLoggedIn();
        $follow = new Follow();
        $follow->setUserId($userLogged->getId());
        $follow->setUserIdFollow($userFollow->getId());
        FollowDAO::getInstance()->addFollow($follow);
        
        $this->updateCache(null, true);
    }

    /**
     * Retorna o modelo DAO de usuário
     * @return UserDAO
     */
    public function getModel() {
        return parent::getModel();
    }

    /**
     * Retorna a instância principal de UserController
     * @return UserController
     */
    public static function getInstance() {
        return parent::getInstance();
    }

    /**
     * Pesquisa por usuários
     * 
     * @param String $name Nome que deseja pesquisar
     * @param Int $count (quantidade minima 1)
     * @param Int $offset (Inicia por 0)
     * @return User
     */
    public function searchUsersByName($name, $count, $offset) {
        return $this->getModel()->searchUsersByName($name, $count, $offset);
    }

    /**
     * 
     * Faz o login do usuário
     * 
     * @param String $email E-email do usuário
     * @param String $password Senha do usuário
     * @return Login Retorna null se e-email ou senha são inválidas
     */
    public function login(DataInput $data) {
        if (($erro = $data->checkFields(array("loginEmail", "loginPassword"), array("E-mail", "Senha"), "b"))) {
            $data->setField("loginError", $erro);
            return null;
        }
        $email = $data->getField('loginEmail');
        $password = $data->getField('loginPassword');
        $keepLogged = $data->getField('auto');
        $user = UserDAO::getInstance()->getUser($email);
        if (is_null($user) || $user->getPassword() != $password) {
            $data->setField("loginError", "E-mail e senha não correspondem!");
            return null;
        }
        try {
            $login = LoginDAO::getInstance()->createLogin($user, true
                    , filter_input(INPUT_SERVER, 'HTTP_USER_AGENT')
                    , $keepLogged);
        } catch (UserDontFoundException $ex) {
            YouCurte::redirect(new URL("register", URL_ROOT) . "?register=2");
        }
        LoginDAO::getInstance()->setUserLoggedIn($user);
        if ($keepLogged) {
            setcookie(COOKIE_USER_AUTH, $login->getAuth()
                    , time() + COOKIE_TIME_LOGGED_IN);
        } else {
            $_SESSION["auth"] = $login->getAuth();
        }
        $this->updateCache($user, true);
        YouCurte::redirect(URL::getRootFromURL());
        return $login;
    }

    /**
     * Cadastra um novo usuário no banco de dados
     * @param User $user Qual usuário cadastrar
     * @param DataInput $data
     * @return boolean true = Usuário registrado
     *          false = Ocorreu algum erro
     *  Se ocorrer algum erro o comando $data->setField("registerError","mensagem de erro") será
     * executada.
     */
    public function registerUser(User $user, DataInput $data) {
        $dao = UserDAO::getInstance();
        $user->setActive(true);
        try {
            $dao->addUser($user);
            return true;
        } catch (EmailAlreadyRegisteredException $e) {
            $data->setField("registerError", $e->getMessage());
        } catch (Exception $e) {
            Log::getInstance()->addLine("Erro em registrar usuário: "
                    . $e->getMessage() . "\r\n" . var_export($user, true));
            $data->SetField("registerError", "Um erro inesperado aconteceu,"
                    . " tente registrar-se mais tarde");
        }
        return false;
    }

    /**
     * Pesquisa por usuário pela view (SearchView)
     * @param DataInput $data
     * @return SearchResult Retorna o count com a quantidade máxima de usuário achados
     *      não limitado pela quantidade máxima de registro para trazer (count_max_return)
     */
    public function searchUsers(DataInput $data) {
        $search = trim($data->getField("search"));
        $data->prepareOffsetAndCount();
        $offset = $data->getField("offset");
        $count = $data->getField("count");
        if (isEmpty($search)) {
            $result = UserDAO::getInstance()->searchAllUsers($count, $offset);
        } else {
            $result = UserDAO::getInstance()->searchUsersByName($search, $count, $offset);
        }
        return $result;
    }

    /**
     * Retorna um objeto de usuário com os dados pego do input enviado por post
     *  ou get
     * @return User
     */
    public function getUserFromInput() {
        return UserDAO::getInstance()->getUserFromInput();
    }

    /**
     * Faz logout local
     */
    public function logout() {
        if (isset($_SESSION["auth"])) {
            $auth = $_SESSION["auth"];
        } else {
            $auth = filter_input(INPUT_COOKIE, "auth");
        }
        LoginDAO::getInstance()->deleteLogin($auth);
        setcookie(COOKIE_USER_AUTH, "", time() - COOKIE_TIME_LOGGED_IN);
        unset($_SESSION["auth"]);
        YouCurte::redirect(URL::getRootFromURL());
        exit(0);
    }

    /**
     * Faz logout de todos os dispositivos
     */
    public function logoutAll() {
        $user = $this->getUserLoggedIn();
        LoginDAO::getInstance()->deleteAllLogins($user);
        $this->logout();
    }

    /**
     * Redifine a senha do usuário
     * @param DataInput $data
     * @return boolean true = sucesso em redifinir <br> false = fracasso
     */
    public function redefinePassword(DataInput $data) {
        if (!$data->hasField("email") || !$data->hasField("code") ||
                !$data->hasField("password")) {
            return false;
        }
        $password = $data->getField("password");
        if ($password != $data->getField("confirmPass")) {
            $data->setField("error", "As senhas não conferem!");
            return false;
        } else if (strlen($password) < 6 || strlen($password) > 12) {
            $data->setField("error", toPT("A senha deve ter de 6 à 12 caracteres!"));
            return false;
        }
        $user = $this->getUser($data->getField("email"));
        if ($user === null) {
            $data->setField("error", toPT("O usuário não foi encontrado!"));
            return false;
        }
        $user->setPassword($password);
        $user->setConfirmationCode(0);
        $this->updateUser($user, false);
        return true;
    }

    /**
     * Atualiza o perfil do usuário
     * @param View $view 
     * @return string Retorna algum tipo de erro que ocorrer
     */
    public function updateUserProfile(View $view) {
        $data = $view->getData();
        $controller = UserController::getInstance();
        if (!$data->hasField("register")) {
            return "Nenhum dado para salvar!";
        }
        $user = $controller->getUserFromInput();
        $user->settoSendEmails($view->getData()->getField("nottoSendEmails")!="1");
        $data->setField("toSendEmails",$user->istoSendEmails());
        $userLogged = clone $controller->getUserLoggedIn();
        $userLogged->settoSendEmails(false);
        foreach ($user->listSets() as $field) {
            $value = $userLogged->get($field);
            if ($field != "password" && $field != "id" && (is_string($value) ||
                    $data->hasField($field))) {
                $userLogged->set($field, $user->get($field));
            }
        }
        if (isNotEmpty($error = $controller->validatesUser($userLogged))) {
            $data->setField("registerError", "$error");
            return $error;
        }
        $default = new URL(USER_IMAGE_DEFAULT);
        if ($default->equals(new URL($data->getField("image"), URL_ROOT))) {
            $userLogged->setImage($default);
        } else {
            $image = ImageController::getInstance()->
                    moveTempImageToUser($data->getField("image"), "Foto do perfil " . DateTimeYC::now()->toPT());
            if (!is_null($image)) {
                $userLogged->setImage($image);
            } else if (isNotEmpty($data->getField("image"))) {
                $error = toPT("A sessão expirou!");
                $data->setField("registerError", $error);
                return $error;
            }
        }
        $userLogged->setActive(true);
        $controller->updateUser($userLogged);
    }

    /**
     * Atualiza a senha do usuário logado
     * @param DataInput $data Dados de entrada
     */
    public function updatePassword(DataInput $data) {
        if (($error = $data->checkFields(
                array("currentPass", "password", "confirmPass"), array("Senha atual", "Nova senha", toPT("Confirmação da senha")))
                ) !== false) {
            die(json_encode_yc(array("error" => "$error", "result" => "error")));
        }
        if ($data->getField("password") !== $data->getField("confirmPass")) {
            $error = toPT("A nova senha não confere com a de confirmação!");
            die(json_encode_yc(array("error" => "$error", "result" => "error")));
        }
        if ($data->getField("currentPass") !== $this->getUserLoggedIn()->getPassword()) {
            $error = toPT("A senha informada não confere com sua senha atual!");
            die(json_encode_yc(array("error" => "$error", "result" => "error")));
        }
        $user = $this->getUserLoggedIn();
        $user->setPassword($data->getField("password"));
        $this->updateUser($user);
        die(json_encode_yc(array("result" => "ok")));
    }

    /**
     * Verifica se os dados do usuário são válidos
     * @param User $user O usuário que deseja verificar
     * @param boolean $checkEmail Verificar se e-mail já está cadastrado no banco de dados
     * @param boolean $darErro Lança o exception
     * @return string Retorna um erro ou vazio ('')
     */
    public function validatesUser($user, $checkEmail = true, $darErro = false) {
        return $this->getModel()->validatesUser($user, $checkEmail, $darErro);
    }

    /**
     * Retorna uma lista com os usuários relacionados com $user
     * @param User|int|string $user
     * @return array
     */
    public function listRelatedUsers(DataInput $data, $user = null) {
        $data->prepareOffsetAndCount();
        $offset = $data->getField("offset");
        $count = $data->getField("count");
        return FollowDAO::getInstance()->listRelatedUsers($user, $count, $offset);
    }

    /**
     * Conta quantos usuários relacionados tem o $user
     * @param User|int $user
     * @return int A quantidade
     */
    public function countRelatedUsers($user = null) {
        $result = FollowDAO::getInstance()->countRelatedUsers($user);
        return $result;
    }

    /**
     * Define ou muda o avatar/imagem de perfil do usuário
     * @param DataInput $data Dados de dados
     */
    public function setProfileImage(DataInput $data) {
        if (($error = $data->checkFields(array("photoId"))) !== false) {
            die(json_encode_yc(array("result" => "error", "error" => $error)));
        }
        $user = $this->getUserLoggedIn();
        $image = ImageDAO::getInstance()->getImage($data->getField("photoId"));
        if ($image === null) {
            $error = "Imagem inválida!";
            die(json_encode_yc(array("result" => "error", "error" => $error)));
        }
        $user->setImage($image);
        $this->updateUser($user);
        die(json_encode_yc(array("result" => "ok")));
    }

}
