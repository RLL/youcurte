<?php

require_once CONTROLLERS."Controller.php";
require_once CLASSES . 'Email.php';

/**
 * Controller para mandar e-mails
 *
 * @author Luiz
 */
class EmailController extends Controller {

    /**
     * Envia um e-mail para confirmar o registro da conta
     * 
     * @param User $user O usuário para quem enviar o e-mail
     * @return boolean|string Confirmação do e-mail enviado <br>
     *      false = mensagem enviada com sucesso <br>
     *      Se retornar uma string com a mensagem de erro.
     */
    public function sendConfirmationEmail(User $user) {
        $email = new Email();
        $email->setSubject(toPT("Bem vindo ao YouCurte!"));
        $url = new URL("register/active/" .
                $user->getId() . "/" . $user->getConfirmationCode(), URL_ROOT);
        $email->setIsHTML(true);
        $content1 = file_get_contents(new File('emails/confirmEmail.html', VIEWS));
        $content2 = str_replace("\$url", $url, $content1);
        $email->setMessage($content2);
        $email->setTo($user->getEmail());
        $email->setToName($user->getFullName());
        return ($email->send());
    }

    /**
     * Envia um e-mail com o link para redefinir a senha do usuário
     * @param DataInput $data
     */
    public function sendEmailForgotPassword(DataInput $data) {
        $userEmail = $data->getField("email");
        $dao = UserDAO::getInstance();
        $user = $dao->getUser($userEmail);
        if ($user == null) {
            echo json_encode(array("result" => "error",
                "error" => "Nenhuma foi cadastrar com esse e-mail($userEmail)!"));
            exit();
        }
        $user->setConfirmationCode(rand(1000, 999999));
        if (strlen($user->getPassword()) < USER_PASSWORD_MIN_LENGTH ||
                strlen($user->getPassword()) > USER_PASSWORD_MAX_LENGTH) {
            $user->setPassword(USER_PASSWORD_DEFAULT);
        }
        $dao->validatesUser($user, false, true);
        $dao->updateUser($user);
        $link = new URL("forgotpassword/" . $user->getId() . "/"
                . $user->getConfirmationCode(), URL_ROOT) . "";
        $email = new Email();
        $email->setTo($userEmail);
        $email->setToName($user->getFullName());
        $email->setSubject("Redefinir senha");
        $email->setMessage("Acesse o seguinte link para redefinir a senha da sua conta: $link");
        $email->send();
            echo json_encode(array("result" => "ok","data"=>"Foi enviado um e-mail com as intruções para redefinir sua senha!"));
            exit(0);
    }

    /**
     * Envia para o e-mail de seguido uma notificação de novo seguidor
     * @param User $user O usuário que realizou a ação
     * @param User $userFollow O usuário que agora está sendo seguido
     */
    public function sendNotificationOfNewFollowing(User $user, User $userFollow) {
        if (!$userFollow->isToSendEmails()) {
            return;
        }
        $email = new Email();
        $email->setTo($userFollow->getEmail());
        $email->setToName($userFollow->getFullName());
        $email->setSubject("Novo seguidor no youcurte.com.br");
        $message = file_get_contents(new File(VIEWS . "emails" . DS . "newFollowing.html") . "");
        $url = new URL("profile/" . $user->getId(), URL_ROOT);
        $email->setMessage(str_replace(":url", $url . "", $message));
        $email->setIsHTML(true);
        $email->send();
    }

    /**
     * Retorna a instância principal do EmailController
     * @return EmailController
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}

/**
 * Classe de exceção para e-mail já registrado
 */
class EmailAlreadyRegisteredException extends Exception {

    /**
     * Método contrutor da exção de e-mail já registrado
     * 
     * @param string $email O e-mail já registrado
     * @param type $code [opcional]
     * @param type $previous [opcional]
     */
    public function __construct($email, $code = null, $previous = null) {
        parent::__construct("E-mail '$email' já está registrado.", $code, $previous);
    }

}
