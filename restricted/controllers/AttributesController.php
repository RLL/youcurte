<?php

require_once MODELS."AttributesDAO.php";

/**
 * Controller para lidar com Atributos dos usuários (Sexy,Confiavel,Legal)
 *
 * @author YouCurte
 */
class AttributesController extends Controller {

    public function __construct() {
        parent::__construct(AttributesDAO::getInstance());
    }

    /**
     * Pega os atributos de quem está sendo visitado.
     * @param MainView $view A view principal
     * @return Attributes Retorna esses abrituos
     */
    public function getAttributes(MainView $view) {
        $userIdFrom = $view->getUserLoggedIn()->getId();
        $userIdTo = $view->getWhoIsBeingVisited()->getId();
        return $this->getModel()->getAttributes($userIdFrom, $userIdTo);
    }
    
    /**
     * Salva os atributos de um usuário no banco de dados
     * @param Attributes $attr Os atributos que deseja salvar
     */
    public function saveAttributes(Attributes $attr){
        return $this->getModel()->saveAttributes($attr);
    }
    
    /**
     * Edita os atributos que algum usuário deu a outro usuário no banco de dados
     *  a partir da view
     * @param MainView $view
     */
    public function editAttributes(MainView $view){
        $attr = $this->getAttributes($view);
        $data = $view->getData();
        $attr->setCool($data->getField("cool"));
        $attr->setSexy($data->getField("sexy"));
        $attr->setReliable($data->getField("reliable"));
        if (isNotEmpty($error = $this->validateAttributes($attr, false))){
            die(json_encode_yc(array("result"=>"error","error"=>$error)));
        } else {
            $this->saveAttributes($attr);
            die(json_encode_yc(array("result"=>"ok")));
        }
    }
    
    /**
     * Valida se os valores dos atributos informados são válidos
     * @param Attributes $attr Os atributos que deseja verificar
     * @param bool $throw Lançar uma exceção em caso de erro? <br>true = sim <br> false = não
     * @throws Exception
     * @return String Retorna uma mensagem de erro se não retorna vazio ("")
     */
    public function validateAttributes(Attributes $attr = null, $throw = false) {
        return $this->getModel()->validateAttributes($attr,$throw);
    }

    /**
     * Retorna o modelo DAO de atributos
     * @return AttributesDAO
     */
    public function getModel() {
        return parent::getModel();
    }
    
    /**
     *  Retorna a instância principal de AttributesController
     * @return AttributesController
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}