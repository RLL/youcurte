<?php

require_once CLASSES . 'Singleton.php';
require_once MODELS . 'DAO.php';
require_once VIEWS . 'View.php';

/**
 * Classe para criar os controladores
 *
 * @author YouCurte
 */
class Controller extends Singleton {

    private $model, $view;

    /**
     * Método construtor dos controladores
     * @param DAO $model Uma classe derivada de DAO
     * @param View $view A view
     */
    public function __construct(DAO $model = null, View $view = null) {
        parent::__construct();
        $this->setModel($model);
        $this->setView($view);
    }

    /**
     * Retorna uma classe derivada de DAO
     * @return DAO
     */
    public function getModel() {
        return $this->model;
    }
    
    /**
     * Define um modelo DAO
     * @param type $model
     */
    public function setModel($model) {
        $this->model = $model;
    }

    /**
     * Retorna uma view
     * @return View
     */
    public function getView() {
        return $this->view;
    }

    /**
     * Define uma view para o controlador
     * @param View $view
     */
    public function setView($view) {
        $this->view = $view;
    }
    
     /**
     * Verifica se os campos informados tem algum valor válido.
     * 
     * Ex de uso:
     *  checkFields($user,array('name','age'),array('nome','idade'));
     *  Saída: 'Os seguintes campos são necessários: nome.'
     *  checkFields($user,array('name','age'),array('nome','idade'),'b');
     *  Saída: 'Os seguintes campos são necessários: <b>nome</b>.';
     * 
     * @return mixed Retorna uma mensagem se tiverem campos faltando.
     *  Retorna false se tiver tudo ok.
     */
    public function checkFields(Row $row,$fields = null, $labels = null, $tag = "") {
        return $this->getModel()->checkFields($row,$fields,$labels,$tag);
    }
    
 

}