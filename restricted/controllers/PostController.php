<?php

require_once CONTROLLERS . "Controller.php";
require_once MODELS . "PostDAO.php";
require_once CONTROLLERS . "UserController.php";
require_once MODELS . "NotificationDAO.php";
require_once CONTROLLERS . "ImageController.php";
require_once MODELS . "PostHistoryDAO.php";

/**
 * Controller para Post
 *
 * @author YouCurte
 */
class PostController extends Controller {

    public function __construct() {
        parent::__construct(PostDAO::getInstance());
    }

    /**
     * Retorna a instância principal de PostController
     * @return PostController
     */
    public static function getInstance() {
        return parent::getInstance();
    }
    
    /**
     * Retorna o modelo DAO de post
     * @return PostDAO
     */
    public function getModel(){
        return parent::getModel();
    }

    /**
     * Retorna uma lista de posts da Timeline
     * @param DataInput $data Dados de entrada
     * @param User $user Timeline de que usuário?
     * @return array Array de Post
     */
    public function getTimeLine(DataInput $data, User $user = null) {
        if (is_null($user)) {
            $user = UserController::getInstance()->getUserLoggedIn();
        }
        $data->declareFields(array("page" => 0));
        if (!isInt($data->getField("page"))) {
            $data->setField("page", 0);
        }
        $offset = $data->getField("page") * COUNT_MAX_RETURN;
        $result = PostDAO::getInstance()->getTimeLine($user, COUNT_MAX_RETURN, $offset);
        return $result;
    }

    /**
     * Lista todos os posts de um determinado usuário
     * @param DataInput $data Dados de entrada
     * @param mixed $user [opcional]Qualquer paramêtro que possa identificar o usuário
     * @return array Array de post
     */
    public function listPosts(DataInput $data, $user = null) {
        $data->declareFields(array("page" => 0));
        if (!isInt($data->getField("page"))) {
            $data->setField("page", 0);
        }
        $offset = $data->getField("page") * COUNT_MAX_RETURN;
        $result = PostDAO::getInstance()->listPosts($user, COUNT_MAX_RETURN, $offset);
        return $result;
    }

    /**
     * Pega e retorna um post específico
     * @param Post|integer $post O post ou id do post
     * @return Post
     */
    public function getPost($post) {
        return $this->getModel()->getPost($post);
    }

    /**
     * Delete um post
     * @param Post|int $post O post ou id do post
     */
    public function deletePost($post) {
        if (($result = $this->getModel()->deletePost($post)) != false) {
            echo json_encode_yc(array("result" => "error","error"=>$result));
            Log::getInstance()->addLine($result);
        } else {
            echo json_encode_yc(array("result" => "ok"));
        }
        exit();
    }

    /**
     * Pesquisa por posts (mesmo que search do post)
     * @param mixed $where
     * @param mixed $values
     * @param int $count
     * @param int $offset
     * @return array
     */
    public function searchPosts($where, $values = null, $count = COUNT_MAX_RETURN, $offset = 0) {
        return $this->getModel()->searchPosts($where, $values, $count, $offset)->getData();
    }

    /**
     * Faz a postagem
     * @param DataInput $data Dados de entrada
     * @return boolean|Post Retorna o Post <br> Se tiver algum problema retorna false
     */
    public function post(DataInput $data) {
        $text = $data->getField("text");
        $post = new Post();
        if (isset($_FILES["image"])) {
            $image = ImageController::getInstance()->uploadImageUser();
            if (is_string($image)) {
                $data->setField("errorMessageDialog", $image);
                return false;
            }
            $post->setImage($image);
            $post->setHasImage($image->getId()!==null);
        } else if (isEmpty(trim("" . $text))&&!$data->hasField("imageId")) {
            $data->setField("errorMessageDialog", "Para postar é necessário um texto ou imagem");
            return false;
        }
        if ($data->hasField("imageId")){
            $image = ImageController::getInstance()->getImage($data->getField("imageId"));
            $post->setImage($image);
        }
        $data->declareFields(array("local" => null, "latitude" => null, "longitude" => null));
        $post->setText($text);
        $post->setLocal($data->getField("local"));
        $post->setLatitude($data->getField("latitude"));
        $post->setLongitude($data->getField("longitude"));
        $post->setTime(DateTimeYC::now());
        PostDAO::getInstance()->addPost($post);
        if ($data->hasField("imageId")){
            WebYouCurte::redirect(new URL("posts",URL_ROOT));
        }
        return $post;
    }

    /**
     * Define a opinião do usuário logado sobre determinado Post
     * @param int $postId O id do post
     * @param int $tri O post é tri? <br> 0 = não <br> 1 = sim
     * @param int $bah  O post é xucro? <br> 0 = não <br> 1 = sim
     */
    public function setOpinion($postId, $tri, $bah = null) {
        try {
            WebYouCurte::checkPrivilege(true);
            $dao = OpinionDAO::getInstance();
            $opinion = $dao->getOpinion($postId);
            if (!is_null($tri)) {
                $opinion->setTri($tri);
            }
            if (!is_null($bah)) {
                $opinion->setBah($bah);
            }
            $dao->saveOpinion($opinion);
            echo json_encode_yc(array('result' => 'ok'));
        } catch (Exception $e) {
            Log::getInstance()->addException($e);
            if (DEBUG_PROJECT) {
                echo $e->getTraceAsString();
                echo $e->getMessage();
            } else {
                echo json_encode_yc(array('result' => 'error'));
            }
        }
        exit();
    }

    /**
     * Atualiza os dados de um post no banco de dados
     * @param Post $post
     * @return boolean|string Retorna false se foi editado com sucesso.
     *  Caso contrário retorna uma string com a mensagem de erro.
     */
    public function updatePost(Post $post) {
        return $this->getModel()->updatePost($post);
    }

    /**
     * Edita os dados de um post num banco de dados 
     * @param DataInput $data Dados de entrada coletados na view
     */
    public function editPost(DataInput $data) {
        $controller = PostController::getInstance();
        $post = $controller->getPost($data->getField("id"));
        $post->setText($data->getField("text"));
        require_once VIEWS . "post" . DS . "PostView.php";
        $view = new PostView();
        if (isEmpty(trim($post->getText() . "")) && $post->getImage() == null) {
            die(json_encode_yc(array("result" => "error",
                "error" => "esse post precisa de um texto!")));
        }
        $view->setPost($post);
        $text = $view->getPostText();
        if ($controller->updatePost($post) == false) {
            echo json_encode_yc(array("result" => "ok", "text" => $text));
        } else {
            echo json_encode_yc(array("result" => "error"));
        }
        exit();
    }

    /**
     * Pesquisa por hashtags nos posts
     * 
     * @return SearchResult Retorna com count indificando o número total encontrado
     *  mas os dados são registrandos pelo count
     */
    public function searchPostsForHashtags(DataInput $data) {
        $data->prepareOffsetAndCount();
        $search = $data->getField("search");
        $offset = $data->getField("offset");
        $count = $data->getField("count");
        return $this->getModel()->searchPostsForHashtags($search, $count, $offset);
    }

    /**
     * Listar posts de um local
     * @param string $local
     * @param int $count
     * @param int $offset
     * @return Array
     */
    public function listPostsFromLocal($local, $count = COUNT_MAX_RETURN, $offset = 0) {
        return $this->getModel()->listPostsFromLocal($local, $count, $offset);
    }

    /*
     * Lista os ultimos usuários que viram a postagem
     * @param mixed $post Qualquer paramêtro que possa identificar o post
     */
    public function listLastUsersWhoHaveVisited($post) {
        $post = $this->getModel()->getPost($post);
        $dao = PostHistoryDAO::getInstance();
        return $dao->listPostsHistories(null, $post->getId());
    }
    
    /**
     * 
     * Registra no histórico que visitou tal post
     * 
     * @param Post|int $post
     */
    public function visitedPost($post){
        $post = $this->getPost($post);
        $history = new PostHistory();
        $history->setPostId($post->getId());
        $history->setUserId(UserController::getInstance()->
                getUserLoggedIn()->getId());
        PostHistoryDAO::getInstance()->savePostHistory($history);
    }

}