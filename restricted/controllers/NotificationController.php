<?php

require_once MODELS.'NotificationDAO.php';
require_once CONTROLLERS . 'Controller.php';

/**
 * Controller para notificações
 *
 * @author YouCurte
 */
class NotificationController extends Controller {
   
    public function __construct(){
        parent::__construct(NotificationDAO::getInstance());
    }
    
    /**
     * Lista todas as notificações de um usuário
     * @param mixed $user [opcional] Qualquer paramêtro que possa identificar o usuário
     *      (se não informado nenhum, pega do usuário logado)
     * @return array Lista de Notification
     */
    public function listNotifications($user = null){
        return $this->getModel()->listNotifications($user,25);
    }
    
    
    /**
     * Pega e retorna uma notificação específica
     * @param int $id Id da notification
     * @param mix $user Qualquer parametro que identifique o usuário
     * @return Notification Retorna null se não achou
     */
    public function getNotification($id,$user=null){
        return $this->getModel()->getNotification($id,$user);
    }
    
    /**
     * Atualiza os dados de uma notificação
     * @param Notification $notification
     * @return boolean  true = conseguiu atualiar <br> false = falha
     */
    public function updateNotification(Notification $notification){
        return $this->getModel()->updateNotification($notification);
    }
    
    /**
     * Retorna o modelo DAO de notificações
     * @return NotificationDAO
     */
    public function getModel() {
        return parent::getModel();
    }
    
    /**
     * Retorna a instância principal de NotificationController
     * @return NotificationController
     */
    public static function getInstance() {
        return parent::getInstance();
    }
    
    /**
     * Conta quantas notificações o usuário têm
     * @param mixed $user Qualquer paramêtro para identificar o usuário
     * @return int A quantidade
     */
    public function countNotification($user = null){
        return $this->getModel()->countNotification($user);
    }
    
}