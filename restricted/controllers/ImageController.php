<?php

require_once CONTROLLERS . "Controller.php";
require_once MODELS . "ImageDAO.php";
require_once MODELS . "Image.php";
require_once MODELS . "AlbumDAO.php";

/**
 * Description of ImageController
 *
 * @author YouCurte
 */
class ImageController extends Controller {

    /**
     * Faz o upload de uma imagem temporária
     * @param input [opcional] Input com o arquivo (<input name='file' type='file'>)
     * @return File
     */
    public function uploadTempImage($input = 'file') {
        $name1 = DateTimeYC::now()->toPT();
        $name2 = str_replace(":", "@", $name1);
        $name3 = str_replace("/", "-", $name2);
        $name4 = $name3 . "$" . rand(0, 999999);
        $this->clearTemp();
        return $this->uploadImage($input, 'tmp', $name4);
    }

    /**
     * Faz o upload de uma imagem normalmente
     * 
     * @param string $input Nome do input (<input name='file' type='file'>)
     * @param string $dir  Diretório a onde salvar o arquivo
     * @param string $filename Nome do arquivo
     * @return \File O arquivo da imagem
     */
    public function uploadImage($input, $dir, $filename) {
        if (isset($_FILES[$input])) {
            $temp = $_FILES[$input]["tmp_name"];
            $local = new File($_FILES[$input]["name"]);
            $filename = str_replace(" ", "_", $filename);
            $remote = new File($filename . $local->getFileExtension(), ROOT . "img" . DS . $dir);
            move_uploaded_file($temp, $remote . "");
            return $remote;
        }
    }

    /**
     * Deleta as imagens dos arquivos temporários
     */
    public function clearTemp() {
        $tmp = new File(IMAGE_DIRECTORY_TEMP);
        $files = $tmp->listFiles();
        foreach ($files as $file) {
            $filename = $file->getNameWithoutExt();
            $strdate = strcopy_r(str_replace("-", "/", $filename), "", "$");
            $strtime = str_replace("@", ":", $strdate);
            $strdatetime = str_replace("_", " ", $strtime);
            $datetime = new DateTimeYC($strdatetime);
            $datetime->add(IMAGE_TEMP_LONG);
            $deleted = 0;
            if ($datetime->isBefore(DateTimeYC::now())) {
                $deleted = ($file->deleteFile() ? 1 : 0) + 1;
            }
            if ($deleted === 1) {
                Log::getInstance()->addLine("Não foi possível deletar o "
                        . "arquivo: " . $file);
            }
        }
    }

    /**
     *  Faz o upload da imagem para a pasta do usuário e salva no banco
     * @param string $input O nome do input
     * @return image|string Retorna a image ou 
     *  uma string com a mensagem de erro
     */
    public function uploadImageUser($input = "image", $albumId = ALBUM_ID_DEFAULT) {
        if (!isset($_FILES[$input])) {
            return "Não foi encontrado a imagem";
        }
        if ($_FILES[$input]["size"] > IMAGE_FILE_SIZE_MAX) {
            return "A imagem é muito grande para ser enviada.";
        }
        $image = new Image();
        $image->setAlbumId($albumId);
        $format = strcopy_r($_FILES[$input]["name"], ".");
        $image->generateURL($format);
        $file = new File($_FILES[$input]["name"]);
        $image->setName($file->getNameWithoutExt());
        if (move_uploaded_file($_FILES[$input]['tmp_name'], $image->getUrl()->toFile())) {
            ImageDAO::getInstance()->addImage($image);
        }
        return $image;
    }

    /**
     * Move uma imagem da pasta temporária para a pasta dos usuários
     *   e registra a imagem no banco
     * @param string $tempImage O url da imagem
     * @return Image Retorna null se não for válida. Objeto da imagem cadastrada na banco.
     */
    public function moveTempImageToUser($tempImage, $name = "") {
        $url = new URL($tempImage);
        $tmp = $url->toFile()->getValidFile();
        if ($tmp->exists()) {
            $image = new Image();
            $image->setName($name);
            $image->generateURL($tmp->getFileExtension());
            $new = $tmp->moveTo($image->getUrl()->toFile());
            if (!$new->exists()) {
                return null;
            }
            ImageDAO::getInstance()->addImage($image);
        } else {
            return null;
        }
        $this->clearTemp();
        return $image;
    }

    /**
     * Lista todos os albuns de um usuário
     * @param int|User $user
     * @return Array
     */
    public function listAlbums($user = null) {
        if ($user === null) {
            $user = UserController::getInstance()->getUserLoggedIn();
        }
        return AlbumDAO::getInstance()->listAlbums($user);
    }

    /**
     * Lista todas as imagens de álbum pela view
     * @param View $view
     * @return array A lista de Image's
     */
    public function listImages(SubView $view) {
        $view->getData()->prepareOffsetAndCount();
        $offset = $view->getData()->getField("page") * COUNT_RETURN_IMAGES;
        $user = $view->getMainView()->getWhoIsBeingVisited()->getId();
        return ImageDAO::getInstance()->
                        listImages($view->getAlbumId(), $user, COUNT_RETURN_IMAGES, $offset);
    }

    /**
     * Deleta imagens/fotos selecionados na view
     * @param DataInput $data Dados de entrada
     */
    public function deleteImages(DataInput $data) {
        $ids = explode(",", $data->getField("ids"));
        $userId = LoginDAO::getInstance()->getUserLoggedIn()->getId();
        ImageDAO::getInstance()->deleteImages($ids, $userId);
    }

    /**
     * Delete um álbum
     * @param DataInput $data
     */
    public function deleteAlbum(DataInput $data) {
        $id = $data->getField("albumId");
        $album = new Album();
        $album->setId($id);
        $album->setUserId(LoginDAO::getInstance()->getUserLoggedIn()->getId());
        AlbumDAO::getInstance()->deleteAlbum($album);
    }

    /**
     * Cria um novo álbum para o usuário logado
     * @param DataInput $data Dados e entrada
     * @return boolean|\Album Retorna false não se for possível criar <br>
     *      Retorna o objeto Album se foi possível
     */
    public function newAlbum(DataInput $data) {
        if (!$data->hasField("albumName")) {
            return false;
        }
        $album = new Album();
        $album->setName($data->getField("albumName"));
        $album->setUserId(LoginDAO::getInstance()->getUserLoggedIn()->getId());
        AlbumDAO::getInstance()->addAlbum($album);
        return $album;
    }

    /**
     * Renomeia um álbum no banco de dados pela view
     * @param SubView $view
     * @return boolean
     *  true = renomeada <br> false = erro
     */
    public function renameAlbum(SubView $view) {
        if (!$view->getData()->hasField("albumName")) {
            return false;
        }
        $albumId = $view->getAlbumId();
        $userId = $view->getMainView()->getUserLoggedIn()->getId();
        $name = $view->getData()->getField("albumName");
        $album = AlbumDAO::getInstance()->getAlbum($albumId, $userId);
        $album->setName($name);
        AlbumDAO::getInstance()->updateAlbum($album);
        return true;
    }

    /**
     * Renomeia uma foto/imagem no banco de dados
     * @param DataInput $data Dados de entrada
     */
    public function renamePhoto(DataInput $data) {
        if (!$data->hasField("photoName")||!$data->hasField("photoId")) {
            die(json_encode_yc(array("error"=>"Campos em branco!","result"=>"error")));
        }
        $photoId = $data->getField("photoId");
        $image = ImageDAO::getInstance()->getImage($photoId);
        $image->setName($data->getField("photoName"));
        if ($image->getUserId()!==LoginDAO::getInstance()->getUserLoggedIn()->getId()){
            die(json_encode_yc(array("error"=>"Sem permissão!","result"=>"error")));
        }
        ImageDAO::getInstance()->update($image);
        die(json_encode_yc(array("result"=>"ok")));
    }

    /**
     * Pega e imprime a anterior e próxima imagem da imagem atual que está sendo exibida
     * @param DataInput $data Os dados de entrada
     */
    public function getImageNeighbors(DataInput $data){
        if (!$data->hasField("photoId")&&!$data->hasField("result")){
            die(json_encode_yc(array("result"=>"error","error"=>"Campos em branco!")));
        }
        $id = $data->getField("photoId");
        $photo = ImageDAO::getInstance()->getImage($id);
        $result = $data->getField("result");
        $img = ImageDAO::getInstance()->getImageNeighbors($id,
                $photo->getAlbumId(), $photo->getUserId())[$result];
        die(json_encode_yc(array("result"=>"ok","src"=>$img->getValidUrl()."",
            "name"=>$img->getName(),"id"=>$img->getId())));
    }
    
    /**
     * Move imagens de um album para outro
     * @param DataInput $data Dados de entrada
     * @return boolean False = se houve algum problema <br> true = tudo okay
     */
    public function moveImageToAlbum(DataInput $data){
        if (($error = $data->checkFields(array("toAlbumId","ids")))!==false){
            return false;
        }
        $array = $ids = explode(",",$data->getField("ids"));
        foreach($ids as $id){
            if (!isInt($id)){
                $array = array_remove_value($array, $id);
            }
        }
        $ids = $array;
        $userId = LoginDAO::getInstance()->getUserLoggedIn()->getId();
        ImageDAO::getInstance()->moveImageToAlbum($ids,
                $data->getField("toAlbumId"),$userId);
        return true;
    }
    
    /**
     * Retorna o objeto de uma imagem do banco de dados
     * @param mixed $param Qualquer paramêtro que possa identificar a imagem
     * @return Image
     */
    public function getImage($param){
        return ImageDAO::getInstance()->getImage($param);
    }
    
    /**
     * Retorna a instância principal de ImageController
     * @return ImageController
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}