<?php

require_once MODELS . "DepositionDAO.php";

/**
 * Controlador para depoimentos
 *
 * @author YouCurte
 */
class DepositionController extends Controller {

    public function __construct() {
        parent::__construct(DepositionDAO::getInstance());
    }

    /**
     * Lista todos os depoimentos que um usuário recebeu
     * @param int $userIdTo O id do usuário que recebeu
     * @return array
     */
    public function listDepositions($userIdTo, DataInput $data) {
        $data->declareFields(array("page" => 0));
        if (!isInt($data->getField("page"))) {
            $data->setField("page", 0);
        }
        $offset = $data->getField("page") * COUNT_MAX_RETURN;
        return $this->getModel()->listDepositions($userIdTo, COUNT_MAX_RETURN,
                $offset);
    }

    /**
     * Fazer depoimento a partir da view
     * @param MainView $view
     */
    public function doDeposition(MainView $view) {
        $data = $view->getData();
        $deposition = new Deposition();
        $deposition->setText($data->getField("text"));
        if (isEmpty(trim($deposition->getText() . ""))) {
            $view->getData()->setField("errorMessageDialog", "O depoimento precisa de um texto!");
            return false;
        }
        $deposition->setUserIdFrom(UserController::getInstance()->getUserIdLoggedIn());
        $deposition->setUserIdTo($view->getWhoIsBeingVisited()->getId());
        $this->getModel()->addDeposition($deposition);
        return true;
    }

    
    /**
     * Deleta um depoimento pela view
     * @param MainView $view
     */
    public function deleteDeposition(MainView $view) {
        $userIdFrom = $view->getUserLoggedIn()->getId();
        $id = $view->getData()->getField("depositionId");
        $deposition = new Deposition();
        $deposition->setUserIdFrom($userIdFrom);
        $deposition->setId($id);
        $this->getModel()->deleteDeposition($deposition);
        die(json_encode_yc(array("result" => "ok")));
    }

    /**
     * Edita um depoimento pela view
     * @param MainView $view
     */
    public function editDeposition(MainView $view) {
        $controller = $this;
        $data = $view->getData();
        $userIdFrom = $view->getUserLoggedIn()->getId();
        $id = $view->getData()->getField("depositionId");
        $deposition = $controller->getModel()->getDeposition($id);
        if ($userIdFrom!=$deposition->getUserIdFrom()){
           die(json_encode_yc(array("result"=>"error","error"=>"O usuário logado não tem permissão  para deletar tal depoimento.")));
        }
        $deposition->setText($data->getField("text"));
        require_once VIEWS . "post" . DS . "PostView.php";
        $postView = new PostView();
        if (isEmpty(trim($deposition->getText() . ""))) {
            die(json_encode_yc(array("result" => "error",
                "error" => "O depoimento precisa de um texto!")));
        }
        $post = new Post();
        $post->setText($deposition->getText());
        $postView->setPost($post);
        $text = $postView->getPostText();
        if ($this->getModel()->updateDeposition($deposition)) {
            echo json_encode_yc(array("result" => "ok", "text" => $text));
        } else {
            echo json_encode_yc(array("result" => "error"));
        }
        exit();
    }
    
    /**
     * Conta a quantidade de depoimentos que um usuário ($user) possui
     * @param mixed $user Qualquer paramêtro que possa identificar o usuário
     * @return int A quantidade
     */
    public function countDepositions($user){
        return $this->getModel()->countDepositions($user);
    }

    /**
     * Retorna o modelo DAO de depoimentos
     * @return DepositionDAO
     */
    public function getModel() {
        return parent::getModel();
    }

    /**
     * Retorna a instância principal de DepositionController
     * @return DepositionController
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}