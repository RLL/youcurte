<?php

require_once MODELS . "Attributes.php";

/**
 * Classe para manipular os atributos (legal,sexy,confiável) de cada usuário
 *
 * @author YouCurte
 */
class AttributesDAO extends DAO {

    public function __construct() {
        parent::__construct("Attributes");
    }

    /**
     * Retorna os atributos que um usuário atribuiu a outro
     * @param int $userIdFrom O id do usuário quem atribuiu
     * @param int $userIdTo O id do usuário quem recebeu
     * @return Attributes 
     */
    public function getAttributes($userIdFrom, $userIdTo) {
        $result = $this->search(array("userIdFrom" => $userIdFrom,
                    "userIdTo" => $userIdTo))->getData(0);
        if ($result !== null) {
            return $result;
        } else {
            $attr = new Attributes();
            $attr->setUserIdFrom($userIdFrom);
            $attr->setUserIdTo($userIdTo);
            return $attr;
        }
    }

    /**
     * Verifica se tal usuário já atribuiu algumas notas para outro usuário
     * @param int $userIdFrom O id do usuário que atribuiu
     * @param int $userIdTo O id do usuário que recebeu os atributos
     * @return boolean true = atribuiu <br> false = não atribuiu
     */
    public function existsAttributes($userIdFrom, $userIdTo) {
        return $this->search(array("userIdFrom" => $userIdFrom,
                    "userIdTo" => $userIdTo))->getCount() > 0;
    }

    /**
     * Salva os atributos no banco de dados
     * @param Attributes $attr
     * @return Foi salvo com sucesso
     */
    public function saveAttributes(Attributes $attr) {
        $this->validateAttributes($attr, true);
        if ($this->existsAttributes($attr->getUserIdFrom(), $attr->getUserIdTo())) {
            $this->update($attr, array("userIdFrom", "userIdTo"));
        } else {
            $this->insert($attr);
        }
        return $this->wasLastExecutionSuccessful();
    }

    /**
     * Verifica se os dados dos atributos são validos.
     * 
     * @param Attributes $attr Os atributos que quer verifiar
     * @param bool $throw Deseja lançar uma exceção se encontrar um erro? true = sim <br> false = não
     * @throws Exception
     * @return String Retorna uma mensagem de erro se não retorna vazio ("")
     */
    public function validateAttributes(Attributes $attr = null, $throw = false) {
        $error = "";
        if (is_null($attr)) {
            $error = "Objeto de Attributes está nulo.";
        } else if (!(isInt($attr->getCool()) && $attr->getCool() >= 0 && $attr->getCool() <= 100)) {
            $error = "A porcentagem de legal deve ser de 0 a 100. "
            . "Valor recebido: '" . $attr->getCool()."'";
        } else if (!(isInt($attr->getReliable()) && $attr->getReliable() >= 0 && $attr->getReliable() <= 100)) {
            $error = "A porcentagem de legal deve ser de 0 a 100."
            . "Valor recebido: '" . $attr->getReliable()."'";
        } else if (!(isInt($attr->getSexy()) && $attr->getSexy() >= 0 && $attr->getSexy() <= 100)) {
            $error = "A porcentagem de sexy deve ser de 0 a 100."
            . "Valor recebido: '" . $attr->getSexy()."'";
        }
        if ($throw && isNotEmpty($error)) {
            $exception = new Exception($error);
            Log::getInstance()->addException($exception);
            throw $exception;
        } else {
            return $error;
        }
    }

    /**
     * Retorna a instância principal de AttributosDAO
     * @return AttributesDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}