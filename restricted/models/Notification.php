<?php


require_once MODELS . 'Row.php';
require_once MODELS . 'User.php';

/**
 * Modelo para notificações dos usuários
 *
 * @author YouCurte
 */
class Notification extends Row{
    
    private  $id,$userId,$userIdFrom,$text,$alreadyRead,$link,$time,$user;
    
    public function __construct($data = null) {
        parent::__construct($data);
        $this->setFirst("time", DateTimeYC::now());
        $this->setFirst("alreadyRead",false);
    }
    
    /**
     * Retonra o id da notificação
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Retorna o id do usuário a qual pertence a notificação
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }
    
    /**
     * Pega e retorna o usuário a quem pertence a notificação
     * @return User
     */
    public function getUser(){
        if ($this->user===null){
            $this->user = UserDAO::getInstance()->getUser($this->getUserId());
        } 
        return $this->user;
    }

    /**
     * Retorna o texto da notificação
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * Verifica e retorna se a notificação já foi lida
     * @return boolean true = foi lido <br>
     *  false = não foi lido
     */
    public function wasAlreadyRead() {
        return $this->alreadyRead;
    }

    /**
     * Retorna o link a qual deve ser redimencionado ao clicar na notificação
     * @return URL
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * Defineo o id da notificação
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Define o id do usuário a qual a notificação pertence/recebe 
     * @param int $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }
    
    /**
     * Define o texto da notificação
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }

    /**
     * Define se o post já foi lido ou não
     * @param boolean $alreadyRead true = foi lido <br>
     *    false = não foi lido
     */
    public function setAlreadyRead($alreadyRead) {
        $this->alreadyRead = $alreadyRead;
    }

    /**
     * Define o link para qual a notificação deve redimensionar
     * @param string $link
     */
    public function setLink($link) {
        $this->link = new URL($link)."";
    }

    /**
     * Retorna o momento em que foi criado a notificação
     * @return Notification
     */
    public function getTime() {
        return $this->time;
    }
    
    /**
     * Define o momento em que foi criado a notificação
     * @param DateTimeYC|string $time
     */
    public function setTime($time) {
        $this->time = new DateTimeYC($time);
    }

    /**
     * Retorna o id do usuário que realizou ação que originou a notificação
     * @return int
     */
    function getUserIdFrom() {
        return $this->userIdFrom;
    }

    /**
     * Define o id do usuário que realizou a ação que originou a notificação
     * @param int $userIdFrom
     */
    function setUserIdFrom($userIdFrom) {
        if ($userIdFrom instanceof User){
            $userIdFrom = $userIdFrom->getId();
        }
        $this->userIdFrom = $userIdFrom;
    }

    /**
     * Pega e retorna o usuário quem realizou o eventou que criou a notificação
     * @return User
     */
    public function getUserFrom(){
        require_once MODELS."UserDAO.php";
        $user =  UserDAO::getInstance()->getUser($this->getUserIdFrom());
        if ($user==null){
            $userDefault = new User();
            return $userDefault;
        }
        return $user;
    }
    
    /**
     * Lista os campos que devem ser salvos no banco de dados
     * @return type
     */
    public function listGets() {
        $gets = parent::listGets();
        $resultGets = array_remove_value($gets, "userFrom");
        return $resultGets;
    }
    
    
}
