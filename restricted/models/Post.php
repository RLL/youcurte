<?php

require_once MODELS . 'Row.php';
require_once MODELS . 'ImageDAO.php';

/**
 * O modelo de post
 *
 * @author YouCurte
 */
class Post extends Row {

    private $id, $text, $hashtags, $imageId, $time, $local, $tri, $bah, $myTri,
            $myBah, $latitude, $longitude, $image, $userId, $user, $hasImage;

    public function __construct($data = null) {
        parent::__construct($data);
        $this->setFirst(array('tri', 'bah', 'myTri', 'myBah'), 0);
        $this->setFirst('time', new DateTimeYC());
        $this->setFirst("hasImage", false);
        $this->setFirst("image", null);
    }

    /**
     * O id do usuário que criou o post
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Define o usuário (pelo seu id) que criou o post
     * @param int $userId O id do usuário
     */
    public function setUserId($userId) {
        $this->userId = $userId;
        $this->setUser($this->userId);
    }

    /**
     * Define o usuário que criou o post
     * @param User $user
     */
    public function setUser($user) {
        $userError = new User();
        $userError->setName(toPT("Usuário não encontrado."));
        if (isInt($user)) {
            $userError->setId($user);
        }
        $user1 = UserDAO::getInstance()->getUser($user, $userError);
        $this->user = $user1;
        $this->userId = $user1->getId();
    }

    /**
     * Obtém o usuário que criou o post
     * @return type
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Retorna a latitude do local de onde foi feito o post
     * @return float
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Retorna a longitude do local de onde foi feito o post
     * @return float
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * Define a latitude do local de onde foi feito o post
     * @param float $latitude
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    /**
     * Retorna se o post tem uma imagem
     * @return boolean true = tem imagem <br> false = não tem imagem
     */
    function getHasImage() {
        if ($this->image !== null) {
            return $this->image->getId() !== null;
        } else {
            return false;
        }
        return $this->hasImage;
    }

    /**
     * Define se o post possui uma imagem
     * @param boolean $hasImage true = tem imagem <br> false = não tem
     */
    function setHasImage($hasImage) {
        $this->hasImage = $hasImage;
    }

    /**
     * Define a longitude de onde foi tirado o post
     * @param float $longitude
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    /**
     * Retorna o URL do post
     * @return URL
     */
    public function getURL() {
        return new URL("post/" . $this->getId(), URL_ROOT);
    }

    /**
     * Retorna uma array com o seguinte formato array("lat"=>...,"lng"=>...)
     *  
     * @return array Retorna null se não tiver latitude ou longitude
     */
    public function getLocation() {
        if (!is_null($this->getLongitude()) && !is_null($this->getLatitude())) {
            $location = array("lat" => $this->getLatitude(),
                "lng" => $this->getLongitude());
            return $location;
        }
        return null;
    }

    /**
     * Retorna o id do post
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Retorna o texto do post
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * Retorna as hashtags que post possui separadas por espaço (' ')
     * @return string
     */
    public function getHashtags() {
        return $this->hashtags;
    }

    /**
     * Retorna o id da imagem
     * @return int 
     */
    public function getImageId() {
        return $this->imageId;
    }

    /**
     * Obtém e retorna um objeto Image da imagem
     * @return Image
     */
    public function getImage() {
        if ($this->image === null && $this->getHasImage()) {
            $url = new URL(IMAGE_DONT_FOUND);
            $image = new Image();
            $image->setUrl($url);
            return $image;
        }
        return $this->image;
    }

    /**
     * Define a imagem do post
     * @param mixed $image (Image,int,String) Imagem ou id, link e etc
     */
    public function setImage($image) {
        $image1 = ImageDAO::getInstance()->getImage($image);
        $this->image = $image1;
        if ($this->image != null) {
            $this->imageId = $image1->getId();
        }
    }

    /**
     * Retorna o momento em que foi criado o post
     * @return DateTimeYC
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * Retorna a localização/local/endereço de onde foi feito o post
     * @return string
     */
    public function getLocal() {
        return $this->local;
    }

    /**
     * Define o id do post
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Define um texto para post
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
        $this->updateHashtags();
    }

    /**
     * Atualiza as hashtags do post a partir do texto do post;
     */
    public function updateHashtags() {
        $alpha = HASHTAGS_ALPHABET;
        $text = $this->getText();
        $newText = "";
        $size = strlen($text);
        for ($i = 0; $i < $size; $i++) {
            $char = $text[$i];
            if (($key = strpos($alpha,($char)))!==false){
                $newText.=$alpha[$key];
            } else if (($key = strpos($alpha,strtolower($char)))!==false) {
                $newText.=strtoupper($alpha[$key]);
            } else {
                $newText.=" ";
            }
        }
        $tags = explode(" ", " " . $newText . " ");
        $hashtags = array();
        foreach ($tags as $tag) {
            if (isNotEmpty(trim($tag))) {
                $tag1 = $tag;
                while (strstr($tag1, "##")) {
                    $tag1 = str_replace("##", "#", $tag1);
                }
                if ($tag1 != "#"&&strpos($tag1, "#") === 0 && strrpos($tag1, "#") === 0) {
                    $hashtags[$tag1] = trim($tag1);
                }
            }
        }
        $this->hashtags = "";
        foreach ($hashtags as $hash){
            $this->hashtags.=$hash." ";
        }
    }

    /**
     * Define as hashtags do post (as hashtags são separadas por espaço)
     * @param string $hashtags
     */
    public function setHashtags($hashtags) {
        $this->hashtags = $hashtags;
    }

    /**
     * Define o id da imagem que o post possui (entre com null para remover a imagem)
     * @param int $imageId 
     */
    public function setImageId($imageId) {
        $this->imageId = $imageId;
        $this->setImage($imageId);
    }

    /**
     * Define o momento em que o post foi feito
     * @param DateTimeYC|string $time
     */
    public function setTime($time) {
        $this->time = new DateTimeYC($time);
    }

    /**
     * Define o local de onde foi feito o post
     * @param string $local
     */
    public function setLocal($local) {
        $this->local = $local;
    }

    /**
     *  Quantidade de tri que o post tem
     * @return int
     */
    public function getTri() {
        return $this->tri;
    }

    /**
     * Quantidade de xucro que o post recebeu
     */
    public function getBah() {
        return $this->bah;
    }

    /**
     * Define a quantidade de tri que o post recebeu
     * @param int $tri
     */
    public function setTri($tri) {
        $this->tri = $tri;
    }

    /**
     * Define a quantidade de xucro que o post recebeu
     * @param int $bah
     */
    public function setBah($bah) {
        $this->bah = $bah;
    }

    /**
     * Verifica se o usuário logado já deu tri no post
     * @return boolean true = tem o tri <br> false = não tem
     */
    public function hasMyTri() {
        return $this->myTri;
    }

    /**
     * Verifica se o post foi marcado como xucro pelo usuário logado
     * @return type
     */
    public function hasMyBah() {
        return $this->myBah;
    }

    /**
     * Define se o usuário logado deu tri no post
     * @param boolean $myTri true = deu tri <br> false = não deu tri
     */
    public function setMyTri($myTri) {
        $this->myTri = $myTri;
    }

    /**
     * Define se o usuário logado deu xucro no post
     * @param boolean $myBah true = deu xucro <br> false = deu xucro
     */
    public function setMyBah($myBah) {
        $this->myBah = $myBah;
    }

    /**
     * Remove os campos que não devem serem salvos no banco de dados
     * @param array $array Lista com os campos do Post
     * @return array A array resultante da remoção dos nomes dos campos da lista informada
     */
    private static function removeFields($array) {
        $remove = array("tri", "myTri", "bah", "myBah", "image", "user");
        foreach ($remove as $field) {
            $array = array_remove_value($array, $field);
        }
        return $array;
    }

    /**
     * Retorna uma lista com os campos que devem ser salvos no banco de dados
     * @return array
     */
    public function listGets() {
        return self::removeFields(parent::listGets());
    }

    /**
     * Retorna uma lista com os campos que devem ser lidos do banco de dados
     * @return array
     */
    public static function listSets() {
        return self::removeFields(parent::listSets());
    }

}
