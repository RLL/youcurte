<?php

require_once MODELS . 'Notification.php';
require_once MODELS . 'DAO.php';

/**
 * Classe para manipular as notificações no banco de dados
 *
 * @author YouCurte
 */
class NotificationDAO extends DAO {

    public function __construct() {
        parent::__construct("Notification");
    }

    /**
     * Lista as notificações de um usuário
     * @param User|int|String Usuário, Id ou e-mail do usuário
     * @param int $count [opcional]A quantidade máxima de registros para retornar
     * @param int $offset [opcional]A quatidade registros que deseja eliminar
     * @return array Uma array com as notificações
     */
    public function listNotifications($user = null, $count = COUNT_MAX_RETURN, $offset = 0) {
        try {
            $user = UserDAO::getInstance()->getUser($user);
            if ($user === null) {
                throw new UserDontFoundException();
            }
            $userId = $user->getId();
            $users = array();
            $data = DateTimeYC::now()->toDB();
            $sql = "select * from Notification where ((TIMESTAMPDIFF(MINUTE,time,'$data') <=60 and alreadyRead)"
                    . " or (not alreadyRead and TIMESTAMPDIFF(day,time,'$data')<=7)) and userId = $userId order by time limit $offset,$count;";
            $query = $this->prepareStatement($sql);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_BOTH)) {
                $object = $this->newObject($row);
                $users[] = $object;
            }
            return $users;
        } catch (Exception $e) {
            Log::getInstance()->addLine($e);
            return array();
        }
    }

    /**
     * Pega uma notificação específica
     * @param int $id O Id da notificação
     * @param mixed $user [Opcional]Parametro de identificação do usuário
     *      Se não informado pega o usuário logado
     * @return Notification Retorna null se não foi encontrado nada
     */
    public function getNotification($id, $user = null) {
        $resultUser = UserDAO::getInstance()->getUser($user);
        return $this->search(array("userId" => $resultUser->getId(),
                    "id" => $id))->getData(0);
    }

    /**
     * Atualizar uma notificação específica no banco de dados
     * @param Notification $notification A notificação que deseja salvar
     * @return boolean Se deu sucesso na atualização
     *  true = sucesso <br> false = falha
     */
    public function updateNotification(Notification $notification) {
        $this->update($notification, array("userId", "id"));
        return $this->wasLastExecutionSuccessful();
    }

    /**
     * Conta quantas notificações o usuário têm
     * @param mixed $user Qualquer paramêtro para identificar o usuário
     * @return int A quantidade
     */
    public function countNotification($user = null) {
        $data = DateTimeYC::now()->toDB();
        $userId = UserDAO::getInstance()->getUser($user)->getId();
        $sql = "select count(id) as 'count' from Notification where ((TIMESTAMPDIFF(MINUTE,time,'$data') <=60 and alreadyRead)"
                . " or (not alreadyRead and TIMESTAMPDIFF(day,time,'$data')<=7)) and userId = $userId;";
        $query = $this->prepareStatement($sql);
        $query->execute();
        return $query->fetchAll()[0]['count'];
    }

    /**
     * Retorna a instância principal de NotificationDAO
     * @return NotificationDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}
