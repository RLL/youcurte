<?php

require_once MODELS . "DAO.php";
require_once MODELS . 'Image.php';
require_once MODELS . "AlbumDAO.php";

/**
 * Description of ImageDAO
 *
 * @author YouCurte
 */
class ImageDAO extends DAO {

    public function __construct() {
        parent::__construct("Image");
    }

    /**
     * Obtém e retorna os campos que servem como identificação de uma imagem
     * @param Image $image
     * @return array Os campos com seus valores
     */
    public function getIdParam(Image $image) {
        $id = array("id" => $image->getId());
        return $id;
    }

    /**
     * Adiciona/registra uma imagem no banco de dados
     * @param Image $image
     * @return \Image
     */
    public function addImage(Image $image) {
        $image->setUserId(LoginDAO::getInstance()->getUserLoggedIn()->getId());
        if (is_null($image->getAlbumId())) {
            $image->setAlbumId(
                    AlbumDAO::getInstance()->getDefaultAlbum()->getId());
        }
        $image->setId($this->insert($image));
        return $image;
    }

    /**
     * Obtém e retorna uma imagem do banco de dados
     * @param mix $param Id ou URL da imagem
     * @return Image
     */
    public function getImage($param) {
        if ($param instanceof Image) {
            return $param;
        } else if (is_int($param) || is_numeric($param)) {
            return $this->search(array("id" => "$param"))->getData(0);
        } else if (strstr($param, "http")) {
            return $this->search(array("url" => $param))->getData(0);
        }
    }

    /**
     * delete uma image do banco de dados e deleta ela
     * @param Image $image A imagem que deseja deleter
     * @return boolean true = foi delete e deletada com sucesso <br>
     *      false = não foi delete ou deletada com sucesso
     */
    public function deleteImage(Image $image) {
        if (!$image->getUrl()->equals(ALBUM_IMAGE_DEFAULT)) {
            if (unlink($image->getUrl()->toFile())) {
                return $this->delete($this->getIdParam($image));
            }
        } else {
            return $this->delete($this->getIdParam($image));
        }
        return false;
    }

    /**
     * Lista as imagens de álbum e usuário
     * @param int $albumId O id do álbum
     * @param int $userId O id do usuário
     * @param int $count Número máximo de registros a retornar
     * @param int $offset deleter quantos registros da busca
     * @return array Retorn uma lista das imagens (Image) encontradas
     */
    public function listImages($albumId, $userId, $count = COUNT_RETURN_IMAGES, $offset = 0) {
        return $this->search("albumId=:albumId and userId=:userId order by time desc", array("albumId" => $albumId, "userId" => $userId), $count, $offset)->getData();
    }

    /**
     * Deletar imagens com o id do usuários
     * @param array $ids Os IDs das imagens que deseja deleter
     * @param int $userId O id do usuário
     * @return int quantas imagens conseguiu deletar
     */
    public function deleteImages($ids, $userId) {
        $sql = "";
        $array = array();
        $count = 0;
        foreach ($ids as $id) {
            $deleted = false;
            $image = $this->getImage($id);
            if ($image !== null && $image->getUserId() === $userId) {
                $file = $image->getUrl()->toFile();
                $deleted = !$file->exists() || $file->deleteFile();
            }
            if ($deleted && isNotEmpty($sql)) {
                $sql.=" or ";
            }
            if ($deleted) {
                $sql.="id=:id$id";
                $array["id$id"] = $id;
                $count++;
            }
        }
        $sql2 = "($sql) and userId=:userId";
        $array["userId"] = $userId;
        if (isEmpty($sql)) {
            return 0;
        }
        return $this->delete($sql2, $array);
    }

    /**
     * Retorna as imagens vizinha de outra imagem (a imagem que vem depois ou antes de $imageId no album)
     * @param int $imageId O id da imagem
     * @param int $albumId O album da imagem
     * @param int $userId O id do usuário
     * @return array 
     *      $array[0] = objeto da imagem que vem antes (Image)
     *      $array[1] = objeto da imagem que vem depois (Image)
     */
    public function getImageNeighbors($imageId, $albumId, $userId) {
        if (!isInt($imageId) || !isInt($albumId) || !isInt($userId)) {
            return array();
        }
        $pdo = $this->getPDO();
        $pdo->beginTransaction();
        $pdo->exec("set @num = 0;");
        $sujo = $pdo->query("select @id:=num from (
	select @num := @num+1 as 'num', img.* from Image as img where userId=$userId"
                . " and albumId=$albumId) as x where id=$imageId;");
        $sujo->fetchAll();
        $sujo2 = $pdo->query("select @min:=min(id),@max:=max(id) from Image"
                . " where userId=$userId and albumId=$albumId;");
        $sujo2->fetchAll();
        $pdo->exec("set @num = 0;");
        $list = $pdo->query("select * from (
	select @num := @num+1 as 'num', img.* from Image as img where userId=$userId and albumId=$albumId
) as x where num=@id+1 or num=@id-1 or id=@min or id=@max order by id;");
        $result = $this->listObjects($list);
        $pdo->commit();
        $resultValid = array();
        $more = array();
        $less = array();
        $equal = null;
        foreach ($result as $image) {
            if ($image->getId() < $imageId) {
                $less[] = $image;
            } else if ($image->getId() === $imageId) {
                $equal = $image;
            } else {
                $more[] = $image;
            }
        }
        if (count($less) > 0 || count($more)) {
            if (count($less) > 0) {
                $resultValid[0] = count($less) == 2 ? $less[1] : $less[0];
            } else {
                $resultValid[0] = count($more) > 1 ? $more[1] : $more[0];
            }
            if (count($more) > 0) {
                $resultValid[1] = $more[0];
            } else {
                $resultValid[1] = $less[0];
            }
        }
        if (count($resultValid) == 0) {
            $resultValid[0] = $equal;
        }
        if (count($resultValid) === 1) {
            if (isset($resultValid[0])) {
                $resultValid[1] = $resultValid[0];
            } else if (isset($resultValid[1])) {
                $resultValid[0] = $resultValid[1];
            } else {
                $resultValid[0] = $this->getImage($imageId);
                $resultValid[1] = $resultValid[0];
            }
        }
        return $resultValid;
    }

    /**
     * Move imagens para um álbum específico
     * @param array $ids Array com os IDs de cada imagem (Image)
     * @param int $albumId O id do álbum para qual deseja morever
     * @param int $userId O id do usuário do álbum e imagem
     * @return int Quantidade imagens movidas com sucesso
     */
    public function moveImageToAlbum($ids, $albumId, $userId) {
        if (!isInt($albumId) || !isInt($userId)) {
            return;
        }
        $sql = "update Image set albumId = $albumId where userId=$userId and ";
        $where = "";
        foreach ($ids as $id) {
            if (!isInt($id)) {
                return;
            }
            if (isNotEmpty($where)) {
                $where.=" or ";
            }
            $where.="id=$id";
        }
        return $this->getPDO()->exec($sql . $where);
    }

    /**
     * Verifica se os dados de uma imagem são válidos para salvar no banco
     * @param Image $image
     * @param boolean $throw <br/> true = lançar exceções
     * 
     */
    public function validateImage(Image $image, $throw = false) {
        $file = $image->getUrl()->toFile();
        $ext = $file->getFileExtension();
        $exts = array(".png", ".bmp", ".jpg");
        $error = "";
        if (!$file->exists()) {
            $error = "A imagem não foi encontrado no diretório do usuário!";
        }
        $exist = false;
        foreach ($exts as $e) {
            if (strtolower($ext) === $e) {
                $exist = true;
            }
        }
        if (!$exist) {
            $error = "O formato da imae não é válido. É apenas acesso os seguintes formatos: .jpg, .bmp e "
                    . ".png";
        }
        if ($throw && isNotEmpty($error)) {
            throw new RuntimeException($error);
        }
        return $error;
    }

    /**
     * Retorna a instância principal de ImageDAO
     * @return ImageDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}
