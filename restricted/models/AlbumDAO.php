<?php

require_once MODELS . "DAO.php";
require_once MODELS . "Album.php";

/**
 * Classe para manipular os álbuns no banco de dados
 *
 * @author YouCurte
 */
class AlbumDAO extends DAO {

    public function __construct() {
        parent::__construct("Album");
    }

    /**
     * Adiciona um álbum ao banco de dados
     * @param Album $album Obs: atualiza seu id
     * @return \Album Retorna a si mesmo com seu id atualizado (não precisa pegar o return)
     */
    public function addAlbum(Album $album) {
        $id = $album->getId();
        if (isEmpty($album->getImage())) {
            $album->setImage(ALBUM_IMAGE_DEFAULT);
        }
        $album->setId($this->insert($album));
        if (!is_null($id) && $id !== $album->getId()) {
            $query = $this->prepareStatement("update Album set id=" . ALBUM_ID_DEFAULT .
                    " where id=" . $album->getId() . ";");
            $query->execute();
            $album->setId($id);
        }
        return $album;
    }

    /**
     * Pega e retorna álbum default do usuário. Se não existir cria.
     * @param mixed $user [opcional] Qualquer paramêtro que possa identificar o usuário.<br>
     *  Obs: Se não informado, pega do usuário logado;
     * @return Album
     */
    public function getDefaultAlbum($user = null) {
        if (is_null($user)||!isInt($user)) {
            $user = $userId = LoginDAO::getInstance()->getUserLoggedIn()->getId();
        }
        $album = $this->getAlbum(ALBUM_ID_DEFAULT, $user);
        if ($album == null) {
            $album = new Album();
            $album->setId(ALBUM_ID_DEFAULT);
            $album->setName(ALBUM_NAME_DEFAULT);
            $album->setUserId($user);
            $this->addAlbum($album);
        }
        return $album;
    }

    /**
     * Pega e retorna um album
     * @param mixed $param Id ou nome do álbum
     * @param int|User $userId O objeto de User ou id do usuário
     * @return Album Retorna null se não encontrado o álbum.
     *  Obs: tem preferência pelo id do álbum
     * 
     */
    public function getAlbum($param, $userId = null) {
        if (is_null($userId)||!isInt($userId)) {
            $user = UserDAO::getInstance()->getUser($userId);
            if ($user===null){
                return null;
            }
            $userId = $user->getId();
        } 
        if (!isInt($param)) {
            $result = $this->search("userId=:userId and "
                    . "name like :param", array("userId" => $userId, "param" => $param));
        } else {
            $result = $this->search(array("userId"=>$userId,"id"=>$param));
        }
        if ($result->getCount() == 0) {
            return null;
        }
        return $result->getData()[0];
    }

    /**
     * Retorna os paramêtros de identificação de um Album
     * @param Album $album
     * @return array Um array com os paramêtros e seus valores
     */
    protected function getIdParam(Album $album) {
        $id = array("id" => $album->getId(), "userId" => $album->getUserId());
        return $id;
    }

    /**
     * Deleta um álbum e suas imagens/fotos
     * @param Album $album O álbum que quer deletar
     * @return boolean Se conseguiu deletar o álbum
     *      true = deletou <br> false = não deletou
     */
    public function deleteAlbum(Album $album) {
        $where = array("userId" => $album->getUserId(), "albumId" => $album->getId());
        $result = ImageDAO::getInstance()->search($where);
        foreach ($result->getData() as $image) {
            $image->getUrl()->toFile()->deleteFile();
        }
        ImageDAO::getInstance()->delete($where);
        return $this->delete($this->getIdParam($album)) > 0;
    }

    /**
     * Lista todos os albuns de um usuário
     * @param int|User $user
     * @return array Um array de Album's
     */
    public function listAlbums($user) {
        if ($user instanceof User) {
            $userId = $user->getId();
        } else {
            $userId = $user;
        }
        $this->getDefaultAlbum($userId);
        return $this->search(array("userId" => $userId))->getData();
    }

    /**
     * Atualiza as informações de um álbum no banco de dados
     * @param Album $album
     */
    public function updateAlbum(Album $album) {
        $this->update($album, array("userId", "id"));
    }

    /**
     * Retorna a instância principal de AlbumDAO
     * @return AlbumDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}

/**
 * Exceção para álbum não encontrado
 */
class AlbumDontFoundException extends Exception {

    /**
     * Método construtor da exceção do álbum não encontrado
     * @param string $albumName o nome do álbum
     * @param type $code
     * @param type $previous
     */
    public function __construct($albumName, $code = null, $previous = null) {
        parent::__construct("O álbum '$albumName' não encontrado.", $code, $previous);
    }

}
