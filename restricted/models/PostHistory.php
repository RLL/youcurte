<?php

/**
 * Modelo que guarda o histórico de um post (quem viu o que)
 *
 * @author YouCurte
 */
class PostHistory extends Row {
    
    private $userId,$postId,$time;
    
    public function __construct($data = null) {
        parent::__construct($data);
        $this->setFirst("time", DateTimeYC::now());
    }
    
    /**
     * Retorna o id do usuário de quem viu o post
     * @return int
     */
    function getUserId() {
        return $this->userId;
    }

    /**
     * Retorna o id do post que foi visto
     * @return int
     */
    function getPostId() {
        return $this->postId;
    }

    /**
     * O momento em q viu o post
     * @return DateTimeYC
     */
    function getTime() {
        return $this->time;
    }

    /**
     * Define o id do usuário que viu o post
     * @param int $userId
     */
    function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * Define o id do post que foi visto pelo usuário
     * @param int $postId
     */
    function setPostId($postId) {
        $this->postId = $postId;
    }

    /**
     * Define o momento em que o post foi visto por tal usuário
     * @param string|DateTimeYC $time
     */
    function setTime($time) {
        $this->time = new DateTimeYC($time);
    }


    
}