<?php

/**
 * Modelo de depoimento
 *
 * @author YouCurte
 */
class Deposition extends Row {
    
    private $userIdTo,$userIdFrom,$text,$time,$id;
    
    public function __construct($data = null) {
        parent::__construct($data);
        $this->setFirst("text", "");
        $this->setFirst("time", DateTimeYC::now());
    }
    
    /**
     * Retorna o id do depoimento
     * @return int
     */
    function getId() {
        return $this->id;
    }
    
    /**
     * Define o id do usuário
     * @param int $id
     */
    function setId($id) {
        $this->id = $id;
    }

        
    /**
     * Retorna o id do usuário que recebeu o depoimento
     * @return int
     */
    function getUserIdTo() {
        return $this->userIdTo;
    }

    /**
     * Retorna o id de quem criou o depoimento
     * @return int
     */
    function getUserIdFrom() {
        return $this->userIdFrom;
    }

    /**
     * Retorna o texto do depoimento
     * @return string
     */
    function getText() {
        return $this->text;
    }

    /**
     * Retorna o momento em que foi criado o depoimento
     * @return DateTimeYC
     */
    function getTime() {
        return $this->time;
    }

    /**
     * Define o id do usuário que recebeu o depoimento
     * @param int $userIdTo
     */
    function setUserIdTo($userIdTo) {
        $this->userIdTo = $userIdTo;
    }

    /**
     * Define o id do usuário que fez o depoimento
     * @param int $userIdFrom
     */
    function setUserIdFrom($userIdFrom) {
        $this->userIdFrom = $userIdFrom;
    }

    /**
     * Define o texto do depoimento
     * @param string $text
     */
    function setText($text) {
        $this->text = $text;
    }

    /**
     * Define o momento em que o depoimento foi criado
     * @param DateTimeYC|string $time
     */
    function setTime($time) {
        $this->time = new DateTimeYC($time);
    }


            
            
    
}