<?php

/**
 * Modelo de opinião(tri,xucro) para cada Post e Usuário 
 *
 * @author YouCurte
 */
class Opinion extends Row{
    
    private $userId,$postId,$tri,$bah;
    
    public function __construct($data = null) {
        parent::__construct($data);
        $this->setFirst(array("tri","bah"), 0);
    }
    
    /**
     * O id do usuário que a opinou sobre o post
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * O id do post que foi opinado pelo usuário
     * @return int
     */
    public function getPostId() {
        return $this->postId;
    }

    /**
     * Retorna se o usuário em questão deu tri para o Post
     * @return int 0 = não deu tri <br> 1 = deu tri
     */
    public function getTri() {
        return $this->tri;
    }

    /**
     * Retorna se o usuário em questão deu xucro para o Post
     * @return int 0 = não deu xucro <br> 1 = deu xucro
     */
    public function getBah() {
        return $this->bah;
    }

    /**
     * Define o id do usuário que deu sua opinião no post
     * @param int $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * Define o post que recebeu a opinião
     * @param int $postId
     */
    public function setPostId($postId) {
        $this->postId = $postId;
    }

    /**
     * Define se o usuário deu tri no post
     * @param int|boolean $tri
     *      true = 1 = deu tri <br>
     *      false = 0 = não deu tri <Br>
     */
    public function setTri($tri) {
        $this->tri = $tri;
    }

    /**
     * Define se o usuário deu xucro no post
     *      true = 1 = deu xucro
     *      false = 0 = não deu xucro
     * @param int|boolean $bah
     */
    public function setBah($bah) {
        $this->bah = $bah;
    }

     
    /**
     * Verifica e retorna se o usuário deu tri no post
     * @return boolean
     *      true = deu tri <br>
     *      false = não deu tri
     */
    public function hasTri(){
        return $this->getTri()==1;
    }
    
    /**
     * Verifica e retorna se o usuário deu xucro no post
     * @return boolean
     *      true = deu xucro <br>
     *      false = não deu xucro <br>
     */
    public function hasBah(){
        return $this->getBah()==1;
    }

    
}