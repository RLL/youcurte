<?php

require_once MODELS . 'Row.php';
require_once CLASSES . 'DateTimeYC.php';

/**
 * Classe que representa os usuários
 *
 * @author YouCurte
 */
class User extends Row {

    private $id, $name, $lastName, $email, $password, $telephone, $city, $state,
            $profession, $academicTraining, $image, $birthDate,
            $registryDate, $confirmationTime, $confirmationCode, $sex,
            $description, $cep, $interest, $cool, $reliable, $sexy, $following, $beingFollow,
            $followingLoaded, $beingFollowLoaded,$active,$toSendEmails;

    public function __construct($data = null) {
        parent::__construct($data);
        $this->setRegistryDate(DateTimeYC::now());
        $this->setFirst(array("sexy", "cool", "reliable"), 0);
        $this->setFirst(array("following", "beingFollow"), false);
        $this->setFirst("active",false);
        $this->setFirst("toSendEmails",true);
    }

    /**
     * @return string Rótulo para o usuário
     *  100% Maneiro, Bagual e tal
     */
    public function getLabel() {
        $array = array("Legal" => $this->getCool(),
            "Confiável" => $this->getReliable(),
            "Sexy" => $this->getSexy());
        if ($this->getBagual() >= 80) {
            return round($this->getBagual()) . "% Bagual";
        }
        if ($this->getSexy() == $this->getCool() &&
                $this->getReliable() == $this->getCool() &&
                $this->getCool() == 0) {
            return "100% Zé ninguém";
        }
        if ($this->getCool()>$this->getSexy()
                &&$this->getReliable()>$this->getSexy()
                &&($this->getReliable()+$this->getCool())>40){
            return round(($this->getReliable()+$this->getCool())/2)."% Maneiro";
        }
        $kb = array_keys($array)[0];
        $b = $array[$kb];
        foreach ($array as $ka => $a) {
            if ($ka != $kb && $a > $b) {
                $kb = $ka;
                $b = $a;
            }
        }
        if ($b>0){
            return round($b)."% ".$kb;
        }
        return "100% Zé ninguém";
    }
    
    /**
     * Retorna o quão legal é
     * @return int
     */
    public function getCool() {
        return round($this->cool);
    }

    /**
     * Retorna o quão confiável é
     * @return int
     */
    public function getReliable() {
        return round($this->reliable);
    }

    /**
     * Retorna o quão sexy o usuário é
     * @return int
     */
    public function getSexy() {
        return round($this->sexy);
    }

    /**
     * Junta o primeiro nome e sobrenome e o retorna
     * @return String 
     */
    public function getFullName() {
        return $this->getName() . " " . $this->getLastName();
    }

    /**
     * Define o quão legal é o usuário
     * @param int $cool
     */
    public function setCool($cool) {
        $this->cool = $cool;
    }

    /**
     * Define o quão confiável é o usuário
     * @param int $reliable
     */
    public function setReliable($reliable) {
        $this->reliable = $reliable;
    }

    /**
     * Define o quão sexy é o usuário
     * @param int int
     */
    public function setSexy($sexy) {
        $this->sexy = $sexy;
    }

    /**
     * Calcula e retorna o quão bagual é o usuário
     * @return int
     */
    private function getBagual() {
        return round(($this->getCool() + $this->getReliable() + $this->getSexy()) / 3);
    }
    

    /**
     * Retorna o CEP do usuário
     * @return string
     */
    public function getCep() {
        return $this->cep;
    }

    /**
     * Retorna o interesse do usuário
     * @return string
     */
    public function getInterest() {
        return $this->interest;
    }

    /**
     * Define o CEP do usuário
     * @param string $cep
     */
    public function setCep($cep) {
        $this->cep = $cep;
    }

    /**
     * Define o interesse do usuário
     * @param string $interest
     */
    public function setInterest($interest) {
        $this->interest = $interest;
    }

    /**
     * Retorna o código de confirmação de cadastro da conta
     * @return int
     */
    public function getConfirmationCode() {
        return $this->confirmationCode;
    }

    /**
     * Define o código de confirmação de cadastro da conta
     * @param int $confirmationCode O código de confirmação
     */
    public function setConfirmationCode($confirmationCode) {
        $this->confirmationCode = $confirmationCode;
    }

    /**
     * Retorna o momento em que foi cadastrado o usuário
     * @return DateTimeYC Retorna null se tiver ativo a conta
     */
    public function getConfirmationTime() {
        return $this->confirmationTime;
    }

    /**
     * Define o momento em que foi cadastrado o usuário
     * @param String|DateTimeYC $confirmationTime
     */
    public function setConfirmationTime($confirmationTime) {
        $this->confirmationTime = new DateTimeYC($confirmationTime);
    }

    /**
     * Retorna a descrição feita de si mesmo
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * Define a descrição do usuário
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /*
     * Métodos gets e sets
     * 
     */

    /**
     * Calcula a idade a retorna
     * @return int
     */
    public function getAge() {
        $interval = $this->getBirthDate()->diff(new DateTimeYC());
        $age = $interval->format("%y");
        return $age;
    }

    /**
     * @return DateTimeYC Retorna a data em que foi cadastrada a conta
     */
    public function getRegistryDate() {
        return $this->registryDate;
    }

    /**
     * 
     * @return int Id do usuário
     */
    function getId() {
        return $this->id;
    }

    /**
     * 
     * @return String O nome do usuário
     */
    function getName() {
        return $this->name;
    }

    /**
     * 
     * @return String Sobrenome do usuário
     */
    function getLastName() {
        return $this->lastName;
    }

    /**
     * 
     * @return String E-mail do usuário
     */
    function getEmail() {
        return $this->email;
    }

    /**
     * Retorna a senha do usuário
     * @return string
     */
    function getPassword() {
        return $this->password;
    }

    /**
     * Retorna o telefone do usuário
     * @return string
     */
    function getTelephone() {
        return $this->telephone;
    }

    /**
     * Retorna a cidade do usuário
     * @return string
     */
    function getCity() {
        return $this->city;
    }

    /**
     * Retorna o estado/federação do usuário
     * @return string
     */
    function getState() {
        return $this->state;
    }
    
    /**
     * Retorna a profissão do usuário
     * @return string
     */
    function getProfession() {
        return $this->profession;
    }

    /**
     * Retorna a formação academica
     * @return string
     */
    function getAcademicTraining() {
        return $this->academicTraining;
    }

    /**
     * Retorna o avatar/imagem de perfil
     * @return URL
     */
    function getImage() {
        $url = new URL($this->image,URL_ROOT);
        if (isEmpty($this->image)||!$url->toFile()->exists()) {
            $this->image = new URL(USER_IMAGE_DEFAULT);
        }
        return $this->image;
    }

    /**
     * Retorna a data de nascimento do usuário
     * @return DateTimeYC
     */
    function getBirthDate() {
        return $this->birthDate;
    }

    /**
     * Define o id do usuário
     * @param int $id
     */
    function setId($id) {
        $this->id = $id;
    }

    /**
     * Define o primeiro nome do usuário
     * @param string $name
     */
    function setName($name) {
        $this->name = $name;
    }

    /**
     * Define sobrenome do usuário
     * @param string $lastName
     */
    function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    /**
     * Define o e-mail do usuário
     * @param string $email
     */
    function setEmail($email) {
        $this->email = $email;
    }

    /**
     * Define a senha do usuário
     * @param string $password
     */
    function setPassword($password) {
        $this->password = $password;
    }

    /**
     * Define o telefone do usuário
     * @param string $telephone
     */
    function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    /**
     * Define a cidade do usuário
     * @param string $city
     */
    function setCity($city) {
        $this->city = $city;
    }

    /**
     * Define o estado/federação do usuário
     * @param string $state
     */
    function setState($state) {
        $this->state = $state;
    }

    /**
     * Define a profissão do usuário
     * @param string $profession
     */
    function setProfession($profession) {
        $this->profession = $profession;
    }

    /**
     * Define a formação acadêmica do usuário
     * @param string $academicTraining
     */
    function setAcademicTraining($academicTraining) {
        $this->academicTraining = $academicTraining;
    }

    /**
     * Define a imagem/avatar do usuário
     * @param URL $image
     */
    function setImage($image) {
        if (is_string($image)) {
            $this->image = new URL($image);
        } else if ($image instanceof URL) {
            $this->image = $image;
        } else if ($image instanceof Image){
            $this->image = $image->getURL();
        } else {
            return new URL(USER_IMAGE_DEFAULT);
        }
    }

    /**
     * Define a data de nascimetno
     * @param string|DateTimeYC $dateBirth
     */
    function setBirthDate($dateBirth) {
        $this->birthDate = new DateTimeYC($dateBirth);
    }

    /**
     * Define a data de quando a conta foi registrada
     * @param DateTimeYC|string $dateRegistry
     */
    public function setRegistryDate($dateRegistry) {
        $this->registryDate = new DateTimeYC($dateRegistry);
    }

    /**
     * Lista os campos necessários para salvar no banco de dados
     * @param array $more Campos que deseja adicionar  a mais
     * @return array Um array de strings com os nomes dos campos necessários
     */
    static function listFieldsNeed($more = null) {
        $fields = array('name', 'lastName', 'email', 'password', 'birthDate','sex');
        if (is_null($more)) {
            $more = array();
        }
        return array_merge($fields, $more);
    }

    /**
     * Define o gênero/sexo do usuário
     * @param String $value Valores: USER_SEX_FEMALE ou USER_SEX_MALE
     *         Os valores estão dentro do array $USER_SEX_ARRAY
     */
    public function setSex($value) {
        $this->sex = $value;
    }

    /**
     * Retorna o gênero/sexo do usuário
     * @return String Valores: USER_SEX_FEMALE ou USER_SEX_MALE
     *         Os valores estão dentro do array $USER_SEX_ARRAY
     */
    public function getSex() {
        return $this->sex;
    }
    

    /**
     * Retorna se o usuário é uma mulher
     * @return boolean true = mulher
     */
    public function isFemale() {
        return $this->getSex() == USER_SEX_FEMALE;
    }

    /**
     * Retorna se o usuário é homem
     * @return boolean true = homem
     */
    public function isMale() {
        return $this->getSex() == USER_SEX_MALE;
    }

    /**
     * Verifica se está seguindo o usuário logado
     * @return boolean true = seguindo <br> false = não seguindo
     */
    public function isFollowing() {
        if ($this->followingLoaded !== true) {
            $logged = LoginDAO::getInstance()->getUserLoggedIn();
            if ($logged === null) {
                return false;
            }
            $follow = FollowDAO::getInstance()->search(array("userId" => $this->getId(),
                "userIdFollow" => $logged->getId()));
            $this->setFollowing($follow->getCount() === 1);
        }
        return $this->following;
    }

    /**
     * Verifica se está sendo seguido pelo usúario logado
     * @return boolean true = sendo seguido <br> false = não sendo seguido
     */
    public function isBeingFollow() {
        if ($this->beingFollowLoaded !== true) {
            $logged = LoginDAO::getInstance()->getUserLoggedIn();
            if ($logged === null) {
                return false;
            }
            $follow = FollowDAO::getInstance()->search(array("userIdFollow" => $this->getId(),
                "userId" => $logged->getId()));
            $this->setBeingFollow($follow->getCount() === 1);
        }
        return $this->beingFollow;
    }

    /**
     * Define se está seguindo o usuário logado
     * @param boolean $following true = seguindo <br> false = não seguindo
     */
    function setFollowing($following) {
        $this->followingLoaded = true;
        $this->following = $following;
    }

    /**
     * Verifica se está sendo seguido pelo usúario logado
     * @param boolean $beingFollow true = sendo seguido <br>
     *   false = não sendo seguido
     */
    function setBeingFollow($beingFollow) {
        $this->beginFollowLoaded = true;
        $this->beingFollow = $beingFollow;
    }

    /**
     * Lista os campos get/is/was/has que são usados para salvar no banco de dados
     * @return array Um array com a lista dos campos 
     */
    public function listGets() {
        $gets1 = parent::listGets();
        $gets2 = array_remove_value($gets1, "following");
        $gets3 = array_remove_value($gets2, "beingFollow");
        $gets4 = array_remove_value($gets3, "cool");
        $gets5 = array_remove_value($gets4, "reliable");
        $resultGets = array_remove_value($gets5, "sexy");
        return $resultGets;
    }
    
    /**
     * Verifica se a conta está ativa
     * @return boolean true = ativa
     */
    function isActive() {
        return $this->active;
    }

    /**
     * Define se a conta está ativa
     * @param boolean $active true = ativa
     */
    function setActive($active) {
        $this->active = $active;
    }

    /**
     * Se é para enviar e-mails de notificações
     * @return boolean true = enviar <br> false = não enviar
     */
    function istoSendEmails() {
        return $this->toSendEmails;
    }

    /**
     * Definir se é para enviar e-mail de notificações
     * @param boolean $toSendEmails true = enviar <br> false = não enviar
     */
    function settoSendEmails($toSendEmails) {
        $this->toSendEmails = $toSendEmails;
    }



}