<?php

require_once MODELS . 'DAO.php';
require_once MODELS . 'UserDAO.php';
require_once MODELS . 'Login.php';
require_once CONTROLLERS . 'EmailController.php';

/**
 * Manipula as informações de login no banco
 *
 * @author YouCurte
 */
class LoginDAO extends DAO {

    private $userLoggedIn;

    public function __construct() {
        parent::__construct('Login');
        $this->userLoggedIn = null;
    }

    /**
     * Retorna o usuário logado;
     * 
     * @return User
     */
    public function getUserLoggedIn() {
        return $this->userLoggedIn;
    }

    /**
     * Define o usuário logado
     * @param User $user
     */
    public function setUserLoggedIn(User $user) {
        $this->userLoggedIn = $user;
    }

    /**
     * Verifica e retorna se tem alguém logado (na sessão)
     * @return boolean true = tem alguém logado <br>
     *      false = não tá logado
     */
    public function isAnyoneLoggedIn() {
        return $this->userLoggedIn != null;
    }

    /**
     * Retorna a instância principal
     * @return LoginDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

    /**
     * Pega e retorna um login
     * @param string $auth A chave de autorização de login
     * @return Login (Retorna null se não encontrado)
     */
    public function getLogin($auth) {
        $sr = $this->searchLogin(array('auth' => $auth), null, 1);
        if ($sr->getCount() < 1) {
            return null;
        } else {
            $login = $sr->getData()[0];
            if ($login->isExpired()) {
                $this->deleteLogin($login);
                return null;
            }
            return $login;
        }
    }

    /**
     * Retorna o usuário apartir do auth de um login
     * @param String $auth
     * @return User 
     *      null = Falha
     */
    public function getUser($auth) {
        $login = $this->getLogin($auth);
        if ($login != null) {
            return UserDAO::getInstance()->getUser($login->getUserId());
        }
        return null;
    }

    /**
     * Pesquisa por um login (funciona da mesma form aque search)
     * @param mixed $where
     * @param mixed $values
     * @param int $count [opcional]
     * @param int $offset [opcional]
     * @return SearchResult
     */
    public function searchLogin($where, $values, $count = COUNT_MAX_RETURN, $offset = 0) {
        return parent::search($where, $values, $count, $offset);
    }

    /**
     * Deleta todos os logins expirados 
     */
    public function deleteExpiredLogins() {
        $now = DateTimeYC::now();
        $this->delete("expirationTime < :now0", array("now0" => $this->getValue($now)));
    }

    /**
     * Adiciona um login no banco de dados
     * @param Login $login Login que deseja registra no banco
     * @param boolean $forceGenerateKey Gerar um código-chave até conseguir 
     *  registrar no banco (evita LoginAlreadyRegisteredException)
     * @return \Login
     * @throws LoginAlreadyRegisteredException
     */
    public function addLogin(Login $login, $forceGenerateKey = false) {
        LoginDAO::getInstance()->deleteExpiredLogins($login->getUser());
        $user = UserDAO::getInstance()->getUser($login->getUserId());
        if ($user === null) {
            throw new UserDontFoundException();
        }
        if ($user->getConfirmationTime() == null) {
            EmailController::getInstance()->sendConfirmationEmail($user);
        }
        do {
            if (!$this->getLogin($login->getAuth())) {
                parent::insert($login);
                return $login;
            }
            if ($forceGenerateKey) {
                $login->generateAuth();
            }
        } while ($forceGenerateKey);
        throw new LoginAlreadyRegisteredException();
    }

    /**
     * Cria um login e registra ele no banco.
     * 
     * @param mix $user (int ou User) Usuário a qual esse registro se refere
     * @param boolean $native Se foi solicitado p/ aplicação nativa (web ou app)
     * @param String $userAgent Navegador ou app que o solicitou
     * @param boolean $keepLogged Manter logado aumenta o intervalo de tempo
     *  que o registro vai expirar
     *      true - usa LOGIN_INTERVAL_MAX
     *      false - usa LOGIN_INTERVAL_MIN
     *          (se necessário olhe em config.php)
     * @return Login Login criado.
     */
    public function createLogin($user, $native, $userAgent, $keepLogged) {
        if (!($user instanceof User)) {
            $user = UserDAO::getInstance()->searchUserById($user);
        }
        $login = new Login($keepLogged);
        $login->setUser($user);
        $login->setNative($native);
        $login->setUserAgent($userAgent);
        $login->generateauth();
        return $this->addLogin($login);
    }

    /**
     *  Deleta um login no banco
     * 
     * @param mix $param (Login,auth)  
     * @return int Número de linhas afetadas
     */
    public function deleteLogin($param) {
        if ($param instanceof Login) {
            return parent::delete(array('auth'), $param);
        } else {
            return parent::delete(array('auth' => $param));
        }
    }

    /**
     * Faz logout de todos os registros
     * @param mix $user (int/User)
     */
    public function deleteAllLogins($user) {
        if ($user instanceof User) {
            $user = $user->getId();
        }
        return parent::delete(array("userId" => $user));
    }

}

/**
 * Exceção o login já estão registrado
 */
class LoginAlreadyRegisteredException extends Exception {

    /**
     * Método construtor da exceção do login já estar registrado
     * 
     * @param string $msg [opcional]
     * @param type $code [opcional] 
     * @param type $previous [opcional]
     */
    public function __construct($msg = null, $code = null, $previous = null) {
        parent::__construct("Tentando registrar um login já registrado. ".$msg,
                $code, $previous);
    }

}
