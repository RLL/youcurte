<?php

require_once MODELS . "DAO.php";
require_once MODELS . "Follow.php";

/**
 * Classe para gerenciar os registros de seguidores no banco de dados
 *
 * @author YouCurte
 */
class FollowDAO extends DAO {

    public function __construct() {
        parent::__construct("Follow");
    }

    /**
     * Retorna um objeto de follow com os dados pego do input enviado por post
     *  ou get
     * @return Follow
     */
    public function getFollowFromInput() {
        return $this->getObjectFromInput();
    }

    /**
     * Registra um usuário seguindo outro no banco de dados
     * 
     * @param Follow $follow
     * @return boolean Se o registrou foi concluído.
     *          true = concluido
     *          false = ocorreu algum problema no caminho
     * @throws AlreadyFollowException
     * 
     */
    public function addFollow(Follow $follow) {
        $response = $this->search('userId=:userId and userIdFollow=:userIdFollow', $follow);
        if ($response->getCount() > 0) {
            throw new AlreadyFollowException();
        } else {
            $userLogged = UserDAO::getInstance()->getUser($follow->getUserId());
            $userFollow = UserDAO::getInstance()->getUser($follow->getUserIdFollow());
            $notification = new Notification();
            $notification->setText($userLogged->getName() . ' começou a seguir voc&ecirc;!');
            $notification->setUserId($userFollow->getId());
            $notification->setUserIdFrom($userLogged);
            $notification->setLink(new URL("profile/" . $userLogged->getId(), URL_ROOT) . "");
            NotificationDAO::getInstance()->insert($notification);
            EmailController::getInstance()->sendNotificationOfNewFollowing(
                    $userLogged, $userFollow);
            return $this->insert($follow);
        }
    }

    /**
     * delete do banco de dados um registro de alguém seguindo alguém
     * @param Follow $follow O registro
     * @return boolean true = sucesso em deleter<br> false = falha
     */
    public function deleteFollow(Follow $follow) {
        return $this->delete('userId=:userId and userIdFollow=:userIdFollow', $follow) > 0;
    }

    /**
     * Lista quem segue o usuário em questão
     * @param mixed $user Qualquer parametro que identifique usuário em questão
     * @return array
     */
    public function listFollowers($user = null) {
        $user1 = UserDAO::getInstance()->getUser($user);
        return $this->search(array("userIdFollow" => $user1->getId()))->getData();
    }

    /**
     * Lista quem o usuário segue
     * @param int|User $user O usuário ou id. Se for null pega o usuário logado.
     * @return array
     */
    public function listWhoFollows($user = null) {
        $user1 = UserDAO::getInstance()->getUser($user);
        return $this->search(array("userId" => $user1->getId()));
    }

    /**
     * Retorna a instância principal de FollowDAO
     * @return FollowDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

    /**
     * Verifica se existe tal registro
     * @param Follow $follow o registro
     * @return boolean true = existe <br> false = não existe
     */
    public function existsFollow(Follow $follow) {
        $result = $this->search(array("userId", "userIdFollow"), $follow);
        return $result->getCount() == 1;
    }

    /**
     * Lista os usuários relacionados (Seguindo e sendo seguido) com o usuário específico
     * @param mixed $user Qualquer parâmetro que identifique o usuário específico
     * @return array Uma lista de User's
     */
    public function listRelatedUsers($user, $count = COUNT_MAX_RETURN, $offset = 0) {
        $user1 = UserDAO::getInstance()->getUser($user);
        $users = array();
        $id = $user1->getId();
        if (!isInt($offset) || !isInt($count)) {
            return array();
        }
        $query = $this->getPDO()->query("select id from ("
                . "select userId as 'id' from Follow where userIdFollow = $id"
                . " union select userIdFollow as 'id' from Follow"
                . " where userId = $id) as x limit $offset,$count;");
        $query->execute();
        $ids = $query->fetchAll();
        foreach ($ids as $obj) {
            $id = $obj['id'];
            $us = UserDAO::getInstance()->getUser($id);
            $users[$id] = $us;
        }
        return $users;
    }

    /**
     * Conta quantos usuários relacionados tem o $user
     * @param User|int $user
     * @return int A quantidade
     */
    public function countRelatedUsers($user = null) {
        $id = UserDAO::getInstance()->getUser($user)->getId();
        $query = $this->getPDO()->query("select count(id) as 'count' from ("
                . "select userId as 'id' from Follow where userIdFollow = $id"
                . " union select userIdFollow as 'id' from Follow"
                . " where userId = $id) as x;");
        $query->execute();
        $result = $query->fetchAll();
        return $result[0]['count'];
    }

}

/**
 * Exceção para já existência de alguém seguindo alguém
 */
class AlreadyFollowException extends Exception {

    /**
     * Método construtor da exceção para a já exitência do registro de seguir alguém
     * @param string $msg Uma mensagem especificando o erro
     * @param type $code
     * @param type $previous
     */
    public function __construct($msg = null, $code = null, $previous = null) {
        parent::__construct("Voc&ecirc; já está seguindo esse usuário. " .
                $msg, $code, $previous);
    }

}
