<?php

require_once MODELS . "Row.php";

/**
 * Modelo de album para cada usuário
 *
 * @author YouCurte
 */
class Album extends Row {

    private $id, $userId, $name, $image;

    public function __construct($data = null) {
        parent::__construct($data);
    }

    /**
     * Retorna o id do álbum 
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Retorna o id do usuário que é dono do álbum
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Retorna o nome/descrição do álbum
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Retorna um link para a capa do álbum
     * @return \URL
     */
    public function getImage() {
        if (isEmpty($this->image)) {
            return new URL(ALBUM_IMAGE_DEFAULT);
        }
        return $this->image;
    }

    /**
     * Define o id do álbum
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Define o id do usuário dono do álbum
     * @param int $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * Define o nome/descrição do álbum
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Define a capa/imagem do álbum
     * @param URL|string $image
     */
    public function setImage($image) {
        $this->image = new URL($image);
    }

    /**
     * Lista todos os nomes dos campos que precisam serem salvos no banco de dados
     * @return type
     */
    public function listGets() {
        $result = parent::listGets();
        if (!is_null($this->getId())) {
            return $result;
        } else {
            return array_remove_value($result,"id");
        }
    }

}
