<?php

require_once 'DAO.php';
require_once 'Post.php';
require_once 'OpinionDAO.php';
require_once 'PostHistoryDAO.php';

/**
 * Classe para manipular posts no banco
 *
 * @author YouCurte
 */
class PostDAO extends DAO {

    public function __construct() {
        parent::__construct("Post");
    }

    /**
     * Adiciona um post no banco de dados
     * @param Post $post
     * @return boolean Se foi adicionado o post <br/> true = foi adicionado 
     *  <br/> false= não foi adicionado
     */
    public function addPost(Post $post) {
        if (is_null($post->getUserId())) {
            $post->setUser(LoginDAO::getInstance()->getUserLoggedIn());
            if ($post->getUser() === null) {
                return false;
            }
        }
        $id = $this->insert($post);
        $post->setId($id);
        $notification = new Notification();
        $notification->setText($post->getUser()->getName() . " fez um novo post.");
        $notification->setLink($post->getURL());
        $notification->setUserIdFrom(UserController::getInstance()->getUserLoggedIn());
        foreach (FollowDAO::getInstance()->listFollowers() as $user) {
            $notification->setUserId($user->getUserId());
            NotificationDAO::getInstance()->insert($notification);
        }
        return $this->wasLastExecutionSuccessful();
    }

    /**
     * Retorna os campos/parametros que identificam uma tupla/linha
     * @param Post $post
     * @return array Uma lista com os nomes dos campos/parametros
     */
    public function getIdParam(Post $post) {
        return array("id" => $post->getId());
    }

    /**
     * Deleta uma post
     * @param Post|int $post Id o objeto Post que deseja deletar.
     *  Exemplo de uso:
     *      $meuPost = new Post();
     *      deletePost($meuPost);
     *      ////ou///
     *      deletePost(12);
     * @return boolean|string Retorna false se tudo funcionou
     *  ou string contendo o erro
     */
    public function deletePost($post) {
        try {
            $post = $this->getPost($post);
            $user = LoginDAO::getInstance()->getUserLoggedIn();
            if ($post->getUserId() != $user->getId()) {
                throw new RuntimeException("O usuário logado não tem permissão para deletar essa imagem!");
            }
            PostHistoryDAO::getInstance()->delete(array("postId" => $post->getId()));
            OpinionDAO::getInstance()->delete(array("postId" => $post->getId()));
            $this->delete(array("id" => $post->getId()));
            return false;
        } catch (Exception $e) {
            Log::getInstance()->addException($e);
            return $e->getMessage();
        }
    }

    /**
     * Prepara um post adicionando informações de imagem, quantidades de tri e xucro.
     *  
     * @param Post $post O post que deseja adicionar as informações a mais
     * @return \Post Retorna o $post mesmo
     */
    public function preparePost(Post $post) {
        if (is_null($post)) {
            return;
        }
        if (is_int($post->getImageId()) || is_numeric($post->getImageId())) {
            $image = ImageDAO::getInstance()->getImage($post->getImageId());
            $post->setImage($image);
        }
        $opinion = OpinionDAO::getInstance()->getOpinion($post);
        $post->setMyTri($opinion->getTri());
        $post->setMyBah($opinion->getBah());
        return $post;
    }

    /**
     * Obtém e retorna um post (já prepara o post)
     * @param Post $postId O id do post
     * @return \Post  Retorna o post encontrado. 
     *      <br>Se não foi encontrado retorna 
     *  <strong>null</strong>.
     */
    public function getPost($postId) {
        if ($postId instanceof Post) {
            return $postId;
        }
        $result = $this->search(array("id" => $postId));
        if ($result->getCount() == 0) {
            return null;
        }
        return $this->preparePost($result->getData()[0]);
    }

    /**
     * Obtém e retorna a timeline de um usuário
     * @param int|User|email $user O usuário de quem deseja obter a linetime
     * @param int $count A quantidade máxima de registro que devem ser retornados
     * @param int $offset Remover tais registros iniciais
     * @return array 
     * 
     */
    public function getTimeLine($user = null, $count = COUNT_MAX_RETURN, $offset = 0) {
        if (is_null($user)) {
            $user = LoginDAO::getInstance()->getUserLoggedIn();
        } else {
            $user = UserDAO::getInstance()->getUser($user);
        }
        $id = $user->getId();
        $sql = "select * from PostView p where p.userId=$id " .
                "union " .
                "select p.id,p.text,p.hashtags,p.imageId,p.hasImage,p.time,p.local," .
                "p.latitude,p.longitude,p.userId,p.tri,p.bah from Follow f " .
                "join PostView p on p.userId=f.userIdFollow " .
                "where f.userId=$id " .
                "order by time desc limit $offset,$count;";
        $statement = $this->prepareStatement($sql);
        $sr = $this->executeStatement($statement);
        foreach ($sr->getData() as $post) {
            $this->preparePost($post);
        }
        return $sr->getData();
    }

    /**
     * Lista as postagens feito por esse usuário.
     * @param mix $user (User,id,e-mail)
     * @return array
     */
    public function listPosts($user = null, $count = COUNT_MAX_RETURN, $offset = 0) {
        if (is_null($user)) {
            $user = LoginDAO::getInstance()->getUserLoggedIn();
        } else {
            $user = UserDAO::getInstance()->getUser($user);
        }
        $result = $this->searchPosts("userId=:id order by time desc", array("id" => $user->getId()), $count, $offset);
        $posts = $result->getData();
        foreach ($posts as $post) {
            $this->preparePost($post);
        }
        return $posts;
    }

    /**
     * Pesquisa de registros no banco
     * 
     * Ex: search('nome like :nome',array('nome','%luiz%'));<br>
     *      search('nome like :name',$user);<br>
     *      search(array('id'),$user);<br>
     *      search(array('id'=>'1'));<br>
     * 
     * @param mix $where Where por extenço
     * @parem mix $values Chave e valor daquilo q deseja pesquisar
     * @param int $count Quantos registro no máximo quer procurar
     * @param int $offset Apartir de quantos registros
     * @return SearchResult Resultado da pesquisa
     */
    public function searchPosts($where, $values = null, $count = COUNT_MAX_RETURN, $offset = 0) {
        $dao = new DAO("PostView");
        $dao->setObjectClass($this->getObjectClass());
        return $dao->search($where, $values, $count, $offset);
    }

    /**
     * Retorna a instância principal de PostDAO
     * @return PostDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

    /**
     * Atualiza os dados de um post no banco de dados
     * @param Post $post O Post que deseja atualizar no banpo
     * @return boolean|string Retorna false se foi editado com sucesso.
     *  Caso contrário retorna uma string com a mensagem de erro.
     */
    public function updatePost(Post $post) {
        try {
            $this->update($post);
            return false;
        } catch (Exception $e) {
            Log::getInstance()->addException($e);
            return $e->getMessage();
        }
    }

    /**
     * Pesquisa posts por hashtagas
     * @param string $search As hashtags separadas por espaço. Ex:
     *          "#boanoite #céu"
     * @param int $count A quantidade máxima de posts que deve ser retornado
     * @param int $offset Quantos registros devem ser eliminados no início
     * @return SearchResult Uma lista de Post's com tais hashtags
     */
    public function searchPostsForHashtags($search, $count = COUNT_MAX_RETURN, $offset = 0) {
        $search = explode(" ", $search);
        $array = array();
        foreach ($search as $part) {
            if (!strstr($part, "#")) {
                $array[] = $part;
            } else {
                $div = explode("#", $part);
                foreach ($div as $key => $value) {
                    if ($key > 0 || strpos($value, "#") === 0) {
                        $array[] = "#$value ";
                    } else {
                        $array[] = $value;
                    }
                }
            }
        }
        $where = "";
        foreach ($array as $value) {
            if (isNotEmpty($value) && $value != "# ") {
                if (isNotEmpty($where)) {
                    $where.=" and ";
                }
                if (strstr($value, "#")) {
                    $field = "hashtags";
                } else {
                    $field = "fullname";
                }
                $where.="$field like '%$value%'";
            }
        }
        if (isEmpty($where)) {
            $where = "hashtags like '%#%'";
        }
        $where = str_replace("# ", "#", $where);
        $dao = new DAO("SearchView");
        $dao->setObjectClass($this->getObjectClass());
        $result = $dao->search($where . " order by id desc", null, $count, $offset);
        $query = $this->getPDO()->query("select count(id) as 'count' from SearchView where " . $where);
        $query->execute();
        $result->setCount($query->fetchAll()[0]['count']);
        return $result;
    }

    /**
     * Listar posts de um local
     * @param string $local O local
     * @param int $count A quantidade máxima que deve retornar
     * @param int $offset Remover tais registros do início
     * @return Array Um list com os posts encontrados
     */
    public function listPostsFromLocal($local, $count = COUNT_MAX_RETURN, $offset = 0) {
        $sr = $this->searchPosts("local=:local order by id", array("local" => $local), $count, $offset);
        $array = array();
        foreach ($sr->getData() as $item) {
            $array[$item->getUserId()] = $item;
        }
        return $array;
    }

    /**
     * Faz a validação do post para salvar no banco de dados
     * @param Post $post
     * @param boolean $throw Lançar uma exceçao
     * @return String A mensagem de erro
     */
    public function validatePost(Post $post, $throw = false) {
        $error = "";
        if (isEmpty($post->getText()) || !$post->getHasImage()) {
            $error = "É necessário que o post tenha uma mensagem de texto ou imagem!";
        }
        if (isNotEmpty($post->getText())) {
            if (strlen($post->getText()) < POST_TEXT_SIZE_MAX) {
                $error = "O texto deve ter no máximo " . POST_TEXT_SIZE_MAX .
                        " caractere(s).";
            }
        }
        if (isNotEmpty($error) && $throw) {
            throw new RuntimeException($error);
        }
        return $error;
    }

    /**
     * Obtém o endereço do local. Se estiver em branco tenta obter a localização 
     *      a partir do latitude e longitude. <br/>
     *      Acaba definindo o valor do campo local na instância do post
     * @return String O endereço
     */
    public function getLocal(Post $post) {
        if (isNotEmpty($post->getLocal())) {
            return $post->getLocal();
        }
        try {
            $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" . $post->getLatitude() . "," . $post->getLongitude();
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4"));
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($curl);
            curl_close($curl);
            $json = json_decode($output);
            $post->setLocal($json->results[0]->formatted_address);
        } catch (Exception $e) {
            throw new RuntimeException($e);
        }
    }

}
