<?php

require_once CLASSES . 'Singleton.php';
require_once CLASSES . 'Input.php';
require_once CLASSES . 'DBConnection.php';
require_once CLASSES . 'DBWhere.php';
require_once CLASSES . 'SearchResult.php';

/**
 * Classe para criar os manipuladores de objetos/linhas/tuplas no banco de dados
 */
class DAO extends Singleton {

    private $table, $objectClass, $sucessful;

    public function __construct($table = "") {
        parent::__construct();
        $this->table = $table;
        $this->objectClass = $table;
    }

    /**
     * Faz um insert no banco
     * @param Row $row O objeto que deseja inserir no banco
     * @param array $fields (Opcional)Os campos que serão usados no insert
     *  Se não informado pega os campos do comando $row->listGets()
     * @return mix Retorna o id da última linha inserida
     */
    public function insert(Row $row, $fields = null) {
        $pdo = DBConnection::getInstance();
        if (is_null($fields)) {
            $fields = $row->listGets();
        }
        $sql = "insert into " . $this->table . "(";
        $count = 0;
        foreach ($fields as $key) {
            if (isNotEmpty($key)) {
                if ($count++ != 0) {
                    $sql.=",";
                }
                $sql.=lcfirst($key);
            }
        }
        $sql.=") values (";
        for ($i = 0; $i < $count; $i++) {
            if ($i != 0) {
                $sql.=",";
            }
            $sql.=":param$i";
        }
        $sql.=");";
        $rs = $pdo->prepare($sql);
        $i = 0;
        foreach ($fields as $field) {
            try {
                $value = $this->getRowValue($row, $field);
                $rs->bindValue(":param$i", $value);
                if ($i++ > $count)
                    break;
            } catch (Exception $e) {
                Log::getInstance()->addException($e);
            }
        }
        $this->setLastExecutionSucessful($rs->execute());
        return $pdo->lastInsertId();
    }

    /**
     * Pesquisa básica de registros no banco 
     *  Use o search para realizar pesquisas
     * 
     * @param Array $where Chave e valor daquilo q deseja pesquisar
     * @param int $count Quantos registro no máximo quer procurar
     * @param int $offset Apartir de quantos registros
     * @return SearchResult Resultado da pesquisa
     */
    public function searchSimple($where, $count = COUNT_MAX_RETURN, $offset = 0) {
        $sql = "select * from " . $this->table . " where ";
        $init = false;
        if (!is_int($offset) || !is_int($count)) {
            throw new Exception("Count ou Offset não é um valor inteiro.");
        }
        foreach ($where as $key => $value) {
            if ($init)
                $sql.= 'and ';
            $sql.="$key = ? ";
            $init = true;
        }
        $sql.=" limit $offset,$count;";
        $query = DBConnection::getInstance()->prepare($sql);
        $i = 1;
        foreach ($where as $value) {
            try {
                if ($value instanceof Row) {
                    throw new Exception("Row sendo passado como valor");
                }
                $query->bindValue($i++, $value);
            } catch (Exception $e) {
                Log::getInstance()->addException($e);
                throw $e;
            }
        }
        $query->execute();
        $result = array();
        while ($row = $query->fetch(PDO::FETCH_BOTH)) {
            $object = $this->newObject($row);
            $result[] = $object;
        }
        return new SearchResult($result, count($result));
    }

    /**
     * Lista objetos (Row) de um query (usa função newObject)
     * @param PDOStatement $query A query já executada
     * @return array O array de objetos
     */
    public function listObjects($query) {
        $result = array();
        while ($row = $query->fetch(PDO::FETCH_BOTH)) {
            $object = $this->newObject($row);
            $result[] = $object;
        }
        return $result;
    }

    /**
     * Pesquisa de registros no banco
     * 
     * Ex: search('nome like :nome',array('nome','%luiz%'));<br>
     *      search('nome like :name',$user);<br>
     *      search(array('id'),$user);<br>
     *      search(array('id'=>'1'));<br>
     * 
     * @param mix $where Where por extenço
     * @parem mix $values Chave e valor daquilo q deseja pesquisar
     * @param int $count Quantos registro no máximo quer procurar
     * @param int $offset Apartir de quantos registros
     * @return SearchResult Resultado da pesquisa
     */
    public function search($where, $values = null, $count = COUNT_MAX_RETURN, $offset = 0) {
        if (is_null($values) && !is_string($where)) {
            return $this->searchSimple($where, $count, $offset);
        }
        if (($values instanceof Row) && is_array($where)) {
            $w = array();
            foreach ($where as $field) {
                $w[$field] = $this->getRowValue($values, $field);
            }
            return $this->search($w, null, $count, $offset);
        }
        if (!is_numeric($offset) || !is_numeric($count)) {
            throw new Exception("Count ou Offset não é um valor inteiro.");
        }
        $sql = "select * from " . $this->table . " where $where limit $offset,$count;";
        $query = DBConnection::getInstance()->prepare($sql);
        if ($values instanceof Row) {
            foreach ($values->listGets() as $field) {
                $query->bindValue(":$field", $this->getRowValue($values, $field));
            }
        } else if (is_array($values)) {
            foreach ($values as $key => $value) {
                $query->bindValue(":$key", $value);
            }
        }
        $this->setLastExecutionSucessful($query->execute());
        $result = array();
        while ($row = $query->fetch(PDO::FETCH_BOTH)) {
            $result[] = $this->newObject($row);
        }
        return new SearchResult($result, count($result));
    }

    public function getPDO() {
        return DBConnection::getInstance();
    }

    /**
     * 
     * @param string $sql
     * @return PDOStatement
     */
    public function prepareStatement($sql) {
        return $this->getPDO()->prepare($sql);
    }

    /**
     * 
     * @param PDOStatement $statement
     * @return \SearchResult
     */
    public function executeStatement(PDOStatement $statement) {
        $this->setLastExecutionSucessful($statement->execute());
        $result = array();
        while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
            $result[] = $this->newObject($row);
        }
        return new SearchResult($result, count($result));
    }

    /**
     * Cria um objeto referente ao Row.
     *  Ex:
     *      newObject(); = new User(); //se $this->objectClass='User';
     * @param mixed $data
     * @return mixed 
     */
    public function newObject($data = null) {
        $rc = new ReflectionClass($this->getObjectClass());
        return $rc->newInstance($data);
    }

    /**
     *  Nome da classe do objeto Row
     * @return String
     */
    public function getObjectClass() {
        return $this->objectClass;
    }

    /**
     * Define o nome da classe do objeto Row
     * @param String $objectClass
     */
    public function setObjectClass($objectClass) {
        $this->objectClass = $objectClass;
    }

    /**
     * Retorna o row com os dados de entrada
     * @return Row
     */
    public function getObjectFromInput() {
        return $this->newObject(Input::getInstance());
    }

    /**
     * Obtém o valor de um objeto tratado para salvar no banco
     * @param Row $row
     * @param String $field
     * @return mix
     */
    public function getRowValue(Row $row, $field) {
        return $this->getValue($row->get($field));
    }

    /**
     * Prepara um valor para ser salvo no banco
     * 
     * @param mix $value
     */
    public function getValue($value) {
        if ($value instanceof DateTimeYC) {
            return $value->toDB();
        } else if ($value === true) {
            return '1';
        } else if ($value === false) {
            return '0';
        } else {
            return $value;
        }
    }

    /**
     * Deleta os registros no banco
     * 
     * Ex: delete('nome like :nome',array('nome','%luiz%'));<br>
     *     delete('nome like :name',$user);<br>
     *     delete(array('id'=>'1'));<br>
     *     delete(array('id'),$row);<br>
     *     delete($row);<br>
     * 
     * @return int O número de linhas afetadas
     */
    public function delete($arg0, $arg1 = null) {
        $sql = "delete from " . $this->table . " where ";
        if (is_string($arg0) && is_array($arg1)) { //delete('nome like :nome',array('nome','%luiz%'))
            $sql.=$arg0;
            $query = DBConnection::getInstance()->prepare($sql);
            foreach ($arg1 as $key => $value) {
                if (strstr($sql, ":$key")) {
                    $query->bindValue(":$key", $value);
                }
            }
            $query->execute();
            return $query->rowCount();
        } else if (is_string($arg0) && ($arg1 instanceof Row)) { //delete('nome like :name',$user);
            return $this->delete($arg0, Row::rowToArray($arg1));
        } else if (is_array($arg0) && is_null($arg1)) { //delete(array('id'=>'1'));
            $where = "";
            foreach ($arg0 as $key => $value) {
                if (isNotEmpty($where)) {
                    $where.=" and ";
                }
                $where .= "$key = :$key";
            }
            return $this->delete($where, $arg0);
        } else if (is_array($arg0) && ($arg1 instanceof Row)) { //delete(array('id'),$row);
            $where = "";
            foreach ($arg0 as $value) {
                if (isNotEmpty($where)) {
                    $where.=" and ";
                }
                $where .= "$value = :$value";
            }
            return $this->delete($where, $arg1);
        } else if (($arg0 instanceof Row) && $arg1 == null) { //delete($row);
            return $this->delete($arg0->listIdParams());
        }
        return 0;
    }

    /**
     * Atualizar um registro
     * Exemplos de uso:
     *          update($row);
     *          update($row,array('id'));
     *          update($row,"name likes :name");
     *          update($row,'idade = :idade",array("idade"=>10));
     *          update($row,array('id'=>1));
     * 
     * @param Row $row
     * @param mix $where
     * @param Array $values
     * @return int O número de linhas afetadas
     */
    public function update(Row $row, $where = "", $values = null) {
        $sql = "update " . $this->table . " set ";
        $is_string = isNotEmpty($where) && is_string($where);
        if ($is_string && !is_null($values)) { //update($row,'idade = :idade",array("idade"=>10));
            $first = true;
            foreach ($row->listGets() as $field) {
                if (!$first) {
                    $sql.=",";
                }
                $sql.="$field=:$field";
                $first = false;
            }
            $sql.=" where $where;";
            $query = DBConnection::getInstance()->prepare($sql);
            foreach ($values as $key => $value) {
                $query->bindValue($key, $value);
            }
            foreach ($row->listGets() as $field) {
                if (!array_key_exists($field, $values)) {
                    $query->bindValue($field, $this->getRowValue($row, $field));
                }
            }
            $this->setLastExecutionSucessful($query->execute());
            return $query->rowCount();
        } else if ($is_string) { //update($row,"name likes :name");
            return $this->update($row, $where, array());
        } else if (is_array($where)) { //update($row,array('id')); or //update($row,array('id'=>1)
            $w = "";
            $values = array();
            foreach ($where as $key => $value) {
                if (is_string($key)) {
                    $field = $key;
                    $values[$key] = $value;
                } else {
                    $field = $value;
                }
                if (isNotEmpty($w)) {
                    $w.=" and ";
                }
                $w.="$field=:$field";
            }
            return $this->update($row, $w, $values);
        } else if (isEmpty($where)) { //update($row);
            return $this->update($row, $row->listIdParams());
        }
        return 0;
    }

    /**
     * Retorna todos os registro ou quase todos 
     * @param string $add_sql Adicionar um comando ao sql (order by ou algo do tipo)v
     * @param int $count Quantidade máxima de registro a trazer
     * @param int $offset Remover do inicio tal quantidade de registros sem 
     *  alterar o número de registro que vai trazer
     * @return \SearchResult Os registros
     */
    public function selectFromAll($add_sql = "",$count = COUNT_MAX_RETURN, $offset = 0) {
        $sql = "select * from " . $this->table . " " . $add_sql ." limit $offset, $count;";
        $query = DBConnection::getInstance()->prepare($sql);
        $this->setLastExecutionSucessful($query->execute());
        $result = array();
        while ($row = $query->fetch(PDO::FETCH_BOTH)) {
            $result[] = $this->newObject($row);
        }
        return new SearchResult($result, count($result));
    }

    /**
     * Verifica se os campos informados tem algum valor válido.
     * 
     * Ex de uso:
     *  checkFields($user,array('name','age'),array('nome','idade'));
     *  Saída: 'Os seguintes campos são necessários: nome.'
     *  checkFields($user,array('name','age'),array('nome','idade'),'b');
     *  Saída: 'Os seguintes campos são necessários: <b>nome</b>.';
     * 
     * @return mix Retorna uma mensagem se tiverem campos faltando.
     *  Retorna false se tiver tudo ok.
     */
    public function checkFields(Row $row, $fields = null, $labels = null, $tag = "") {
        if (is_null($fields)) {
            $fields = $row->listFieldsNeed();
        }
        if (is_null($labels)) {
            $labels = $fields;
        }
        $emptys = array();
        foreach ($fields as $key => $field) {
            if (isEmpty($this->getRowValue($row, $field))) {
                $emptys[] = $labels[$key];
            }
        }
        if (count($emptys) == 0) {
            return false;
        } else {
            $msg = "";
            foreach ($emptys as $field) {
                if (isNotEmpty($msg)) {
                    $msg.=",";
                }
                if (isEmpty($tag)) {
                    $msg.=$field;
                } else {
                    $msg.="<$tag>$field</$tag>";
                }
            }
            return "Os seguintes campos são necessários: $msg.";
        }
    }

    /**
     * Conta a quantidade de registros     * 
     * 
     * Ex: count("nome",'nome like :nome',array('nome','%luiz%'));<br>
     *      count("name",'nome like :name',$user);<br>
     *      count("id",array('id'),$user);<br>
     *      count("id",array('id'=>'1'));<br>
     * @param String|array $where [opcional]
     * @param Array|row $values [opcional]
     * @param int $count [opcional]
     * @param int $offset [opcional]
     * @return int Retorna a quantidade obtida 
     */
    public function count($field = "id", $where = "", $values = null, $count = COUNT_MAX_RETURN
    , $offset = 0) {
        $dbwhere = new DBWhere($where, $values, $count, $offset);
        return $this->countFromDBWhere($field, $dbwhere);
    }

    /**
     * Conta a quantidade de registros
     * @param string $field [opcional] O nome do campo que deseja contar
     * @param DBWhere $dbwhere [opcional] O where do select
     * @return int Retorna a quantidade obtida
     */
    public function countFromDBWhere($field = "id", DBWhere $dbwhere = null) {
        if ($dbwhere === null) {
            $dbwhere = new DBWhere("");
        }
        $where = "select count($field) as 'resultCount' from " . $this->table;
        $where.= $dbwhere->getWhere();
        $query = $this->getPDO()->prepare($where);
        $dbwhere->bindValues($query);
        $query->execute();
        return $query->fetchAll()[0]["resultCount"];
    }

    /**
     * Indica se a ultima execução de busca ou alteração no banco de dados foi um sucesso
     * @return boolean true = sucesso <br> false = fracasso
     */
    public function wasLastExecutionSuccessful() {
        return $this->sucessful;
    }

    /**
     * Define se a última execução foi um sucesso
     * @param boolean $value true = sucesso <br> false = fracasso
     */
    public function setLastExecutionSucessful($value) {
        $this->sucessful = $value;
    }

    /**
     * Retorna a instância principal de DAO
     * @return DAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}

/**
 * Exception para campo não encontrado
 */
class FieldDontFoundException extends Exception {

    /**
     * Método construtor do Exception para campo não encontrado
     * @param String $field O nome do campo
     * @param type $code [opcional]
     * @param type $previous [opcional]
     */
    public function __construct($field, $code = null, $previous = null) {
        parent::__construct("O campo '$field' não foi encontrado.", $code, $previous);
    }

}

class ImpossibleDeleteException extends Exception {

    /**
     * Não foi possível deletar um registro/row
     * @param String $msg Uma mensagem de erro
     * @param type $code
     * @param type $previous
     */
    public function __construct($msg, $code = null, $previous = null) {
        parent::__construct("Impossível deletar um registro de '"
                . $this->table . "': $msg", $code, $previous);
    }

}

class FieldEmptyException extends Exception {

    /**
     * Um campo está em branco e não poderia estar
     * @param string $field O nome do campo
     * @param type $code
     * @param type $previous
     */
    public function __construct($field, $code = null, $previous = null) {
        parent::__construct("O campo '$field' está vazio.", $code, $previous);
    }

}
