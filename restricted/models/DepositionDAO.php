<?php

require_once MODELS . "Deposition.php";

/**
 * Classe para manipular no banco de dados os depoimentos
 *
 * @author YouCurte
 */
class DepositionDAO extends DAO {

    public function __construct() {
        parent::__construct("Deposition");
    }

    /**
     * Pega o paramêtro que serve para identificar um depoimento
     * @param Deposition $deposition
     * @return array Um lista com os nomes dos paramêtros e seus valores
     */
    public function getIdParam(Deposition $deposition) {
        return array("id" => $deposition->getId());
    }

    /**
     * Deleta um depoimento específico
     * @param Deposition $deposition O depoimento que deseja deletar
     * @return boolean true =  Depoimento deletado com sucesso  <br>
     *      false = depoimento não deletado
     */
    public function deleteDeposition(Deposition $deposition) {
        return $this->delete($this->getIdParam($deposition)) > 0;
    }

    /**
     * Atualiza um depoimento
     * @param Deposition $deposition 
     * @return boolean true = depoimento atualizado <br>
     *      false = não foi possível atualizar o depoimento
     */
    public function updateDeposition(Deposition $deposition) {
        $this->update($deposition, $this->getIdParam($deposition));
        return $this->wasLastExecutionSuccessful();
    }

    /**
     * Retorna um depoimento específico
     * @param int $id O id do depoimento
     * @return Deposition
     */
    public function getDeposition($id) {
        return $this->search(array("id" => $id))->getData(0);
    }

    /**
     * Lista todos os depoimentos que um usuário recebeu
     * @param int $userIdTo O id do usuário
     * @param int $count Quantos registror retornar no máximo
     * @param int $offset Quantos registros ignorar
     * @return array
     */
    public function listDepositions($userIdTo, $count = COUNT_MAX_RETURN, $offset = 0) {
        if ($userIdTo instanceof User) {
            $userIdTo = $userIdTo->getId();
        }
        return $this->search("userIdTo=:userIdTo order by time desc", array("userIdTo" => $userIdTo), $count, $offset)->getData();
    }

    /**
     * Retorna a instância principal de DepositionDAO
     * @return DepositionDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

    /**
     * Conta a quantidade de depoimentos que usuário recebeu
     * @param mixed $user Qualquer parametro que possa identificar o usuário
     * @return int a quantidade
     */
    public function countDepositions($user) {
        $resultUser = UserDAO::getInstance()->getUser($user);
        return $this->count("userIdTo", array("userIdTo" => $resultUser->getId()));
    }

    /**
     * Adiciona um depoimento no banco de dados
     * @param Deposition $deposition
     * @return boolean Se foi adicionado com sucesso
     */
    public function addDeposition(Deposition $deposition) {
        $id = $this->insert($deposition);
        $deposition->setId($id);
        $sucess = $this->wasLastExecutionSuccessful();
        $notification = new Notification();
        $notification->setLink(new URL("depositions/" .
                $deposition->getUserIdTo(), URL_ROOT));
        $user = UserDAO::getInstance()->getUser($deposition->getUserIdTo());
        $notification->setText($user->getName() . " fez um depoimento para voc&ecirc;.");
        $notification->setUserId($user->getId());
        $notification->setUserIdFrom($deposition->getUserIdFrom());
        NotificationDAO::getInstance()->insert($notification);
        return $sucess;
    }

}
