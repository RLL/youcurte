<?php

require_once CLASSES . 'Input.php';

/**
 * Uma linha/tupla que vai ser salva no banco
 *
 * @author YouCurte
 */
class Row {

    /**
     * Método construtor da linha/tupla
     * @param array/object/json $data Um objeto com os dados para preencher
     *      a linha/tupla
     */
    public function __construct($data = null) {
        if (isset($data)) {
            if ($data instanceof Row) {
                $array = array();
                foreach ($data->listFields() as $field) {
                    $array[$field] = $data->get($field);
                }
                $data = $array;
            }
            if (is_string($data)) {
                $data = json_decode($data);
            }
            if (is_array($data)) {
                foreach ($this->listFields() as $field) {
                    $value = array_return_valid($data, $field);
                    if (!is_null($value)) {
                        $this->set($field, $value);
                    }
                }
            } else if (is_object($data)) {
                foreach ($this->listFields() as $field) {
                    $value = object_return_valid($data, $field);
                    if (isset($value)) {
                        $this->set($field, $value);
                    }
                }
            }
        }
    }

    /**
     * Lista o nome de todos os campos da linha/tupla
     * @return array
     */
    public static function listFields() {
        $sets = array();
        $obj = get_called_class();
        foreach (get_class_methods($obj) as $method) {
            if (strpos($method, "set") === 0) {
                $set = substr($method, 3);
                foreach (array('get', 'is', 'has', 'was') as $method) {
                    if (isNotEmpty($set) && method_exists($obj, $method . $set)) {
                        $sets[] = lcfirst($set);
                        break;
                    }
                }
            }
        }
        return $sets;
    }

    /**
     * Lista os nomes dos campos que possuem o método set para definir
     * @return array
     */
    public static function listSets() {
        $sets = array();
        $obj = get_called_class();
        foreach (get_class_methods($obj) as $method) {
            if (strpos($method, "set") === 0) {
                $set = substr($method, 3);
                if (isNotEmpty($set)) {
                    $sets[] = lcfirst($set);
                }
            }
        }
        return $sets;
    }

    /**
     * Lista os nomes dos campos que possuem o método get/was/is/has
     * @return array
     */
    public function listGets() {
        $fields = self::listFields();
        //$fields = array_remove_value($fields,"dateBirth");
        return $fields;
    }

    /**
     * Lista os campos necessários/obrigatórios para salvar no banco de dados
     * @return array
     */
    static function listFieldsNeed() {
        return array();
    }

    /**
     * Define o valor de um campo da linha/tupla
     * @param string $key O nome do campo
     * @param mixed $value O valor do campo
     * @return mixed Retorna o <strong>valor</strong> do comando set do campo.
     *      Obs: retorna <strong>false</strong> se não foi possivel executar
     *   o set
     * @throws Exception
     */
    public function set($key, $value) {
        if ($key == '' || !method_exists($this, 'set' . $key)) {
            throw new Exception("Campo '$key' não encontrado.");
        }
        $key = 'set' . ucfirst($key);
        return call_user_func(array($this, $key), $value);
    }

    /**
     * Define um valor para tal variavel se não foi definida ou estiver null.
     *  Ex:
     *    setFirst(array("hasDog","hasCat"),true);
     *    setFirst("hasDog",true);
     * @param Array|String $keys
     * @param Array|mixed $values
     */
    public function setFirst($keys, $values) {
        if (!is_array($keys)) {
            $keys = array($keys);
        }
        if (!is_array($values)) {
            $values = array($values);
        }
        foreach ($keys as $i => $key) {
            $old = $this->get($key);
            if (!isset($old) || is_null($old)) {
                $value = count($values) == 1 ? $values[0] : $values[$i];
                $this->set($key, $value);
            }
        }
    }

    /**
     *    
     * Retorna o valor de um campo da linha/tupla
     * @param string $key O nome do campo
     * @return mixed O valor do campo
     */
    public function get($key) {
        foreach (array('get', 'is', 'was', 'has') as $method) {
            $name = $method . ucfirst($key);
            if (method_exists($this, $name)) {
                $value = call_user_func(array($this, $name));
                return $value;
            }
        }
        return null;
    }

    /**
     * Converte um objeto Row para array
     * @param Row $row A linha/tupla para converter
     * @param array [opcional] $removeFields Uma array com o nome dos campos que
     *       devem ser removidos
     * @param array [opcional] $fields Um array com o nome dos campos (se não informado,
     *  usa todos os campos do linha/tupla)
     * @return array O resultado da conversão
     */
    public static function rowToArray(Row $row, $removeFields = null, $fields = null) {
        $array = array();
        if (is_null($fields)) {
            $fields = $row->listGets();
        }
        if (is_null($removeFields)) {
            $removeFields = array();
        }
        foreach ($fields as $field) {
            if (!array_keys($removeFields, $field)) {
                $array[$field] = DAO::getInstance()->getRowValue($row, $field);
            }
        }
        return $array;
    }
    
    /**
     * Lista os paremetros de idenficação de uma dupla (para usar no delete ou update)
     * 
     * @return array Ex: array("id"=>"1");
     */
    public function listIdParams(){
        return array("id"=>$this->get("id"));
    }

}
