<?php

require_once MODELS.'Row.php';

/**
 * Registro de alguém seguindo alguém
 *
 * @author YouCurte
 */
class Follow extends Row {
    
    private $userIdFollow,$userId;
    
    public function __construct($data = null) {
        parent::__construct($data);
    }
    
    /**
     * Retorna o id de quem está sendo seguido
     * @return int
     */
    public function getUserIdFollow() {
        return $this->userIdFollow;
    }

    /**
     * Retorna quem é o seguidor
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Define o id de quem vai ser seguido
     * @param int $userIdFollow
     */
    public function setUserIdFollow($userIdFollow) {
        $this->userIdFollow = $userIdFollow;
    }

    /**
     * Define o id do usuário seguidor
     * @param int $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }


    
}