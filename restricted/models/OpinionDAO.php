<?php

require_once MODELS . 'Opinion.php';

/**
 * Classe para gerencias as opiniões de post (tri/xucro) no banco de dados
 *
 * @author YouCurte
 */
class OpinionDAO extends DAO {

    public function __construct() {
        parent::__construct("Opinion");
    }

    /**
     * Obtém e retorna a opinião de um usuário sobre um post
     * @param Post $post O objeto post ou id do post
     * @param User $user [opcional] O usuário ou id do usuário
     *      E se não é informado pega do usuário logado
     * @return Opinion 
     *      Retorna um default se não encontrado no banco de dados
     */
    public function getOpinion($post, $user = null) {
        if (is_null($user)) {
            $user = UserController::getInstance()->getUserLoggedIn();
        }
        if (!is_int($post)) {
            $post = PostDAO::getInstance()->getPost($post)->getId();
        }
        $hasUser = !is_null($user);
        if ($hasUser) {
            if (!is_int($user)) {
                $user = UserDAO::getInstance()->getUser($user)->getId();
            }
            $result = $this->search(array('userId' => $user,
                'postId' => $post), null, 1);
        }
        if (!$hasUser || is_null($post) || $result->getCount() === 0) {
            $op = new Opinion();
            $op->setUserId($user);
            $op->setPostId($post);
            return $op;
        }
        return $result->getData(0);
    }

    /**
     * Lista as opinions de um post
     * @param Post|int $post O post ou id do post
     * @param int $count [opcional]A quantidade máxima de registros que podem ser retornados
     * @param int $offset [opcional]Quantidade de registros a serem ignorados
     * @return array
     */
    public function listOpinions($post, $count = COUNT_MAX_RETURN, $offset = 0) {
        if (!is_int($post)) {
            $post = PostDAO::getInstance()->getPost($post)->getPost();
        }
        $result = $this->search(array('postId' => $post), null, $count, $offset);
        return $result->getData();
    }

    /**
     * Slava uma opinion no banco de dados
     * @param Opinion $opinion
     */
    public function saveOpinion(Opinion $opinion) {
        $result = $this->search(array("postId" => $opinion->getPostId(),
            "userId" => $opinion->getUserId()), null, 1);
        if ($result->getCount() == 0) {
            $this->insert($opinion);
        } else {
            $this->update($opinion, array('postId', 'userId'));
        }
    }

    /**
     * Retorna a instância principa de OpinionDAO
     * @return OpinionDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}
