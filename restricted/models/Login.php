<?php

require_once MODELS . 'Row.php';
require_once CLASSES . 'DateTimeYC.php';

/**
 * Modelo de login para cada usuário
 *
 * @author YouCurte
 */
class Login extends Row {

    private $auth, $userId, $creationTime, $expirationTime,
            $native, $userAgent, $user;

    public function __construct($data = null, $keepLogged = false) {
        parent::__construct($data);
        $this->creationTime = DateTimeYC::now();
        $this->setNative(false);
        $this->expirationTime = DateTimeYC::now();
        if ($keepLogged) {
            $this->expirationTime->add(LOGIN_INTERVAL_MAX);
        } else {
            $this->expirationTime->add(LOGIN_INTERVAL_MIN);
        }
    }

    /**
     * Código-chave que indica que e qual usuário está logado
     * 
     * @return String Token/Cookie/código-chave 
     */
    public function getAuth() {
        return $this->auth;
    }

    /**
     * 
     * @return int Id do usuário referente
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * @return User Retorna o usuário referente
     */
    public function getUser() {
        if (isset($this->user)) {
            return $this->user;
        } else {
            $ud = UserDAO::getInstance();
            $result = $ud->searchUserById($this->userId);
            $this->user = $result->getData();
        }
    }

    /**
     * @return DateTimeYC Momento em que logou
     */
    public function getCreationTime() {
        return $this->creationTime;
    }

    /**
     * 
     * @return DateTimeYC Momento que irá expirar
     */
    public function getExpirationTime() {
        return $this->expirationTime;
    }

    /**
     * Retorna se esse log foi criado pela aplicação principal (pelo próprio youcurte.com.br)
     *  ou por uma aplicação de terceiros(aplicação mobile por exemplo)
     * 
     * @return boolean 
     */
    public function isNative() {
        return $this->native;
    }

    /**
     * Retorna qual navegador ou aplicação que soliciou a criação desse log 
     * 
     * @return String 
     */
    public function getUserAgent() {
        return $this->userAgent;
    }

    /**
     * Define o código-chave
     * 
     * @param String $auth 
     */
    public function setAuth($auth) {
        $this->auth = $auth;
    }

    /**
     * Define o id do usuário
     * 
     * @param int $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
        $this->setUser($userId);
    }

    /**
     * Método derivado de setUserId
     *  Acaba setando o id de usuário apartir de um usuário.
     *     -- setUserId($user->getId();
     * @param User $user
     */
    public function setUser($user) {
        $this->user = UserDAO::getInstance()->getUser($user);
        if (!is_null($this->user)) {
            $this->userId = $this->user->getId();
        }
    }

    /**
     * Momento em que foi criado esse registro
     * 
     * @param Mix $creationTime
     */
    public function setCreationTime($creationTime) {
        $this->creationTime = new DateTimeYC($creationTime);
    }

    /**
     * Momento que irá explirar
     * @param Mix $expirationTime
     */
    public function setExpirationTime($expirationTime) {
        if (isNotEmpty($expirationTime)) {
            $this->expirationTime = new DateTimeYC($expirationTime);
        }
    }

    /**
     * Se esse log foi solicitado pela aplicação principal (youcurte.com.br)
     *  ou aplicação de terceiros
     * @param boolean $native
     */
    public function setNative($native) {
        $this->native = $native;
    }

    /**
     *  Especifica qual navegador ou aplicação(de terceiros) solicitou esse log
     * 
     * @param String $userAgent
     */
    public function setUserAgent($userAgent) {
        $this->userAgent = $userAgent;
    }

    /**
     * Gera e seta um novo código-chave
     * @param int $length Tamanho do código-chave (máximo é 12)
     * @return String Código-chave gerado.
     */
    public function generateAuth($length = 50) {
        $key = randText($length);
        $this->setauth($key);
        return $key;
    }

    /**
     * Se esse login expirou
     * @return boolean
     */
    public function isExpired() {
        return DateTimeYC::now()->isAfter($this->getExpirationTime());
    }

    /**
     * Retorna uma lista os campos que devem ser salvos no banco de dados
     * @return aray
     */
    public function listGets() {
        $gets = parent::listGets();
        $resultGets = array_remove_value($gets, "user");
        return $resultGets;
    }

}