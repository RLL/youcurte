<?php

require_once MODELS."Row.php";

/**
 * Modelo dos atributos que um usuário atribui a outro
 *
 * @author YouCurte
 */
class Attributes extends Row {
    
    private $cool,$reliable,$sexy,$userIdFrom,$userIdTo;
    
    public function __construct($data = null){
        parent::__construct($data);
        $this->setFirst(array("cool","reliable","sexy"), 0);
    }
    
    /**
     * Retorna o quão legal é o usuário que é atribuído
     * @return int
     */
    function getCool() {
        return $this->cool;
    }
    
    /**
     * Retorna o quão sexy é o usuário que é atribuído
     * @return int
     */
    function getSexy() {
        return $this->sexy;
    }

    /**
     * Retorna o id usuário que atribui os valores
     * @return int
     */
    function getUserIdFrom() {
        return $this->userIdFrom;
    }

    /**
     * Retorna o id do usuário que recebe os atributos
     * @return type
     */
    function getUserIdTo() {
        return $this->userIdTo;
    }

    /**
     * Define o quão legal é usuário atribuído
     * @param int $cool
     */
    function setCool($cool) {
        $this->cool = $cool;
    }

    /**
     * Define o quão sexy é o usuário atribuído
     * @param int $sexy
     */
    function setSexy($sexy) {
        $this->sexy = $sexy;
    }
    
    /**
     * Retorna o quão confiável é o usuário atribuído
     * @return int
     */    
    function getReliable() {
        return $this->reliable;
    }

    /**
     * Define o quão confiável é o usuário atribuído
     * @param type $reliable
     */
    function setReliable($reliable) {
        $this->reliable = $reliable;
    }    

    /**
     * Define qual usuário atribuiu os valores
     * @param int $userIdFrom O id do usuário que atribuiu os valores
     */
    function setUserIdFrom($userIdFrom) {
        $this->userIdFrom = $userIdFrom;
    }

    /**
     * Define o id do usuário que recebeu os atributos
     * @param int $userIdTo
     */
    function setUserIdTo($userIdTo) {
        $this->userIdTo = $userIdTo;
    }


    
}