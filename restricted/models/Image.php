<?php

require_once MODELS.'Row.php';
require_once CLASSES.'DateTimeYC.php';

/**
 * Modelo de cada imagem
 *
 * @author YouCurte
 */
class Image extends Row {
   
    private $id,$albumId,$userId,$url,$name,$time;
    
    public function __construct($data = null) {
        parent::__construct($data);
        $this->setFirst("name","");
        $this->setFirst("albumId",ALBUM_ID_DEFAULT);
        $this->setFirst("time",  DateTimeYC::now());
    }
    
    /**
     * Retorna o id da imagem
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Retorna o id de qual álbum a imagem pertence
     * @return int
     */
    public function getAlbumId() {
        return $this->albumId;
    }
    
    /**
     * Retorna o nome/descrição da imagem
     * @return string
     */
    function getName() {
        return $this->name;
    }

    /**
     * Define o nome/descrição da imagem
     * @param string $name
     */
    function setName($name) {
        $this->name = $name;
    }
    
    /**
     * Retorna o id do usuário a qual a imagem pertence
     * @return int
     */
    function getUserId() {
        return $this->userId;
    }

    /**
     * Define o id do usuário a qual a imagem pertence
     * @param int $userId
     */
    function setUserId($userId) {
        $this->userId = $userId;
    }

    
    
    /**
     * Retorna o url/link da imagem
     * @return URL
     */
    public function getUrl() {
        return $this->url;
    }
    
    /**
     * Verifica e retorna um link válido para a imagem
     * @return \URL
     */
    public function getValidUrl(){
        $file = $this->url->toFile();
        if (!$file->exists()){
            return new URL(IMAGE_DONT_FOUND);
        }
        return $this->getUrl();
    }
    
    /**
     * Gera um link para a imagem
     * @param string $format Formato/extensão da imagem ".jpg",".bmp" ou ".png"
     */
    public function generateURL($format){
        $name = randText(10).$format;
        $this->setUrl(new URL($name, IMAGE_URL_PATH));
    }

    /**
     * Define o id da imagem
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Define o id do álbum a qual a imagem pertence
     * @param int $albumId
     */
    public function setAlbumId($albumId) {
        $this->albumId = $albumId;
    }

    /**
     * Define o url/link da imagem
     * @param URL|string $url
     */
    public function setUrl($url) {
        $this->url = new URL($url);
    }

    /**
     * Retorna o momento em que a imagem foi enviada/registrada
     * @return DateTimeYC
     */
    function getTime() {
        return $this->time;
    }

    /**
     * Define o momento em que a imagem foi enviada para o servidor
     * @param DateTimeYC/string $time
     */
    function setTime($time) {
        $this->time = new DateTimeYC($time);
    }
    
}