<?php

require_once MODELS."PostHistory.php";

/**
 * Classe para lidar com os Históricos de posts no banco de dados
 *
 * @author YouCurte
 */
class PostHistoryDAO extends DAO {
    
    public function __construct() {
        parent::__construct("PostHistory");
    }
    
    /**
     * Save um histórico de post no banco de dados
     * @param PostHistory $postHistory
     */
    public function savePostHistory(PostHistory $postHistory){
        $userId = $postHistory->getUserId();
        $postId = $postHistory->getPostId();
        if ($this->existPostsHistory($userId, $postId)){
            $this->update($postHistory,array("userId","postId"));
        } else {
            $this->insert($postHistory);
        }
    }
    
    /**
     * Verifica se tal usuário já visitou tal post
     * @param int $userId O id do usuário
     * @param int $postId O id do post
     * @return boolean
     */
    public function existPostsHistory($userId,$postId){
        $result=$this->search(array("userId"=>$userId,"postId"=>$postId));
        return $result->getCount()>0;
    }
    
    /**
     * 
     * Lista históricos de posts. Ex de uso:
     * listPostsHistories(1,1); //Retorna o histórico de post de tal usuário com tal post
     * listPostsHistories(null,1); //Retorna todos os históricos de tal post
     * listPostsHistories(1); //Retorna todos os históricos de post com tal usuário
     * 
     * @param int|null $userId O id do usuário (entre com null se não quer limitar por usuário)
     * @param int $postId [opcional] O id do post
     * @return array Um array com os PostHistory's
     */
    public function listPostsHistories($userId,$postId = null,
            $count = COUNT_MAX_RETURN,$offset = 0){
        $array = array();
        if (!is_null($userId)){
            $array["userId"] = $userId;
        } 
        if (!is_null($postId)) {
           $array["postId"] = $postId;
        }
        return $this->search($array,null,$count,$offset)->getData();
    }
    
    
    /**
     * 
     * Retorna a instância principal de PostHistoryDAO
     * 
     * @return PostHistoryDAO
     */
    public static function getInstance() {
       return parent::getInstance();
    }
    
}