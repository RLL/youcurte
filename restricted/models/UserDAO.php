<?php

require_once MODELS . 'DAO.php';
require_once MODELS . 'User.php';
require_once CLASSES . 'DBConnection.php';
require_once CLASSES . 'SearchResult.php';


/**
 * Classe para trabalhar os dados do usuário no banco de dados
 *
 * @author YouCurte
 */
class UserDAO extends DAO {

    public function __construct() {
        parent::__construct("User");
    }

    /**
     * Retorna um objeto de usuário com os dados pego do input enviado por post
     *  ou get
     * @return User
     */
    public function getUserFromInput() {
        return $this->getObjectFromInput();
    }

    /**
     * Retorna o usuário a partir de seu id ou e-mail.
     * @param mix $param (String,int) Id, e-email
     * @param $return [opcional] Que valor deve retorna se não achar um usuário
     * @return User Retorna $return se não achar um usuário
     */
    public function getUser($param, $return = null) {
        try {
            if (is_null($param)) {
                return LoginDAO::getInstance()->getUserLoggedIn();
            } else if ($param instanceof User) {
                return $param;
            } else if (isInt($param)) {
                return $this->searchUserById($param);
            } else if (is_string($param)) {
                return $this->searchUsersByEmail($param);
            } else {
                throw new Exception("Parâmetro '$param' inválido para identificar usuário.");
            }
        } catch (Exception $e) {
            Log::getInstance()->addException($e);
            return $return;
        }
    }

    /**
     * Realiza uma pesquisa por usuários.
     * 
     * @param mixed $where
     * @param mixed $values
     * @param int $count
     * @param int $offset
     * @return SearchResult
     */
    public function searchUser($where, $values = null, $count = COUNT_MAX_RETURN, $offset = 0) {
        $dao = new DAO("UserView");
        $dao->setObjectClass($this->getObjectClass());
        $result = $dao->search($where, $values, $count, $offset);
        return $result;
    }

    /**
     * Pesquisa um usuário por seu id
     * @param int $id Id do usuário
     * @return User
     */
    public function searchUserById($id) {
        $sr = $this->searchUser(array("id" => $id), null, 1);
        if ($sr->getCount() < 1) {
            throw new UserDontFoundException();
        }
        return $sr->getData()[0];
    }

    /**
     * Pesquisa por usuários por nomes
     * 
     * @param String $name Nome que deseja pesquisar
     * @param Int $count (quantidade minima 1)
     * @param Int $offset (Inicia por 0)
     * @return SearchResult
     */
    public function searchUsersByName($name, $count = COUNT_MAX_RETURN, $offset = 0) {
        $parts = explode(" ", $name);
        $where = "active";
        foreach ($parts as $part) {
            if (isNotEmpty($where)) {
                $where.=" and ";
            }
            $where .= "concat(name,' ',lastName) like '%$part%'";
        }
        $where.=" order by name,lastName";
        $result = $this->searchUser($where, null
                , $count, $offset);
        $query = $this->getPDO()->query("select count(id) as 'count' from UserView where " . $where);
        $query->execute();
        $result->setCount($query->fetchAll()[0]['count']);
        return $result;
    }

    /**
     * Pesquisa por usuários por e-mail
     * 
     * @param $email E-email do usuário que deseja procurar
     * @return User Returna null se não encontra um usuário com esse e-email
     */
    public function searchUsersByEmail($email) {
        $sr = $this->searchUser(array("email" => $email));
        return $sr->getCount() == 0 ? null : $sr->getData()[0];
    }
    
    /**
     * Pesquisa por todos os usuários e retorna a quantidade de tudo sem contar o count
     * 
     * @param Int $count (quantidade minima 1)
     * @param Int $offset (Inicia por 0)
     * @return DataInput
     */
    public function searchAllUsers($count = COUNT_MAX_RETURN,$offset = 0){
        $result = $this->search("active order by id desc",null,$count, $offset);
        $result->setCount($this->count("id","active"));
        return $result;
    }

    /**
     * Cadastra um novo usuário
     * 
     * @param User $user
     * @return boolean Se o usuário foi cadastrado com sucesso.
     * @throws EmailAlreadyRegisteredException
     */
    public function addUser(User $user) {
        try {
            $this->validatesUser($user, true, true);
            $user->setConfirmationCode(rand(1000, 999999));
            $result = $this->insert($user);
            UserDAO::getInstance()->getUser($user->getEmail());
            EmailController::getInstance()->sendConfirmationEmail($user);
        } catch (Exception $e) {
            Log::getInstance()->addException($e);
            return false;
        }
        return $result;
    }

    /**
     * Verifica se os dados do usuário são válidos
     * @param User $user O usuário que deseja verificar
     * @param boolean $checkEmail Verificar se e-mail já está cadastrado no banco de dados
     * @param boolean $darErro Lança o exception
     * @return string Retorna um erro ou vazio ('')
     */
    public function validatesUser($user, $checkEmail = true, $darErro = false) {
        $error = "";
        $userLogged = $this->getUser(null);
        if ($checkEmail && ($userLogged == null || ($userLogged->getId() == $user->getId() &&
                $userLogged->getEmail() != $user->getEmail()))) {
            $response = $this->searchUser(array("email" => $user->getEmail()));
            if ($response->getCount() > 0) {
                $error = "O e-mail já está registrado em outro conta!";
            }
        }
        if (($erro = $this->checkFields($user, User::listFieldsNeed())) != false) {
            $error = $erro;
        }
        if (strlen($user->getPassword()) < USER_PASSWORD_MIN_LENGTH ||
                strlen($user->getPassword()) > USER_PASSWORD_MAX_LENGTH) {
            $error = "A senha do usuário deve ter no mínimo " . USER_PASSWORD_MIN_LENGTH .
                    " a " . USER_PASSWORD_MAX_LENGTH . " caracteres.";
        }
        if ($user->getSex() != USER_SEX_MALE && $user->getSex() != USER_SEX_FEMALE) {
            $error = "Valor do sexo/gen&ecirc;ro não é válido.";
        }
        if ($user->getAge() < 16) {
            $error = "Voc&ecirc; deve ter 16 anos ou mais para se cadastrar no YouCurte.";
        }
        if ($darErro && isNotEmpty($error)) {
            throw new Exception($error);
        }
        return $error;
    }

    /**
     * Atualiza os dados do usuário no banco
     * @param User $user O usúario que deseja salvar
     * @return boolean true = sucesso <br> false = fracasso em salvar
     */
    public function updateUser(User $user) {
        if (LoginDAO::getInstance()->isAnyoneLoggedIn()) {
            if ($user->getId() === $this->getUser(null)->getId()) {
                LoginDAO::getInstance()->setUserLoggedIn($user);
            }
        }
        return $this->update($user);
    }

    /**
     * Retorna a instância principal
     * @return UserDAO
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}

/**
 * Exceção para usuário não encontrado
 */
class UserDontFoundException extends Exception {

    /**
     * 
     * Método construtor de exceção para usuário não encontrado
     * 
     * @param type $msg [opcional]
     * @param type $code [opcinal] 
     * @param type $previous [opcional]
     */
    public function __construct($msg = null, $code = null, $previous = null) {
        parent::__construct("Usuário não encontrado. " . $msg, $code, $previous);
    }

}
