<?php

/**
 * View para exibir e editar o perfil do usuário
 *
 * @author YouCurte
 */
class ProfileView extends SubView {

    public function __construct(\View $parent = null, \View $child = null, \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    protected function commands() {
        if (!$this->getMainView()->isVisiting()) {
            WebYouCurte::checkPrivilege(true);
        }
        $this->updateUser();
        $this->getMainView()->loadDefaultDashboard();
        $this->getMainView()->loadDefaultProfilePanel();
        if (!$this->getMainView()->isVisiting()) {
            $this->getMainView()->loadBootStrapDateTimePicker();
            $this->getMainView()->loadBootStrapValidator();
            $head = $this->getMainView()->getHead();
            $head->addJavascript("register.js");
            $head->addStylesheet("register.css");
        }
        return true;
    }

    protected function content() {
        if (!$this->getMainView()->isVisiting()) {
            requireView("register");
            $register = new RegisterView($this, null, $this->getData());
            $register->setUser($this->getMainView()->getUserLoggedIn());
            $register->setButtonCaption("Salvar");
            $register->setEdit(true);
            $register->output();
        } else {
            requireView("posts");
            $subview = new PostsView($this, null, $this->getData());
            $subview->setChild($subview);
            $subview->output();
        }
        return false;
    }

    /**
     * Função para atualizar os dados do usuário logado
     */
    public function updateUser() {
        UserController::getInstance()->updateUserProfile($this);
    }
    
    /**
     * Ação para editar a senha do usuário logado
     */
    public function action_edit_password(){
        UserController::getInstance()->updatePassword($this->getData());
    }

}