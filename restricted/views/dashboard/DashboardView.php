<?php

/**
 * View da dashboard
 *
 * @author YouCurte
 */
class DashboardView extends SubView {

    private $menuItems, $user, $order, $listOrdered;

    public function __construct(\View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
        $this->menuItems = array();
        $this->order = 0;
    }

    /**
     * Retorna o usuário que será exibido os dados
     * @return User
     */
    public function getUser() {
        return $this->user;
    }
    
    /**
     * Define um usuário que será exibido os dados
     * @param User $user
     */
    public function setUser(User $user = null) {
        $this->user = $user;
    }

    /**
     * Adiciona um item no menu
     * @param String $label O rótulo do menu
     * @param String $link O link do item de menu
     * @param boolean $active Se true = ativo ou  flase = não
     *  Indica se é para estar marcado como selecionado
     */
    public function addMenuItem($label, $link, $active = false) {
        $this->menuItems[$label] = array("label" => $label, "link" => $link,
            "active" => $active, "order" => $this->order++);
        if ($active) {
            $this->setMenuItemActived($label);
        }
    }

    /**
     * Edita um item do menu
     * @param string $label O rótulo do menu
     * @param array $item Informações do menu
     */
    public function setMenuItem($label, $item) {
        $this->menuItems[$label] = $item;
    }

    /**
     * Edita um parametro de algum item do menu
     * @param string $label O rótulo do menu
     * @param string $param O nome do parametro que quer editar
     * @param mixed $value O valor para o parametro
     */
    public function setParamMenuItem($label, $param, $value) {
        $this->menuItems[$label][$param] = $value;
    }

    /**
     * Edita o rótulo de algum item do menu
     * @param string $oldLabel O rótulo do item de menu atual
     * @param string $newLabel  O novo rótulo do item
     */
    public function setMenuItemLabel($oldLabel, $newLabel) {
        $this->menuItems[$newLabel] = $this->menuItems[$oldLabel];
        $this->menuItems[$newLabel]['label'] = $newLabel;
        $this->removeMenuItem($oldLabel);
    }

    /**
     * Remove um item do menu
     * @param string $label Rótulo do item
     */
    public function removeMenuItem($label) {
        if (isInt($label)){
            $i = 0;
            foreach ($this->menuItems as $key=>$value){
                if ($i==$label){
                    $label = $key;
                    break;
                }
                $i++;
            }
        }
        unset($this->menuItems[$label]);
    }

    /**
     * Marca um item do menu como selecionado(ativo)
     * @param string $arg Entre com rótulo (label) do menuitem
     */
    public function setMenuItemActived($arg) {
        foreach ($this->menuItems as $key => $item) {
            $this->menuItems[$key]["active"] = false;
        }
        if ($arg !== null) {
            $this->menuItems[$arg]["active"] = true;
        }
    }

    /**
     * Retorna o item de menu que está ativo/selecionado
     * @return array|null Item que está ativo
     *  Se não encontrar ninguem retorna null
     */
    public function getMenuItemActived() {
        foreach ($this->menuItems as $item) {
            if ($item["active"]) {
                return $item;
            }
        }
        return null;
    }

    /**
     * Lista os itens de menu em html
     * Não retorna nada apenas imprime
     */
    public function listMenuItemsInHtml() {
        foreach ($this->listMenuInOrder() as $item) {
            if ($item["active"]) {
                require __dir__ . DS . "_menuItemActive.php";
            } else {
                require __dir__ . DS . "_menuItem.php";
            }
        }
    }

    /**
     * Atualiza informações importantes da lista
     */
    public function update() {
        $active = false;
        foreach ($this->menuItems as $key => $item) {
            if ($item["active"]) {
                $item["active"] = !$active;
                $active = true;
            }
            if ($key !== $item['label']) {
                if (!isset($this->menuItems[$item['label']])) {
                    $this->setMenuItemLabel($key, $item['label']);
                } else {
                    $this->removeMenuItem($key);
                }
            }
        }
    }

    /**
     * Lista os itens de menu em um array
     * @return array
     */
    public function listMenuItems() {
        return $this->menuItems;
    }

    /**
     * Lista os itens de menu de forma ordenada
     * @return array
     */
    public function listMenuInOrder() {
        $list = array();
        foreach ($this->listMenuItems() as $item) {
            if (isset($list[$item["order"]])) {
                $item["order"] ++;
            }
            $list[$item["order"]] = $item;
        }
        ksort($list);
        return $list;
    }

    /**
     * Retorna a quantidade de itens de menu
     * @return int
     */
    public function countMenuItems() {
        return count($this->menuItems);
    }

    protected function commands() {
        return true;
    }

    protected function content() {
        return true;
    }

}