<!--inicio do dashboard-->
<div class="row margin-0"> 
    <div class="col-md-2 well" id="dashboard">
        <center>
            <!-- informações do usuário logado -->
            <img src="<?=$this->getUser()->getImage()?>" alt="imagem de perfil" id="userImage" class="img-thumbnail">
            <div id="userInfo">
                <h3 id="name"><b><?= $this->getUser()->getName() ?></b></h3>
                <span class="label label-primary"><b><?=$this->getUser()->getLabel()?></b></span>
            </div>
            <!-- fim das informações do usuário logado-->
            <!-- items do menu -->
            <ul class="nav nav-pills nav-stacked">
                <?php $this->listMenuItemsInHtml(); ?>
            </ul>
            <!-- fim dos items do menu -->
        </center>
    </div>
</div>
 <!--fim do dashboard-->

