<?php

class PermissionErrorView extends SubView{

    protected function commands() {
        $this->setTitle("YouCurte - Error");
        return true;
    }

    protected function content() {
        return "_permissionError.php";
    }

}