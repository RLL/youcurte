<?php

class Page404View extends SubView{

    protected function commands() {
        $this->setTitle("YouCurte - 404");
        return true;
    }

    protected function content() {
        return "_404.php";
    }

}