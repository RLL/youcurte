<?php

class PermissionErrorView extends SubView{

    protected function commands() {
        $this->setTitle("YouCurte - Error");
        $this->getData()->declareFields("logged");
        if (!$this->getData()->hasField("logged")){
            WebYouCurte::redirect(URL_ERROR_404);
        }
        return true;
    }

    protected function content() {
        return $this->getData()->getField("logged")!='in'?'_in.php':'_out.php';
    }

}