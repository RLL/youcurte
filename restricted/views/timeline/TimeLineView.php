<?php

requireView('post');
require_once CONTROLLERS . 'PostController.php';

/**
 * Description of Home
 *
 * @author YouCurte
 */
class TimeLineView extends SubView {

    private $posts;
    
    public function __construct(View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    /**
     * Lista os posts em html
     * @return A quantidade de posts
     */
    public function listPostsInHtml() {
        $user = $this->getMainView()->getWhoIsBeingVisited();
        $timeline = PostController::getInstance()->getTimeLine($this->getData(), $user);
        foreach ($timeline as $post) {
            $postView = new PostView();
            $postView->setPost($post);
            $postView->output();
        }
        $this->posts = $timeline;
        return count($timeline);
    }

    protected function commands() {
        WebYouCurte::checkPrivilege(true);
        $this->setTitle("YouCurte - Timeline de "
                . $this->getMainView()->getWhoIsBeingVisited()->getFullName());
        $this->getMainView()->loadDefaultDashboard();
        $this->getMainView()->loadBootStrapValidator();
        $this->getMainView()->loadDefaultGPS();
        return true;
    }

    protected function content() {
        require __DIR__.DS."_timeline.php";
        foreach($this->posts as $post){
            PostController::getInstance()->visitedPost($post);
        }
        return false;
    }

}