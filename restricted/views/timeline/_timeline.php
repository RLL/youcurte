<!--inicio timeline-->


<?php
requireView('postFormView', 'post');
if (!$this->getMainView()->isVisiting()) {
    PostFormView::execute();
}
requireView('postEditView', 'post');
$count = $this->listPostsInHtml();
requireView('BottomButtons');
if ($count === 0) {
    if ($this->getData()->getField("page") == 0) {
        echo toPT("<center>Nenhum post encontrado!</center><BR>");
    } else {
        echo toPT("<center>Mais nenhum post encontrado!</center><BR>");
    }   
}
PostEditView::execute();
BottomButtonsView::execute($this, $this->getData(),$count);
?>



<!--fim do timeline-->

