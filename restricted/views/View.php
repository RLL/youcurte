<?php

require_once CLASSES . 'Singleton.php';

/**
 * Classe abstrata para views
 *
 * @author YouCurte
 */
abstract class View extends Singleton {

    private $parent, $child, $visible, $data, $autoRunAction;
    
    /**
     * Método construtor de views
     * @param View $parent A view pai
     * @param View $child A view filha
     * @param DataInput $data Dados de entrada
     */
    public function __construct(View $parent = null, View $child = null, DataInput $data = null) {
        $this->setParent($parent);
        $this->setData($data);
        $this->setChild($child);
        $this->setVisible(true);
        $this->setAutoRunAction(true);
    }

    /**
     * Retorna o dados de entrada
     * @return DataInput
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Verifica se tem dados de entradas
     * @return boolean true = tem <br> false = não tem
     */
    public function hasData() {
        return $this->data != null;
    }

    /**
     * Define os dados de entrada
     * @param DataInput $data Os dados de entrada que deseja definir
     * 
     */
    public function setData(DataInput $data = null) {
        if ($this->data == $data) {
            return;
        }
        $this->data = $data;
        if ($this->hasParent()) {
            $this->getParent()->setData($data);
        }
    }

    /**
     * Verifica se vai executar as ações automaticamente
     * @return boolean true = executar autómaticamente <br> false = não executar
     *  automaticamente
     */
    public function isAutoRunAction() {
        return $this->autoRunAction;
    }

    /**
     * Define se é para executar as ações automaticamente ou não
     * @param boolean $autoRunAction 
     * true = executar autómaticamente <br> false = não executar automaticamente
     */
    public function setAutoRunAction($autoRunAction) {
        $this->autoRunAction = $autoRunAction;
    }

    /**
     * Se é para exibir o conteúdo da view
     * @param boolean $value true = exibir <br> false = não exibir
     */
    public function setVisible($value) {
        $this->visible = $value;
    }
    

    /**
     * Retorna se vai ser exibido o conteúdo da view
     * @return boolean true = vai ser exibido <br> não vai ser exibido
     */
    public function isVisible() {
        return $this->visible;
    }

    /**
     * Função contendo o conteúdo da view
     * @return boolean|string 
     *     Boolean:
     *      true = imprime na tela um arquivo com o mesmo nome da classe. Ex:
     *          Nome da classe: 'HomeView'
     *          Arquivo que vai ser aberto: '_home.php'
     *      false = não abrir nenhum arquivo
     *    String:
     *      O caminho do arquivo(php) que deseja imprimir na tela
     */
    abstract protected function content();

    /**
     * Comandos que devem ser executados antes de mostrar o conteúdo da view
     * @return boolean
     *      true = deve ser mostrado a view
     *      false = ñ mostrar o conteúdo da view
     */
    abstract protected function commands();

    /**
     * Imprime o conteúdo da view na tela
     * @return boolean|string
     *      false = não pode imprimir o conteúdo (não está habilitado para mostrar setVisible)
     *      Se retornar um string vai ser o caminho do arquivo que abriu
     * 
     */
    public function output() {
        if (!$this->isVisible()) {
            return false;
        }
        $content = $this->content();
        $reflectionClass = new ReflectionClass(get_called_class());
        $dir = dirname($reflectionClass->getFileName());
        if ($content === true) {
            $name = strcopy_r(get_called_class(), "", "View");
            $php = new File("_" . $name . ".php", $dir);
            require $php->getValidFile();
        } else if (is_string($content)) {
            require new File($content, $dir);
        } else if ($content instanceof File) {
            require $content;
        }
    }

    /**
     * Roda a view, executando seus comandos e mostrando sua saída (junto com pai e filho)
     */
    public function run() {
        if ($this->isAutoRunAction()) {
            $this->runAction();
        }
        if ($this->commands()) {
            if ($this->hasParent()) {
                $this->getParent()->run();
            } else {
                $this->output();
            }
        }
    }

    /*
     * Executa ações.
     *  Para declarar um ação, nomeia-se uma função por ex:
     *      public function action_login(){ ...
     * @param DataInput $data [opcional] se não informado pega o default ($this->getData)
     */

    public function runAction(\DataInput $data = null) {
        $data1 = $data == null ? $this->getData() : $data;
        if ($data1 == null) {
            return;
        }
        $data1->setCoded(false);
        $action = $data1->getField("action");
        if (isNotEmpty($action)) {
            $methodName = "action_$action";
            if (method_exists($this, $methodName)) {
                call_user_func(array($this, $methodName), $data1);
            }
        }
    }

    /**
     * Atalho para instânciar uma view e rodar seus comandos e mostrar seu 
     *      conteúdo 
     * @params
     *      Seus parametros são os mesmos que o método construtor da view usa
     * @return View
     */
    public static function execute() {
        $rc = new ReflectionClass(get_called_class());
        $view = $rc->newInstanceArgs(func_get_args());
        if ($view->hasParent()) {
            $view->getParent()->setChild($view);
        }
        $view->run();
        return $view;
    }

    /**
     * Retorna a view pai
     * @return View
     */
    public function getParent() {
        return $this->parent != $this ? $this->parent : null;
    }
    
    
    /**
     * Verifica se a view tem um pai
     * @return boolean true = tem pai <br> false = ñ tem  pai
     */
    public function hasParent() {
        return $this->getParent() != null;
    }

    /**
     * Define uma view pai para essa view
     * @param View $parent A view pai
     */
    public function setParent(View $parent = null) {
        if ($parent != null) {
            $this->parent = $parent;
        }
    }

    /**
     * Define uma view como filha dessa
     * @param View $child A view filha
     */
    public function setChild(View $child = null) {
        if ($child != null) {
            $this->child = $child;
        }
    }

    /**
     * Retorna a view filha
     * @return View
     */
    public function getChild() {
        return $this->child;
    }

    /**
     * Verifica se tem a view filha
     * @return boolean true = tem view filha <br> false = não tem view filha
     */
    public function hasChild() {
        return $this->child != null;
    }

    /**
     * Retorna a instância principal da view
     * @return View
     */
    public static function getInstance() {
        return parent::getInstance();
    }

}