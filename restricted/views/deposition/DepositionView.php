<?php

requireView("post");

/**
 * View para cada depoimento
 *
 * @author YouCurte
 */
class DepositionView extends PostView {

    private $deposition;

    /**
     * Método construtor 
     * @param Deposition $deposition O objeto de Deposition (Depoimento)
     * @param View $parent A view pai
     */
    public function __construct(Deposition $deposition = null,View $parent = null) {
        parent::__construct($parent);
        $this->setDeposition($deposition);
        $this->setLikeButtonsVisible(false);
        $this->setKnowMore(false);
        $this->setName("depoimento");
        $this->setViewOptionsVisible(false);
    }
    
    /**
     * Retorna o depoimento que vai ser exibido
     * @return Deposition
     */
    function getDeposition() {
        return $this->deposition;
    }
    
    /**
     * Define o depoimento que será exibido
     * @param Deposition $deposition O depoimento que será exibido
     */
    function setDeposition(Deposition $deposition = null) {
        $this->deposition = $deposition;
        $post = new Post();
        $post->setText($deposition->getText());
        $post->setUserId($deposition->getUserIdFrom());
        $post->setTime($deposition->getTime());
        $post->setId($deposition->getId());
        $this->setPost($post);
        $id = $post->getId();
        $this->setDeleteAction("deleteDeposition('$id')");
        $this->setEditAction("editDeposition('$id')");
    }

    public function commands() {
        return true;
    }

    public function content() {
        return new File("_post.php",VIEWS."post");
    }


    
}