<!-- formulário de depoimento -->
<div class="panel-write-post margin-top-0 <?=$this->isHidden()?'hide':''?>" id="depositionFormDiv">
    <form method="post" enctype="multipart/form-data" id="depositionForm">
        <input type="hidden" name="MAX_FILE_SIZE" value="<?= IMAGE_FILE_SIZE_MAX ?>" />
        <input type="hidden" name="action" value="do_deposition">
        <textarea class="form-control post-message" maxlength="<?= DEPOSITION_TEXT_SIZE_MAX ?>" id="depositionText" name="text" rows="3" placeholder="Faça um depoimento para esse peão..."></textarea>
        <div class="row" id="postBoard">
            <div class="align-right col-lg-12">
                <input type="button" onclick="doDeposition()" class="btn btn-primary" id="postButton" value = "Enviar">
            </div>
        </div>
    </form>
</div>
<!-- fim do formulário de depoimento -->

