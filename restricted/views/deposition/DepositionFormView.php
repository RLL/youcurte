<?php

requireView("PostFormView", "post");

/**
 * View para fazer depoimentos
 *
 * @author Luiz
 */
class DepositionFormView extends PostFormView {

    /**
     * Se é para ficar oculto(=true)
     * @return boolean
     */
    public function isHidden() {
        return !($this->getMainView()->isVisiting() && $this->getMainView()->isAnyoneLoggedIn());
    }

}
