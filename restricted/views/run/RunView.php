<?php

require_once MODELS."PostDAO.php";

/**
 * View para realizar uma ação / não é utilizada por usuário normais
 *
 * @author Luiz
 */
class RunView extends SubView {
    
    public function __construct(View $parent){
           parent::__construct($parent);
    }

    protected function commands() {
        echo "Carregando os posts...";
        $posts = PostDAO::getInstance()->selectFromAll(9999)->getData();
        echo "Atualizando hashtags...";
        foreach ($posts as $post){
            $post->updateHashtags();
        }
        echo "Salvando no banco de dados...";
        foreach ($posts as $post){
            PostDAO::getInstance()->update($post);
        }
        exit();
    }

    protected function content() {
        return false;
    }

}
