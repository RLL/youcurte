<?php

require_once CONTROLLERS."NotificationController.php";

/**
 * View para trabalhar com Navbar
 *
 * @author YouCurte
 */
class NavbarView extends SubView {
    
    public function __construct(\View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
        if (!UserController::getInstance()->isAnyoneLoggedIn()){
            $this->getMainView()->loadBootStrapValidator();
            $this->getMainView()->getHead()->addJavascript("login.js");
            $this->getMainView()->getHead()->addStylesheet("login.css");
        }
    }

    protected function commands() {
        return true;
    }

    protected function content() {
        return true;
    }

}