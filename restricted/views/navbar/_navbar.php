<div class="navbar navbar-default navbar-fixed-top"> <!-- div navbar -->
    <div class="container">
        <div class="navbar-header">
            <?php if ($this->getParent()->hasDashboard()): ?>
            <!-- botão para exibir/ocultar dashboard -->
                <button class="navbar-toggle" type="button" data-toggle="collapse" id="dashboardToggle" alt="Mostrar/ocultar dashboard">
                    <span class="glyphicon glyphicon-th-list"></span>
                </button>
            <!-- fim do botão dashboard -->
            <?php endif; ?>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-youcurte-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand"><img class="icon" src="/img/logo.png" width="128" height="64"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-youcurte-collapse">
            <!-- form de pesquisa -->
            <form class="navbar-form navbar-left" role="search" action="/search">
                <div class="form-group">
                    <input type="text" name="search" id="inputSearch" class="form-control" placeholder="Usuários, hashtags..." value="<?=$this->getData()->getField("search")?>">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
            <!-- fim do form de pesquisa -->
            <ul class="nav navbar-nav  navbar-right" role="search btn-defa">
                <li><a href="/">Início</a></li>
                <?php 
                $notificated = false;
                if (UserController::getInstance()->isAnyoneLoggedIn()) {
                    $notifications = NotificationController::getInstance()->listNotifications();
                    $count = count($notifications);
                    $news = 0;
                    foreach($notifications as $noti){
                        $news+=$noti->wasAlreadyRead()?0:1;
                    }
                    ?>
                    <!-- notificações -->
                    <li class="dropdown">
                        <a id="notification" class="dropdown-toggle click" data-toggle="dropdown" role="button" aria-expanded="false">Notificaç&otilde;es <span class="badge" id="notificationBadge"><?=$news>0?$news:''?></span><span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-note" id="notifications" role="menu">
                            <?php
                            foreach (array_reverse($notifications) as $value):
                                ?>
                                <li class="click <?=!$value->wasAlreadyRead()?'alert-info':''?>" id="notification<?=$value->getId()?>">
                                    <a href="<?=new URL("notification/".$value->getId(),URL_ROOT).""?>" class="inline-block">
                                        <input title="Marcar como <?=$value->wasAlreadyRead()?'não ':''?>lido" type="checkbox" onclick="markNotification(this);" class="form-control inline-block notification-checkbox" number-id="<?=$value->getId()?>" <?=$value->wasAlreadyRead()?'checked':''?>>
                                        <div class="inline-block notification-content">
                                            <div class="inline-block">
                                                <img src="<?=$value->getUserFrom()->getImage()?>" alt="imagem do perfil" class="img-thumbnail user-img-sm">
                                            </div>
                                            <div class="inline-block user-note notification-text">
                                                <?=$value->getText();?><br><?=$value->getTime()->toPT();?>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                
                            <?php 
                                endforeach; 
                                if ($count===0){
                                    echo "<center>Nenhuma nova notificação!</center>";
                                }
                            ?>
                        </ul>
                    </li>
                    <!-- fim das notificações -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Sair&emsp13;<span class="glyphicon glyphicon-log-out"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/logout">Sair apenas local</a></li>
                            <li><a href="/logoutAll">Sair de todos os dispositivos</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li><a href="/register">Cadastrar</a></li>
                    <!-- login -->
                    <li class="dropdown">
                        <a href="#" id="login" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Entrar<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!-- form de login -->
                            <form class="navbar-form form-login" method="post" id="loginForm">
                                <input type="hidden" name="action" value="login">
                                <div class="form-group">
                                    <label for="loginInputEmail" class="col-lg-12">E-mail:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginEmail" type="email" class="form-control" id="loginInputEmail" placeholder="Seu e-mail" value="<?=Input::getInstance()->getParameter("loginEmail")?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="loginInputPassword" class="col-lg-12 margin-top-10">Senha:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginPassword" type="password" class="form-control" id="loginInputPassword" placeholder="Sua senha">
                                    </div>
                                </div>
                                <center>
                                    <div class="margin-top-10 margin-bot-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="auto" class="form-login-check">  Mantenha-me conectado
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <a onclick="forgotPassword()" class="link-blue-and-white click">Esqueceu a senha?</a>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default margin-top-10">Entrar</button>
                                    <?php if ($this->getParent()->hasLoginError()): ?>
                                        <!-- alerta de erro -->
                                        <div class="alert alert-dismissible alert-danger margin-top-10 margin-bot-0">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>Erro:</strong> <?= $this->getParent()->getLoginError() ?>
                                        </div>
                                        <!-- fim do alerta de erro -->
                                    <?php endif ?>
                                </center>
                            </form>
                            <!--fim do form de login-->
                        </ul>
                    </li><!-- fim do login -->
                <?php } ?>
            </ul>
        </div>
    </div>
</div> <!-- fim div navbar -->
