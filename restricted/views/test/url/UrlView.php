<?php


/**
 * Description of UrlView
 *
 * @author Luiz
 */
class UrlView extends SubView {
    protected function commands() {
        return true;
    }

    protected function content() {
        echo "Iniciando o teste da classe URL...<br/>";
        $url_root = $this->getData()->getField("url_root");
        $urlRoot = new URL();
        if ($urlRoot.""!==get_full_path()){
            echo "Não funciona para pegar o url atual!<br/>";
            echo "urlRoot: ".$urlRoot."<br/>";
            echo "full path: ".get_full_path()."<br/>";
        }
        $teste = new URL("teste",URL_ROOT);
        if ($teste.""!==$url_root."teste"){
            echo "Nâo funciona o URL_ROOT:"."<br/>";
            echo $teste."<br/>";
        }
        $urlImg = "http://img.img/novo";
        $img = new URL($urlImg);
        if ($img.""!==$urlImg){
            echo "Links de fora não funciona:"."<br/>";
            echo $urlImg."<br/>";
            echo $img."<br/>";
        }
        $new = new URL($img,URL_ROOT);
        if ($new.""!==$url_root."novo"){
            echo "Problema em substituir o root:"."<br/>";
            echo $new."<br/>";
        }
        $sub = new URL("http://www.fim.com/legal","http://www.inicio.com");
        if ($sub.""!=="http://www.inicio.com/legal"){
            echo "Erro em substituir o root por string:"."<br/>";
            echo $sub."<br/>";
        }
        $root = URL::getRootFromURL();
        $test1 = new URL("http://youcurte.com",$root);
        $test2 = new URL("http://www.youcurte.com",$root);
        if ($test1.""!==$root.""||$test2.""!==$root.""){
            echo "Falha em arrumar o root:<br/>";
            echo "Root: ".$root."<br/>";
            echo $test1."<br/>".$test2."<br/>";
        }
        
        echo "Fim dos testes!";
    }

//put your code here
}
