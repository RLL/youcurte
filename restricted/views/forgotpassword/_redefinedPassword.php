<!-- título da página -->
<div class="page-header">
    <center>
        <h1 id="navbar">Senha redefinida?</h1>
    </center>
</div>
<!-- fim do título da página -->
<div class="col-lg-6 col-lg-offset-3">
    <?php if ($this->getData()->hasField("error")): ?>
    <!-- mensagem de erro -->
        <div class="alert alert-dismissible alert-danger">
            <p><center><strong>Erro:&nbsp;</strong><?= $this->getData()->getField("error"); ?></center></p>
        </div>
    <!-- fim da mensagem de erro -->
    <?php else: ?>
    <!-- mensagem de sucesso -->
    <div class="alert alert-dismissible alert-info">
        <p><center>A senha da conta <?=$this->getData()->getField("email")?> foi redefinida com sucesso!<br> Logue na sua conta.</center></p>
    </div>
    <!-- fim mensagem de sucesso -->
    <?php endif; ?>
</div>
