<!-- título da página -->
<div class="page-header">
    <center>
        <h1 id="navbar">Redefinir senha</h1>
    </center>
</div>
<!-- fim do título da página -->
<div class="col-lg-6 col-lg-offset-3">
    <?php if ($this->getData()->hasField("error")): ?>
        <!-- mensagem de erro -->
        <div class="alert alert-dismissible alert-danger">
            <p><center><?=$this->getData()->getField("error");?></center></p>
        </div>
        <!-- fim da mensagem de erro -->
    <?php endif; ?>
    <?php if ($this->isFormVisible()): ?>
        <div class="alert alert-dismissible alert-info">
            <p>Entre com uma nova senha para a sua conta:</p>
        </div>
        <!-- formulário de mudar a senha -->
        <form class="form-horizontal" method="post" id="registerForm">
            <input type="hidden" class="form-control disabled" name="code" value="<?=$this->getData()->getField("code")?>" readonly>
            <!-- email -->
            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Seu e-mail:</label>
                <div class="col-lg-10">
                    <input type="email"  tabindex="5" class="form-control disabled" name="email" id="inputEmail" value="<?=$this->getData()->getField("email")?>" readonly>
                </div>
            </div>
            <!-- fim do email -->
            <!-- nova senha -->
            <div class="form-group">
                <label for="inputPassword" class="col-lg-2 control-label">Senha:</label>
                <div class="col-lg-10">
                    <input type="password"  tabindex="6" class="form-control" name="password" id="inputPassword" placeholder="Entre com uma senha">
                </div>
            </div>
            <div class="form-group">
                <label for="inputConfirmPass" class="col-lg-2 control-label">Confirmar:</label>
                <div class="col-lg-10">
                    <input type="password" tabindex="7" class="form-control" name="confirmPass" id="inputConfirmPass" placeholder="Repita senha">
                </div>
            </div>
            <!-- fim da nova senha -->
            <div class="form-group">
                <div class="align-right col-lg-12">
                    <input type="submit"  tabindex="8" class="btn btn-primary" value="Continuar">
                </div>
            </div>
        </form>
        <!-- fim do formulário de mudar a senha-->
    <?php endif; ?>
    <a href="/">Vá para o início do site.</a>
</div>
