<?php

/**
 * Description of ForgotPasswordView
 *
 * @author YouCurte
 */
class ForgotPasswordView extends SubView {

    private $formVisible, $redefinedPassword;

    public function __construct(\View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
        $this->formVisible = true;
    }

    protected function commands() {
        $this->setTitle("YouCurte - Esqueceu a senha?");
        WebYouCurte::checkPrivilege(false);
        $this->getMainView()->loadBootStrapValidator();
        $this->getMainView()->getHead()->addJavascript("register.js");
        $data = $this->getData();
        $data->declareFields("user", "code");
        if ($data->hasField("email") && !$data->hasField("code")) {
            EmailController::getInstance()->sendEmailForgotPassword($data);
        } else if (!$data->hasField("code")) {
            $data->setField("error", toPT("O link não é valido!"));
            $this->setFormVisible(false);
        }
        $user = UserController::getInstance()->getUser($data->getField("user"));
        if ($user == null) {
            $data->setField("error", toPT("O usuário não foi encontrado!"));
            $this->setFormVisible(false);
        } else if ($data->getField("code") != $user->getConfirmationCode()) {
            $data->setField("error", toPT("O link não é mais válido!"));
            $this->setFormVisible(False);
        } else {
            $data->setField("email", $user->getEmail());
        }
        if (!$data->hasField("error")
                &&UserController::getInstance()->redefinePassword($data)) {
            $this->setRedefinedPassword(true);
            return true;
        }
        return true;
    }

    protected function content() {
        if ($this->wasRedefinedPassword()) {
            return '_redefinedPassword.php';
        } else {
            return true;
        }
    }

    /**
     * Define se é para exibir o formulário
     * @param boolean $value
     */
    public function setFormVisible($value) {
        $this->formVisible = $value;
    }

    /**
     * Verifica se é para exibir o formulário
     * @return boolean
     */
    public function isFormVisible() {
        return $this->formVisible;
    }

    /**
     * Verifica se a senha já foi trocada
     * @return boolean
     */
    public function wasRedefinedPassword() {
        return $this->redefinedPassword;
    }

    /**
     * Define se a senha já foi modificada
     * @param boolean $redefinedPassword
     */
    public function setRedefinedPassword($redefinedPassword) {
        $this->redefinedPassword = $redefinedPassword;
    }

}