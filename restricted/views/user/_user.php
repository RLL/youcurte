<!-- início do usuário -->
<div class="well yc-well div-user">
    <div class="row">
        <div class="align-top">
            <!-- lado esquerdo do usuário -->
            <div class="col-lg-10 padding-0">
                <!-- avatar do usuário -->
                <div class="inline-block user-img-div">
                    <img src="<?= $this->getUser()->getImage() . "" ?>" alt="imagem do perfil" class="img-thumbnail user-img">
                </div>
                <!-- fim do avatar do usuário -->
                <!-- informações principais do usuário -->
                <div class="inline-block user-info">
                    <a href="/profile/<?= $this->getUser()->getId() ?>"><?= $this->getUser()->getName() . " " . $this->getUser()->getLastName() ?></a><br>
                    <b>Idade:</b> <?= $this->getUser()->getAge() ?> anos<br>
                    <?php if (isNotEmpty($this->getUser()->getCity())): ?>
                        <b>Cidade:</b> <?= $this->getUser()->getCity() ?>
                    <?php endif; ?>
                </div>
                <!-- fim informações principais do usuário -->
            </div>
            <!-- fim do lado esquerdo -->
            <!-- lado direito do usúario -->
            <div class="div-more col-lg-2 padding-0">
                <!-- informações de relacionamentos sobre o usuário-->
                <div class="b-more">
                    <h5><?=$this->getUser()->isBeingFollow()?'<span class="glyphicon glyphicon-star"></span>&emsp14;Voc&ecirc; está seguindo':'&nbsp'?></h5>
                </div>
                <div class="b-more">
                    <h5><?=$this->getUser()->isFollowing()?'<span class="glyphicon glyphicon-hand-right"></span>&emsp14;Está seguindo voc&ecirc;':'&nbsp'?></h5>
                </div>
                <!-- fim das informações de relacionamentos de usúario-->
                <!-- botão saber mais -->
                <div class="b-more know-user">
                    <a href="/profile/<?= $this->getUser()->getId() ?>" class="btn btn-primary btn-button-know-more">Saber mais...</a>
                </div>
                <!-- botão saber mais-->
            </div>
            <!-- fim do lado direito do usuário -->
        </div>
    </div>
</div>
<!-- fim do usuário -->