<?php

/**
 * Description of SmallUserView
 *
 * @author YouCurte
 */
class SmallUserView extends SubView {

    private $user, $postHistory, $post;

    public function __construct(\View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }
    /**
     * Define qual usuário será exibido as informações
     * @return User
     */
    function getUser() {
        return $this->user;
    }

    /**
     * Retorna o usuário que será exibido
     * @param User $user
     */
    function setUser($user) {
        $this->user = $user;
    }

    /**
     * Retorna o histórico do post
     * @return PostHistory
     */
    function getPostHistory() {
        return $this->postHistory;
    }

    /**
     * Retorna o post que será usado no link
     * @return type
     */
    function getPost() {
        if (!isset($this->post)) {
            $this->post = PostController::getInstance()->
                    getPost($this->getPostHistory()->getPostId());
            $this->setPost($this->post);
        }
        return $this->post;
    }

    /**
     * Define o post que será usado no link
     * @param type $post
     */
    function setPost($post) {
        $this->post = $post;
        $this->setUser($post->getUser());
    }

    /**
     * Define o histórico do post
     * @param type $postHistory
     */
    function setPostHistory($postHistory) {
        $this->postHistory = $postHistory;
        $user = UserController::getInstance()->getUser($postHistory->getUserId());
        $this->setUser($user);
    }

    /**
     * Obtém o url para o post ou usuário
     * @return String
     */
    function getUrl() {
        if (isset($this->postHistory)) {
            $url = new URL("profile/" . $this->getUser()->getId(), URL_ROOT);
        } else {
            $url = new URL("post/" . $this->getPost()->getId(), URL_ROOT);
        }
        return $url . "";
    }

    protected function commands() {
        return true;
    }

    protected function content() {
        return true;
    }

}