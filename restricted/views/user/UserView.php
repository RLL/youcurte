<?php

class UserView extends View{
    
    private $user;
    
    /**
     * Define o usuário que é para ser exibido
     * @param User $user
     */
    public function setUser($user){
        $this->user = $user;
    }
    
    /**
     * Obtém o usuário que vai ser exibido
     * @return User
     */
    public function getUser(){
        return $this->user;
    }

    protected function commands(){
        return true;
    }

    protected function content() {
        return true;
    }

}

