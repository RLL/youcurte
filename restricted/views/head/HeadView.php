<?php

require_once VIEWS . 'View.php';

/**
 * View para trabalhar com o head(cabeçalho) da página
 *
 * @author YouCurte
 */
class HeadView extends View {

    private $content, $icon, $title;

    public function __construct() {
        parent::__construct();
        $this->content = array();
    }

    /**
     * Definir o título da página
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
        $data = array("title" => "$title");
        $this->setTag("title", "title", $data);
    }

    /**
     * Retorna o título da página
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Adiciona uma tag no head
     * @param String $tag A tag
     * @param String $data Os dados da tag
     * @param String $name [opcional] Um nome de identificação para a tag
     */
    public function addTag($tag, $data, $name = null) {
        $data = array_merge(array("tag" => $tag), $data);
        if (is_null($name)) {
            $this->content[] = $data;
        } else {
            $this->content[$name . ""] = $data;
        }
    }

    /**
     * Redefine os dados de uma tag
     * @param string $name O nome de identificação da tag
     * @param string $tag A tag
     * @param string $data Os dados da tag
     */
    public function setTag($name, $tag, $data) {
        return $this->addTag($tag, $data, $name);
    }

    /**
     * Remove uma tag por nome
     * @param String $name O nome da tag
     */
    public function removeTagByName($name) {
        unset($this->content[$name . ""]);
    }

    /**
     * Adiciona uma tag link
     * @param String $rel Define o atributo rel
     * @param String $type Define o atributo type
     * @param String $href Define o atributo href (com um link)
     */
    public function addLink($rel, $type, $href) {
        $data = array("rel" => $rel, "type" => $type, "href" => $href);
        $this->addTag("link", $data, $href);
    }

    /**
     * Adiciona uma folha de estilo no cabeçalho (head)
     * @param String $href O link da folha de estilo
     * @param boolean $hasRoot Se o link já tem a raiz do link 
     *      true = tem raiz
     *      false = não tem raiz (adiciona a url dos css)
     */
    public function addStylesheet($href, $hasRoot = false) {
        if (!$hasRoot) {
            $file = new URL($href, URL_CSS);
        } else {
            $file = new URL($href);
        }
        $this->addLink("stylesheet", "text/css", $file);
    }

    /**
     * Adicionar um script no cabeçalho (head)
     * @param String $type Define o atributo type
     * @param String $src Define o atributo src 
     *      true = tem raiz
     *      false = não tem raiz ( adiciona a url do diretório de javascript)
     */
    public function addScript($type, $src) {
        $data = array("type" => $type, "src" => $src);
        $this->addTag("script", $data, $src);
    }

    /**
     * Adicionar um javascript no cabeçalho (head)
     * @param string $src Define o atributo src com um link 
     * @param boolean $hasRoot Se o link tem a raiz
     */
    public function addJavascript($src, $hasRoot = false) {
        $url = new URL($src);
        if (!$hasRoot) {
            $url = new URL($src, URL_JAVASCRIPT);
        }
        $this->addScript("text/javascript", $url);
    }

    protected function commands() {
        return true;
    }

    /**
     * Retorna o URL do icone da página
     * @return URL
     */
    public function getIcon() {
        return $this->icon;
    }

    /**
     * Define o url do icone
     * @param String|URL $icon O url do icone
     */
    public function setIcon($icon) {
        $this->icon = new URL($icon, URL_ROOT);
        $data = array("rel" => "shortcut icon", "type" => "image/x-icon");
        $data["href"] = $icon;
        $this->setTag("icon", "link", $data);
    }

    protected function content() {
        require_once __DIR__.DS.'_head.php';
        foreach ($this->content as $item) {
            $file = new File("_" . $item["tag"] . ".php", VIEWS . "head");
            if ($file->getValidFile()->exists()) {
                require $file;
            }
        }
        return false;
    }

}