<?php

require_once CONTROLLERS."ImageController.php";

/**
 * Description of TmpView
 *
 * @author YouCurte
 */
class TmpView extends SubView {
    
    protected function commands() {
        if (isset($_FILES["file"])){
            $img = ImageController::getInstance()->uploadTempImage();
            echo json_encode_yc(array("result"=>"ok","img"=>$img->toURL().""));
        } else {
            
        }
        return false;
    }

    protected function content() {
        return false;
    }
}