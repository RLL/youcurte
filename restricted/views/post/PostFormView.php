<?php

/**
 * View com o formulário para realizar post
 *
 * @author YouCurte
 */
class PostFormView extends SubView{
    
    private $image;
    
    /**
     * Método construtor
     * @param \View $parent A view pai
     * @param \DataInput $data Os dados de entrada
     */
    public function __construct(\View $parent = null,
            \DataInput $data = null) {
        parent::__construct($parent, null, $data);
        $this->setImage(null);
    }
    
    /**
     * Retorna a imagem anexada ao formulário de post
     * @return type
     */
    function getImage() {
        return $this->image;
    }

    /**
     * Anexa uma imagem no formulário de post
     * @param type $image
     */
    function setImage($image) {
        $this->image = $image;
    }
    
    /**
     * Verifica se tem uma imagem anexada ao formulário
     * @return boolean true = tem <br>
     *      false = não tem
     */
    function hasImage(){
        return $this->image!=null;
    }

        
    protected function commands() {
        return true;
    }

    protected function content() {
        return true;
    }

}