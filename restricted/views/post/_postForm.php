<!-- form de post -->
<div class="panel-write-post">
    <form method="post" enctype="multipart/form-data" id="postForm">
        <?php if (!$this->hasImage()): ?>
            <input type="hidden" name="MAX_FILE_SIZE" value="<?= IMAGE_FILE_SIZE_MAX ?>" />
            <input id="postImage" type="file" class="hide"  name="image">
        <?php else: ?>
            <input id="postImage" type="hidden"  name="imageId" value="<?= $this->getImage()->getId() ?>">
        <?php endif; ?>
        <input type="hidden" name="action" value="post">
        <input type="hidden" name="local" id="inputLocal" value="">
        <input type="hidden" name="latitude" id="inputLatitude" value="">
        <input type="hidden" name="longitude" id="inputLongitude" value="">
        <textarea class="form-control post-message" maxlength="<?= POST_TEXT_SIZE_MAX ?>" id="postText" name="text" rows="3" id="textArea" placeholder="O que voc&ecirc; est&aacute fazendo? Onde voc&ecirc; está? Tem alguma foto macanuda?"></textarea>
        <div class="row" id="postBoard">
            <?php if (!$this->hasImage()): ?>
                <div class="col-lg-2 align-left">
                    <a onclick="selectImage();" id="selectImage" class="inline-block click"><span class="glyphicon glyphicon-picture"></span>&nbsp;Selecionar imagem</a>
                    <a onclick="clearImage();" id="clearImage" class="click"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <div class="col-lg-8 align-left">
                <?php else: ?>
                    <div class="col-lg-10 align-left">
                    <?php endif; ?>
                    <a onclick="checkIn()" class="click" id="location"><span class="glyphicon glyphicon-map-marker"></span>&nbsp;Check-in</a>
                    <a onclick="clearLocation()" id="clearLocation" class="click simple-hide"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <div class="align-right col-lg-2">
                    <input type="button" onclick="post()" class="btn btn-primary" id="postButton" value = "Postar">
                </div>
        </div>
    </form>
</div>
<!-- fim do form de post -->
