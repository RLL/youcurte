<!-- form de edição de post -->
<div class="panel-write-post hide" id="postEditDiv">
    <form method="post" enctype="multipart/form-data" id="editPostForm" class="form">
        <input type="hidden" class="post-edition-id" name="id" value="">
        <textarea class="form-control post-message post-edition-text" maxlength="<?=POST_TEXT_SIZE_MAX?>" name="text" rows="3" placeholder="Entre com uma mensagem"></textarea>
    </form>
</div>
<!-- fim do form de edição de post -->