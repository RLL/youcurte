<?php
if ($this->getPost()->hasMyTri()) {
    $t = 'hide';
    $nt = '';
} else {
    $t = '';
    $nt = 'hide';
}

if ($this->getPost()->hasMyBah()) {
    $b = 'hide';
    $nb = '';
} else {
    $b = '';
    $nb = 'hide';
}

$menu1 = $this->isViewOptionsVisible();
$menu2 = UserController::getInstance()->isAnyoneLoggedIn() && $this->getPost()->getUserId() == UserController::getInstance()->getUserLoggedIn()->getId();
?><div class="well yc-well  <?= $this->getPost()->getUserId() == UserController::getInstance()->getUserIdLoggedIn() ? 'well-blue' : 'well-white' ?>" id="post<?= $this->getPost()->getId() ?>">
<?php if ($this->isMenuEnabled()): ?>
        <!-- opções do post -->
        <div class="dropdown post">
            <?php if ($menu1 || $menu2): ?>
                <span class="dropdown-toggle glyphicon glyphicon-menu-down options-icon" type="button" data-toggle="dropdown" aria-expanded="true"></span>
                <ul class="dropdown-menu dropdown-yc" role="menu">
                    <?php if ($menu1): ?>
                        <!-- opções de visualização -->
                        <li role="presentation"><a role="menuitem" tabindex="0" target="_blank" href="/post/<?= $this->getPost()->getId() ?>">Ver <?= $this->getName() ?> único</a></li>
                        <li role="presentation"><a role="menuitem" class="click" tabindex="1" onclick="copyToClipBoard('<?= $this->getPost()->getURL() . '' ?>')">Copiar link do <?= $this->getName() ?></a></li>
                        <!-- fim das opções de visualização -->
                    <?php endif; ?>
                    <?php if ($menu1 && $menu2): ?>
                        <li role="presentation" class="divider"></li>
                    <?php endif; ?>
                    <?php if ($menu2): ?>
                        <!-- opções de edição -->
                        <li role="presentation"><a role="menuitem" class="click" tabindex="2" onclick="<?= $this->getEditAction() ?>"><span class="glyphicon glyphicon-pencil"></span>&emsp14;Editar <?= $this->getName() ?></a></li>
                        <li role="presentation"><a role="menuitem" class="click" tabindex="3" onclick="<?= $this->getDeleteAction() ?>"><span class="glyphicon glyphicon-trash"></span>&emsp14;Deletar <?= $this->getName() ?></a></li>
                        <!-- fim das opções de edição -->
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
        </div>
        <!-- fim das opções do post -->
    <?php endif; ?>
    <div class="row">
        <!-- imagem do usuário do post -->
        <div class="inline-block">
            <img src="<?= $this->getPost()->getUser()->getImage() . "" ?>" alt="imagem do perfil" class="img-thumbnail user-img">
        </div>
        <!-- fim da imagem do usuário do post -->
        <div class="inline-block user-info">
            <!-- informações do post -->
            <a href="/profile/<?= $this->getPost()->getUser()->getId() ?>"><?= toPT($this->getPost()->getUser()->getName() . " " . $this->getPost()->getUser()->getLastName()) ?></a><br>
            <?= $this->getPost()->getTime()->toPT() ?>
            <?php if ($this->countLikes() > 1): ?>
                <br><br>
                <p><b><?= $this->countLikes() ?></b> pessoas acharam isso tri <span class="glyphicon glyphicon-thumbs-up"></span></p>
            <?php endif; ?>
            <!-- fim das informações do post -->
        </div>
    </div>
    <div class="panel panel-default">
        <!-- corpo do post (com texto e imagem) -->
        <div class="panel-body">
            <?php if ($this->getPost()->getHasImage()): ?>
                <a class="inline-block click" onclick='showImage("<?= $this->getPost()->getImage()->getValidUrl() . "" ?>");'><img src="<?= $this->getPost()->getImage()->getValidUrl() . "" ?>" alt="imagem da postagem" class="img-thumbnail post-img"></a>
            <?php endif; ?>
            <div class="inline-block postText"><?= $this->getPostText() ?></div>
        </div>
        <!-- fim do corpo do post -->
    </div>
    <?php if (isNotEmpty($this->hasFooter())): ?>
        <!-- rodapé -->
        <div class="<?= isNotEmpty($this->getPost()->getLocal()) ? 'post-footer-local' : '' ?> post-footer">
            <?php if (isNotEmpty($this->getPost()->getLocal())): ?>
                <!-- local da publicação -->
                <div class="div-local inline-block align-left post-local">
                    <b>Local da publicação:</b> <?= $this->getPost()->getLocal(); ?>
                </div>
                <!-- fim do local da publicação -->
            <?php endif; ?>
            <div class="panel-button inline-block">
                <?php if ($this->isLikeButtonsVisible()): ?>
                    <!-- botões de like -->
                    <button onclick="markPost(<?= $this->getPost()->getId(); ?>, 'bah');" id="bah<?= $this->getPost()->getId() ?>" class="btn btn-primary align-right <?= $b ?>">Xucro <span class="glyphicon glyphicon-thumbs-down"></span></button>
                    <button onclick="markPost(<?= $this->getPost()->getId(); ?>, 'notbah');" id="notbah<?= $this->getPost()->getId() ?>" class="btn btn-default active align-right <?= $nb ?>">Xucro <span class="glyphicon glyphicon-thumbs-down"></span></button>
                    <button onclick="markPost(<?= $this->getPost()->getId(); ?>, 'tri');" id="tri<?= $this->getPost()->getId() ?>" class="btn btn-primary align-right <?= $t ?>">Tri <span class="glyphicon glyphicon-thumbs-up"></span></button>
                    <button onclick="markPost(<?= $this->getPost()->getId(); ?>, 'nottri');" id="nottri<?= $this->getPost()->getId() ?>" class="btn btn-default align-right active <?= $nt ?>">Tri <span class="glyphicon glyphicon-thumbs-up"></span></button>
                    <!-- fim dos botões de like -->
                <?php endif; ?>
                <?php if ($this->hasKnowMore()): ?>
                    <!-- botão saber mais -->
                    <a href="#" class="btn btn-primary align-right">Saber mais...</a>
                    <!-- fim do botão saber mais -->
                <?php endif; ?>
            </div>
        </div>
        <!-- fim do rodapé -->
    <?php endif; ?>
</div>