<?php
$posts = array();
if (isNotEmpty($this->getPost()->getLocal())) {
    $posts1 = PostController::getInstance()->
            listPostsFromLocal($this->getPost()->getLocal());
    foreach ($posts1 as $post) {
        if ($post->getId()!==$this->getPost()->getId()){
            $posts[] = $post;
        }
    }
}
$users = PostController::getInstance()->
        listLastUsersWhoHaveVisited($this->getPost());
if (isEmpty($users)) {
    $users = array($this->getMainView()->getUserLoggedIn());
}
?>
<!-- últimos usuários que viram -->
<div class="panel panel-default">
    <div class="panel-body">
        Os últimos usuários que viram o post:
    </div>
    <div class="panel-footer">
        <?php
        foreach ($users as $user) {
            $userView = new SmallUserView();
            if ($user instanceof User) {
                $userView->setUser($user);
            } else {
                $userView->setPostHistory($user);
            }
            $userView->output();
        }
        ?>
    </div>
</div>
<!-- fim dos últimos usuários que viram -->
<?php if (isNotEmpty($posts)): ?>
    <!-- últimos usuários que postaram no mesmo local -->
    <div class="panel panel-default">
        <div class="panel-body">
            Os últimos que postaram no mesmo local:
        </div>
        <div class="panel-footer">
            <?php
            foreach ($posts as $post) {
                $user = new SmallUserView();
                $user->setPost($post);
                $user->output();
            }
            ?>
        </div>
    </div>
    <br><br>
    <!-- fim dos últimos usuários que postaram no mesmo local -->
<?php endif; ?>

<?php if ($this->getMainView()->isAnyoneLoggedIn()){
    requireView("PostEditView","post");
    PostEditView::execute();
}?>
