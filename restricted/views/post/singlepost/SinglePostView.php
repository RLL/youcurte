<?php

require_once VIEWS . 'post' . DS . 'PostView.php';
require_once MODELS . 'PostDAO.php';
requireView('smallUserView', 'user');

/**
 * View para exibir um único post
 *
 * @author Luiz Henrique
 */
class SinglePostView extends SubView {

    private $post;

    public function __construct(\View $parent = null, \View $child = null, \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
        $this->setTitle("YouCurte");
        $this->getData()->declareFields("postId");
        $this->setAutoRunAction(false);
    }

    protected function commands() {
        $controller = PostController::getInstance();
        if (!$this->getData()->hasField("postId") ||
                !isInt($this->getData()->getField("postId"))) {
            YouCurte::redirect(URL_ERROR_POST_DONT_FOUND);
        }
        $post = $controller->getPost($this->getData()->getField("postId"));
        if ($post === null) {
            YouCurte::redirect(URL_ERROR_POST_DONT_FOUND);
        }
        $this->setPost($post);
        $this->runAction();
        $this->getMainView()->loadDefaultDashboard();
        if ($this->getMainView()->isAnyoneLoggedIn()) {
            $controller->visitedPost($post);
        }
        return true;
    }

    protected function content() {
        $post = new PostView();
        $post->setPost($this->getPost());
        $post->output();
        return true;
    }

    /**
     * Ação para marcar o post como 'Mas Bah'
     */
    public function action_bah() {
        $this->setOpinion(null, 1);
    }

    /**
     * Ação para desmarcar o post como 'Mas Bah'
     */
    public function action_notbah() {
        $this->setOpinion(null, 0);
    }

    /**
     * Ação para marcar o post com o 'Tri'
     */
    public function action_tri() {
        $this->setOpinion(1, null);
    }

    /**
     * Ação para desmacar o post como 'Tri'
     */
    public function action_nottri() {
        $this->setOpinion(0, null);
    }

    public function action_delete() {
        $data = $this->getData();
        $data->declareFields("postId");
        $postId = $data->getField("postId");
        PostController::getInstance()->deletePost($postId);
    }

    public function setOpinion($tri, $bah) {
        $this->getData()->declareFields("postId");
        $postId = $this->getData()->getField("postId");
        PostController::getInstance()->setOpinion($postId, $tri, $bah);
    }

    function getPost() {
        return $this->post;
    }

    function setPost($post) {
        $this->post = $post;
    }

}
