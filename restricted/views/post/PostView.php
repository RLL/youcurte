<?php

require_once CONTROLLERS . 'PostController.php';

/**
 * Description of Home
 *
 * @author Luiz Henrique
 */
class PostView extends View {

    protected $likeButtons, $enableMenu, $knowMore, $countLikes, $post, $name,
            $deleteAction, $editAction, $viewOptionsVisible;

    /**
     * Método construtor
     * 
     * @param \View $parent
     */
    public function __construct(\View $parent = null) {
        parent::__construct($parent);
        $this->setLikeButtonsVisible(true);
        $this->setEnableMenu(true);
        $this->setKnowMore(false);
        $this->setCountLikes(10);
        $this->setName("post");
        $this->setViewOptionsVisible(true);
    }

    /**
     * Define o post que a view deverá mostrar
     * @param Post $post
     */
    public function setPost(Post $post) {
        $this->post = $post;
        $this->setCountLikes($post->getTri());
        $this->setEditAction("editPost('" . $this->getPost()->getId() . "')");
        $this->setDeleteAction("deletePost('" . $this->getPost()->getId() . "')");
    }

    /**
     * Retorna o post que a view vai mostrar
     * @return Post
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * Retorna o código html de um hashtag
     * @param String $tag A hashtag
     * @return String o código html
     */
    private function getHashtag($tag) {
        $start2 = "<span class='label label-primary hashtag-span hash" . $tag . "' hash='$tag'>";
        $start1 = "<a onclick='searchTag(this)' class='click hashtag'>";
        return $start1 . $start2 . "#" . $tag . "</span></a>";
    }

    /**
     * Retorna o texto do post com as hashtags formatadas
     * @return String
     */
    public function getPostText() {
        $alpha = HASHTAGS_ALPHABET;
        $content = $this->getPost()->getText();
        $iframe = "";
        if (strpos($content,"http:")===0){
            $link = explode(" ",$content)[0];
            $value = strcopy_l($link,"v=","&");
            $iframe = '<iframe class="video" width="420" height="315" src="https://www.youtube.com/embed/'.$value.'" frameborder="0" allowfullscreen></iframe>';
            $text = substr($content,strlen($link)+1);
        } else {
            $text = $content;
        }
        if (strpos($text, "<luiz>") === 0) {
            $text = substr($text,6);
        } else {
            $text = toPT(adaptTags($text));
        }
        $size = strlen($text);
        $isTag = false;
        $tag = "ab";
        $newText = '';
        $hash = false;
        for ($i = 0; $i < $size; $i++) {
            $char = $text[$i];
            $hasAlpha = strstr($alpha, $char) || strstr($alpha, strtolower($char));
            if ($char == "#" && $hash) {
                $newText.="#";
            } else if ($char == "#" && $isTag) {
                $isTag = false;
                $newText.="#" . $tag . "#";
            } else if ($char == "#") {
                $isTag = true;
                $tag = "";
                $hash = true;
            } else if ($hasAlpha && $isTag) {
                $tag.=$char;
                $hash = false;
            } else if (!$hasAlpha && $isTag) {
                $newText.=$this->getHashtag($tag) . $char;
                $hash = false;
                $isTag = false;
            } else {
                $newText.=$char;
                $hash = false;
            }
        }
        if ($isTag) {
            $newText.=$this->getHashtag($tag);
        }
        return str_replace("\n", "<br>", $newText)."<br/>".$iframe;
    }

    public function commands() {
        return true;
    }

    /**
     * Define a quantidade de likes (tri) que o post tem
     * @param int $value A quantidade
     */
    public function setCountLikes($value) {
        $this->countLikes = $value;
    }

    protected function content() {
        require "_post.php";
        return false;
    }

    /**
     * Exibir os botões de curtir (tri) e (mas bah!)
     * @param boolean $value
     *      true - exibir
     *      false - ocultar
     */
    public function setLikeButtonsVisible($value) {
        $this->buttonVisible = $value;
    }

    /**
     * 
     * @return boolean Se os botões curtir (tri) e (mas bah!) estão visiveis
     *          true - visiveis
     *          false - ocultos
     */
    public function isLikeButtonsVisible() {
        return $this->buttonVisible && UserController::getInstance()->isAnyoneLoggedIn();
    }

    /**
     * 
     * @return int Retorna quantas vezes o post foi curtido
     */
    public function countLikes() {
        return $this->countLikes;
    }

    /**
     * 
     * @return string Retorna o local onde foi publicado esse post
     */
    public function getLocal() {
        return $this->getPost()->getLocal();
    }

    /**
     * Se é para exibir o menu com opções
     *      true = exibir <br> false = não exibir
     * @return boolean
     */
    public function isMenuEnabled() {
        return $this->enableMenu;
    }

    /**
     * Define se é para exibir o menu de opções
     * @param boolean $value true = exibir <br> false = não exibir
     */
    public function setEnableMenu($value) {
        $this->enableMenu = $value;
    }

    /**
     * Se tem o botão exibir mais
     * @return boolean 
     */
    public function hasKnowMore() {
        return $this->knowMore;
    }

    /**
     * Se é para exibir o botão de saber mais
     * @param boolean $knowMore
     */
    public function setKnowMore($knowMore) {
        $this->knowMore = $knowMore;
    }

    /**
     * Se é para exibir o rodapé com os botões e locais da publicação
     * @return boolean
     *      true = exibir <br> false = não exibir
     */
    public function hasFooter() {
        return $this->hasKnowMore() || isNotEmpty($this->getLocal()) ||
                $this->isLikeButtonsVisible();
    }

    /**
     * Retorna o nome do usuário
     * @return String
     */
    function getName() {
        return $this->name;
    }

    /**
     * Define o nome do usuário
     * @param String $name O nome do usuário
     */
    function setName($name) {
        $this->name = $name;
    }

    /**
     * Retorna a função em javascript para realizar a ação de deletar
     * @return type
     */
    function getDeleteAction() {
        return $this->deleteAction;
    }

    /**
     * Retorna a função do javascript para realizar a ação de editar
     * @return type
     */
    function getEditAction() {
        return $this->editAction;
    }

    /**
     * Define a função de edição no javascript
     * @param string $deleteAction A ação
     */
    function setDeleteAction($deleteAction) {
        $this->deleteAction = $deleteAction;
    }

    /**
     * Define a função de edição de ação
     * @param string $editAction A ação
     */
    function setEditAction($editAction) {
        $this->editAction = $editAction;
    }

    /**
     * Se as opções da view estão visíveis
     * @return boolean true = visível <br> false = não visível
     */
    function isViewOptionsVisible() {
        return $this->viewOptionsVisible;
    }

    /**
     * Define se é para exibir as opções da view
     * @param boolean $viewOptionsVisible true = visível <br> false = não visível
     */
    function setViewOptionsVisible($viewOptionsVisible) {
        $this->viewOptionsVisible = $viewOptionsVisible;
    }

}
