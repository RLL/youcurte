<?php

requireView('post');

/**
 * Description of Home
 *
 * @author YouCurte
 */
class HomeView extends SubView {
    
    public function __construct(\View $parent = null, \View $child = null,
            DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    protected function commands() {
        WebYouCurte::checkPrivilege(false);
        $this->getMainView()->loadBootStrapDateTimePicker();
        $this->getMainView()->loadBootStrapValidator();
        $this->getMainView()->getHead()->addJavascript("register.js");
        return true;
    }

    protected function content() {
        return true;
    }

}