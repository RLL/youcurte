<!-- títuo da página -->
<div class="page-header">
    <center>
        <h1 id="navbar">Bem-vindo ao YouCurte!</h1>
    </center>
</div>
<!-- fim do título da página -->
<!-- conteúdo da página home -->
<div class="row padding-horizontal-default margin-0">
    <div class="col-lg-7">
        <legend>Postagens mais tri, tch&ecirc;!</legend>
        <!-- lista de posts mais tris -->
        <?php
        $posts = PostController::getInstance()->searchPosts("1 order by tri desc",null,POST_NUMBER_MOST_TRI);
        foreach ($posts as $key => $post) {
            $postView = new PostView();
            $postView->setLikeButtonsVisible(false);
            $postView->setPost($post);
            $postView->output();
            if ($key == 2) {
                break;
            }
        }
        ?>
        <!-- fim da lista de posts mais tris -->
    </div>
    <!-- formulário de inscrição rápida -->
    <div class="col-lg-5">
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" action="/register" method="post" id="registerForm">
                    <input type="hidden" name="register" value="1">
                    <fieldset class="">
                        <center>
                            <legend><h3 class="align-center" id="titleRegister">Abra uma conta agora mesmo!</h3></legend>
                        </center>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-3 control-label">Nome</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" id="inputName" placeholder="Seu nome" name="name" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-3 control-label">Sobrenome</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" id="inputLastName" placeholder="Seu sobrenome" name="lastName" tabindex="2">
                            </div>
                        </div>
                        <div class="form-group input-birth-email">
                            <label for="inputEmail" class="col-lg-3 control-label">E-mail</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" id="inputEmail" placeholder="seu@mail.com" name="email" tabindex="3">
                            </div>
                        </div>
                        <!-- genero -->
                        <div class="form-group sex-div">
                            <label class="col-lg-3 control-label">Sexo</label>
                            <div class="col-lg-9">
                                <div class="radio">
                                    <div class="col-lg-5">
                                        <label>
                                            <input type="radio" name="sex" id="optionsRadios1" value="<?= USER_SEX_FEMALE ?>" checked="" tabindex="4">
                                            Feminino
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>
                                            <input type="radio" name="sex" id="optionsRadios2" value="<?= USER_SEX_MALE ?>">
                                            Masculino
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fim do genero -->
                        <div class="form-group input-birth-email">
                            <label for="inputEmail" class="col-lg-5 control-label">Data de Nascimento</label>
                            <div class="col-lg-7">
                                <div class='input-group date' id='inputBirthDiv'>
                                    <input type='text' class="form-control" name="birthDate" id="inputBirth" placeholder="dia/m&ecirc;s/ano" tabindex="5">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- senha -->
                        <div class="form-group">
                            <label for="inputPassword" class="col-lg-3 control-label">Senha</label>
                            <div class="col-lg-9">
                                <input type="password"  tabindex="6" class="form-control" name="password" id="inputPassword" placeholder="Entre com uma senha" tabindex="6">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputConfirmPass" class="col-lg-3 control-label">Confirmar</label>
                            <div class="col-lg-9">
                                <input type="password" tabindex="7" class="form-control" name="confirmPass" id="inputConfirmPass" placeholder="Repita senha" tabindex="7"> 
                            </div>
                        </div>
                        <!-- fim da senha -->
                        <div class="form-group">
                            <div class="col-lg-12">
                                <center>
                                    <button type="submit" id="registerButton" class="btn btn-primary" tabindex="8">Cadastrar-me</button>
                                </center>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <!-- formulário de inscrição rápida -->
</div>
<!-- fim do conteúdo da página home -->