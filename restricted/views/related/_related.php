<div class="page-header sub-profile-header">
    <center>
        <h1 id="navbar">Parceiros do Mat&ecirc;</h1>
    </center>
</div>
<?php
requireView('user');
$bool = false;
$users = $this->getUsers();
foreach ($users as $user) {
    if ($user !== null) {
        $post = new UserView();
        $post->setUser($user);
        $post->output();
        $bool = true;
    }
}
if (!$bool) {
    if (!$this->getMainView()->isVisiting()) {
        echo "<center>Voc&ecirc; está relacionado com mais ninguém!</center><br> ";
    } else {
        echo "<center>Está relacionado com ninguém!</center>";
    }
}
requireView("BottomButtons");
$bottom = new BottomButtonsView(null, $this->getData(),count($users));
$bottom->setNextLabel("Próximo");
$bottom->setPrevLabel("Anterior");
$bottom->output();
?>
