<?php

/**
 * View que exibi os usuários relacionados com usuário logado (Parceiros de Mat&ecirc;)
 *
 * @author YouCurte
 */
class RelatedView extends SubView {

    private $user;

    public function __construct(\View $parent = null, \View $child = null, \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    protected function commands() {
        if (!$this->getMainView()->isVisiting()) {
            WebYouCurte::checkPrivilege(true);
        }
        $this->user = $this->getMainView()->getWhoIsBeingVisited();
        $this->getMainView()->loadDefaultDashboard();
        $this->getMainView()->loadDefaultProfilePanel();
        return true;
    }

    protected function content() {
        return true;
    }

    /**
     * Retorna os usuários relacionados com o usuário logado
     * @return array
     */
    public function getUsers() {
        return UserController::getInstance()->listRelatedUsers($this->getData(),
                $this->user);
    }

}
