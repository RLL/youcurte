<!-- título da página -->
<div class="page-header sub-profile-header">
    <center>
        <?php if ($this->getMainView()->isVisiting()): ?>
            <h1>Depoimentos:</h1>
        <?php else: ?>
            <h1>Depoimentos recebidos:</h1>
        <?php endif; ?>
    </center>
</div>
<!-- fim do título da página -->
<?php
requireView('depositionFormView', 'deposition');
$formView = new DepositionFormView($this);
$formView->output();
$count = $this->listDepositionsInHtml();
requireView('BottomButtons');
if ($count === 0) {
    if ($this->getData()->getField("page") == 0) {
        echo toPT("<center>Nenhum depoimento encontrado!</center><BR>");
    } else {
        echo toPT("<center>Mais nenhum depoimento encontrado!</center><BR>");
    }
}
$this->getData()->setField("count", $count);
requireView('postEditView', 'post');
PostEditView::execute();
BottomButtonsView::execute(null, $this->getData());
?>