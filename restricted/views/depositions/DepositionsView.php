<?php

require_once CONTROLLERS . "DepositionController.php";
requireView("deposition");

/**
 * View para exibir depoimentos (página de depoimentos)
 *
 * @author YouCurte
 */
class DepositionsView extends SubView {

    public function __construct(\View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    public function commands() {
        if (!$this->getMainView()->isVisiting()) {
            WebYouCurte::checkPrivilege(true);
        }
        $this->getMainView()->loadDefaultDashboard();
        $this->getMainView()->loadDefaultProfilePanel();
        return true;
    }
     
    /**
     * Lista os depoimentos em html
     * @return int A quantidade de depoimentos
     */
    public function listDepositionsInHtml() {
        $user = $this->getMainView()->getWhoIsBeingVisited();
        $depositions = DepositionController::getInstance()->listDepositions(
                $user, $this->getData());
        foreach ($depositions as $deposition) {
            $view = new DepositionView($deposition);
            $view->output();
        }
        return count($depositions);
    }

    public function content() {
        return true;
    }
    
    /**
     * Ação de realizar um depoimento
     */
    public function action_do_deposition() {
        DepositionController::getInstance()->doDeposition($this->getMainView());
        $this->getMainView()->updateCache(true);
    }

    /**
     * Ação de deletar um depoimento
     */
    public function action_delete_deposition() {
        DepositionController::getInstance()->deleteDeposition($this->getMainView());
        $this->getMainView()->updateCache(true);
    }

    /**
     * Ação de editar um depoimento
     */
    public function action_edit_deposition() {
        DepositionController::getInstance()->editDeposition($this->getMainView());
    }

}