<?php

/**
 * View dos botões "Mais Recentes" e "Mais Antigos"
 *
 * @author YouCurte
 */
class BottomButtonsView extends View {

    private $countMax, $count, $link, $nextLabel, $prevLabel;

    /**
     *  Método construtor
     *  
     * @param \View $parent [Opicional]A view pai dessa view
     * @param \DataInput [Opicional]$data Os dados de entrada
     * @param int $count [Opicional]A quantidade de dados
     * @param string $link [Opicional]O link atual
     */
    public function __construct(\View $parent = null, \DataInput $data = null, $count = null, $link = null) {
        parent::__construct($parent, null, $data);
        $this->setCountMax(COUNT_MAX_RETURN);
        $this->count = $count;
        $this->link = $link;
        $this->nextLabel = "Mais antigos";
        $this->prevLabel = "Mais recentes";
    }

    /**
     * Retorna a entrade de dados
     * @return InputData
     */
    public function getData() {
        if (parent::getData() == null && $this->getParent() != null) {
            return $this->getParent()->getData();
        } else {
            return parent::getData();
        }
    }

    protected function commands() {
        $this->getData()->prepareOffsetAndCount();
        return true;
    }

    protected function content() {
        return true;
    }

    public function run() {
        $this->output();
    }

    /**
     * Retorna a quantidade máxima que pode ter
     * @return int
     */
    function getCountMax() {
        return $this->countMax;
    }

    /**
     * Define a quatidade máxima que pode ter
     * @param int $countMax
     */
    function setCountMax($countMax) {
        $this->countMax = $countMax;
    }

    /**
     * Retorna a quantidade
     * @return int
     */
    function getCount() {
        if ($this->count === null) {
            return $this->getData()->getField("count");
        }
        return $this->count;
    }

    /**
     * Define a quantidade corrente
     * @param int $count
     */
    function setCount($count) {
        $this->count = $count;
    }

    /**
     * Pega o link atual
     * @return string
     */
    function getLink() {
        return $this->link;
    }

    /**
     * Define o link atual
     * @param string $link
     */
    function setLink($link) {
        $this->link = $link;
    }

    /**
     * Retorna o link válido para a criação do próximo e anterior link
     * @return string
     */
    public function getValidLink() {
        if ($this->link !== null) {
            return $this->link . "&";
        } else {
            return URL::getFromURL() . "?";
        }
    }

    /**
     * 
     * Pega o próximo link (Mais antigos)
     * @return string
     */
    public function getNextLink() {
        return $this->getValidLink() . "page=" . (1 + $this->getPage());
    }

    /**
     * Pega o link anterior (Mais recentes)
     * @return string
     */
    public function getPrevLink() {
        return $this->getValidLink() . "page=" . ($this->getPage() - 1);
    }

    /**
     * Retorna o número da página atual
     * @return int
     */
    public function getPage() {
        return $this->getData()->getField("page");
    }

    /**
     * Retorna o rótulo do botão proximo
     * @return string
     */
    function getNextLabel() {
        return $this->nextLabel;
    }

    /**
     * Retorna o rótulo do botão anterior
     * @return string
     */
    function getPrevLabel() {
        return $this->prevLabel;
    }

    /**
     * Define o rótulo do botão proximo
     * @param string $nextLabel
     */
    function setNextLabel($nextLabel) {
        $this->nextLabel = $nextLabel;
    }

    /**
     * Define o rótulo do botão anterior
     * @param string $nextLabel
     */
    function setPrevLabel($prevLabel) {
        $this->prevLabel = $prevLabel;
    }

}