<!-- botões inferiores de mais antigos ou mais recentes -->
<div class="bottom-buttons align-center">
    <?php if ($this->getData()->getField("page")>0): ?>
    <!-- botão mais recentes-->
    <a href="<?= $this->getPrevLink() ?>" class="btn btn-primary inline-block"><span class="glyphicon glyphicon-chevron-left"></span><?=$this->getPrevLabel()?></a>
    <!-- fim do botão mais recentes -->
    <?php endif; ?>
    <?php if ($this->getCount()>=$this->getCountMax()): ?>
    <!-- botão mais antigos -->
    <a href="<?=$this->getNextLink()?>" class="btn btn-primary inline-block"><?=$this->getNextLabel()?><span class="glyphicon glyphicon-chevron-right"></span></a>
    <!-- fim do botão mais antigso -->
    <?php endif; ?>
</div>
<!-- fim dos botões inferiores -->