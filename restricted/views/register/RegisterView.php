<?php

require_once MODELS . 'User.php';

/**
 * View para cadastrar um novo usuário
 *
 * @author YouCurte
 */
class RegisterView extends SubView {

    private $user, $buttonCaption, $edit;

    public function __construct(View $parent = null, View $child = null, DataInput $data) {
        parent::__construct($parent, $child, $data);
        $this->setTitle("YouCurte - Cadastrar");
        $control = UserController::getInstance();
        if (!$data->hasField("user")) {
            if ($control->isAnyoneLoggedIn()) {
                $user = $control->getUserLoggedIn();
            } else {
                $user = $control->getUserFromInput();
                $user->settoSendEmails($this->getData()->getField("nottoSendEmails")!="1");
            }
        } else {
            $user = $data->getField("user");
        }
        $this->setUser($user);
        $this->setEdit(false);
        $this->setButtonCaption("Concluir Cadastro");
    }

    /**
     * Retorna o rótulo do botão Salvar/Cadastrar/Concluir
     * @return String
     */
    public function getButtonCaption() {
        return $this->buttonCaption;
    }

    /**
     * Define o rótulo do botão Salvar/Cadastrar/Concluir
     * @param String $buttonCaption
     */
    public function setButtonCaption($buttonCaption) {
        $this->buttonCaption = $buttonCaption;
    }

    /**
     * Define de qual usuário os dados serão coletados para preencher os campos
     *  no formulário
     * @param User $user
     */
    public function setUser(User $user) {
        $this->user = $user;
    }

    /**
     * Retorna o usuário que está editando ou um objeto de User com os dados que 
     *  foram preenchidos pelo formulário
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Define uma mensagem de erro
     * @param string $msg A mensagem
     */
    public function setErrorMessage($msg) {
        $this->getData()->setField("registerError", $msg);
    }

    /**
     * Retorna a mensagem de erro
     * @return string
     */
    public function getErrorMessage() {
        return $this->getData()->getField("registerError");
    }

    /**
     * Verificar se tem erro (messagem de erro)
     * @return boolean (true = tem erro) (false = não tem erro)
     */
    public function hasError() {
        return isNotEmpty($this->getErrorMessage());
    }

    protected function commands() {
        WebYouCurte::checkPrivilege(false);
        $controller = UserController::getInstance();
        $path = Path::getFromURL();
        if ($controller->isAnyoneLoggedIn() && $path->getLastPart() == "register") {
            YouCurte::redirect(URL_ROOT);
        }
        $user = $this->getUser();
        $data = $this->getData();
        if ($data->getField("register") == 1) {
            $this->checkFields();
            if (!$this->hasError()) {
                if ($data->getField("password") == $data->getField("confirmPass")) {
                    if ($controller->registerUser($user, $this->getData())) {
                        $this->getData()->setField("register", "2");
                    }
                } else {
                    $this->setErrorMessage("As senhas não conferem!");
                }
            }
        }
        $this->loadHead();
        return true;
    }

    /**
     * Adiciona scripts e folhas de estilo no cabeçalho
     */
    public function loadHead() {
        $this->getMainView()->getHead()->addStylesheet("register.css");
        if ($this->getData()->getField("register") != 2) {
            $this->getMainView()->loadBootStrapValidator();
            $this->getMainView()->loadBootStrapDateTimePicker();
            $this->getMainView()->getHead()->addJavascript("register.js");
        }
    }

    protected function content() {
        $registred = $this->getData()->getField("register") == 2;
        if (!$registred) {
            return '_register.php';
        } else {
            return '_registered.php';
        }
    }

    /**
     * Verifica se os campos submetidos tem valores válidos
     */
    private function checkFields() {
        $user = $this->getUser();
        $controller = UserController::getInstance();
        $this->setErrorMessage($controller->checkFields(
                        $user, $user->listFieldsNeed(), array("primeiro nome", "sobrenome", "e-email",
                    "senha", "data de nascimento")
                        , "b"));
        if (isEmpty($this->getErrorMessage())) {
            $this->setErrorMessage($this->getData()->checkFields(
                            array("confirmPass"), array("confirmar senha")), "b");
        }
    }

    /**
     * Define se está editando um perfil (e não cadastrando um novo usuário)
     * @param booelan $value
     *  true = editando <br> false = cadastrando
     */
    public function setEdit($value) {
        $this->edit = $value;
    }

    /**
     * Retorna se está editando um perfil (e não cadastrando)
     * @return boolean
     *       true = editando <br> false = cadastrando
     */
    public function isEdit() {
        return $this->edit;
    }

}