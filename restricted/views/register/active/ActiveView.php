<?php

require_once CONTROLLERS.'EmailController.php';

/**
 * View para ativar a conta de um usuário e informar se o mesmo conseguiu ativar
 *
 * @author Luiz Henrique
 */
class ActiveView extends SubView{
    
    private $actived;
    
    protected function commands() {
        $data = $this->getData();
        $data->declareFields("user", "cod");
        if ($data->getField("cod")==="luiz"){
            $user = UserController::getInstance()->getModel()->search(
                    array("email"=>$data->getField("user")))->getData(0);
            $data->setField("user", $user->getId());
            $data->setField("cod", $user->getConfirmationCode());
        }
        if (!($data->hasField("cod")&&isInt($data->getField("user")))){
            $this->actived = false;
            return true;
        }
        $this->actived = UserController::getInstance()->active($data);
        $user = UserController::getInstance()->getUser($data->getField("user"));
        if ($user->isActive()){
            $this->actived = true;
            return true;
        }
        if ($user===null){
            return true;
        } else if (!$this->actived&&$user->getConfirmationCode()!=0){
            EmailController::getInstance()->sendConfirmationEmail($user);
        }
        return true;
    }

    protected function content() {
        return $this->actived?true:"_Fail.php";
    }
}
