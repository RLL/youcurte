<center>
    <div class="alert alert-dismissible alert-danger">
        <strong>Seu e-mail não foi confirmado. URL não é válida! Tente verificar seu e-mail novamente.</strong>  <a href="/" class="alert-link"><u>Ir para a página inicial.</u></a>
    </div>
</center>