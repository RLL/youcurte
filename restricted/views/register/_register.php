<div class="page-header"><!-- page-header -->
    <center>
        <h1 id="navbar">Informe seus dados atualizados</h1>
    </center>
</div><!-- page-header -->

<center class="col-lg-10 col-lg-offset-1">
    <?php if ($this->hasError()): ?>
        <div class="alert alert-dimdissible alert-danger">
            <button type="button" class="close" data-dimdiss="alert">×</button>
            <?= toPT($this->getErrorMessage()) ?>
        </div>
    <?php endif; ?>
    <?php if (UserController::getInstance()->isAnyoneLoggedIn()): ?>
        <div class="">
            <img src="<?= $this->getUser()->getImage() ?>" alt="imagem de perfil" class="img-thumbnail user-img" id="userPhoto">
            <div>
                <div class="align-center inline-block margin-top-10">
                    <a class="btn btn-primary btn-md" onclick="selectProfileImage()">
                        <span class="glyphicon glyphicon-folder-open"></span>&emsp;Carregar Foto
                    </a>
                </div>
                <div class="align-center inline-block margin-top-10">
                    <a class="btn btn-primary btn-md" onclick="clearProfileImage()">
                        <span class="glyphicon glyphicon-trash"></span>&emsp;Remover Foto
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <form class="margin-y-10 register-form form-horizontal" method="post" id="registerForm">
        <input type="hidden" name="register" value="1">
        <?php if (UserController::getInstance()->isAnyoneLoggedIn()): ?>
            <input id="profileImageName" type="hidden" name="image">
        <?php endif; ?>
        <fieldset>
            <div class="form-group">
                <label for="name" class="col-lg-2 control-label">*Primeiro nome:</label>
                <div class="col-lg-10">
                    <input type="text" tabindex="1" class="form-control" name="name" id="inputFirstName" placeholder="Seu primeiro nome" value="<?= $this->getUser()->getName() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputLastName" class="col-lg-2 control-label">*Sobrenome:</label>
                <div class="col-lg-10">
                    <input type="text" tabindex="2" class="form-control" name="lastName" id="inputLastName" placeholder="Seu sobrenome" value="<?= $this->getUser()->getLastName() ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="sex-div">
                    <label for="inputSex" class="col-lg-2 control-label">*Sexo:</label>
                    <div class="col-lg-4">
                        <select class="form-control"  tabindex="2"  name="sex" id="inputSex">
                            <option value="male" <?php if ($this->getUser()->isMale()) echo 'selected'; ?>>Masculino</option>
                            <option value="female" <?php if ($this->getUser()->isFemale()) echo 'selected'; ?>>Feminino</option>
                        </select>
                    </div>
                </div>
                <div>
                    <label for="inputTel" class="col-lg-2 control-label" >Telefone:</label>
                    <div class="col-lg-4">
                        <input id="inputTel" type="tel" class="form-control"  tabindex="3" placeholder="Digite seu telefone" name="telephone" value="<?= $this->getUser()->getTelephone() ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-birth-email">
                    <label for="inputBirth" class="col-lg-3 control-label">*Data de nascimento:</label>
                    <div class="col-lg-3">
                        <div class='input-group date' id='inputBirthDiv'>
                            <input type='text' class="form-control" name="birthDate" id="inputBirth" placeholder="dia/m&ecirc;s/ano" value="<?=($d = $this->getUser()->getBirthDate()) != null?$d->setOnlyDate(true)->toPT()."":''?>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="input-birth-email">
                    <label for="inputEmail" class="col-lg-2 control-label">*E-mail:</label>
                    <div class="col-lg-4">
                        <input type="email"  tabindex="5" id="inputEmail" class="form-control" name="email" value="<?= $this->getUser()->getEmail() ?>" id="inputEmail" placeholder="Digite seu email">
                    </div>
                </div>
            </div>
            <?php if (!$this->isEdit()): ?>
                <div class="form-group">
                    <label for="inputPassword" class="col-lg-2 control-label">*Senha:</label>
                    <div class="col-lg-4">
                        <input type="password"  tabindex="6" class="form-control" name="password" id="inputPassword" placeholder="Entre com uma senha">
                    </div>
                    <label for="inputConfirmPass" class="col-lg-2 control-label">*Confirmar:</label>
                    <div class="col-lg-4">
                        <input type="password" tabindex="7" class="form-control" name="confirmPass" id="inputConfirmPass" placeholder="Repita senha">
                    </div>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <label for="inputWho" class="col-lg-2 control-label">Descreva-se:</label>
                <div class="col-lg-10">
                    <textarea id="inputWho"  tabindex="8" name="description" class="form-control max-width-100" rows="3" placeholder="Quem é voc&ecirc;?"><?= $this->getUser()->getDescription() ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label" for="inputCEP">CEP:</label>
                <div class="col-lg-6">
                    <input type="text"  tabindex="9" class="form-control" name="cep" value="<?= $this->getUser()->getCep() ?>" id="inputCEP" placeholder="CEP">
                </div>
                <label class="col-lg-2 control-label" for="inputState" >Estado:</label>
                <div class="col-lg-2">
                    <select class="form-control" name="state" id="inputState"  tabindex="10">
                        <?php
                        global $USER_STATE_ARRAY;
                        $st = $this->getUser()->getState();
                        if (isEmpty($st)) {
                            $st = "RS";
                        }
                        foreach ($USER_STATE_ARRAY as $state) {
                            $selected = "";
                            if ($st == $state) {
                                $selected = "selected";
                            }
                            echo "<option value='$state' $selected>$state</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2" for="inputCity">Cidade:</label>
                <div class="col-lg-4">
                    <input type="text"  tabindex="11" class="form-control" placeholder="Cidade" name="city" id="inputCity" value="<?= $this->getUser()->getCity() ?>">
                </div>
                <label class="control-label col-lg-2" for="inputInterest">Interesse:</label>
                <div class="col-lg-4">
                    <select class="form-control"  tabindex="12" name="interest" id="inputInterest">
                        <?php
                        global $USER_INTEREST_ARRAY;
                        foreach ($USER_INTEREST_ARRAY as $interest) {
                            $selected = "";
                            if ($this->getUser()->getInterest() == $interest) {
                                $selected = "selected";
                            }
                            echo "<option value='$interest' $selected>$interest</option>";
                        }
                        ?>
                    </select>  
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2" for="inputAT">Formação:</label>
                <div class="col-lg-4">
                    <input type="text" id="inputAT"  tabindex="13" name="academicTraining" value="<?= $this->getUser()->getAcademicTraining() ?>" class="form-control" placeholder="Formação Acad&ecirc;mica">
                </div>
                <label class="control-label col-lg-2" for="inputProfession">Profissão:</label>
                <div class="col-lg-4">
                    <input type="text" tabindex="14" class="form-control" name="profession" value="<?= $this->getUser()->getProfession() ?>" id="inputPro" placeholder="Sua profissão">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-12"> 
                    <input type="checkbox" id="inputNottoSendEmails"  tabindex="15" name="nottoSendEmails" value="1" <?=!$this->getUser()->get("toSendEmails")?'checked':''?>>
                    Não receber e-mails de notificações do YouCurte.
                </div>
            </div>
            <?php if (UserController::getInstance()->isAnyoneLoggedIn()): ?>
                <div id="divCharactersForm">
                    <form class="form-horizontal" id="divCharactersForm">
                        <div class="form-group">
                            <div class="col-lg-4">
                                <label class="control-label col-md-4" for="inputCool">Legal:</label>
                                <div class="col-md-8 input-group">
                                    <input type="number" id="inputCool"  tabindex="16" name="cool" value="<?= $this->getUser()->getCool() ?>" class="form-control" readonly>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label col-md-5" for="inputReliable">Confiável:</label>
                                <div class="col-md-7 input-group">
                                    <input type="number" tabindex="17" class="form-control" name="reliable" value="<?= $this->getUser()->getReliable() ?>" id="input" readonly>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label col-md-4" for="inputSexy">Sexy:</label>
                                <div class="col-md-8 input-group">
                                    <input type="number" tabindex="18" class="form-control" name="sexy" value="<?= $this->getUser()->getSexy() ?>" id="inputSexy" readonly>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            <?php endif; ?>
            <div class="form-group align-center">
                <?php if ($this->isEdit()): ?>
                    <button tabindex="16" class="btn btn-warning" onclick="editPassword()">Alterar senha</button>
                <?php endif; ?>
                <input type="submit"  tabindex="15" class="btn btn-info" value="<?= $this->getButtonCaption() ?>">
            </div>
        </fieldset>
    </form>
    <div class="subtitle-div">(*) Campos que são obrigatoriamente necessários para o cadastro.</div>
</center>

<?php if (UserController::getInstance()->isAnyoneLoggedIn()): ?>
    <div id="divCharactersForm" class="simple-hide">
        <form class="form-horizontal" id="divCharactersForm">
            <div class="form-group">
                <label class="control-label col-md-2" for="inputCool">Legal:</label>
                <div class="col-md-2 input-group">
                    <input type="text" id="inputCool"  tabindex="15" name="cool" value="<?= $this->getUser()->getCool() ?>" class="form-control">
                    <span class="input-group-addon">%</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="inputReliable">Confiável:</label>
                <div class="col-md-2 input-group">
                    <input type="text" tabindex="16" class="form-control" name="reliable" value="<?= $this->getUser()->getReliable() ?>" id="input">
                    <span class="input-group-addon">%</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="inputSexy">Sexy:</label>
                <div class="col-md-2 input-group">
                    <input type="text" tabindex="17" class="form-control" name="sexy" value="<?= $this->getUser()->getSexy() ?>" id="inputSexy">
                    <span class="input-group-addon">%</span>
                </div>
            </div>
        </form>
    </div>
    <div id="divPasswordForm" class="hide">
        <form class="form-horizontal" method="post" id="passwordForm">
            <div class="form-group">
                <label for="inputPassword" class="col-md-4 control-label">Senha atual:</label>
                <div class="col-lg-8">
                    <input type="password"  tabindex="6" class="form-control" name="currentPass" id="inputCurrentPass" placeholder="Entre sua senha total">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-lg-4 control-label">Nova senha:</label>
                <div class="col-lg-8">
                    <input type="password"  tabindex="6" class="form-control" name="password" id="inputPassword" placeholder="Entre com uma nova senha">
                </div>
            </div>
            <div class="form-group">
                <label for="inputConfirmPass" class="col-lg-4 control-label">Confirmar nova senha:</label>
                <div class="col-lg-8">
                    <input type="password" tabindex="7" class="form-control" name="confirmPass" id="inputConfirmPass" placeholder="Repita a nova senha">
                </div>
            </div>
        </form>
    </div>
<?php endif; ?>
