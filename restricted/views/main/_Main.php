<!DOCTYPE html>
<html lang="pt">
    <head>
        <?php $this->getHead()->output(); ?>
    </head>
    <body>
        <!-- navbar -->
        <?php $this->getNavbar()->output(); ?>
        <!-- fim da navbar -->
        <!--começo de  centro da página-->
        <div class="content">

            <?php
            if ($this->hasDashboard()) {
                $this->getDashboard()->output();
            }
            ?>
            <?php if ($this->hasDashboard()): ?>
                <div class="col-md-10 col-md-offset-2 section" id="section">
                <?php endif; ?>

                <?php
                if ($this->hasProfilePanel()) {
                    $this->getProfilePanel()->output();
                }
                ?>        
                <!--início do conteúdo child-->
                <?php $this->getChild()->output(); ?>               
                <!--fim do conteúdo child-->
                <?php if ($this->hasDashboard()): ?>
                </div>
            <?php endif; ?>  
        </div> 
        <!--end centro da página-->
        <!--começo de  messageDialog-->
    <center>
        <div class="modal fade" id="messageDialog" data-backdrop = "static" data-keyboard = "false"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" message-dialog-id="0">
            <div class="modal-dialog" id="messageDialogDiv">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close click" id="messageDialogClose"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="messageDialogLabel"></h4>
                    </div>
                    <div class="modal-body" id="messageDialogContent">
                    </div>
                    <div class="modal-footer" id="messageDialogFooter">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    <!--end messageDialog -->
    <!--uploadFile-->
    <form class="hide" id="formUploadFile">
        <input type="hidden" name="MAX_FILE_SIZE" value="<?= IMAGE_FILE_SIZE_MAX ?>" />
        <input type="file" name="file" id="uploadFile">
    </form>
    <!--end uploadFile-->
    <!-- javascript com variaveis globais -->
    <script type="text/javascript">
<?php if ($this->hasLoginError()): ?>
            $(
                    function () {
                        $("#login").dropdown("toggle");
                    }
            );
<?php endif; ?>
        $(function () {
<?php if ($this->isAnyoneLoggedIn()): ?>
                YouCurte.LoggedIn = true;
                $(".if-logged-in").show();
<?php else: ?>
                YouCurte.LoggedIn = false;
                $(".if-logged-in").hide();
<?php endif; ?>
        });
    </script>
    <!-- fim do javascript -->
</body>
</html>




