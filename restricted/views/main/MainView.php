<?php

require_once VIEWS . 'SubView.php';
requireView("head");
requireView("navbar");

/**
 * Description of YouCurteView
 *
 * @author YouCurte
 */
class MainView extends View {

    private $head, $dashboard, $navbar, $profilePanel, $visiting, $user;

    public function __construct(\View $parent = null, \View $child = null, \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
        $this->user = null;
        $this->loadDefaultHead();
        $this->loadDefaultNavbar();
        $this->setIcon(new URL("favicon.ico", URL_ROOT));
        if ($this->isAnyoneLoggedIn()) {
            $this->setTitle("YouCurte - "
                    . $this->getUserLoggedIn()->getFullName());
        } else {
            $this->setTitle("YouCurte");
        }
    }

    /**
     * Instância o navbar padrão)
     * @return \NavbarView
     */
    protected function newDefaultNavbar() {
        $navbar = new NavbarView($this, null, $this->getData());
        return $navbar;
    }

    /**
     * Instância o head padrão (já preenchido com os dados)
     * @return \HeadView
     */
    protected function newDefaultHead() {
        $head = new HeadView();
        $head->addJavascript("jquery-2.1.3.min.js");
        $head->addJavascript("moments.js");
        if (IS_LOCAL) {
            $head->addStylesheet("bootstrap.css");
            $head->addJavascript("bootstrap.js");
        } else {
            $head->addStylesheet("bootstrap.min.css");
            $head->addJavascript("bootstrap.min.js");
        }
        $head->addStylesheet("youcurte.css");
        $head->addJavascript("message.js");
        $head->addJavascript("youcurte.js");
        if ($this->isAnyoneLoggedIn()) {
            $head->addJavascript("loggedIn.js");
        } else {
            $head->addJavascript("login.js");
        }
        return $head;
    }

    /**
     * Instância o dashboard padrão (já com os dados preenchidos)
     * @return \DashboardView
     */
    protected function newDefaultDashboard() {
        requireView("dashboard");
        if ($this->isAnyoneLoggedIn()) {
            $this->getHead()->addStylesheet("dashboard.css");
            $this->getHead()->addJavascript("dashboard.js");
        }
        $dashboard = new DashboardView($this);
        $dashboard->setUser($this->getUserLoggedIn());
        $dashboard->addMenuItem("Perfil", new URL('profile', URL_ROOT));
        $dashboard->addMenuItem("Timeline", new URL('timeline', URL_ROOT));
        $dashboard->addMenuItem("Minhas Postagens", new URL('posts', URL_ROOT));
        $dashboard->addMenuItem("Depoimentos", new URL('depositions', URL_ROOT));
        $dashboard->addMenuItem("Álbuns", new URL('albums', URL_ROOT));
        $dashboard->addMenuItem("Parceiros do Mate", new URL('related', URL_ROOT));
        if (URL::isCurrentRoot()) {
            $dashboard->setMenuItemActived("Timeline");
        } else {
            foreach ($dashboard->listMenuItems() as $key => $item) {
                if (strpos(URL::getFromURL() . "", $item["link"] . "") === 0) {
                    $dashboard->setMenuItemActived($key);
                    break;
                }
            }
        }
        return $dashboard;
    }

    /**
     * Instância o ProfilePanel padrão (já com os dados preenchidos)
     * @return \ProfilePanelView
     */
    protected function newDefaultProfilePanel() {
        requireView("ProfilePanel");
        $profilePanel = new ProfilePanelView($this, $this->getData());
        $profilePanel->setUser($this->getWhoIsBeingVisited());
        if ($this->hasDashboard()) {
            $dashboard = $this->getDashboard();
        } else {
            $dashboard = $this->newDefaultDashboard();
        }
        foreach ($dashboard->listMenuItems() as $item) {
            $profilePanel->addMenuItem($item["label"], new URL($this->getWhoIsBeingVisited()->getId(), $item["link"]), $item["active"]);
        }
        $profilePanel->setMenuItemActived($dashboard->getMenuItemActived()["label"]);
        $dashboard->setMenuItemActived(null);
        $profilePanel->removeMenuItem("Perfil");
        $profilePanel->removeMenuItem("Timeline");
        $profilePanel->setMenuItemLabel("Minhas Postagens", "Postagens");
        $profilePanel->setParamMenuItem("Postagens", "order", 0);
        if ($profilePanel->getMenuItemActived() == null) {
            $profilePanel->setMenuItemActived("Postagens");
        }
        return $profilePanel;
    }

    /**
     * Carrega os arquivos dos BootStrapValidator no Head
     */
    public function loadBootStrapValidator() {
        $head = $this->getHead();
        if (IS_LOCAL) {
            $head->addStylesheet("bootstrapValidator.css");
            $head->addJavascript("bootstrapValidator.js");
        } else {
            $head->addStylesheet("bootstrapValidator.min.css");
            $head->addJavascript("bootstrapValidator.min.js");
        }
        $head->addJavascript("/language-validator/pt_BR.js");
    }

    /**
     * Carrega os arquivos do BootStrapDateTimePicker no Head
     */
    public function loadBootStrapDateTimePicker() {
        $head = $this->getHead();
        if (IS_LOCAL) {
            $head->addStylesheet("bootstrap-datetimepicker.css");
        } else {
            $head->addStylesheet("bootstrap-datetimepicker.min.css");
        }
        $head->addJavascript("bootstrap-datetimepicker.min.js");
    }

    /**
     * Instância o default Head e define no MainView
     */
    public function loadDefaultHead() {
        $this->setHead($this->newDefaultHead());
    }

    /**
     * Instância o default Navbar e define o MainView
     */
    public function loadDefaultNavbar() {
        $this->setNavbar($this->newDefaultNavbar());
    }

    /**
     * Instância o default dashboard e define o MainView
     */
    public function loadDefaultDashboard() {
        if ($this->isAnyoneLoggedIn()) {
            $this->setDashboard($this->newDefaultDashboard());
        }
    }

    /**
     * Instância o default ProfilePanel e define o MainView
     */
    public function loadDefaultProfilePanel() {
        if ($this->isVisiting()) {
            $this->setProfilePanel($this->newDefaultProfilePanel());
        }
    }

    /**
     * Carrega as bibliotecas padrões de GPS no MainView
     */
    public function loadDefaultGPS() {
        //$this->getHead()->addJavascript("http://js.maxmind.com/js/geoip.js",true);
    }

    /**
     * Retorna o Navbar
     * @return Navbar
     */
    public function getNavbar() {
        return $this->navbar;
    }

    /**
     * Verifica se tem o Navbar
     * @return boolean true = tem Navbar
     *      false = não tem Navbar
     */
    public function hasNavbar() {
        return $this->getNavbar() != null;
    }

    /**
     * Define o Navbar no MainView
     * @param Navbar $navbar
     * @return Navbar Retorna $navbar mesmo
     */
    public function setNavbar($navbar) {
        return $this->navbar = $navbar;
    }

    /**
     * Retorna o Dashboard
     * @return DashboardView
     */
    public function getDashboard() {
        return $this->dashboard;
    }

    /**
     * Retorna o ProfilePanel
     * @return ProfilePanelView
     */
    public function getProfilePanel() {
        return $this->profilePanel;
    }

    /**
     * Define o ProfilePanel que será exibido
     * @param ProfilePanel $profilePanel
     */
    public function setProfilePanel($profilePanel) {
        $this->profilePanel = $profilePanel;
    }

    /**
     * Verifica se tem ProfilePanel
     * @return boolean true = tem <br>
     *  false = não tem
     */
    public function hasProfilePanel() {
        return isset($this->profilePanel) && $this->profilePanel != null;
    }

    public function hasDashboard() {
        return $this->dashboard != null;
    }

    public function setDashboard($dashboard) {
        $this->dashboard = $dashboard;
    }

    public function setIcon($url) {
        $this->getHead()->setIcon($url);
    }

    public function getIcon() {
        return $this->getHead()->getIcon();
    }

    /**
     * Define o titulo da página
     * @param String $title titulo
     */
    public function setTitle($title) {
        $this->getHead()->setTitle($title);
    }

    /**
     * Retorna o título da página
     * @return string
     */
    public function getTitle() {
        return $this->getHead()->getTitle();
    }

    /**
     * Retorna o head
     * @return HeadView
     */
    public function getHead() {
        return $this->head;
    }

    /**
     * Verifica se tem o head
     * @return boolean (true = se tem) (false = se não tem)
     */
    public function hasHead() {
        return $this->head != null;
    }

    /**
     * Define o head
     * @param Head $head
     */
    public function setHead(HeadView $head) {
        $this->head = $head;
    }

    protected function content() {
        if ($this->hasDashboard()) {
            require_once CONTROLLERS . "DepositionController.php";
            $dashboard = $this->getDashboard();
            $countDep = $_SESSION["deposition"];
            $deposition = "Depoimentos <span class='badge'>$countDep</span>";
            $dashboard->setMenuItemLabel("Depoimentos", $deposition);
            $countRelated = $_SESSION["related"];
            $related = "Parceiros do Mate <span class='badge'>$countRelated</span>";
            $dashboard->setMenuItemLabel("Parceiros do Mate", $related);
        }
        if ($this->hasProfilePanel()) {
            require_once CONTROLLERS . "DepositionController.php";
            $panel = $this->getProfilePanel();
            $countDep = $_SESSION["visitedDeposition"];
            $deposition = "Depoimentos <span class='badge' id='countDeposition'>$countDep</span>";
            $panel->setMenuItemLabel("Depoimentos", $deposition);
            $countRelated = $_SESSION["visitedRelated"];
            $related = "Parceiros do Mate <span class='badge'>$countRelated</span>";
            $panel->setMenuItemLabel("Parceiros do Mate", $related);
        }
        return true;
    }

    /**
     * Atualiza o cache relacionado ao número de  Depoimentos e Relacionamentos 
     *  Obs: Antes de atualizar ele verifica se houve alguma mudança que 
     *      necessite atualizar o cache
     * 
     *  (Parceiros de Mat&ecirc;)
     * @param boolean $force Força a atualização
     */
    public function updateCache($force = false) {
        if ($this->getWhoIsBeingVisited() === null) {
            return;
        }
        if ($this->isAnyoneLoggedIn()) {
            UserController::getInstance()->updateCache($this->getUserLoggedIn());
        }
        if (!isset($_SESSION["visited"])) {
            $_SESSION["visited"] = -1;
        }
        if ($force || $_SESSION["visited"] != $this->getWhoIsBeingVisited()->getId()) {
            require_once CONTROLLERS . "DepositionController.php";
            $_SESSION["visited"] = $this->getWhoIsBeingVisited()->getId();
            $_SESSION["visitedDeposition"] = DepositionController::getInstance()
                    ->countDepositions($this->getWhoIsBeingVisited());
            $_SESSION["visitedRelated"] = UserController::getInstance()
                    ->countRelatedUsers($this->getWhoIsBeingVisited());
        }
    }

    protected function commands() {
        $this->setLoginError($this->getData()->getField("loginError"));
        $this->updateCache();
        return true;
    }

    /**
     * Define os dados de entrada
     * @param DataInput $data Dados de entrada
     */
    public function setData(DataInput $data = null) {
        if ($this->hasNavbar()) {
            $this->getNavbar()->setData($data);
        }
        if ($this->hasDashboard()) {
            $this->getDashboard()->setData($data);
        }
        parent::setData($data);
    }

    /**
     * Verifica se tem erro em realizar o login
     * @return boolean true = tem erro <br>
     *      false = não tem erro
     */
    public function hasLoginError() {
        return $this->getData()->hasField("loginError");
    }

    /**
     * Define um erro de login 
     * @param String $msg A mensagem de erro de login
     */
    public function setLoginError($msg) {
        $this->getData()->setField("loginError", $msg);
    }

    /**
     * Retorna o erro de login
     * @return String Retorna vazio ('') se não tiver erro
     */
    public function getLoginError() {
        return $this->getData()->getField("loginError");
    }

    /**
     * Verifica se está visitando o perfil de outro usuário
     * 
     *  [ Verifica pelo getData()->hasField("user") ]
     * @return boolean 
     *      true = está visitando <br>
     *      false = não está
     * 
     */
    public function isVisiting() {
        $user = $this->getWhoIsBeingVisited();
        $id = $this->getUserLoggedIn() != null ? $this->getUserLoggedIn()->getId() : -1;
        return $user !== null && $id != $user->getId();
    }

    /**
     * 
     * @return User O usuário que está sendo visitado;
     *      Se não está sendo visitado ninguém, retorna o usuário logado mesmo.
     */
    public function getWhoIsBeingVisited() {
        $this->getData()->declareFields("userVisited");
        $id = $this->getData()->getField("userVisited");
        if (isInt($id) && ($this->user == null || $id != $this->user->getId())) {
            $this->user = UserController::getInstance()->getUser($id);
        } else if ($this->user == null) {
            $this->user = $this->getUserLoggedIn();
        }
        return $this->user;
    }

    /**
     * Verifica se alguém está logado
     * @return boolean  true = tem alguém logado <br>
     *      false = não tem ninguém logado
     */
    public function isAnyoneLoggedIn() {
        return UserController::getInstance()->isAnyoneLoggedIn();
    }

    /**
     * Retorna o usuário logado
     * @return User O usuário logado.
     *  Retorn null se não tiver ninguém logado.
     */
    public function getUserLoggedIn() {
        return UserController::getInstance()->getUserLoggedIn();
    }

    /*
     * Ação de realizar login
     * URL: /?action=login
     * 
     */
    public function action_login(DataInput $data) {
        $uc = UserController::getInstance();
        $uc->login($data);
    }

    /*
     * Ação de realizar uma postagem
     * URL: /?action=login
     * 
     */
    public function action_post(DataInput $data) {
        if ($data->hasField("text") || isset($_FILES["image"])||$data->hasField("imageId")) {
            PostController::getInstance()->post($this->getData());
        }
    }

    /**
     * Ação para editar post
     * @param DataInput $data Dados de entrada
     */
    public function action_edit_post(DataInput $data) {
        PostController::getInstance()->editPost($data);
    }

    /**
     * Ação de editar atributos de um usuário (legal,confiável,sexy)
     */
    public function action_edit_attributes() {
        require_once CONTROLLERS . "AttributesController.php";
        AttributesController::getInstance()->editAttributes($this);
    }

    /**
     * Ação de seguir
     */
    public function action_follow() {
        UserController::getInstance()->follow($this);
        $this->updateCache(true);
    }

    /**
     * Ação de deixar de seguir
     */
    public function action_unfollow() {
        UserController::getInstance()->unfollow($this);
        $this->updateCache(true);
    }

}
