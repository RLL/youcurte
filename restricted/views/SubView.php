<?php

require_once VIEWS . "View.php";

/**
 * Classe para controlar subviews (Views filhas de MainView)
 *
 * @author YouCurte
 */
abstract class SubView extends View {

    private $main,$msg = array();

    /**
     * Retorna o MainView que é um dos parentes
     * @return MainView
     */
    public function getMainView() {
        if ($this->main == null) {
            for ($i = $this; $i != null; $i = $i->getParent()) {
                if ($i instanceof MainView) {
                    return $i;
                }
            }
        } else {
            return $this->main;
        }
    }
    
    /**
     * Define o título da página
     * @param type $title O título
     */
    public function setTitle($title){
        $this->getMainView()->setTitle($title);
    }
    
    /**
     * Retorna o título atual da página
     * @return string
     */
    public function getTitle(){
        return $this->getMainView()->getTitle();
    }

    
    /**
     * Define uma mensagem
     * @param string $key Tipo de mensagem
     * @param string $msg A mensagem
     */
    public function setMessage($key,$msg){
        $this->msg[$key] = $msg;
    }
    
    /**
     * 
     * Retorna a mensagem
     * @param string $key O tipo de mensagem
     * @return string
     */
    public function getMessage($key){
        if (!array_key_exists($key, $this->msg)){
            return "";
        }
        return $this->msg[$key];
    }
    
    /**
     * Verique se tem mensagem de erro
     * @param string $key O tipo de mensagem
     * @return boolean true = tem <br> false = não tem 
     */
    public function hasMessage($key){
        return isNotEmpty($this->getMessage($key));
    }
    
}