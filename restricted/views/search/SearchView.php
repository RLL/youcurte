<?php

require_once CONTROLLERS."PostController.php";

/**
 * View para pesquisas
 *
 * @author YouCurte
 */
class SearchView extends SubView {
    
    protected $count,$posts,$users,$page;
    
    
    public function __construct(\View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }
    
    /**
     * Retorna a quantidade de resultados 
     * @return type
     */
    public function countResults(){
        return $this->count;
    }
    
   /**
    * Retorna a lista de usuários encontrados
    * @return array
    */
    public function getUsers() {
        return $this->users;
    }
    
    /**
     * Retorna a lista de posts encontrados
     * @return array
     */
    public function getPosts(){
        return $this->posts;
    }

    protected function commands() {
        $data = $this->getData();
        $this->getMainView()->loadDefaultDashboard();
        $this->getMainView()->loadBootStrapValidator();
        $this->getMainView()->getHead()->addJavascript("search.js");
        $this->users = $this->posts = array();
        $result = 0;
        if (!strstr($data->getField("search"),"#")){
            $result = UserController::getInstance()->searchUsers($data);;
            $this->users = $result->getData();
        } else {
            $result = PostController::getInstance()
                    ->searchPostsForHashtags($data);
             $this->posts = $result->getData();
        }
        $this->count = $result->getCount();
        return true;
    }

    protected function content() {
        return true;
    }

}