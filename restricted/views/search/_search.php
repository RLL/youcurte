<!-- título da página -->
<div class="page-header sub-profile-header more-sub-profile-header">
    <center>
        <h1 id="navbar">Foram encontrados <?= $this->countResults() ?> registro(s) na sua busca</h1>
    </center>
</div>
<!-- fim do título da página -->
<?php
$count = 0;
if (count($this->getUsers()) > 0) {
    foreach ($this->getUsers() as $user) {
        requireView("user");
        $userView = new UserView();
        $userView->setUser($user);
        $userView->output();
    }
    $count = count($this->getUsers());
}
if (count($this->getPosts()) > 0) {
    requireView("post");
    foreach ($this->getPosts() as $post) {
        $postView = new PostView();
        $postView->setPost($post);
        $postView->output();
    }
    $count = count($this->getPosts());
}
requireView("bottomButtons");
if ($this->getMainView()->isAnyoneLoggedIn()) {
    requireView('PostEditView', 'post');
    PostEditView::execute();
}
$link = URL::getFromURL() . "?search=" . $this->getData()->getField("search");
$link = str_replace("#", "%23", $link);
$bottom = new BottomButtonsView(null, $this->getData(), $count, $link);
$bottom->setNextLabel("Próximo");
$bottom->setPrevLabel("Anterior");
$bottom->output();
?>
