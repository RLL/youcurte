<?php

require_once CONTROLLERS . 'UserController.php';
requireView('PostFormView', 'post');
requireView('PostView', 'post');
requireView('PostEditView', 'post');

/**
 * View para listar as postagens do usuário logado
 *
 * @author YouCurte
 */
class PostsView extends SubView {

    private $data,$image;

    public function __construct(\View $parent = null, \View $child = null, \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    protected function commands() {
        if (!$this->getMainView()->isVisiting()) {
            WebYouCurte::checkPrivilege(true);
        }
        if ($this->getData()->hasField("photoId")){
            $photoId = $this->getData()->getField("photoId");
            $this->image = ImageController::getInstance()->getImage($photoId);
        }
        $this->getMainView()->loadDefaultDashboard();
        $this->getMainView()->loadDefaultProfilePanel();
        $this->getMainView()->loadBootStrapValidator();
        $this->getMainView()->loadDefaultGPS();
        return true;
    }

    /**
     * Lista os posts em html
     * @return int A quantida de posts
     */
    public function listPostsInHtml() {
        $user = $this->getMainView()->getWhoIsBeingVisited();
        $posts = PostController::getInstance()->listPosts($this->getData(), $user);
        foreach ($posts as $post) {
            $postView = new PostView();
            $postView->setPost($post);
            $postView->output();
        }
        return count($posts);
    }

    protected function content() {
        if ($this->getMainView()->isVisiting()) {
            echo "<br>";
        }
        PostEditView::execute();
        if ($this->hasImage()) {
            return true;
        } else {
            return new File("_timeline.php", VIEWS . 'timeline');
        }
    }
    
    /**
     * Se tem uma imagem anexada ao post
     * @return boolean true = tem <br> false = não tem
     */
    public function hasImage(){
        return $this->image!==null;
    }
    
    /**
     * Retorna a  imagem anexada ao psot
     * @return string
     */
    public function getImage(){
        return $this->image;
    }

}