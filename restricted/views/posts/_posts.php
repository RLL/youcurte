<?php
requireView('postFormView', 'post');
if (!$this->getMainView()->isVisiting()) {
    $view = new PostFormView();
    $view->setImage($this->getImage());
    $view->run();
}
?>
<!--imagem anexada -->
<center>
    <div class="max-width-100 thumbnail">
        <img src="<?= $this->getImage()->getValidUrl() ?>" alt="Imagem anexada ao post">
    </div>
</center>
<!-- fim da imagem anexada -->

