<?php

/**
 * View dos albuns
 *
 * @author Luiz Henrique
 */
class AlbumsView extends SubView {

    public function __construct(\View $parent = null, \View $child = null,
            \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    protected function commands() {
        if (!$this->getMainView()->isVisiting()) {
            WebYouCurte::checkPrivilege(true);
        }
        $this->getMainView()->loadDefaultDashboard();
        $this->getMainView()->loadDefaultProfilePanel();
        $this->getMainView()->getHead()->addJavascript("album.js");
        $this->getMainView()->getHead()->addStylesheet("album.css");
        if (isset($_FILES["file"])) {
            ImageController::getInstance()->uploadImageUser("file", $this->getAlbumId());
            die(json_encode_yc(array("result" => "ok")));
        }
        return true;
    }

    protected function content() {
        return true;
    }

    /**
     * Pega o id do album atual
     * @return int
     */
    public function getAlbumId() {
        $this->getData()->declareFields(array("albumId" => ALBUM_ID_DEFAULT));
        return $this->getData()->getField("albumId");
    }

    /**
     * Ação para deletar o album 
     * @param DataInput $data
     */
    public function action_delete_album(DataInput $data) {
        ImageController::getInstance()->deleteAlbum($data);
    }

    /**
     * Ação para criar um novo album
     * @param DataInput $data
     */
    public function action_new_album(DataInput $data) {
        $album = ImageController::getInstance()->newAlbum($data);
        $this->getData()->setField("albumId", $album->getId());
    }

    /**
     * Ação para renomear um album
     * @param DataInput $data
     */
    public function action_rename_album(DataInput $data) {
        ImageController::getInstance()->renameAlbum($this);
    }

    /**
     * Ação para renomear uma foto
     * @param DataInput $data
     */
    public function action_rename_photo(DataInput $data) {
        ImageController::getInstance()->renamePhoto($data);
    }
    
    /**
     * Ação para pegar a próxima foto ou anterior
     * @param DataInput $data
     */
    public function action_set_photo(DataInput $data){
        ImageController::getInstance()->getImageNeighbors($data);
    }
    
    /**
     * Ação move fotos para outro álbum
     * @param DataInput $data
     */
    public function action_move_to(DataInput $data){
        ImageController::getInstance()->moveImageToAlbum($data);
    }
    
    /**
     * Deleta fotos de um álbum
     */
    public function action_delete_photos(){
        ImageController::getInstance()->deleteImages($this->getData());
    }

    /**
     * Define uma foto como avatar do usuário
     * @param DataInput $data
     */
    public function action_set_profile(DataInput $data){
        UserController::getInstance()->setProfileImage($data);
    }
    
}
