<?php
$albums = ImageController::getInstance()->listAlbums($this->getMainView()
                ->getWhoIsBeingVisited());
$images = ImageController::getInstance()->listImages($this);
?>

<!-- cabeça da página com o título -->
<div class="page-header sub-profile-header more-sub-profile-header">
    <center>
        <h1 id="navbar">Álbuns</h1>
    </center>
</div>
<!-- final do título -->

<?php 
    if ($this->getMainView()->isVisiting()){
        echo '<br>';
    }
?>
<div class="row align-center">
    <div class="panel panel-default">
        <div class="panel-body alert-info album-panel">
            <h3 class="panel-title">
                <div class='col-lg-5 form-group margin-bot-0'>
                    <label class='control-label album-label'>álbum:</label>
                    <div class='col-lg-9'>
                        <div class="input-group">
                            <!-- albuns -->
                            <select id='selectAlbum' class='form-control'>
                                <?php foreach ($albums as $album): ?>
                                    <option id="option<?= $album->getId() ?>" value="<?= $album->getId() ?>" <?= $album->getId() == $this->getAlbumId() ? 'selected' : '' ?>><?= $album->getName() ?></option>
                                <?php endforeach; ?>
                                <?php if (count($albums) === 0): ?>
                                    <option>Sem album</option>
                                <?php endif; ?>
                            </select>
                            <!-- fim dos albuns -->
                            <?php if (!$this->getMainView()->isVisiting()): ?>
                                <!-- opções de albuns -->
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle album-option-button" data-toggle="dropdown" aria-expanded="false">
                                        <span class="glyphicon glyphicon-option-vertical"></span>
                                    </button>
                                    <ul class="dropdown-menu album-options" role="menu">
                                        <li><a class="click" onclick="newAlbum()">Criar novo álbum</a></li>
                                        <li><a class="click" onclick="renameAlbum()">Renomear álbum</a></li>
                                        <li class="divider"></li>
                                        <li><a class="click" onclick="deleteAlbum()">Deletar álbum</a></li>
                                    </ul>
                                </div>
                                <!-- fim das opções de albuns-->
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if (!$this->getMainView()->isVisiting()): ?>
                    <!-- botões com as opções sobre botões -->
                    <div class="align-right inline-block float-right buttons-panel">
                        <button class="btn btn-danger disabled" id="deletePhotoButton" onclick="deletePhotos()"><span class="glyphicon glyphicon-trash"></span>&nbsp;Deletar foto(s)</button>
                        <button class="btn btn-warning disabled" id="movePhotoButton" onclick="movePhotos()"><span class="glyphicon glyphicon-arrow-right"></span>&nbsp;Mover foto(s)</button>
                        <button class="btn btn-primary" id="uploadPhotoButton" onclick="uploadPhoto()"><span class="glyphicon glyphicon-picture"></span>&nbsp;Enviar foto</button>
                    </div>
                    <!-- fim dos botões com opções sobre fotos-->
                <?php endif; ?>
            </h3>
        </div>
        <div class="panel-footer photos-div">
            <?php foreach ($images as $image): ?>
                <?php $id = $image->getId(); ?>
                <div class='col-lg-3 photo-col'> <!-- inicio da imagem-->
                    <div class="thumbnail photo-div">
                        <a onclick='showAlbumPhoto(<?= $id ?>)' image-id='<?= $id ?>' class='click'>
                            <img src="<?= $image->getValidUrl() ?>" id="image<?= $id ?>" alt="Foto <?= $id ?> do álbum.">
                        </a>
                    </div
                    <!-- nome da imagem com checkbox -->
                    <div class="img-footer align-center label label-primary">
                        <a onclick='selectPhoto(<?= $id ?>)' class='click'>
                            <?php if (!$this->getMainView()->isVisiting()): ?>
                                <div class='inline-block'>
                                    <input photo-id="<?= $id ?>" type='checkbox' class='photo-check' id="checkPhoto<?= $id ?>" onclick='selectPhoto(<?= $id ?>)'>
                                </div>
                            <?php endif ?>
                            <div class='inline-block color-white' title="<?= $image->getName() ?>" id="title<?= $id ?>"><?= substr($image->getName(), 0, 25) ?></div>
                        </a>
                    </div>
                    <!-- fim do nome da imagem -->
                </div> <!-- fim da imagem-->
            <?php endforeach; ?>
            <?php if (isEmpty($images)&&$this->getData()->getField("page")>0): ?>
                <center>Sem mais fotos para exibir!</center>
            <?php elseif (isEmpty($images)): ?>    
                <center>Sem fotos para exibir!</center>
            <?php endif; ?>
            <?php
            requireView("BottomButtons");
            $bottom = new BottomButtonsView($this,null,count($images),
                    URL::getFromURL()."?albumId=".$this->getAlbumId());
            $bottom->setCountMax(COUNT_RETURN_IMAGES);
            $bottom->output();
            ?>
        </div>
    </div>
</div>


<!-- formulário para renomear álbuns ou fotos -->
<form id="albumForm" class="hide text-left">
    <div class="text-left">
        <label class="control-label" id="labelName">Nome do álbum:</label>
        <input text="text" id="inputName" class="form-control">
    </div>
</form>
<!-- fim do formulári para renomear-->

<?php if (!$this->getMainView()->isVisiting()): ?>
    <!-- dialog para selecionar um álbum onde mover as fotos -->
    <div class='col-lg-5 form-group margin-bot-0 hide' id="selectAlbumDiv">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-left">&nbsp;álbum:</div>
                <select class='form-control select-album-move'>
                    <?php foreach ($albums as $album): ?>
                        <?php if ($album->getId() != $this->getAlbumId()): ?>
                            <option id="option<?= $album->getId() ?>" value="<?= $album->getId() ?>"><?= $album->getName() ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if (count($albums) === 0): ?>
                        <option>Sem album</option>
                    <?php endif; ?>
                </select>
            </div>
        </div>
    </div>
    <!-- fim do dialog para mover fotos -->
<?php endif; ?>
