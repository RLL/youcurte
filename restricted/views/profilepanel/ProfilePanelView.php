<?php

require_once CONTROLLERS."AttributesController.php";
requireView("dashboard");

/**
 * Description of ProfilePanel
 *
 * @author YouCurte
 */
class ProfilePanelView extends DashboardView {
    
    public function __construct(\View $parent = null,
            \DataInput $data = null) {
        parent::__construct($parent, null, $data);
        $this->getMainView()->getHead()->addStylesheet("profilePanel.css");
    }

    protected function commands() {
        return true;
    }

    protected function content() {
        return true;
    }
    
    public function getAttributes(){
        return AttributesController::getInstance()->
                getAttributes($this->getMainView());
    }

}