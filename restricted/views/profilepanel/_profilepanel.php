<?php $user = $this->getMainView()->getWhoIsBeingVisited(); ?>
<div class="profile-header page-header row "><!-- begin profile panel  -->
    <div class="inline-block float-left profile-image">
        <img src="<?= $user->getImage() ?>"  alt="imagem de perfil" class="img-thumbnail user-img">
    </div>
    <div class="col-lg-6 inline-block profile-info">
        <!--informações sobre o usuário que está visitando-->
        <h2 class="margin-0"><?= $user->getName() . " " . $user->getLastName() ?></h2>
        <h5 class="text color-panel-text"><?= $user->getCity() ?>-<?= $user->getState() ?></h5>
        <h5 class="text color-panel-text"><?= $user->getProfession() ?></h5>
        <?php if (isNotEmpty($user->getInterest())): ?>
            <h5 class="text color-panel-text">Interesse em: <?= $user->getInterest() ?></h5>
        <?php endif; ?>
        <?php if ($this->getMainView()->isAnyoneLoggedIn()): ?>
            <h5 class="text color-panel-text">
                <a onclick="editAttributes()" class="click" title="Defina atributos para seu amigo(a)">
                    <span class="glyphicon glyphicon-arrow-right"></span> <?= $user->getCool() ?>% Legal, <?= $user->getReliable() ?>% Confiável, <?= $user->getSexy() ?>% Sexy <span class="glyphicon glyphicon-arrow-left"></span>
                </a>
            </h5>
        <?php else: ?>
            <h5 class="text color-panel-text">
                <?= $user->getCool() ?>% Legal, <?= $user->getReliable() ?>% Confiável, <?= $user->getSexy() ?>% Sexy
            </h5>
        <?php endif; ?>
        <h5 class="text color-panel-text"><?= $user->getDescription() ?></h5>
        <!--fim das informações sobre o usuário que está visitando-->
    </div>
    <div class="inline-block float-right profile-kind">
        <!-- status -->
        <h1 class="margin-0 color-panel-text"><?= $user->getLabel() ?></h1>
        <!-- fim de status -->
        <!-- botão de seguir -->
        <?php if (!UserController::getInstance()->isAnyoneLoggedIn()): ?>
            <form method="post" id="formFollow">
                <input type="hidden" name="action" value="follow">
                <input type="hidden" name="user" value="<?= $user->getId() ?>">
                <button type="submit" disabled="disabled" form="formFollow" class="btn btn-default" id="followButton"><span class="glyphicon glyphicon-star-empty"></span>&emsp14;Seguir</button>
            </form>
        <?php elseif (!UserController::getInstance()->isFollowing($this->getMainView())): ?>
            <form method="post" id="formFollow">
                <input type="hidden" name="action" value="follow">
                <input type="hidden" name="user" value="<?= $user->getId() ?>">
                <button type="submit" form="formFollow" class="btn btn-default" id="followButton"><span class="glyphicon glyphicon-star-empty"></span>&emsp14;Seguir</button>
            </form>
        <?php else: ?>
            <form method="post" id="formFollow">
                <input type="hidden" name="action" value="unfollow">
                <input type="hidden" name="user" value="<?= $user->getId() ?>">
                <button type="submit" form="formFollow" class="btn btn-default" id="followButton"><span class="glyphicon glyphicon-star"></span>&emsp14;Deixar de Seguir</button>
            </form>
        <?php endif; ?>
        <!-- fim do botão de seguir -->
    </div>
</div><!-- end profile panel  -->
<ul class="nav nav-tabs">
    <?php $this->listMenuItemsInHtml(); ?>
</ul>

 <?php if (UserController::getInstance()->isAnyoneLoggedIn()): ?>
<!-- formulário para edição dos atributos do usuário -->
<div id="attributesFormDiv" class="hide">
    <form class="form-horizontal" id="attributesForm">
        <div class="form-group">
            <label class="control-label col-lg-4" for="inputCool">Legal:</label>
            <div class="col-lg-7 input-group">
                <input type="number" id="inputCool"  tabindex="15" name="cool" value="<?= $this->getAttributes()->getCool() ?>" class="form-control" min="0" max="100">
                <span class="input-group-addon">%</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4" for="inputReliable">Confiável:</label>
            <div class="col-lg-7 input-group">
                <input type="number" id="inputReliable" tabindex="16" class="form-control" name="reliable" value="<?= $this->getAttributes()->getReliable() ?>" min="0" max="100">
                <span class="input-group-addon">%</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4" for="inputSexy">Sexy:</label>
            <div class="col-lg-7 input-group">
                <input type="number" id="inputSexy" tabindex="17" class="form-control" name="sexy" value="<?= $this->getAttributes()->getSexy() ?>" min="0" max="100">
                <span class="input-group-addon">%</span>
            </div>
        </div>
    </form>
</div>
<!-- fim do formulário para edição dos atributos do usuário -->
<?php endif; ?>
