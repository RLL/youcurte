<?php

/**
 * View para redirecionar para o link destino de uma Notificação
 *
 * @author YouCurte
 */
class NotificationView extends SubView {

    public function __construct(\View $parent = null, \View $child = null, \DataInput $data = null) {
        parent::__construct($parent, $child, $data);
    }

    protected function commands() {
        WebYouCurte::checkPrivilege(true);
        $controller = NotificationController::getInstance();
        $this->getData()->declareFields("noteId");
        if (!$this->getData()->hasField("noteId")) {
            $this->error();
        }
        $noteId = $this->getData()->getField("noteId");
        $note = $controller->getNotification($noteId);
        if ($note === null) {
            $this->error();
        }
        if ($this->getData()->hasField("alreadyRead")){
            $note->setAlreadyRead($this->getData()->getField("alreadyRead"));
            $controller->updateNotification($note);
            die(json_encode_yc(array("array"=>"ok")));
            return false;
        }
        $note->setAlreadyRead(true);
        $controller->updateNotification($note);
        WebYouCurte::redirect(new URL($note->getLink(),URL_ROOT)."");
        return false;
    }

    protected function content() {
        return false;
    }

    /**
     * Função que redireciona a página atual para a de erro
     */
    public function error() {
        WebYouCurte::redirect(URL_ROOT);
    }

}