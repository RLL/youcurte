package br.com.youcurte.utils;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestLog;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class TestUtils {

    public static boolean isLoggedIn(TestParams params) {
        return getUser(params).get("loggedIn", 0).asBool();
    }
    
    public static TestData getUser(TestParams params){
        String type = params.getTestType();
        TestData result = params.con.submit("user?auth=" + params.getData().get(type + "Auth")
                .asString()).asTestData();
        return result.get("data","{}").asTestData();
    }
    
    public static TestData getUser(TestParams params,String user){
        String type = params.getTestType();
        TestData result = params.con.submit("user/" + user).asTestData();
        return result.get("data","{}").asTestData();
    }

    public static boolean checkLogin(TestParams params,TestLog log){
        if (params.getData().existKey(params.getTestType()+"Auth")){
            return true;
        } else {
            log.printError("Precisa estar logado para realizar esse teste");
        }
        return false;
    }
    
    public static void insertAuth(TestParams params,TestData data){
        String auth = getAuth(params);
        data.put("auth", auth);
    }
    
    public static String getAuth(TestParams params){
        return params.getData().get(params.getTestType()+"Auth").asString();
    }
    
    public static TestData getImage(TestParams params,String id){
        return params.con.submit("album/image/"+id).asTestData().get("data","{}").asTestData();
    }
    
    public static TestData getAlbum(TestParams params,String albumId){
        String userId = getAuth(params);
        return params.con.submit("album/"+albumId+"/"+userId).asTestData().get("data","{}").asTestData();
    }
    
    public static TestData listImages(TestParams params,String albumId){
        String auth = getAuth(params);
        return params.con.submit("album/image/list/"+albumId+"/"+auth)
                .asTestData().get("data","[]").asTestData();
    }
    
    public static TestData listAlbums(TestParams params){
        String auth = getAuth(params);
        return params.con.submit("album/list/"+auth).asTestData().get("data","[]").asTestData();
    }
    
    public static TestData getPost(TestParams params,String postId){
        return params.con.submit("post/"+postId+"?auth="+getAuth(params)).asTestData().get("data","{}").asTestData();
    }
    
    public static TestData listNotifications(TestParams params){
        return params.con.submit("notification/"+getAuth(params)).asTestData()
                .get("data","[]").asTestData();
    }
    
    public static TestData listDepositions(TestParams params,String userId){
        return params.con.submit("deposition/list/"+userId).asTestData()
                .get("data","[]").asTestData();
    }
    
}
