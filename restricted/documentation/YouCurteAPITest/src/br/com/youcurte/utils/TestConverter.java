package br.com.youcurte.utils;

import java.util.Objects;

/**
 *
 * @author Luiz
 */
public class TestConverter {
    
    private Object data = null;
    
    public TestConverter(Object data){
        this.data = data;
    }
    
    public String asString(){
        return data.toString();
    }
    
    public TestData asTestData(){
        return TestDataStream.getInstance().getTestDataFromString(asString());
    }
    
    public boolean asBool(){
        return asString().equalsIgnoreCase("true")||asString().equals("1");
    }
    
    public int asInt(){
        return Integer.parseInt(asString());
    }
    
    public float asFloat(){
        return Float.parseFloat(asString());
    }
    
    public Object asObject(){
        return this.data;
    }
    
    @Override
    public boolean equals(Object other){
        if (other instanceof TestConverter){
            other = ((TestConverter)other).asObject();
        }
        return data.equals(other);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.data);
        return hash;
    }
    
}
