package br.com.youcurte.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 * Os dados a serem enviados para a API, também os dados recebidos
 *
 * @author Luiz
 * @param <Key> O tipo para key
 */
public abstract class TestData<Key> {

    protected final Map<Key, Object> map;

    public TestData() {
        map = new HashMap<>();
    }

    /**
     *
     * Método construtor
     *
     * @param data Inicia os com os dados deste TestData
     */
    public TestData(Object data) {
        this(((TestData) data));
    }

    /**
     * Método construtor
     *
     * @param testData Inicia os com os dados deste TestData
     */
    public TestData(TestData testData) {
        this.map = testData.map;
    }

    public TestData<Key> put(Key key, Object value) {
        if (value instanceof TestConverter){
            value = ((TestConverter)value).asObject();
        }
        this.map.put(key, value);
        return this;
    }

    public TestConverter get(Key key) {
        return new TestConverter(this.map.get(key));
    }
    
    public int size(){
        return this.map.size();
    }

    public TestConverter get(Key key, Object default_value) {
        if (existKey(key)) {
            return get(key);
        } else {
            return new TestConverter(default_value);
        }
    }

    public boolean existKey(Key key) {
        return listKeys().indexOf(key) > -1;
    }

    public List<TestConverter> listValues() {
        List<TestConverter> list = new ArrayList();
        map.values().stream().forEach((obj) -> {
            list.add(new TestConverter(obj));
        });
        return list;
    }

    public List<Key> listKeys() {
        List<Key> list = new ArrayList();
        map.keySet().stream().forEach((obj) -> {
            list.add(obj);
        });
        return list;
    }

    public TestData<Key> remove(Key key) {
        map.remove(key);
        return this;
    }

    public Map<Key, Object> getMap() {
        return map;
    }

    public boolean isEmpty() {
        return listValues().size() <= 0;
    }

    public boolean equals(TestData data, TestLog log) {
        for (Key key : listKeys()) {
            if (data.existKey(key)) {
                boolean stop = false;
                Object value1, value2;
                value1 = get(key).asObject();
                value2 = data.get(key).asObject();
                if (value1 instanceof String && value2 instanceof String) {
                    try {
                        if (!value1.equals(value2)) {
                            TestData t1, t2;
                            t1 = TestDataStream.getInstance().getTestDataFromString(value1.toString());
                            t2 = TestDataStream.getInstance().getTestDataFromString(value2.toString());
                            if (t1.equals(t2,log)) {
                                stop = true;
                            } else {
                                return false;
                            }
                        }
                    } catch (Exception e) {
                    }
                }
                if (!stop && !get(key).asString().trim().equals(data.get(key).asString().trim())) {
                    log.printOnScreen("Key incompatível: " + key);
                    log.printOnScreen("Valor sobre this: " + get(key).asString());
                    log.printOnScreen("Valor sobre o outro: " + data.get(key).asString());
                    return false;
                }
            } else {
                log.printOnScreen("Key não encontrada no outro dado: " + key);
                return false;
            }
        }
        for (Object key:data.listKeys()){
            if (!this.existKey((Key)key)){
                log.printOnScreen("Key não encontrada no this: " + key);
            }
        }
        return true;
    }

    public abstract String toJson();

    public String toXml() {
        String json = toJson();
        Object obj = null;
        if (json.startsWith("{")) {
            obj = new JSONObject(json);
        } else {
            obj = new JSONArray(json);
        }
        String xml = XML.toString(obj);
        return "<YouCurte>" + xml + "</YouCurte>";
    }
    
    public String to(String contentType){
        if (TestConnection.CONTENT_JSON.equals(contentType)){
            return toJson();
        } else if (TestConnection.CONTENT_XML.equals(contentType)) {
            return toXml();
        }
        throw new RuntimeException("Content-type inválido!");
    }

    public TestData<Key> loadXml(String xml) {
        loadJson(adaptFromXML(XML.toJSONObject(xml).toString()));
        return this;
    }

    public abstract TestData loadJson(String json);
    
    public static String adaptFromXML(String xml){
        xml = xml.replace ("&#xe1;","á");
        return xml.replace("&#xe9;", "é");
    }

    public TestData getTestData(Key key) {
        String value = get(key).asString();
        TestDataStream tds = TestDataStream.getInstance();
        return tds.getTestDataFromString(value);
    }

    public TestData getAsTestData(Key key) {
        return getTestData(key);
    }

    public TestData<Key>[] listValuesAsTestData() {
        List<TestData<Key>> list = new ArrayList();
        for (Key key : listKeys()) {
            list.add(getTestData(key));
        }
        return list.toArray(new TestData[list.size()]);
    }

}
