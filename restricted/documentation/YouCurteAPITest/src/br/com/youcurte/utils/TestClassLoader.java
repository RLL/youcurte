package br.com.youcurte.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Luiz
 */
public class TestClassLoader extends ClassLoader {

    public static byte[] loadBytesForClass(ClassLoader loader, String fqn) throws IOException {
        InputStream input = loader.getResourceAsStream(fqn.replace(".", "/") + ".class");
        final int length = 1024*4;
        byte[] buffer = new byte[length];
        if (input == null) {
            System.out.println("Could not load bytes for class: " + fqn);
        }
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while (input.read(buffer)>=length){
            output.write(buffer);
        }
        return output.toByteArray();
    }

}
