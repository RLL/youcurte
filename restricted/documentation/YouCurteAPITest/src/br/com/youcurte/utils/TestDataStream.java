package br.com.youcurte.utils;

import br.com.youcurte.main.YouCurteAPITester;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.json.XML;

/**
 *
 * @author Luiz
 */
public class TestDataStream {

    protected TestLog log;
    
    private final static TestDataStream instance = new TestDataStream();

    public TestDataStream() {
        log = new TestLog("TestDateStream.txt");
        log.setPrintTime(true);
    }


    public TestConverter loadFromResource(String filename) {
        log.printLogEnd();
        log.printOnFile("Lendo resource...");
        String content = loadContentFromResource(filename);
        return new TestConverter(content);
    }

    public TestData getTestDataFromString(String string) {
        log.printOnFile("Convertendo uma string em TestData...");
        log.printSeparetorOnFile();
        log.printOnFile(string);
        log.printSeparetorOnFile();
        if (string.trim().startsWith("<")) {
            return getTestDataFromString(TestData.adaptFromXML(XML.toJSONObject(string).toString())).
                    getAsTestData("YouCurte");
        } else if (string.trim().startsWith("{")) {
            TestData result = new TestDataObject();
            result.loadJson(string);
            if (result.existKey("item")){
                result = result.getTestData("item");
                if (result instanceof TestDataObject){
                    TestData array = new TestDataArray();
                    array.put(0, result.toJson());
                    result = array;
                }
            }   
            return result;
        } else if (string.trim().startsWith("[")) {
            TestData result = new TestDataArray();
            result.loadJson(string);
            return result;
        } else {
            log.printError("Erro na conversão! Padrão não reconhecido:");
            log.printError(string);
            RuntimeException e = new RuntimeException("Problema na conversão de uma string em dados de teste:\r\n"
                    + " não foi reconhecido o padrão");
            e.printStackTrace(log.getPrintWriter());
            throw e;
        }
    }

    public String loadContentFromResource(String filename) {
        try {
            String content = "";
            filename = "/"+(YouCurteAPITester.PACKAGE_WITH_RESOURCES+".").replace(".","/")
                    .replaceFirst("package ", "")+filename;
            log.printOnFile("Lendo o arquivo '" + filename + "'....");
            InputStream in = this.getClass().getResourceAsStream(filename);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line = null;
            while ((line = reader.readLine()) != null) {
                content+=line+"\r\n";
            }
            log.printOnFile("Conteúdo do arquivo:");
            log.printOnFile(content);
            return content;
        } catch (Exception ex) {
            log.printError("Erro em abrir o arquivo: "+filename);
            throw new RuntimeException("Erro em abrir o arquivo: "+filename,ex);
        }
    }

    public static TestDataStream getInstance(){
        return instance;
    }
    
}
