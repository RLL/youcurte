package br.com.youcurte.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Luiz
 */
public class TestConnection {

    private String urlRoot, url;

    public static String CONTENT_XML = "application/xml";
    public static String CONTENT_JSON = "application/json";
    public static String CONTENT_TEXT = "text/plain; charset=utf-8";

    private TestLog log = new TestLog(new File("TestConnection.txt"));
    
    /**
     * Método construto
     *
     * @param urlRoot Define a url raiz do API
     */
    public TestConnection(String urlRoot) {
        this.urlRoot = urlRoot;
    }

    public TestConnection() {
        this("");
    }

    /**
     * Retorna o url raiz
     *
     * @return
     */
    public String getUrlRoot() {
        return urlRoot;
    }

    /**
     * Define url raiz da API (Parte inicial da url que não muda)
     *
     * @param urlRoot
     */
    public void setUrlRoot(String urlRoot) {
        this.urlRoot = adaptUrl(urlRoot);
    }
    
    public String adaptUrl(String url){
        return url.replace(" ", "%20");
    }

    /**
     * Retorna a url da API
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     * Define a url para a api
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = adaptUrl(url);
    }

    /**
     * Retorna todo o url
     *
     * @return
     */
    public String getAllUrl() {
        return this.getUrlRoot() + this.getUrl();
    }

    /**
     * Submete os dados de input para a API e retorna a resposta
     *
     * @param method O método que vai ser enviado (POST,GET,PUT,DELETE)
     * @param contentType O tipo de conteúdo JSON ou OUTRO
     * @param input Os dados de entrada
     * @return Retorna a resposta da API
     */
    public TestConverter submit(String method, String contentType, String input) {
        String output = "", line = "";
        log.printLogEnd();
        try {
            log.printTimeOnFile();
            log.printOnFile("Conectando ao servidor...");
            log.printOnFile("URL:  " + getAllUrl() + "\r\nMétodo: " + method + "\r\n");
            log.printOnFile("Content-type: " + contentType);
            log.printOnFile("Dados de entrada:");
            log.printOnFile(input);
            URL api = new URL(this.getAllUrl());
            HttpURLConnection conn = (HttpURLConnection) api.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod(method.toUpperCase());
            conn.setRequestProperty("Content-Type", contentType);
            if (!method.equalsIgnoreCase("get")) {
                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();
            }

            /*if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
             throw new RuntimeException("Failed : HTTP error code : "
             + conn.getResponseCode());
             */
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            while ((line = br.readLine()) != null) {
                output += line + "\r\n";
            }
            log.printOnFile("Dados de saída:");
            log.printOnFile(output);
            conn.disconnect();
            log.printLogEnd();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new TestConverter(output);
    }

    public TestConverter submit(String method, String input) {
        return submit(method, TestConnection.CONTENT_JSON, input);
    }

    public TestConverter submit(String url) {
        this.setUrl(url);
        return submit("GET", CONTENT_TEXT, "");
    }
    
    public TestConverter submit(String method,String url, TestData input){
        setUrl(url);
        return submit(method,CONTENT_JSON, input.toJson());
    }
    
    public TestConverter submit(){
        return submit(this.getUrl());
    }

    public TestLog getLog() {
        return log;
    }

    public void setLog(TestLog log) {
        this.log = log;
    }
    
    

}
