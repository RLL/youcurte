package br.com.youcurte.utils;

/**
 * Classe com os dados de teste
 * @author Luiz
 */
public class TestParams {
    
    private TestDataObject data;
    public TestConnection con;
    private TestLog log;
    private TestDataStream stream;
    private String testType;

    public TestDataObject getData() {
        return data;
    }

    public void setData(TestDataObject data) {
        this.data = data;
    }

    public TestConnection getConnection() {
        return con;
    }

    public void setConnection(TestConnection connection) {
        this.con = connection;
    }

    public TestLog getLog() {
        return log;
    }

    public void setLog(TestLog log) {
        this.log = log;
    }

    public TestDataStream getStream() {
        return stream;
    }

    public void setStream(TestDataStream stream) {
        this.stream = stream;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }
    
    public boolean isTestType(String testType){
        return this.testType.equalsIgnoreCase(testType);
    }
    
    public String getContentType(){
        if (getTestType().equals("json")){
            return TestConnection.CONTENT_JSON;
        } else if(getTestType().equals("xml")) {
            return TestConnection.CONTENT_XML;
        }
        return TestConnection.CONTENT_TEXT;
    }
    
    
    
}
