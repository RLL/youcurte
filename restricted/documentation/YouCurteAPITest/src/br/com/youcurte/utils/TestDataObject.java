package br.com.youcurte.utils;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

/**
 * Os dados a serem enviados para a API, também os dados recebidos
 *
 * @author Luiz
 */
public class TestDataObject extends TestData<String> {

    public TestDataObject(TestData data){
        super(data);
    }
    
    public TestDataObject(Object data){
        super(data);
    }
    
    public TestDataObject(){
        super();
    }

    @Override
    public String toJson() {
        JSONObject obj = new JSONObject();
        for (String key:listKeys()){
            Object value = get(key).asObject();
            if (value instanceof TestData){
                value = ((TestData)value).toJson();
            }
            obj.put(key, value);
        }
        return obj.toString();
    }

    @Override
    public TestData<String> loadJson(String json) {
        JSONObject obj = new JSONObject(json);
        for (String key:obj.keySet()){
            this.put(key,obj.get(key));
        }
        return this;
    }

    
    

}
