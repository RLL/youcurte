package br.com.youcurte.utils;

import org.json.JSONArray;

/**
 *
 * @author Luiz
 */
public class TestDataArray extends TestData<Integer> {

    /**
     * Qual vai ser a primeira key
     */
    public static int INIT_VALUE = 0;

    public TestDataArray(TestData data) {
        super(data);
    }

    public TestDataArray(Object data) {
        super(data);
    }

    public TestDataArray() {
        super();
    }

    /**
     * Inseri um novo valor nos dados
     * @param value O valor que quer inserir
     * @return O chave(key) do value
     */
    public int put(Object value) {
        int i;
        for (i = INIT_VALUE; existKey(i); i++) {
        }
        put(i, value);
        return i;
    }

    @Override
    public String toJson() {
        JSONArray array = new JSONArray();
        for (TestConverter value : listValues()) {
            array.put(value.asString());
        }
        return array.toString();
    }

    @Override
    public TestData<Integer> loadJson(String json) {
        JSONArray array = new JSONArray(json);
        array.forEach((value) -> {
            put(value);
        });
        return this;
    }

}
