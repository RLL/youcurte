package br.com.youcurte.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luiz
 */
public class TestLog {

    private File file = null;
    private boolean printTime = false;
    private int contator = 0, errors = 0;
    private PrintWriter pw = null;
    private FileWriter fw = null;
    
    public TestLog() {

    }

    public TestLog(String file) {
        this(new File(file));
    }

    /**
     * Define o arquivo de log
     *
     * @param file
     */
    public TestLog(File file) {
        this.file = file;
    }

    /**
     * Imprime um mensagem no arquivo de log
     *
     * @param content O conteúdo da mensagem
     */
    public void printOnFile(String content) {
        PrintWriter gravarArq = getPrintWriter();
        if (isPrintTime()) {
            content = getTime() + ": " + content;
        }
        gravarArq.println(content);
        gravarArq.flush();
    }

    public PrintWriter getPrintWriter() {
        if (this.pw == null) {
            FileWriter fw = getFileWriter();
            pw = new PrintWriter(fw);
        }
        return this.pw;
    }

    public FileWriter getFileWriter() {
        if (this.fw == null) {
            try {
                this.fw = new FileWriter(file);
            } catch (IOException ex) {
                Logger.getLogger(TestLog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.fw;
    }

    public void flush() {
        try {
            getFileWriter().flush();
        } catch (IOException ex) {
            //Logger.getLogger(TestLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printOnFile(String msg, boolean printTime) {
        boolean print = isPrintTime();
        setPrintTime(printTime);
        printOnFile(msg);
        setPrintTime(print);
    }

    public void printError(String msg) {
        errors++;
        String content = "Error:" + getTime() + ": " + msg;
        System.err.println(content);
        printOnFile(content, false);
    }

    public void printOnScreen(String content) {
        System.out.println(getTime() + ": " + content);
        printOnFile(getTime() + ": " + content);
    }

    public void printLogEnd() {
        printOnFile("", false);
        printOnFile("", false);
    }

    /**
     * Imprime no arquivo de log o ----- hora e data atual -----
     */
    public void printTimeOnFile() {
        printOnFile("----- " + getTime() + " -----");
    }

    /**
     * Obtém a hora e data ----- hora e data atual ------
     *
     * @return
     */
    public String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        Date date = new Date();
        return (contator++) + "-" + dateFormat.format(date);
    }

    /**
     * É para imprimir em cada linha do arquivo a hora e data?
     *
     * @return true = sim <br/> false = não
     */
    public boolean isPrintTime() {
        return printTime;
    }

    /**
     * Define se é para imprimir em cada linha do arquivo a hora e data
     *
     * @param printTime true = imprimir <br/> false = não
     */
    public void setPrintTime(boolean printTime) {
        this.printTime = printTime;
    }

    public void printSeparetorOnFile() {
        String separator = "";
        int size = getTime().length();
        for (int i = 0; i < size; i++) {
            separator += "-";
        }
        printOnFile(separator);
    }

    public int countErrors() {
        return errors;
    }

    public void setCountErrors(int count) {
        this.errors = count;
    }

    
    public File getFile(){
        return file;
    }


    @Override
    public void finalize() {
        try {
            flush();
            getPrintWriter().close();
            getFileWriter().close();
            super.finalize();
        } catch (Throwable ex) {
           // Logger.getLogger(TestLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
