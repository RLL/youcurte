package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PostLocalTest extends Test {
    
    TestData input;
    
    @Override
    public String getTestName() {
        return "Postando com local definido...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = new TestDataObject();
        input.put("text", "Testando...");
        input.put("local","Montenegro");
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("post");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        for (Object key:input.listKeys()){
            if (!key.equals("location")&&!key.equals("auth")&&
                    !input.get(key).equals(output.getTestData("data").get(key))){
                return false;
            }
        }
        return true;
    }
    
}
