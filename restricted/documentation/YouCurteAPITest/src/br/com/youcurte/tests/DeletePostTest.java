package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class DeletePostTest extends Test {

    String postId;
    
    @Override
    public String getTestName() {
        return "Deletando um post...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = new TestDataObject();
        postId = params.isTestType("json")?"7":"5";
        input.put("postId",postId);
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("post");
        return params.con.submit("DELETE", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData post = TestUtils.getPost(params, postId);
        return post.isEmpty();
    }
    
}
