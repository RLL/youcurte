package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PostAttributesTest extends Test {

    String userId = "2";
    TestData input = new TestDataObject();
    
    @Override
    public String getTestName() {
        return "Post atributos...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input.put("sexy","10");
        input.put("cool","20");
        input.put("reliable","30");
        input.put("user",userId);
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user/attributes");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").asString().equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData data = output.getTestData("data");
        final String fields[] = new String[]{"sexy","cool","reliable"};
        for (String field:fields){
            if (!data.get(field).asString().equals(input.get(field).asString())){
                return false;
            }
        }
        params.getData().put("attributes", input.toJson());
        return true;
    }
    
}
