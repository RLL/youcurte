/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestConnection;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class LogoutAllTest extends LogoutTest {
    
    @Override
    public boolean checkParams(TestParams params) {
        auth = params.getTestType() + "Auth";
        if (!params.getData().existKey(auth)) {
            getLog().printError("Deve ter um usuário logado para efeituar o logout de tudo.");
            return false;
        }
        auth = params.getData().get(auth).asString();
        return params.isTestType("json");
    }
    
    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user/login/all");
        return params.con.submit("DELETE", contentType, input).asTestData();
    }
    
}
