package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;
import java.util.List;

/**
 *
 * @author Luiz
 */
public class UpdateNotificationTest extends Test {

    String notificationId = "3";
    @Override
    public String getTestName() {
        return "Marcando uma notificação como lida...";
    }
    
    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = new TestDataObject();
        input.put("notificationId",notificationId);
        input.put("read",true);
        TestUtils.insertAuth(params, input);
        return input;
    }
    
    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("notification");
        return params.con.submit("PUT", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.getTestData("data").get("read").asBool();
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData[] notifications = TestUtils.listNotifications(params).listValuesAsTestData();
        for (TestData not:notifications){
            if (not.get("notificationId").asString().equals(notificationId)){
                    return not.get("read").asBool();
            }
        }
        return false;
    }
    
}
