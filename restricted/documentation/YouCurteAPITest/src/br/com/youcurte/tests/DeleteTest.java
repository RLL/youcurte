package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class DeleteTest extends Test {

    static final String KEY_IS_DB_RESETED = "isDBReseted";

    @Override
    public String getTestName() {
        return "Teste de Delete";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = new TestDataObject();
        data.put("database", "YouCurte");
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("test/");
        return params.con.submit("DELETE", input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return true;
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        if (output.get("result", "").asString().equals("ok")) {
            params.getLog().printOnScreen(output.get("data").asString());
            params.getData().put(KEY_IS_DB_RESETED, true);
        }
        return true;
    }

    @Override
    public boolean checkParams(TestParams params) {
        return params.isTestType("json");
    }

}
