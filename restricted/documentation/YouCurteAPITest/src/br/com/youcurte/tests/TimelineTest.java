package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class TimelineTest extends ListPostsTest {

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return params.con.submit("post/timeline/" + TestUtils.getAuth(params)).asTestData();
    }

}
