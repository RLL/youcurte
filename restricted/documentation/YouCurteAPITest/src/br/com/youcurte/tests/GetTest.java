package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class GetTest extends Test {

    @Override
    public String getTestName() {
        return "Método GET";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return new TestDataObject();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        System.out.print(output.get("data","").asString());
        return output.get("data","").asString().equals("value");
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return params.con.submit("test/value").asTestData();
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }
    
}
