package br.com.youcurte.tests;

import br.com.youcurte.utils.TestConverter;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;
import java.util.List;

/**
 *
 * @author Luiz
 */
public class ListDepositionTest extends Test {

    String userId = "2";
    int count = 2;
    
    @Override
    public String getTestName() {
        return "Listando depoimentos";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return TestUtils.listDepositions(params,userId);
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.size()==count;
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        List<TestConverter> list = output.listValues();
        for (TestConverter item:list){
            TestData data = item.asTestData();
            if (!data.existKey("depositionId")||!data.getTestData("userFrom").existKey("id")){
                return false;
            }
        }
        return true;
    }
    
    
    
}
