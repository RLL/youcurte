/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class ListAlbumsTest extends Test {

    /**
     * Quantidades de álbum
     */
    int count = 4;
    
    @Override
    public String getTestName() {
        return "Listando albuns...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return TestUtils.listAlbums(params);
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return !output.isEmpty();
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return output.size()==count;
    }
    
}
