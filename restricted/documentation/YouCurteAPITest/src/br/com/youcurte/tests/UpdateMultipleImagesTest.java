/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class UpdateMultipleImagesTest extends UpdateImageTest {

    String album, images[] = new String[]{"1", "3"};

    @Override
    public String getTestName() {
        return "Editando dados uma imagem...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        album = "Meu álbum - " + params.getTestType();
        input = new TestDataObject();
        input.put("album", album);
        input.put("images", images);
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        for (String id : images) {
            TestData image = TestUtils.getImage(params, id);
            boolean result = album.equals(image.get("album").asString());
            getLog().printOnFile(album);
            getLog().printOnFile(image.toJson());
            if (!result) {
                return false;
            }
        }
        return true;
    }

}
