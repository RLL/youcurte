/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class UpdateUserImageTest extends Test {
    
    @Override
    public String getTestName() {
        return "Troca o avatar do usuario logado...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = params.getStream().loadFromResource("sendImage.json").asTestData();
        TestUtils.insertAuth(params, data);
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user");
        return params.con.submit("PUT", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData user = TestUtils.getUser(params);
        return user.existKey("imageId")&&!user.get("image", "").asString().contains("default");
    }
    
}
