/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class SearchByLucasTest extends SearchBySilvasTest {

    @Override
    public String getTestName() {
        return "Pesquisando por Lucas...";
    }
    
    
    @Override
    public TestData send(TestParams params, String input, String contentType) {
        String auth = TestUtils.getAuth(params);
        return params.con.submit("user/search/l gabriel?auth="+auth).asTestData();
    }
    
    
    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData users = output.getTestData("data").getTestData("users");
        int count = output.getTestData("data").get("count",-1).asInt();
        if (users.size()!=1){
            getLog().printError("Retorna um valor diferente que devia ser de usuários: "+users.size());
        }
        return users.size()==1&&count==1&&users.getTestData(0).get("following",false).asBool();
    }
    
}
