package br.com.youcurte.tests;

import br.com.youcurte.utils.TestConnection;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 * Teste de registrar contas
 *
 * @author Luiz
 */
public class RegisterFernandaTest extends RegisterLuizTest {

    private TestData input;

    @Override
    public String getTestName() {
        return "Registrar conta de Fernanda...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = params.getStream().loadFromResource("registerFernanda.json").asTestData();
        return input;
    }

}
