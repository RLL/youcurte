package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class ListImagesTest extends Test{

    String album = "0";
    
    @Override
    public String getTestName() {
        return "Listar imagens de álbum...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return TestUtils.listImages(params, album);
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return !output.isEmpty();
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData image = output.getTestData(0);
        boolean id;
        id = image.get("imageId","-1").asInt()>0;
        return id;
    }
    
}
