
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PostDepositionTest extends Test {

    String userId = "2",text="tu é show";
    
    @Override
    public String getTestName() {
        return "Fazendo um depoimento...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = new TestDataObject();
        input.put("text",text);
        input.put("user",userId);
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("deposition");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        boolean bText,bUser,bUserLogged;
        TestData data = output.getTestData("data");
        bText = text.equals(data.get("text").asString());
        bUser = data.getTestData("userFrom").existKey("id");
        bUserLogged = data.get("userIdTo").asString().equals(userId);
        return bText&&bUser&&bUserLogged;
    }
    
}
