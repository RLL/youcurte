package br.com.youcurte.tests;

import br.com.youcurte.utils.TestUtils;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class FollowTest extends Test{
    
    String[] files = new String[]{"followFernanda.json","followLucas.json"};
    int index = 0;

    @Override
    public String getTestName() {
        return "Seguindo alguém...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params,getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = params.getStream().loadFromResource(files[index]).asTestData();
        TestUtils.insertAuth(params,data);
        index++;
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user/follow");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData user = TestUtils.getUser(params);
        return user.get("related", 0).asInt()==index;
    }

    
    
}
