
package br.com.youcurte.tests;

import br.com.youcurte.main.YouCurteAPITester;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class ActiveAccounts extends Test {

    static final String[] USERS = new String[]{"Luiz","Fernanda","Lucas"};
    
    @Override
    public String getTestName() {
        return "Ativando as contas...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return params.isTestType("json");
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        TestData out = new TestDataObject();
        int ok = 0;
        for (String user:USERS){
            TestData data = params.getStream().loadFromResource("register"+user+".json").asTestData();
            String email = data.get("email").asString();
            params.con.setUrlRoot(YouCurteAPITester.URL_ROOT);
            params.con.setUrl("register/active/"+email+"/luiz");
            String result = params.con.submit().asString();
            if (result.contains("Seu e-mail foi confirmado!")){
                ok++;
            }
        }
        out.put("ok", ok);
        return out;
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("ok",0).asInt()==USERS.length;
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }
    
}
