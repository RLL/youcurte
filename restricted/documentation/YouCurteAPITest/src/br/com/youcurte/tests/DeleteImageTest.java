/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class DeleteImageTest extends Test {

    String imageId;
    
    @Override
    public String getTestName() {
        return "Deletando uma imagem...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        imageId = params.isTestType("json")?"1":"3";
        TestData data = new TestDataObject();
        data.put("image",imageId);
        TestUtils.insertAuth(params, data);
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("album/image");
        return params.con.submit("DELETE", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData image = TestUtils.getImage(params,imageId);
        return image.isEmpty();
    }
    
}
