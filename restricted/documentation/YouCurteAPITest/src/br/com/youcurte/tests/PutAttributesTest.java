package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class PutAttributesTest extends PostAttributesTest {

    @Override
    public String getTestName() {
        return "Atualizando atributos...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = super.getDataInput(params);
        input.put("sexy", "1");
        input.put("cool", "45");
        input.put("reliable", "50");
        return input;
    }

}
