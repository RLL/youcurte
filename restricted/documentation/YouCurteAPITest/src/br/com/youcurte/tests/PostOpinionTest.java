package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PostOpinionTest extends Test {

    String tri,bah,postId = "1";
    
    @Override
    public String getTestName() {
        return "Opinando sobre um post...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = new TestDataObject();
        TestUtils.insertAuth(params, input);
        tri = params.isTestType("json")?"1":"0";
        bah = params.isTestType("json")?"0":"1";
        input.put("tri",tri);
        input.put("bah",bah);
        input.put("postId",postId);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("post/opinion");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        boolean bTri,bBah;
        TestData data = output.get("data","{}").asTestData();
        bTri = data.get("my_tri","-1").asString().equals(tri);
        bBah = data.get("my_bah","-1").asString().equals(bah);
        return bTri&&bBah;
    }
    
}
