package br.com.youcurte.tests;

import br.com.youcurte.tests.Test;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class SearchBySilvasTest extends Test {

    @Override
    public String getTestName() {
        return "Pesquisando por Silvas...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return params.con.submit("user/search?name=Fe Silva").asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData users = output.getTestData("data").getTestData("users");
        int count = output.getTestData("data").get("count",-1).asInt();
        if (users.size()!=2){
            getLog().printError("Retorna um valor diferente que devia ser de usuários: "+users.size());
        }
        return users.size()==2&&!users.getTestData(0).isEmpty()&&!users.getTestData(1).isEmpty()&&count==2;
    }
    
}
