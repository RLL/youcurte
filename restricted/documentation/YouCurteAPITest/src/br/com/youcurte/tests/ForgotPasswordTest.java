package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class ForgotPasswordTest extends Test {

    String file = "registerLuiz.json";
    
    @Override
    public String getTestName() {
        return "Testando o esqueceu a senha?";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData user,input = new TestDataObject();
        user = params.getStream().loadFromResource(file).asTestData();
        input.put("email",user.get("email").asString());
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user/forgotpassword");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","").asString().equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }
    
}
