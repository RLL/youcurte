package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class UpdateLuizTest extends Test {

    String auth, password = "";
    TestData input;

    @Override
    public String getTestName() {
        return "Alterando os dados de Luiz...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = params.getStream().loadFromResource("updateLuiz.json").asTestData();
        auth = params.getData().get(params.getTestType() + "Auth").asString();
        password = data.get("newPassword").asString();
        data.put("auth", auth);
        input = data;
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user");
        return params.con.submit("PUT", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result").asString().equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData loginInput, loginOutput, user, update;
        String city1, city2;
        loginInput = params.getStream().loadFromResource("loginLuiz.json").asTestData();
        loginInput.put("password", password);
        loginOutput = params.con.submit("POST", "user/login", loginInput).asTestData();
        auth = loginOutput.getTestData("data").get("auth").asString();
        user = params.con.submit("user/" + auth).asTestData();
        city1 = user.getTestData("data").get("city").asString();
        city2 = input.get("city").asString();

        /*
         * Voltando aos dados originais (senha)
         */
        getLog().printOnFile("Voltando a senha original...");
        update = params.getStream().loadFromResource("updateLuiz.json").asTestData();
        update.put("newPassword",update.get("password"));
        update.put("password",password);
        update.put("auth", auth);
        update = params.con.submit("PUT", "user", update).asTestData();
        if (!update.get("result").equals("ok")){
            getLog().printOnScreen("Não foi possível voltar a senha para a forma original!");
        }
        return city1.equals(city2);
    }

}
