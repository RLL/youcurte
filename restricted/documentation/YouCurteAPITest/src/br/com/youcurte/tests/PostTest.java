package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class PostTest extends Test {

    @Override
    public String getTestName() {
        return "Teste de post";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = new TestDataObject();
        data.put("value", "teste");
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("test");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("data", "").asString().equals("teste");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }
    
}
