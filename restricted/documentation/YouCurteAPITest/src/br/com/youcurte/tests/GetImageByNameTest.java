/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class GetImageByNameTest extends GetImageTest {

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return TestUtils.getImage(params, "nome");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return output.get(0).asTestData().existKey("imageId");
    }

}
