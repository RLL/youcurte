package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PutDepositionTest extends PostDepositionTest {

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = new TestDataObject();
        text = "Sei "+params.getTestType()+"...";
        input.put("text", text);
        input.put("depositionId",1);
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("deposition");
        return params.con.submit("PUT", contentType, input).asTestData();
    }

}
