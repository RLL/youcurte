package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class PutTest extends Test {

    @Override
    public String getTestName() {
        return "Teste de PUT";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = new TestDataObject();
        data.put("teste", "asdf");
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("test/data");
        return params.con.submit("PUT", input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("data", "").asString().isEmpty();
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

}
