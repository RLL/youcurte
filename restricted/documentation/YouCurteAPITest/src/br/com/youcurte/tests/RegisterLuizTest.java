package br.com.youcurte.tests;

import br.com.youcurte.utils.TestConnection;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 * Teste de registrar contas
 *
 * @author Luiz
 */
public class RegisterLuizTest extends Test {

    private TestData input;

    @Override
    public String getTestName() {
        return "Registrar conta de Luiz Henrique...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = params.getStream().loadFromResource("registerLuiz.json").asTestData();
        return input;
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "").asString().equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        TestConnection tc = params.getConnection();
        tc.setUrl("user");
        return tc.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean checkParams(TestParams params) {
        return params.isTestType("json");
    }

}
