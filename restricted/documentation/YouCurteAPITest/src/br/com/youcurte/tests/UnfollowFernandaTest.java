
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class UnfollowFernandaTest extends Test{

    int related;
    
    @Override
    public String getTestName() {
        return "Deixar de seguir Fernanda...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        related = TestUtils.getUser(params).get("related","0").asInt();
        TestData data = params.getStream().loadFromResource("followFernanda.json").asTestData();
        TestUtils.insertAuth(params, data);
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user/follow");
        return params.con.submit("DELETE", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData user = TestUtils.getUser(params);
        if (params.isTestType("json")){
            related--;
        }
        return user.get("related", related).asInt()==related;
    }
    
}
