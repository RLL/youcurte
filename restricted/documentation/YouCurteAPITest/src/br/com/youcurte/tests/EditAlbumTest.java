/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class EditAlbumTest extends Test {

    String album,albumId = "0";
    
    @Override
    public String getTestName() {
        return "Editando álbum...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        album = "x - "+params.getTestType();
        TestData input = new TestDataObject();
        input.put("albumId",albumId);
        input.put("album",album);
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("album");
        return params.con.submit("PUT", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        boolean id,name;
        TestData data = output.get("data","{}").asTestData();
        id = data.get("albumId","-1").asString().equals(albumId);
        name = data.get("album","").asString().equals(album);
        return id&&name;
    }
    
}
