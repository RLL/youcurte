package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class MyTest extends Test {

    private TestData input;
    
    @Override
    public String getTestName() {
        return "Testando a si mesmo...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = new TestDataObject();
        data.put("teste","1");
        data.put("forte","{x:1,a:\"feito\"}");
        this.input = data;
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        TestData data = new TestDataObject();
        data.put("teste","1");
        data.put("forte","{x:1,a:\"feito\"}");
        return data;
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.equals(input,getLog());
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }

    
    
}
