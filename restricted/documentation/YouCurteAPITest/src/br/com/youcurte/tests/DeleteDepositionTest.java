
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class DeleteDepositionTest extends Test {

    String depositionId,userId = "2";
    int count = 2;
    
    @Override
    public String getTestName() {
        return "Deletando depoimento...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = new TestDataObject();
        TestUtils.insertAuth(params, data);
        depositionId = params.isTestType("json")?"1":"2";
        data.put("depositionId", depositionId);
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("deposition");
        count--;
        return params.con.submit("DELETE", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData depositions = TestUtils.listDepositions(params, userId);
        return depositions.size()==count;
    }
    
}
