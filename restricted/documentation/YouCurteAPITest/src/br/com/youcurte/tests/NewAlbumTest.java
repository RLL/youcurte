package br.com.youcurte.tests;



import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class NewAlbumTest extends Test{

    String album;
    
    @Override
    public String getTestName() {
        return "Criar um álbum...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = new TestDataObject();
        album = "Novo álbum - "+params.getTestType();
        input.put("album",album);
        TestUtils.insertAuth(params,input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("album");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        boolean id,name;
        TestData data =  output.get("data","{}").asTestData();
        id = data.get("albumId","-1").asInt()>0;
        name = data.get("album", "").asString().equals(album);
        return id&&name;
    }
    
}
