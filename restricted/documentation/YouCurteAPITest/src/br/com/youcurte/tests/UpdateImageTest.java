package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class UpdateImageTest extends Test {

    TestData image,input;
    
    @Override
    public String getTestName() {
        return "Editando dados uma imagem...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        image = TestUtils.getImage(params, "1");
        input = new TestDataObject();
        input.put("album","Novo");
        input.put("name","Teste.png");
        input.put("image",image.get("imageId").asString());
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("album/image");
        return params.con.submit("PUT" ,contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData image2 = TestUtils.getImage(params, "1");
        boolean album,name;
        album = input.get("album").equals(image2.get("album"));
        name = input.get("name").equals(image2.get("imageName"));
        getLog().printOnFile("Input");
        getLog().printOnFile(input.toJson());
        getLog().printOnFile("Image");
        getLog().printOnFile(image2.toJson());
        return album&&name;
    }
    
    
    
}
