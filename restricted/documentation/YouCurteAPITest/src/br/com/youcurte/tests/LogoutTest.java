/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestUtils;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class LogoutTest extends Test {

    String auth;
    
    @Override
    public String getTestName() {
        return "Logout local...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }
    

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = new TestDataObject();
        TestUtils.insertAuth(params, data);
        auth = data.get("auth").asString();
        return data;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user/login");
        return params.con.submit("DELETE", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").asString().equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return !TestUtils.isLoggedIn(params);
    }
    
}
