package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class XTest extends Test {

    @Override
    public String getTestName() {
        return "Teste de X";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return new TestDataObject();
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("test/test/1");
        return params.con.submit("DELETE", input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("data","").asString().equals("1");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }
    
   
    
}
