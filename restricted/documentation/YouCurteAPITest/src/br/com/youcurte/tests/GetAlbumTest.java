package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class GetAlbumTest extends Test {

    String album = "Default",albumId = "0";
    
    @Override
    public String getTestName() {
        return "Obtendo dados de um álbum";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return TestUtils.getAlbum(params, albumId);
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return !output.isEmpty();
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        boolean id,name;
        id = output.get("albumId","-1").asString().equals(albumId);
        name = output.get("album","").equals(album);
        return id&&name;
    }
    
}
