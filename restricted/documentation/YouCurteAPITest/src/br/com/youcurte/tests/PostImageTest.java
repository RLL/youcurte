package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PostImageTest extends PostLocalTest {
    
        
    @Override
    public String getTestName() {
        return "Postando uma imagem...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = params.getStream().loadFromResource("postImage.json").asTestData();
        TestUtils.insertAuth(params, input);
        return input;
    }
    
    
    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return output.getTestData("data").existKey("image");
    }
    
}
