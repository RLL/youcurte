package br.com.youcurte.tests;

import static br.com.youcurte.main.YouCurteAPITester.API_URL_ROOT;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestLog;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public abstract class Test {

    private TestLog log = null;

    public abstract String getTestName();

    public abstract boolean checkParams(TestParams params);

    public abstract TestData getDataInput(TestParams params);

    /**
     * Envia a entrada para o servidor e retorna a saída
     *
     * @param params Os paramêtros gerais
     * @param input Os dados de entrada
     * @return Os dados de saída
     */
    public abstract TestData send(TestParams params, String input, String contentType);

    /**
     * Valida se a saída tem os dados que são esperados
     *
     * @param params Os paramêtros globais
     * @param output Os dados de saída
     * @return true = dados de saída okay <br/> false = saída diferente do que
     * esperado
     */
    public abstract boolean valideOutput(TestParams params, TestData output);

    /**
     * Verifica se passou no teste
     *
     * @param params Os paramêtros globais
     * @param output Os dados de saída
     * @return true = passou no teste <br/> false = não passou no teste
     */
    public abstract boolean checkPassed(TestParams params, TestData output);

    public TestLog getLog() {
        if (this.log == null) {
            this.log = new TestLog(getClass().getName() + ".txt");
        }
        return this.log;
    }

    public boolean run(TestParams params) {
        TestLog origin = params.con.getLog();
        params.con.setLog(getLog());
        try {
            TestData input, output;
            if (!checkParams(params)) {
                return true;
            } else {
                input = getDataInput(params);
                if (null == input) {
                    input = new TestDataObject();
                }
                output = send(params, input.to(params.getContentType()), params.getContentType());
                params.getConnection().setUrlRoot(API_URL_ROOT);
                params.getLog().printOnScreen("Checando os dados de recebidos...");
                if (!valideOutput(params, output)) {
                    getLog().printError("Os dados recebidos não são como esperado!");
                    getLog().printOnFile("Entrada: " + input);
                    getLog().printOnFile("Saída: " + output.toJson());
                    getLog().printLogEnd();
                    params.getLog().printOnScreen("Os dados de recebidos não são como o esperado!");
                    System.out.println(output.toJson());
                    return false;
                }
                params.getLog().printOnScreen("Verificando se passou no teste...");
                if (!checkPassed(params, output)) {
                    getLog().printError("Não passou no teste!");
                    getLog().printOnFile("Saída: " + output.toJson());
                    getLog().printLogEnd();
                    params.getLog().printError("Não passou no teste! T_T");
                    System.out.println(output.toJson());
                    return false;
                } else {
                    params.getLog().printOnScreen("Passou! XD");
                }
            }
            params.con.setLog(origin);
            return true;
        } catch (Exception e) {
            e.printStackTrace(getLog().getPrintWriter());
            return false;
        }
    }

}
