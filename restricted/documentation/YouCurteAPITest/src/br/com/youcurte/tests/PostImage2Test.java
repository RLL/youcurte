package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PostImage2Test extends PostImageTest {

    @Override
    public String getTestName() {
        return "Postando uma imagem do álbum...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = params.getStream().loadFromResource("postImage2.json").asTestData();
        TestUtils.insertAuth(params, input);
        return input;
    }
}
