package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class SearchPostForHashtagsTest extends Test {

    int found = 2;
    
    @Override
    public String getTestName() {
        return "Pesquisando por hashtags...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return params.con.submit("post/search/?search=%23checking&auth="+TestUtils.getAuth(params)).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        TestData data,post,user;
        boolean bFound,bPost,bUser;
        data = output.getTestData("data");
        post = data.getTestData("posts").getTestData(0);
        user = post.getTestData("user");
        bFound = data.get("found", -1).asInt()==found;
        bPost = post.existKey("postId");
        bUser = user.existKey("id");
        return bFound&&bPost&&bUser;
    }
    
}
