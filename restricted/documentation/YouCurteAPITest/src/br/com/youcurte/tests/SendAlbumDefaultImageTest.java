/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class SendAlbumDefaultImageTest extends SendImageTest {

    @Override
    public String getTestName() {
        return "Enviando imagem para o album default...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData data = params.getStream().loadFromResource("sendImage.json").asTestData();
        TestUtils.insertAuth(params, data);
        return data;
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return output.getTestData("data").get("album", "").equals("Default");
    }
    
}
