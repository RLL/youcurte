package br.com.youcurte.tests;

import br.com.youcurte.utils.TestConnection;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 * Teste de registrar contas
 *
 * @author Luiz
 */
public class RegisterLucasTest extends RegisterLuizTest {

    private TestData input;


    @Override
    public String getTestName() {
        return "Registrar conta de Lucas...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = params.getStream().loadFromResource("registerLucas.json").asTestData();
        return input;
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result", "").asString().equals("ok");
    }
    
    @Override
    public boolean checkParams(TestParams params){
        return params.isTestType("xml");
    }

}
