package br.com.youcurte.tests;

import br.com.youcurte.utils.TestUtils;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class LoginLuizTest extends Test {

    @Override
    public String getTestName() {
        return "Testando login...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return params.getStream().loadFromResource("loginLuiz.json").asTestData();
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("user/login");
        return params.con.submit("post", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        if (output.get("result", "").asString().equals("ok")) {
            params.getData().put(params.getTestType() + "Auth",
                    output.getTestData("data").get("auth").asString());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return TestUtils.isLoggedIn(params);
    }
    
    
}
