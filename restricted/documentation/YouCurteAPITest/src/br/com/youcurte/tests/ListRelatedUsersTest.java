package br.com.youcurte.tests;

import br.com.youcurte.utils.TestConverter;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;
import java.util.List;

/**
 *
 * @author Luiz
 */
public class ListRelatedUsersTest extends Test{

    int minCount = 1;
    
    @Override
    public String getTestName() {
        return "Listando os parceiros de mate...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return params.con.submit("user/related/"+TestUtils.getAuth(params)).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","error").equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        List<TestConverter> data = output.getTestData("data").listValues();
        for (TestConverter user:data){
            if (!user.asTestData().get("following").asBool()){
                return false;
            }
        }
        return data.size()>=minCount;
    }
    
}
