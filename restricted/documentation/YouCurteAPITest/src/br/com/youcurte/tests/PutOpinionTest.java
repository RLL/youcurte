/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class PutOpinionTest extends PostOpinionTest {

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("post/opinion");
        return params.con.submit("POST", contentType, input).asTestData();
    }

}
