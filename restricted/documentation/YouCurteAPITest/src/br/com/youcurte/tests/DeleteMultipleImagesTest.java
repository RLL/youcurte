package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class DeleteMultipleImagesTest extends DeleteImageTest {

    String  images[] = new String[]{"2", "4"};

    
    @Override
    public String getTestName() {
        return "Deletando multiplas imagens...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = new TestDataObject();
        input.put("images", images);
        TestUtils.insertAuth(params, input);
        return input;
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        for (String id:images){
            TestData image = TestUtils.getImage(params, id);
            if (!image.isEmpty()){
                return false;
            }
        }
        return true;
    }

}
