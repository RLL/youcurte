package br.com.youcurte.tests;

import br.com.youcurte.utils.TestConverter;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;
import java.util.List;

/**
 *
 * @author Luiz
 */
public class ListNotificationTest extends Test{

    int countNotification = 0;
    @Override
    public String getTestName() {
        return "Listar notificações...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        TestData input = params.getStream().loadFromResource("loginLucas.json").asTestData();
        TestData output = params.con.submit("POST","user/login", input).asTestData();
        String auth = output.getTestData("data").get("auth").asString();
        TestData post = new TestDataObject();
        post.put("auth", auth);
        post.put("text","Notificando...");
        params.con.submit("POST","post", post);
        countNotification++;
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return TestUtils.listNotifications(params);
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.size()==countNotification;
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return !output.getTestData(0).get("read").asBool();
    }
    
}
