package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;

/**
 *
 * @author Luiz
 */
public class GetLucasLoggedOutTest extends Test {

    @Override
    public String getTestName() {
        return "Retorna de informações do Lucas (sem ter alguém logado)...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return true;
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return params.con.submit("user/2").asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        TestData example = params.getStream().loadFromResource("getLucasLoggedOut.json")
                .asTestData().get("data", "{}").asTestData();
        output = output.get("data","{}").asTestData();
        example.put("registryDate", output.get("registryDate").asObject());
        output.put("image", "");
        example.put("image","");
        return example.equals(output,getLog());
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return true;
    }
    
}
