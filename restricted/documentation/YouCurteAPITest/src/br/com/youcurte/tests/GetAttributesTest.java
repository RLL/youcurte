package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class GetAttributesTest extends Test {

    String userId = "2";
    @Override
    public String getTestName() {
        return "Lendo atributos...";
    }

    @Override
    public boolean checkParams(TestParams params) {
        return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData getDataInput(TestParams params) {
        return null;
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        return params.con.submit("user/attributes?auth="+TestUtils.getAuth(params)
                +"&user="+userId).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        return output.get("result","").asString().equals("ok");
    }

    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        String []fields = new String[]{"sexy","cool","reliable"};
        TestData check = params.getData().getTestData("attributes");
        for (String field:fields){
            if (!check.get(field).asString().equals(output.getTestData("data").get(field).asString())){
                return false;
            }
        }
        return true;
    }
    
}
