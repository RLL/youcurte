package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public class PostGPSTest extends PostLocalTest {
    
    @Override
    public String getTestName() {
        return "Postando com checkIn...";
    }

    @Override
    public TestData getDataInput(TestParams params) {
        input = params.getStream().loadFromResource("postGPSTest.json").asTestData();
        TestUtils.insertAuth(params, input);
        return input;
    }
    
    
    @Override
    public boolean checkPassed(TestParams params, TestData output) {
        return super.checkPassed(params,output)&&
                output.getTestData("data").get("local")
                        .equals("Rua Boa Vista, 737 - Santo Antonio, Montenegro - RS, 95780-000, Brasil");
    }
    
    
}
