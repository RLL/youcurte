package br.com.youcurte.tests;

import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestParams;
import br.com.youcurte.utils.TestUtils;

/**
 *
 * @author Luiz
 */
public abstract class SendImageTest extends Test {

    @Override
    public boolean checkParams(TestParams params) {
       return TestUtils.checkLogin(params, getLog());
    }

    @Override
    public TestData send(TestParams params, String input, String contentType) {
        params.con.setUrl("album/image");
        return params.con.submit("POST", contentType, input).asTestData();
    }

    @Override
    public boolean valideOutput(TestParams params, TestData output) {
        if (!output.get("result", "error").equals("ok")){
            return false;
        }
        return output.getTestData("data").get("image").asString().startsWith("http");
    }

    
}
