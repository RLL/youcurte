package br.com.youcurte.main;

import br.com.youcurte.tests.Test;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.com.youcurte.utils.TestClassLoader;
import br.com.youcurte.utils.TestConnection;
import br.com.youcurte.utils.TestData;
import br.com.youcurte.utils.TestDataObject;
import br.com.youcurte.utils.TestDataStream;
import br.com.youcurte.utils.TestLog;
import br.com.youcurte.utils.TestParams;
import java.io.IOException;
import java.nio.file.Files;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Classe principal para executar o teste
 *
 * @author Luiz
 */
public class YouCurteAPITester {

    public final static String URL_ROOT = "http://youcurte.com/";
    public final static String API_URL_ROOT = URL_ROOT + "api/";
    public final static String JSON_API_URL_ROOT = API_URL_ROOT;
    public final static String XML_API_URL_ROOT = API_URL_ROOT + "xml/";

    public static String PACKAGE_WITH_RESOURCES = "br.com.youcurte.resources";
    public static String PACKAGE_WITH_TESTS = "br.com.youcurte.tests";

    private TestParams params = null;
    private List<String> tests = null;
    private int countFailure = 0;
    private List<TestLog> logsOk = new ArrayList();

    public YouCurteAPITester() {
        TestConnection con = new TestConnection(API_URL_ROOT);
        params = new TestParams();
        params.setConnection(con);
        params.setLog(new TestLog("YouCurteAPI.txt"));
        params.setData(new TestDataObject());
        params.setStream(TestDataStream.getInstance());
    }

    public List<String> listTests() {
        return tests;
    }

    public void setTests(List<String> tests) {
        this.tests = tests;
    }

    public List<String> listTestsFromResource() {
        params.getLog().printOnScreen("Lendo arquivo com a ordem de testes...");
        String text = params.getStream().loadFromResource("Tests.txt").asString();
        params.getLog().printOnFile(text);
        return Arrays.asList(text.split("\r\n"));
    }

    public void run() {
        File ok = new File("ok");
        ok.mkdir();
        for (File file:ok.listFiles()){
            file.delete();
        }
        int countTests = 0;
        params.getLog().setCountErrors(0);
        params.getLog().printLogEnd();
        params.getLog().printTimeOnFile();
        if (tests == null) {
            setTests(listTestsFromResource());
        }
        for (String test : listTests()) {
            if (!test.isEmpty()) {
                try {
                    countTests++;
                    runTest(test);
                } catch (Exception e) {
                    params.getLog().printError("Erro na execução do teste: " + test);
                    e.printStackTrace();
                }
            }
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(YouCurteAPITester.class.getName()).log(Level.SEVERE, null, ex);
        }
        params.getLog().printOnFile("", false);
        System.out.println();
        params.getLog().printOnScreen("Número de testes aplicados: " + countTests);
        params.getLog().printOnScreen("Quantidade de erros: " + params.getLog().countErrors());
        params.getLog().printOnScreen("Número de testes que fracassaram: " + countFailure);
        for (TestLog log : logsOk) {
            if (log.countErrors() <= 0) {
                File file = log.getFile();
                log.finalize();
                try {
                    File to = new File(ok, file.getName());
                    Files.copy(file.toPath(), to.toPath(), REPLACE_EXISTING);
                    file.delete();
                } catch (IOException ex) {
                    Logger.getLogger(YouCurteAPITester.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void runTest(String testName) throws InstantiationException, IllegalAccessException {
        boolean xml = false, json = false;
        String testClass = PACKAGE_WITH_TESTS + "." + testName;
        params.getLog().printOnScreen("Executando o teste: " + testClass);
        ClassLoader loader = new TestClassLoader();
        Test test = null;
        TestData input;
        try {
            Class clazz = loader.loadClass(testClass);
            test = (Test) clazz.newInstance();
        } catch (ClassNotFoundException ex) {
            params.getLog().printError("Classe de teste '" + testName + "' não encontrada!");
            throw new RuntimeException(null, ex);
        }
        params.getLog().printOnScreen("Nome do teste: " + test.getTestName());
        test.getLog().printTimeOnFile();
        params.setTestType("json");
        params.getLog().printOnScreen("Testando com json...");
        try {
            params.getConnection().setUrlRoot(JSON_API_URL_ROOT);
            json = test.run(params);
        } catch (Exception e) {
            json = false;
            params.getLog().printError("Erro na execução do teste com json!");
            e.printStackTrace();
        }
        params.setTestType("xml");
        params.getLog().printOnScreen("Testando com xml...");
        try {
            params.getConnection().setUrlRoot(XML_API_URL_ROOT);
            xml = test.run(params);
        } catch (Exception e) {
            xml = false;
            params.getLog().printError("Erro na execução do teste com xml!");
            e.printStackTrace();
        }
        countFailure += !xml || !json ? 1 : 0;
        if (xml && json) {
            logsOk.add(test.getLog());
        }
        test.getLog().printLogEnd();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        YouCurteAPITester tester = new YouCurteAPITester();
        tester.run();
    }

}
