----- 0-01:38:44 28/06/2015 -----


----- 1-01:38:44 28/06/2015 -----
Conectando ao servidor...
URL:  http://youcurte.com.br/register/active/luizhenrique-rs@windowslive.com/luiz
Método: GET

Content-type: text/plain; charset=utf-8
Dados de entrada:

Dados de saída:
<!DOCTYPE html>
<html lang="pt">
    <head>
                <!-- head padrão -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- fim do head padrão -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/jquery-2.1.3.min.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/moments.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/bootstrap.min.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/bootstrap.min.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/youcurte.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/message.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/youcurte.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/login.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/bootstrapValidator.min.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/bootstrapValidator.min.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/language-validator/pt_BR.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/login.css">
<!-- fim do link -->
<!-- link -->
<link rel="shortcut icon" type="image/x-icon" href="http://youcurte.com.br/favicon.ico">
<!-- fim do link -->
<!-- título -->
<title>YouCurte</title>
<!-- fim do título -->
    </head>
    <body>
        <!-- navbar -->
        <div class="navbar navbar-default navbar-fixed-top"> <!-- div navbar -->
    <div class="container">
        <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-youcurte-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand"><img class="icon" src="/img/logo.png" width="128" height="64"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-youcurte-collapse">
            <!-- form de pesquisa -->
            <form class="navbar-form navbar-left" role="search" action="/search">
                <div class="form-group">
                    <input type="text" name="search" id="inputSearch" class="form-control" placeholder="Usuários, hashtags..." value="">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
            <!-- fim do form de pesquisa -->
            <ul class="nav navbar-nav  navbar-right" role="search btn-defa">
                <li><a href="/">Início</a></li>
                                    <li><a href="/register">Cadastrar</a></li>
                    <!-- login -->
                    <li class="dropdown">
                        <a href="#" id="login" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Entrar<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!-- form de login -->
                            <form class="navbar-form form-login" method="post" id="loginForm">
                                <input type="hidden" name="action" value="login">
                                <div class="form-group">
                                    <label for="loginInputEmail" class="col-lg-12">E-mail:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginEmail" type="email" class="form-control" id="loginInputEmail" placeholder="Seu e-mail" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="loginInputPassword" class="col-lg-12 margin-top-10">Senha:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginPassword" type="password" class="form-control" id="loginInputPassword" placeholder="Sua senha">
                                    </div>
                                </div>
                                <center>
                                    <div class="margin-top-10 margin-bot-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="auto" class="form-login-check">  Mantenha-me conectado
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <a onclick="forgotPassword()" class="link-blue-and-white click">Esqueceu a senha?</a>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default margin-top-10">Entrar</button>
                                                                    </center>
                            </form>
                            <!--fim do form de login-->
                        </ul>
                    </li><!-- fim do login -->
                            </ul>
        </div>
    </div>
</div> <!-- fim div navbar -->
        <!-- fim da navbar -->
        <!--começo de  centro da página-->
        <div class="content">

                        
                        
                <!--início do conteúdo child-->
                <center>
    <div class="alert alert-dismissible alert-info">
        <strong>Seu e-mail foi confirmado!</strong>  <a href="/" class="alert-link"><u>Ir para a página inicial.</u></a>
    </div>
</center>               
                <!--fim do conteúdo child-->
                  
        </div> 
        <!--end centro da página-->
        <!--começo de  messageDialog-->
    <center>
        <div class="modal fade" id="messageDialog" data-backdrop = "static" data-keyboard = "false"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" message-dialog-id="0">
            <div class="modal-dialog" id="messageDialogDiv">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close click" id="messageDialogClose"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="messageDialogLabel"></h4>
                    </div>
                    <div class="modal-body" id="messageDialogContent">
                    </div>
                    <div class="modal-footer" id="messageDialogFooter">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    <!--end messageDialog -->
    <!--uploadFile-->
    <form class="hide" id="formUploadFile">
        <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
        <input type="file" name="file" id="uploadFile">
    </form>
    <!--end uploadFile-->
    <!-- javascript com variaveis globais -->
    <script type="text/javascript">
        $(function () {
                YouCurte.LoggedIn = false;
                $(".if-logged-in").hide();
        });
    </script>
    <!-- fim do javascript -->
</body>
</html>









----- 2-01:38:44 28/06/2015 -----
Conectando ao servidor...
URL:  http://youcurte.com.br/register/active/fernandasilva@hotmail.com/luiz
Método: GET

Content-type: text/plain; charset=utf-8
Dados de entrada:

Dados de saída:
<!DOCTYPE html>
<html lang="pt">
    <head>
                <!-- head padrão -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- fim do head padrão -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/jquery-2.1.3.min.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/moments.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/bootstrap.min.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/bootstrap.min.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/youcurte.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/message.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/youcurte.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/login.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/bootstrapValidator.min.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/bootstrapValidator.min.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/language-validator/pt_BR.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/login.css">
<!-- fim do link -->
<!-- link -->
<link rel="shortcut icon" type="image/x-icon" href="http://youcurte.com.br/favicon.ico">
<!-- fim do link -->
<!-- título -->
<title>YouCurte</title>
<!-- fim do título -->
    </head>
    <body>
        <!-- navbar -->
        <div class="navbar navbar-default navbar-fixed-top"> <!-- div navbar -->
    <div class="container">
        <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-youcurte-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand"><img class="icon" src="/img/logo.png" width="128" height="64"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-youcurte-collapse">
            <!-- form de pesquisa -->
            <form class="navbar-form navbar-left" role="search" action="/search">
                <div class="form-group">
                    <input type="text" name="search" id="inputSearch" class="form-control" placeholder="Usuários, hashtags..." value="">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
            <!-- fim do form de pesquisa -->
            <ul class="nav navbar-nav  navbar-right" role="search btn-defa">
                <li><a href="/">Início</a></li>
                                    <li><a href="/register">Cadastrar</a></li>
                    <!-- login -->
                    <li class="dropdown">
                        <a href="#" id="login" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Entrar<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!-- form de login -->
                            <form class="navbar-form form-login" method="post" id="loginForm">
                                <input type="hidden" name="action" value="login">
                                <div class="form-group">
                                    <label for="loginInputEmail" class="col-lg-12">E-mail:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginEmail" type="email" class="form-control" id="loginInputEmail" placeholder="Seu e-mail" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="loginInputPassword" class="col-lg-12 margin-top-10">Senha:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginPassword" type="password" class="form-control" id="loginInputPassword" placeholder="Sua senha">
                                    </div>
                                </div>
                                <center>
                                    <div class="margin-top-10 margin-bot-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="auto" class="form-login-check">  Mantenha-me conectado
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <a onclick="forgotPassword()" class="link-blue-and-white click">Esqueceu a senha?</a>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default margin-top-10">Entrar</button>
                                                                    </center>
                            </form>
                            <!--fim do form de login-->
                        </ul>
                    </li><!-- fim do login -->
                            </ul>
        </div>
    </div>
</div> <!-- fim div navbar -->
        <!-- fim da navbar -->
        <!--começo de  centro da página-->
        <div class="content">

                        
                        
                <!--início do conteúdo child-->
                <center>
    <div class="alert alert-dismissible alert-info">
        <strong>Seu e-mail foi confirmado!</strong>  <a href="/" class="alert-link"><u>Ir para a página inicial.</u></a>
    </div>
</center>               
                <!--fim do conteúdo child-->
                  
        </div> 
        <!--end centro da página-->
        <!--começo de  messageDialog-->
    <center>
        <div class="modal fade" id="messageDialog" data-backdrop = "static" data-keyboard = "false"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" message-dialog-id="0">
            <div class="modal-dialog" id="messageDialogDiv">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close click" id="messageDialogClose"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="messageDialogLabel"></h4>
                    </div>
                    <div class="modal-body" id="messageDialogContent">
                    </div>
                    <div class="modal-footer" id="messageDialogFooter">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    <!--end messageDialog -->
    <!--uploadFile-->
    <form class="hide" id="formUploadFile">
        <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
        <input type="file" name="file" id="uploadFile">
    </form>
    <!--end uploadFile-->
    <!-- javascript com variaveis globais -->
    <script type="text/javascript">
        $(function () {
                YouCurte.LoggedIn = false;
                $(".if-logged-in").hide();
        });
    </script>
    <!-- fim do javascript -->
</body>
</html>









----- 3-01:38:45 28/06/2015 -----
Conectando ao servidor...
URL:  http://youcurte.com.br/register/active/lucasgabriel@gmail.com/luiz
Método: GET

Content-type: text/plain; charset=utf-8
Dados de entrada:

Dados de saída:
<!DOCTYPE html>
<html lang="pt">
    <head>
                <!-- head padrão -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- fim do head padrão -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/jquery-2.1.3.min.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/moments.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/bootstrap.min.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/bootstrap.min.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/youcurte.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/message.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/youcurte.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/login.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/bootstrapValidator.min.css">
<!-- fim do link -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/bootstrapValidator.min.js"></script>
<!-- fim do script -->
<!-- script -->
<script type="text/javascript" src="http://youcurte.com.br/js/language-validator/pt_BR.js"></script>
<!-- fim do script -->
<!-- link -->
<link rel="stylesheet" type="text/css" href="http://youcurte.com.br/css/login.css">
<!-- fim do link -->
<!-- link -->
<link rel="shortcut icon" type="image/x-icon" href="http://youcurte.com.br/favicon.ico">
<!-- fim do link -->
<!-- título -->
<title>YouCurte</title>
<!-- fim do título -->
    </head>
    <body>
        <!-- navbar -->
        <div class="navbar navbar-default navbar-fixed-top"> <!-- div navbar -->
    <div class="container">
        <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-youcurte-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand"><img class="icon" src="/img/logo.png" width="128" height="64"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-youcurte-collapse">
            <!-- form de pesquisa -->
            <form class="navbar-form navbar-left" role="search" action="/search">
                <div class="form-group">
                    <input type="text" name="search" id="inputSearch" class="form-control" placeholder="Usuários, hashtags..." value="">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
            <!-- fim do form de pesquisa -->
            <ul class="nav navbar-nav  navbar-right" role="search btn-defa">
                <li><a href="/">Início</a></li>
                                    <li><a href="/register">Cadastrar</a></li>
                    <!-- login -->
                    <li class="dropdown">
                        <a href="#" id="login" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Entrar<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!-- form de login -->
                            <form class="navbar-form form-login" method="post" id="loginForm">
                                <input type="hidden" name="action" value="login">
                                <div class="form-group">
                                    <label for="loginInputEmail" class="col-lg-12">E-mail:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginEmail" type="email" class="form-control" id="loginInputEmail" placeholder="Seu e-mail" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="loginInputPassword" class="col-lg-12 margin-top-10">Senha:</label>
                                    <div class="col-lg-12 align-center">
                                        <input name="loginPassword" type="password" class="form-control" id="loginInputPassword" placeholder="Sua senha">
                                    </div>
                                </div>
                                <center>
                                    <div class="margin-top-10 margin-bot-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="auto" class="form-login-check">  Mantenha-me conectado
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <a onclick="forgotPassword()" class="link-blue-and-white click">Esqueceu a senha?</a>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default margin-top-10">Entrar</button>
                                                                    </center>
                            </form>
                            <!--fim do form de login-->
                        </ul>
                    </li><!-- fim do login -->
                            </ul>
        </div>
    </div>
</div> <!-- fim div navbar -->
        <!-- fim da navbar -->
        <!--começo de  centro da página-->
        <div class="content">

                        
                        
                <!--início do conteúdo child-->
                <center>
    <div class="alert alert-dismissible alert-info">
        <strong>Seu e-mail foi confirmado!</strong>  <a href="/" class="alert-link"><u>Ir para a página inicial.</u></a>
    </div>
</center>               
                <!--fim do conteúdo child-->
                  
        </div> 
        <!--end centro da página-->
        <!--começo de  messageDialog-->
    <center>
        <div class="modal fade" id="messageDialog" data-backdrop = "static" data-keyboard = "false"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" message-dialog-id="0">
            <div class="modal-dialog" id="messageDialogDiv">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close click" id="messageDialogClose"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="messageDialogLabel"></h4>
                    </div>
                    <div class="modal-body" id="messageDialogContent">
                    </div>
                    <div class="modal-footer" id="messageDialogFooter">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </center>
    <!--end messageDialog -->
    <!--uploadFile-->
    <form class="hide" id="formUploadFile">
        <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
        <input type="file" name="file" id="uploadFile">
    </form>
    <!--end uploadFile-->
    <!-- javascript com variaveis globais -->
    <script type="text/javascript">
        $(function () {
                YouCurte.LoggedIn = false;
                $(".if-logged-in").hide();
        });
    </script>
    <!-- fim do javascript -->
</body>
</html>









