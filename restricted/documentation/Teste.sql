create table if not exists PostHistory(
        postId int,
        userId int,
        time datetime,
	primary key(userId,postId),
	foreign key(userId) references User(Id),
	foreign key(postId) references Post(Id)
)ENGINE=InnoDB;
