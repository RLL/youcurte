drop database YouCurte;
create database if not exists YouCurte;
use YouCurte;

/**
*    boolean active Conta do usuário ativa
*    boolean sendEmails Enviar e-mails com notificações (Seguindo, não seguindo)
*    text name O primeiro nome do usuário
*    text lastName O sobrenome do usuário
*    text description Descrição do usuário que faz de si mesmo
*    datetime confirmationTime O momento no qual a conta foi confirmada
*    datetime confirmationCode Código de confirmação da conta enviado para o e-mail do usuário
*/
create table if not exists User(
    id int primary key AUTO_INCREMENT,
    name text not null,
    lastName text not null,
    email text not null,
    password text not null,
    description text,
    telephone text,
    city text,
    state text,
    academicTraining text,
    interest text,
    image text,
    birthDate date not null,
    registryDate datetime,
	confirmationTime datetime,
	confirmationCode int,
	sex enum('male','female'),
	cep text,
	profession text,
  active boolean,
  toSendEmails boolean
)ENGINE=InnoDB;

/**
*    text image A capa do album
*    text name O nome do album
*    int userId A quem pertecem esse álbum
*/
create table if not exists Album(
    id int AUTO_INCREMENT,
    userId int not null,
    name text,
    image text,
    foreign key (userId) references User(id),
    primary key(id,userId)
)ENGINE=InnoDB;

/**
*    text url O link da imagem
*    datetime time O momento em que a imagem foi enviada para o servidor
*    text name Nome fictício/descrição para a imagem
*/
create table if not exists Image(
    id int primary key AUTO_INCREMENT,
    albumId int,
    userId int,
    url text,
    name text,
    time datetime,
    foreign key (userId) references User(id)
)ENGINE=InnoDB;

/**
*    varchar(..) auth Chave para identificar usuário no site ou API
*    boolean native Se o chave vai ser usado pelo youcurte mesmo (true) ou terceiro (false)
*    text userAgent Nome,modelo do navegador ou APP
*    datetime Momento em que a chave foi criada
*    datetime Momento que a chave vai expirar
*/
create table if not exists Login(
    auth varchar(60) primary key,
    userId int not null,
    creationTime datetime,
    expirationTime datetime,
    native boolean,
    userAgent text,
    foreign key (userId) references User(id)
)ENGINE=InnoDB;

/**
*    text hashtags Campo onde mantem apenas as hashtags do texto para facilitar busca
*    boolean hasImage Para indificar se o post tem imagem
*    datetime time O momento em que post foi feito
*    text local O endereço do local onde foi feito o post
*/
create table if not exists Post(
    id int primary key AUTO_INCREMENT,
    text text,
    hashtags text,
    imageId int null,
    hasImage boolean,
    time datetime,
    local text,
    latitude float,
    longitude float,
    userId int not null,
    foreign key (userId) references User(id) ON DELETE CASCADE,
    foreign key (imageId) references Image(id) ON DELETE SET NULL
)  ENGINE=InnoDB;

/**
* @field boolean tri para indicar que like no post
* @field boolean bah para indificar deslike no post
*/
create table if not exists Opinion(
    userId int,
    postId int,
    tri boolean,
    bah boolean,
    primary key(userId,postId),
    foreign key(userId) references User(id),
    foreign key(postId) references Post(id)
)ENGINE=InnoDB;

/**
* @field int userId a quem pertecem/recebe a notificação 
* @field int userIdFrom Quem realizou a ação que criou a notificação
* @field boolean alreadyRead se já foi lida
* @field datetime time O momento em que ela foi criada
* @field text link Um link para qual a notificação deva levar se clicada
*/
create table if not exists Notification(
    id int AUTO_INCREMENT,
    userId int not null,
    userIdFrom int not null,
    text text,
    alreadyRead boolean,
    link text,
    time datetime,
    primary key (id,userId),
    foreign key (userId) references User(id)
)ENGINE=InnoDB;

/**
* @field int userIdTo Quem vai receber o depoimento
* @field int userIdFrom Quem fez o depoimento
* @field datetime time O momento em que o post foi criado
*/
create table if not exists Deposition(
    id int AUTO_INCREMENT,
    userIdTo int,
    userIdFrom int,
    text text,
    time datetime,
    primary key(id),
    foreign key (userIdTo) references User(id),
    foreign key (userIdFrom) references User(id)
)ENGINE=InnoDB;

/**
* @field int userId  O usuário
* @field int userIdFollow Quem o usuário vai seguir
*/
create table if not exists Follow(
	userId int,
	userIdFollow int,
	primary key(userId,userIdFollow),
	foreign key(userId) references User(Id),
	foreign key(userIdFollow) references User(Id)
)ENGINE=InnoDB;

/**
* @field int userIdFrom Quem deu as notas de atributos
* @field int userIdTo Quem recebeu essas notas
* @field int cool nota de legal
* @field int reliable nota de confiável
* @field int sexy nota de sexy
*/
create table if not exists Attributes(
        cool int,
        reliable int,
        sexy int,
        userIdFrom int,
	userIdTo int,
	primary key(userIdTo,userIdFrom),
	foreign key(userIdFrom) references User(Id),
	foreign key(userIdTo) references User(Id)
)ENGINE=InnoDB;

/**
* Histórico de visitas em post
* @field datetime time O momento em que foi visto o post
* @field int userId Quem viu o post
*/
create table if not exists PostHistory(
        postId int,
        userId int,
        time datetime,
	primary key(userId,postId),
	foreign key(userId) references User(Id),
	foreign key(postId) references Post(Id)
)ENGINE=InnoDB;

-- DROP VIEW UserView;
-- DROP VIEW OpinionView;
-- DROP VIEW PostView;
-- DROP VIEW NotificationView;
-- DROP VIEW SearchView;
/**
* View que junta os dados do usuário com os atributos que recebeu de outros usuários
*/
CREATE VIEW UserView as select user.*,avg(att.cool) as 'cool',avg(att.reliable) as 'reliable',avg(att.sexy) as 'sexy' from User as user left join Attributes as att on user.id=att.userIdTo group by user.id;

/**
* View que junta todas as opinions por post e calcula suas quantidades de like e dislike
*/
CREATE VIEW OpinionView AS select postId,sum(tri) as 'tri',sum(bah) as 'bah' from Opinion group by postId;
/**
*View que une os posts com suas opniões
*/
CREATE VIEW PostView as select po.*,IFNULL(op.tri,0) as 'tri',IFNULL(op.bah,0) as 'bah' from Post po left join OpinionView op on po.id=op.postid;
/**
*View que lida com a regra de negócio de exibir que notificações
*/
CREATE VIEW NotificationView as select * from Notification where (TIMESTAMPDIFF(HOUR,time,now()) <=1 and alreadyRead) or (alreadyRead and TIMESTAMPDIFF(day,time,now())<=7);
/**
* View usara na pesquisa de posts (junta as informações de um post com a de seu usuário(quem criou o post))
*/
CREATE VIEW SearchView as select post.*,concat(name,' ',lastname) as 'fullname' from  PostView as post left join UserView as user on post.userid=user.id order by time desc;
