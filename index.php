<?php
//Inicia a sessão
session_start();
//Ajusta os parametros recebidos com caracteres especiais
get_magic_quotes_gpc();
//Realiza o loader das classes principais
require 'restricted' . DIRECTORY_SEPARATOR . 'autoloader.php';
//Obtém o path do link atual
$path = Path::getFromUrl();
//Verifica se está acessando a API ou Site
if ($path->getFirstPart() === 'api') {
    //Verifica se está chamando a API por XML ou JSON
    if ($path->hasPartIndex(1) && strtolower($path->getPart(1)) == "xml") {
        //Inicia a classe responsável pela API em XML
        require CLASSES . 'APIXMLYouCurte.php';
        $api = new APIXMLYouCurte();
        $api->run($path->copy(2));
    } else {
        //Inicia a classe responsável pela API em JSON
        require CLASSES . 'APIJsonYouCurte.php';
        $api = new APIJsonYouCurte();
        $api->run($path->copy(1));
    }
} else {
    //Inicia a classe responsável pelo site
    require CLASSES . 'WebYouCurte.php';
    $web = new WebYouCurte();
    $web->run($path);
}