/**
 * Inicia variáveis
 */
var oldHashtags;

$(function () {
    window.addEventListener("resize", YouCurte.resize, false);
    window.addEventListener("orientationchange", YouCurte.resize, false);
    $('[data-toggle="tooltip"]').tooltip();
    setInterval(function () {
        if (YouCurte.hideModal) {
            $(".modal-backdrop").remove();
        }
        $(".content").css('margin-top',($(".navbar-fixed-top").height()+15)+"px");
    }, 100);
    updateHashtags();
});

/**
 * Exibe uma mensagem de espera.
 * @param {string} message A mensagem
 * @param {string} title [opcional] Título da mensagem
 * @returns {MessageDialog} Retorna o objeto de MessageDialog criado para 
 *  exibir a mensagem
 */
function messageWait(message, title) {
    if (title === undefined) {
        title = "Aguarde...";
    }
    message = new MessageDialog();
    message.setTitle(title);
    message.setCloseVisible(false);
    message.setContent("<img src='/img/load.gif' alt='+message+'>");
    message.show();
    return message;
}

/**
 * Verifica se a imagem informada tem a extensão válida.
 *  
 * @param {string} file O arquivo da imagem
 * @param {string} clearField
 * @returns {Boolean}
 */
function validateExtensionImage(file, clearField) {
    valids = [".jpg", ".png", ".bmp"];
    var change = false;
    valids.forEach(function (valid) {
        if (file.toLowerCase().endsWith(valid)) {
            change = true;
        }
    });
    if (!change) {
        message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
        message.setContent("<b>Aceita apenas imagens '.jpg', '.png' e '.bmp'.</b>");
        message.setTitle("Imagem inválida");
        message.show();
        if (clearField !== undefined&&clearField!==null) {
            $("#uploadFile").val(null);
        }
    }
    return change;
}

/**
 * Exibe a imagem em um dialog
 * @param {string} image O link da imagem
 * @returns {MessageDialog} O objeto dialog
 */
function showImage(image) {
    message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
    message.setContent("<div class='load-img'><img id='msgImage' src='" + image + "'></div>");
    message.setTitle("Imagem");
    message.setClass("modal-lg");
    message.show();
    return message;
}

/**
 * Abre uma nova aba com o url da hashtag
 * @param {string} obj O html da hashtag
 * @returns {undefined}
 */
function searchTag(obj) {
    tag = $($(obj).html()).html().replace("#", "%23");
    openLink("/search?search=" + tag,true);
}


function isResponseValid(data, result) {
    if (result === undefined) {
        result = "ok";
    }
    if (!data.toString().startsWith("{")) {
        return false;
    }
    json = $.parseJSON(data);
    return json !== null && json.result === result;
}


function copyToClipBoard(data)
{
    if (window.clipboardData) {
        window.clipboardData.setData("Text", data);
    } else {
        message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
        message.setTitle("Copie o seguinte:");
        message.setContent("<input class='col-lg-12' id='copyText' type='text' value='" + data + "' readonly><br>");
        message.show();
        setTimeout(function () {
            $("#copyText").focus();
            $("#copyText").select();
        }, 1000);
    }
}

function YouCurte() {
    

}

YouCurte.resize = function () {
    YouCurte.resizeEvents.forEach(function (item) {
        item();
    });
};

YouCurte.update = function(){
    if (YouCurte.LoggedIn){
        $(".if-logged-in").show();
    } else {
        $(".if-logged-in").hide();
    }
};

YouCurte.resizeEvents = new Array();

YouCurte.hideModal = false;

YouCurte.addResizeEvent = function (func) {
    YouCurte.resizeEvents.push(func);
};

function openLink(link,newTab){
    window.open(link, newTab?'_blank':'');
}

function updateHashtags() {
    var search = $("#inputSearch").val();
    var array = search.split(" ");
    var hashtags = [];
    array.forEach(function (item) {
        if (item.indexOf("#") >= 0) {
            subarray = item.split("#");
            subarray.forEach(function (item, key) {
                if (key > 0 || item.indexOf("#") === 0) {
                    hashtags.push(item);
                }
            });
        }
    });
    hashtags.forEach(function (item) {
        $(".hash" + item).addClass("label-warning");
    });
    if (oldHashtags !== undefined) {
        oldHashtags.forEach(function (a) {
            if (hashtags.indexOf(a) < 0) {
                $("#hash" + a).removeClass("label-warning");
            }
        });
    }
    oldHashtags = hashtags;
}