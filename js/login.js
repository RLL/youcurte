$(function() {
    /**
     * Faz a validação dos campos de login
     */
    $('#loginForm').bootstrapValidator({
        message: 'Esse valor não é válido',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            loginEmail: {
                group: '.col-lg-12',
                validators: {
                    notEmpty: {
                        message: 'O e-mail não pode ficar em branco'
                    }
                }
            },
            loginPassword: {
                group: '.col-lg-12',
                validators: {
                    notEmpty: {
                        message: 'A senha não pode ficar em branco'
                    }
                }
            }
        }
    });
});


/**
 * Realiza os procedimentos para recuperar a senha de quem esqueceu
 * @param {string) from [opcional] E-mail de quem esqueceu a senha
 * @returns {undefined}
 */
function forgotPassword(from){
    from = from===undefined?'':from;
    message = new MessageDialog();
    message.addButton(new Button('Enviar','btn-primary',function(msg){
        email = $("#forgotEmail").val();
        $.post("/forgotPassword",{email:email}).always(function(data){
            if (isResponseValid(data)){
                msgSucess = new MessageDialog(MessageDialog.MESSAGE_SUCESS);
                msgSucess.setTitle("Esqueceu a senha? Okay!");
                msgSucess.setContent("Foi enviado um link para seu e-mail que o \n\
                    permitirá modificar a senha da sua conta!");
                msgSucess.show();
            } else if (isResponseValid(data,"error")){
                json = $.parseJSON(data);
                msgError = new MessageDialog(MessageDialog.MESSAGE_ERROR);
                msgError.setContent(json.error);
                msgError.getButton(0).setCallable(function(){
                   forgotPassword(email);
                });
                msgError.show();
            } else { 
                console.error("Não foi retornado uma resposta válida:",data);
                msgError = new MessageDialog(MessageDialog.MESSAGE_ERROR);
                button = msgError.getButton(0);
                button.setLabel("OK");
                button.setCallable(function(){
                   forgotPassword(email) ;
                });
                msgError.setContent("Houve algum problema com a solicitação! Tente novamente mais tarde.");
                msgError.show();
            }
        });
        wait = new MessageDialog();
        wait.setTitle("Aguarde...");
        wait.setCloseVisible(false);
        wait.setContent("<img src='/img/load.gif' alt='Carregando...'>");
        wait.show();
    }));
    message.setTitle("Esqueceu a senha?");
    message.setContent("<div class='align-left margin-bot-10'>Informe o e-mail da sua conta:</div>\n\
        <input id='forgotEmail' class='form-control' type='email' value='"+from+"'><br>");
    message.addButton(new Button('Fechar','',MessageDialog.FUNCTION_CLOSE));
    message.show();
    setTimeout(function() {
            $("#forgotEmail").focus();
            $("#forgotEmail").select();
        }, 800);
}
