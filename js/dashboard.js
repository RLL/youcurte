$(function() {
    //Faz os preparativos para iniciar o dashboard
    var dashboard = new Dashboard("#dashboard", "#section", "#dashboardToggle");
    dashboard.prepare();
    YouCurte.addResizeEvent(function(){
        dashboard.update();
    });
    console.info("Dashboard pronta");
    $("#dashboard").bind('style', function() {
        console.info("resize:");
    });
});

/**
 * Classe do dashboard
 * @param {String} name O id da tag do dashboard
 * @param {String} posts O id da tag com os posts
 * @param {String} button O id da tag para exibir/ocultar dashboard
 * @returns {Dashboard}
 */
function Dashboard(name, posts, button) {
    var buttonDisplay;
    this.button = button;
    this.posts = posts;
    this.name = name;
    this.visible = $(name).css('display') !== 'none';
    
    /**
     * Retorna o nome(id da tag) do dashboard
     * @returns {String}
     */
    this.getName = function() {
        return this.name;
    };

    /**
     * Retorna o id da tag do dashboard
     * @returns {String}
     */
    this.getPosts = function() {
        return this.posts;
    };

    /**
     * Se o dashboard fica visivel ou não
     * @param {boolean} visible
     * @returns {undefined}
     */
    this.setVisible = function(visible) {
        if (visible === true) {
            //w = $(this.getName()).width()+40;
            //$(this.getPosts()).css({'margin-left': w+'px'});
            $(this.getName()).css('display','block');
            $(this.getPosts()).css('display','none');
        } else {
            $(this.getName()).css('display','none');
            $(this.getPosts()).css('display','block');
            //$(obj.getPosts()).css({'margin-left': '0px'});
        }
        $(this.getName()).css('width','100%');
        this.visible = visible;
        this.update();
    };

    /**
     * Se está visivel o dashboard
     * @returns {Boolean}
     */
    this.isVisible = function() {
        return this.visible;
    };

    /**
     * Função para alternar a visibilidade do dashboard
     * @param {$} obj O objeto que terá a visibilidade inversa do dashboard
     */
    this.toggleVisible = function(obj) {
        obj.setVisible(!obj.isVisible());
    };

    /**
     * Define o botão que alternará a visibilidade do dashboard
     * @param {String} button Id do butão
     * @returns {undefined}
     */
    this.setButton = function(button) {
        if (this.button !== null) {
            $(this.button).unbind('click');
        }
        this.button = button;
    };
    /**
     * Retorna o id da tag do botão que alterna a visibilidade do dashboard
     * @returns {String} Id do botão
     */
    this.getButton = function() {
        return this.button;
    };

    /**
     * Prepara o dashboard
     * @returns {undefined}
     */
    this.prepare = function() {
        this.buttonDisplay = $(this.getButton()).css('display');
        func = this.toggleVisible;
        obj = this;
        $(this.getButton()).click(function() {
            func(obj);
        });
        this.update();
    };

    /**
     * Atualiza o dashboard no navegador (Tela)
     */
    this.update = function() {
        var h = $(window).height();
        t = $('.content').css('margin-top').replace("px","");
        
        $(this.getName()).css('top',t-18);
        $(this.getName()).height(h-t-25);
        console.log(t);
    };

    /**
     * Timer para verificar alterações do ambiente e executar uma ação sobre o dashboard
     * @param {Dashboard} obj
     * @returns {undefined}
     */
    this.timer = function(obj) {
        display = $(obj.getButton()).css('display');
        if (display !== obj.buttonDisplay) {
            if (display === 'none') {
                obj.setVisible(true);
            } else {
                obj.setVisible(false);
            }
            obj.buttonDisplay = display;
        }
    };
    setInterval(this.timer, 100, this);
}

