/**
 * 
 * Inicia as variaveis de validação
 */
var valid = false;
var age_invalid = false;

$(function () {
    /**
     * Inicia a validação da data
     */
    if ($().datetimepicker !== undefined) {
        $('#inputBirthDiv').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    }
});

$(function () {
    /**
     * Faz verificação por time na data de nascimento
     */
    $('#inputBirth').on("dp.change", valide);
    setInterval(function () {
        valide();
    }, 500);
    /**
     * Define as mensagens de erros
     */
    field_empty = 'Este campo não pode ficar em branco.';
    age_less = 'Voc&ecirc; tem menos de 16 anos, não pode se cadastrar no site.';
    /**
     * Faz a validação do bootstrap
     */
    $('#registerForm').bootstrapValidator({
        message: 'Esse valor não é válido',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                group: '.form-group',
                validators: {
                    stringLength: {
                        min: 3,
                        message: 'O nome deve ter ao menos 3 letras.'
                    },
                    notEmpty: {
                        message: field_empty
                    }
                }
            },
            lastName: {
                group: '.form-group',
                stringLength: {
                    min: 3,
                    message: 'O sobrenome deve ter ao menos 3 letras.'
                },
                validators: {
                    notEmpty: {
                        message: field_empty
                    }
                }
            },
            birthDate: {
                group: '.input-birth-email',
                validators: {
                    notEmpty: {
                        message: field_empty
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'A data informada é inválida.'
                    },
                    callback: {
                        message: age_less,
                        callback: validBirth
                    }
                }
            },
            password: {
                group: '.form-group',
                validators: {
                    notEmpty: {
                        message: field_empty
                    },
                    stringLength: {
                        min: 6,
                        max: 12,
                        message: 'A senha deve ter pelo menos 6 à 12 caracteres.'
                    },
                    identical: {
                        field: 'confirmPass',
                        message: 'A senha não combina com o campo de confirmação.'
                    }
                }
            },
            confirmPass: {
                group: '.form-group',
                validators: {
                    notEmpty: {
                        message: field_empty
                    },
                    identical: {
                        field: 'password',
                        message: 'As senhas não combinam!'
                    }
                }
            },
            email: {
                group: '.input-birth-email',
                validators: {
                    notEmpty: {
                        message: field_empty
                    }
                }
            }
        }
    }).on('error.form.bv', function (e, data) {
        console.log("validado");
        valid = true;
    });
});


/**
 * Faz a validação manualmente
 * @returns {undefined}
 */
function valide() {
    if (valid) {
        $('#registerForm').data('bootstrapValidator').resetForm(false);
        $('#registerForm').bootstrapValidator('validate');
    }
}

/**
 * Valida a data
 * @returns {Boolean}
 */
function validBirth() {
    var yearAge = 0;
    //Cortando a string birthDate para calcular e testar as informações
    birthDate = $("#inputBirth").val();
    var day = birthDate.substring(0, 2);
    var month = birthDate.substring(3, 5);
    var year = birthDate.substring(6, 10);

    //Pegando a data atual para ajudar no calculo
    dateNow = new Date();
    dayNow = dateNow.getDay();
    monthNow = dateNow.getMonth();
    yearNow = dateNow.getFullYear();

    //Convertendo os dados para inteiro
    day = parseInt(day);
    month = parseInt(month);
    year = parseInt(year);
    dayNow = parseInt(dayNow);
    monthNow = parseInt(monthNow);
    yearNow = parseInt(yearNow);

    //Agora são os testes para calcular a idade
    if (day >= dayNow && month >= monthNow) {
        yearAge = (yearNow - year);
    } else {
        yearAge = ((yearNow - year) - 1);
    }

    age_invalid = yearAge < 16;
    return !age_invalid;
}
