/**
 * Classe para botões
 * @param {string} label O texto do botão
 * @param {string} clazz As classes de estilo para o botão (css)
 * @param {function} callable A função que o botão irá realizar
 * @param {int} id
 * @returns {Button}
 */
function Button(label, clazz, callable, id) {
    var id, label, clazz, callable;

    this.label = label;
    this.clazz = clazz;
    this.callable = callable;
    this.id = id;

    /**
     * Define as classes de estilo que o botão terá
     * @param {String} clazz
     * @returns {Button}
     */
    this.setClass = function (clazz) {
        this.clazz = clazz;
        return this;
    };

    /**
     * Define a função que o botão irá executar
     * @param {function} callable
     * @returns {Button}
     */
    this.setCallable = function (callable) {
        this.callable = callable;
        return this;
    };

    /**
     * Define o texto do botão
     * @param {string} label
     * @returns {Button}
     */
    this.setLabel = function (label) {
        this.label = label;
        return this;
    };

}

/**
 * Mensagem de dialog
 * @param {int} messageType O tipo de mensagem. Ex:
 *      MessageDialog.MESSAGE_ERROR = -1;
 *      MessageDialog.MESSAGE_SUCESS = 1;
 *      MessageDialog.MESSAGE_CONFIRM = 0;
 *      MessageDialog.MESSAGE_CUSTOM = 2;
 *      MessageDialog.MESSAGE_ALERT = 3;
 * @returns {MessageDialog}
 */
function MessageDialog(messageType) {
    var type, id, content, buttons, visible, title, closeVisible, clazz,
            autoClose, specialClose, closedEvent, closingEvent;

    this.id = Math.random();
    this.buttons = [];
    this.closeVisible = true;
    this.autoClose = false;
    this.specialClose = true;
    if (MessageDialog.registered === undefined) {
        MessageDialog.registered = [];
    }
    MessageDialog.registered[this.id] = this;

    /**
     * 
     * @returns {Number} Retorna o id da message
     */
    this.getId = function () {
        return this.id;
    };


    /**
     * Abre o dialog da mensagem
     * @returns MessageDialog
     */
    this.show = function () {
        YouCurte.hideModal = false;
        this.updateGUI();
        this.visible = true;
        $("#messageDialog").modal("show");
        return this;
    };

    /**
     * Oculta a mensagem
     * @returns {MessageDialog|boolean} Retorna false se não foi possivel fechar
     */
    this.hide = function () {
        if (this.getClosingEvent()!==undefined){
            if (!this.getClosingEvent()()){
                return false;
            }
        }
        dialog = $("#messageDialog");
        if (dialog.attr("message-dialog-id") == this.getId()) {
            dialog.modal("hide");
            this.visible = false;
        } else {
            console.error("ids das mensagens diferentes",
                    dialog.attr("message-dialog-id"), this.getId());
        }
        $(".modal-backdrop").remove();
        YouCurte.hideModal = true;
        if (this.getClosedEvent() !== undefined) {
            this.getClosedEvent()(this);
        }
        return this;
    };

    /**
     * Prepara e atualiza todos os códigos
     * @returns {MessageDialog}
     */
    this.update = function () {
        this.buttons.forEach(function (button, key) {
            button.id = key;
        });
        return this;
    };

    /**
     * Prepara a parte do dialog no html
     * @returns {MessageDialog}
     */
    this.updateGUI = function () {
        this.update();
        footer = "";
        this.buttons.forEach(function (button, id) {
            footer += '<button type="button" class="btn btn-default ' + button.clazz + '" onclick="MessageDialog.getMessageDialog(' + this.id + ').clickButton(' + id + ')">' + button.label + '</button>';
        }.bind(this));
        $("#messageDialog").attr("message-dialog-id", this.getId());
        $("#messageDialogLabel").html(this.title);
        $("#messageDialogClose").css('display', this.isCloseVisible() ? '' : 'none');
        $("#messageDialogClose").attr('onclick', 'MessageDialog.getMessageDialog(' + this.id + ').hide()');
        if (!this.getSpecialClose()) {
            $("#messageDialog").attr({'data-backdrop': "static", 'data-keyboard': "false"});
        } else {
            $("#messageDialog").removeAttr("data-backdrop");
            $("#messageDialog").removeAttr("data-keyboard");
        }

        $("#messageDialogContent").html(this.content);
        $("#messageDialogDiv").attr("class", "modal-dialog " + this.clazz);
        $("#messageDialogFooter").html(footer);
        return this;
    };

    /**
     * Define um título
     * @param {String} title O título do dialog
     * @returns {MessageDialog}
     */
    this.setTitle = function (title) {
        this.title = title;
        return this;
    };

    /**
     * O título do dialog
     * @returns {String}
     */
    this.getTitle = function () {
        return this.title;
    };


    /**
     * Adiciona um novo botão
     * @param {Button} button 
     * @returns {Number} O id do botão
     */
    this.addButton = function (button) {
        return this.buttons.push(button) - 1;
    };


    /**
     * Remove um botão
     * @param {Button|Integer) arg O id do botão ou o botão em si
     * @returns {Boolean} true = Deletado
     *      false = Não deletado
     */
    this.removeButton = function (arg) {
        if (arg instanceof Button) {
            arg = buttons.indexOf(arg);
        }
        count = buttons.length;
        this.buttons.splice(arg, 1);
        return count !== buttons.length;
    };

    /**
     * Redefine um botão
     * @param {Intenger} id O id do botão que deseja substituir
     * @param {Button} button O novo botão
     * @returns {MessageDialog} Retorna a si mesmo
     */
    this.setButton = function (id, button) {
        this.buttons[id] = button;
        return this;
    };

    /**
     * Retorna um botão específico
     * @param {String|Integer} arg O id ou label do button
     * @returns {Button}
     */
    this.getButton = function (arg) {
        if (isFinite(arg)) {
            return this.buttons[arg];
        } else {
            var result = null;
            this.buttons.forEach(function (item) {
                if (item.label === arg) {
                    result = item;
                    return;
                }
            });
            return result;
        }
    };

    /**
     * Defineo conteudo do dialog
     * @param {String} content O Conteúdo do dialog
     * @returns {MessageDialog} Retorna a si mesmo
     */
    this.setContent = function (content) {
        this.content = content;
        return this;
    };

    /**
     * Retorna todo o conteúdo do dialog
     * @returns {String}
     */
    this.getContent = function () {
        return this.content;
    };

    /**
     * Realiza a mesma função que show e hide
     * @param {Boolean} visible
     * @returns {MessageDialog}
     */
    this.setVisible = function (visible) {
        if (visible) {
            return this.show();
        } else {
            return this.hide();
        }
    };


    /**
     * 
     * @returns {Boolean} Retorna se a mensagem está visivel
     */
    this.isVisible = function () {
        return this.visible;
    };

    /**
     * Define o tipo de mensagem:
     *      MessageDialog.MESSAGE_ERROR,
     *      MessageDialog.SUCESS,
     *      MessageDialog.CONFIRM,
     *      MessageDialog.CUSTOM
     * @param {Number} type
     * @returns {MessageDialog} Retorna a si mesmo
     
     this.setMessageType = function(type) {
     this.type = type;
     return this;
     };
     */

    /**
     * Retorna o tipo de mensagem 
     *      MessageDialog.MESSAGE_ALERT e etc;
     * @returns {Number}
     */
    this.getMessageType = function () {
        return this.type;
    };

    /**
     * @param {Boolean} value Se exibir o botão de fechar no título
     *      true = exibe
     *      false = falso
     * @returns {MessageDialog} Retorna a sí mesmo
     */
    this.setCloseVisible = function (value) {
        this.closeVisible = value;
        this.setSpecialClose(value);
        return this;
    };

    /**
     * Define se deve fechar ao pressionar 'ESC' ou clicar no fundo
     * @param {type} value
     * @returns {MessageDialog}
     */
    this.setSpecialClose = function (value) {
        this.specialClose = value;
        return this;
    };

    /**
     * Se deve fechar ao pressionar 'ESC' ou clicar no fundo
     * @returns {boolean}
     */
    this.getSpecialClose = function () {
        return this.specialClose;
    };

    /**
     * @returns {Boolean} Se o botão fechar no título vai aparecer
     *      true = aparece
     *      false = não
     */
    this.isCloseVisible = function () {
        return this.closeVisible;
    };

    /**
     * Executa a ação do click de um botão
     * @param {Number} id O id do botão
     * @returns {MessageDialog} Retorna a sí a mesmo
     */
    this.clickButton = function (id) {
        cal = this.buttons[id].callable;
        if (cal === MessageDialog.FUNCTION_CLOSE) {
            this.hide();
        } else if (cal !== undefined) {
            cal(this, this.buttons[id]);
        } else {
            if (!this.getAutoClose()) {
                console.warn("Nenhuma ação foi definida");
            }
        }
        if (this.getAutoClose()) {
            this.hide();
        }
        return this;
    };

    /**
     * Define se deve fechar o dialog ao pressionar um botão
     * @param {boolean} value
     * @returns {MessageDialog}
     */
    this.setAutoClose = function (value) {
        this.autoClose = value;
        return this;
    };

    /**
     * 
     * @returns {Boolean} Se deve fechar o dialog ao pressionar um botão qualquer
     */
    this.getAutoClose = function () {
        return this.autoClose;
    };


    /**
     * 
     * @param {String} clazz Classes de estilo para o dialog
     * @returns {MessageDialog} Retorna sí meso
     */
    this.setClass = function (clazz) {
        this.clazz = clazz;
        return this;
    };


    /**
     * 
     * @returns {String} Retorna as classes de estilo do dialog
     */
    this.getClass = function () {
        return this.clazz;
    };

    /**
     * Define um evento para quando o MessageDialog for oculto
     * @param {function} event A função do evento
     * @returns {undefined}
     */
    this.setClosedEvent = function (event) {
        this.closedEvent = event;
        return this;
    };

    /**
     * Retorna o evento do messageDialog de quando fechar
     * @returns {function}
     */
    this.getClosedEvent = function () {
        return this.closedEvent;
    };

    /**
     * Define um evento para quando o MessageDialog for fechar
     * @param {function} event A função do evento.
     *  A função do evento deve retornar true ou false;
     *         true = fecha o messagedialog
     *         false = não fecha
     * @returns {undefined}
     */
    this.setClosingEvent = function (event) {
        this.closingEvent = event;
        return this;
    };

    /**
     * Retorna o evento de quando messageDialog for fechar
     * 
     * @returns {function}
     */
    this.getClosingEvent = function () {
        return this.closingEvent;
    };

    if (messageType !== undefined) {
        this.type = messageType;
        if (this.type === MessageDialog.MESSAGE_ALERT) {
            this.addButton(new Button("Fechar", "", MessageDialog.FUNCTION_CLOSE));
        } else if (this.type === MessageDialog.MESSAGE_SUCESS) {
            this.addButton(new Button("OK", "", MessageDialog.FUNCTION_CLOSE));
        } else if (this.type === MessageDialog.MESSAGE_ERROR) {
            this.setTitle("Erro");
            this.addButton(new Button("Fechar", "", MessageDialog.FUNCTION_CLOSE));
        } else if (this.type === MessageDialog.MESSAGE_CONFIRM) {
            this.addButton(new Button("Sim", "btn-warning", MessageDialog.FUNCTION_CLOSE));
            this.addButton(new Button("Não", "btn-primary", MessageDialog.FUNCTION_CLOSE));
        }
    }

}

MessageDialog.MESSAGE_ERROR = -1;
MessageDialog.MESSAGE_SUCESS = 1;
MessageDialog.MESSAGE_CONFIRM = 0;
MessageDialog.MESSAGE_CUSTOM = 2;
MessageDialog.MESSAGE_ALERT = 3;

MessageDialog.FUNCTION_CLOSE = 0;

/**
 * 
 * @param {Number} id O id do message dialog
 * @returns {MessageDialog} O message dialog
 */
MessageDialog.getMessageDialog = function (id) {
    return MessageDialog.registered[id];
};
