var locationHTML, profileImageDefault, profileImageBefore;

$(function () {
    /**
     * Inicia variáveis ne cessárias para alterar o perfil e fazer postagens
     */
    locationHTML = $("#location").html();
    profileImageBefore = $("#userPhoto").attr("src");
    profileImageDefault = "/img/user/default.png";
    caption = $("#selectImage").html();
    /**
     * Verifica quando é selecionada uma imagem para o post
     */
    $('#postImage').on("change", function (e) {
        file = $("#postImage").val();
        name = "..." + file.substring(file.length - 15);
        namel = name.toLowerCase();
        if (validateExtensionImage(file, "#postImage")) {
            $("#selectImage").html(name);
            $("#selectImage").attr("title", file);
            $("#clearImage").css("display", "inline-block");
        }
    });
    /*
     * Verifica quando é selecionado uma imagem para o perfil
     */
    $('#uploadFile').on("change", function (e) {
        file = $("#uploadFile").val();
        if (validateExtensionImage(file, "#uploadFile")) {
            fileUpload("/image/tmp", function (data) {
                updateProfileImage(data);
            });
        }
    });
});

/**
 * Atualiza a imagem do perfil que é exibida
 * @param {string} data O json recebido
 */
function updateProfileImage(data) {
    console.info("upload-image", data);
    if (!isResponseValid(data)) {
        message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
        message.setContent("Não foi possível carregar a imagem!");
        message.show();
        img = profileImageBefore;
    } else {
        img = $.parseJSON(data).img;
    }
    $("#userPhoto").attr("src", img);
    $("#profileImageName").val(img);
}

/**
 * Remove a imagem do perfil e coloca a default
 * @returns {undefined}
 */
function clearProfileImage() {
    $("#userPhoto").attr("src", profileImageDefault);
    $("#profileImageName").val(profileImageDefault);
}

/**
 * Abre a janela de selação de imagens para perfil
 * @returns {undefined}
 */
function selectProfileImage() {
    $('#uploadFile').click();
}

/**
 * Verifica os campos para realizar o post
 */
function post() {
    if ($("#postImage").val() !== "" || $("#postText").val() !== "") {
        $("#postForm").submit();
        $("#postButton").addClass("disabled");
        $("#postButton").val("Postando...");
    } else {
        message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
        message.setContent("<b>Entre com um texto ou imagem para postar.</b>");
        message.setTitle("Post inválida");
        message.show();
    }
}

/**
 * Verifica os campos para fazer um depoimento
 * @returns {undefined}
 */
function doDeposition() {
    if ($("#depositionText").val() !== "") {
        $("#depositionForm").submit();
        $("#postButton").addClass("disabled");
        $("#postButton").val("Enviado...");
    } else {
        message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
        message.setContent("<b>O depoimento precisa de um texto.</b>");
        message.setTitle("Depoimento inválida");
        message.show();
    }
}

/**
 * Converte a primeira letra da string passada para minuscula e retorna a nova string
 *  Ex:
 *      lowerFirstLetter("LUIZ"); //ex: lUIZ
 * @param {string} $str
 * @returns {string}
 */
function lowerFirstLetter($str) {
    return $str[0].toLowerCase() + $str.substr(1);
}

/**
 * Exibe a mensagem de erro em editar o post
 * @param {int} postId O id do post
 * @param {string} text O texto do post
 * @param {string} error A mensagem de erro
 */
function errorEditPost(postId, text, error) {
    if (error !== undefined) {
        errorMessageToPost(postId, "Erro em editar o post: "
                + lowerFirstLetter(error), function () {
            editPost(postId);
        });
    } else {
        errorMessageToPost(postId, "Erro em editar o post.", function () {
            submitEditionPost(postId, text);
        });
    }
}

/**
 * Exibe a mensagem de erro em editar o depoimento
 * @param {int} depositionId O id do depoimento
 * @param {string} text O texto do depoimento
 * @param {string} error A mensagem de erro do depoimento
 */
function errorEditDeposition(depositionId, text, error) {
    if (error !== undefined) {
        errorMessageToDeposition(depositionId, "Erro em editar o depoimento: " +
                lowerFirstLetter(error), function () {
            editDeposition(depositionId);
        });
    } else {
        errorMessageToDeposition(depositionId, "Erro em editar o depoimento.", function () {
            editDeposition(depositionId, text);
        });
    }
}

/**
 * Envia para o servidor os dados de edição do post
 * @param {int} postId O id do post
 * @param {string} text O novo texto do post
 * @returns {undefined}
 */
function submitEditionPost(postId, text) {
    $.post("/post/" + postId + "?action=edit_post", {id: postId, text: text})
            .done(function (data) {
                if (isResponseValid(data)) {
                    text = $.parseJSON(data).text;
                    $("#post" + postId + " .postText").html(text);
                    updateHashtags();
                } else if (isResponseValid(data, "error")) {
                    json = $.parseJSON(data);
                    errorEditPost(postId, text, json.error);
                } else {
                    errorEditPost(postId, text);
                }
            })
            .fail(function () {
                errorEditPost(postId, text);
            });
}

/**
 * Envia para o servidor os novos dados do depoimento que está sendo editado
 * @param {int} depositionId O id do depoimento
 * @param {string} text O novo texto do depoimento
 * @returns {undefined}
 */
function submitEditionDeposition(depositionId, text) {
    $.post("", {action: "edit_deposition",depositionId:depositionId, text: text})
            .always(function (data) {
                if (isResponseValid(data)) {
                    text = $.parseJSON(data).text;
                    $("#post" + depositionId + " .postText").html(text);
                } else if (isResponseValid(data, "error")) {
                    json = $.parseJSON(data);
                    errorEditDeposition(depositionId, text, json.error);
                } else {
                    errorEditDeposition(depositionId, text);
                }
            });
}

/**
 * Selecina uma imagem para o post (abre o dialog para selecionar a imagem)
 * @returns {undefined}
 */
function selectImage() {
    $('#postImage').click();
}

/**
 * Limpa(remove) a imagem selecionada para o post
 * @returns {undefined}
 */
function clearImage() {
    $("#selectImage").html(caption);
    $("#clearImage").css("display", "none");
    document.getElementById('postImage').value = null;
}

/**
 * Realiza uma ação sobre um post
 * @param {int} postId O id do post
 * @param {string} action A ação
 * @param {function} errorFunc A função para caso ocorra um erro
 * @returns {undefined}
 */
function doPostAction(postId, action, errorFunc) {
    link = "/post/" + postId + "?action=" + action;
    $("#" + action + postId).addClass("hide");
    $("#" + reverseAction(action) + postId).removeClass("hide");
    $.ajax(link)
            .done(function (data) {
                if (!isResponseValid(data)) {
                    $("#" + reverseAction(action) + postId).addClass("hide");
                    $("#" + action + postId).removeClass("hide");
                    errorFunc();
                } else {

                }
            })
            .fail(function () {
                $("#" + reverseAction(action) + postId).addClass("hide");
                $("#" + action + postId).removeClass("hide");
                errorFunc();
            });
    if ($("#notbah"+postId).is(":visible")&&$("#nottri"+ postId).is(":visible")){
        $("#post"+postId+" .panel-button").html("<div class='inline-block comment'>Cê é traíra--></div>"+$("#post"+postId+" .panel-button").html());
    } else {
        $("#post"+postId+" .comment").remove();
    }
}

/**
 * Retorna a ação inversa da ação informada
 * @param {String} name O nome da ação 
 * @returns {String} A ação inversa
 */
function reverseAction(name) {
    var type1, type2, result;
    type1 = ["tri", "bah"];
    type2 = ["nottri", "notbah"];
    type1.forEach(function (value, key) {
        if (value === name) {
            result = type2[key];
            return;
        }
    });
    if (result !== undefined)
        return result;
    type2.forEach(function (value, key) {
        if (value === name) {
            result = type1[key];
            return;
        }
    });
    if (result !== undefined)
        return result;
}

/**
 * Marca a postagem como 'tri', 'mas bah' e etc
 * @param {int} postId O id do post
 * @param {string} action A ação. Ex: 'tri','bah'
 * @returns {undefined}
 */
function markPost(postId, action) {
    var originAction = action;
    doPostAction(postId, action, function () {
        if (action === "nottri") {
            action = "Não é tri.";
        } else if (action === "notbah") {
            action = "Não é xucro!";
        } else if (action === "bah") {
            action = "Xucro!";
        } else {
            action = "Tri.";
        }
        message = "Não foi possível marcar a postagem como '" + action + "'";
        errorMessageToPost(postId, message, function () {
            markPost(postId, originAction);
        });
    });
}

/**
 * Confirma se é para deletar tal post
 * @param {int} postId O id da postagem
 * @returns {undefined}
 */
function deletePost(postId) {
    message = new MessageDialog(MessageDialog.MESSAGE_CONFIRM);
    message.setContent("Tem certeza que deseja deletar o post?");
    message.setTitle("Deletar post");
    message.setAutoClose(true);
    sim = message.getButton(0);
    nao = message.getButton(1);
    sim.setClass("btn-warning");
    sim.setCallable(function () {
        confirmDeletePost(postId);
    });
    nao.setClass("btn-primary");
    message.show();
}

/**
 * Confrima se é para deletar o depoimento
 * @param {int} depositionId O id do depoimento
 * @returns {undefined}
 */
function deleteDeposition(depositionId) {
    message = new MessageDialog(MessageDialog.MESSAGE_CONFIRM);
    message.setContent("Tem certeza que deseja deletar teu depoimento?");
    message.setTitle("Deletar depoimento");
    message.setAutoClose(true);
    sim = message.getButton(0);
    nao = message.getButton(1);
    sim.setClass("btn-warning");
    sim.setCallable(function () {
        confirmDeleteDeposition(depositionId);
    });
    nao.setClass("btn-primary");
    message.show();
}

/**
 * Envia para o servidor o comando de deletar o post
 * @param {int} postId O id do post
 * @returns {undefined}
 */
function confirmDeletePost(postId) {
    $.ajax("/post/" + postId + "?action=delete")
            .done(function (data) {
                if (!isResponseValid(data)) {
                    errorDeletePost(postId);
                } else {
                    $("#post" + postId).remove();
                }
            })
            .fail(function () {
                errorDeletePost(postId);
            });
}

/**
 * Envia para o servidor o comando de deletar depoimento
 * @param {int} depositionId O id do depoimento
 * @returns {undefined}
 */
function confirmDeleteDeposition(depositionId) {
    $.post("", {depositionId: depositionId, action: 'delete_deposition'})
            .always(function (data) {
                if (isResponseValid(data)) {
                    $("#post" + depositionId).remove();
                    $("#countDeposition").html($("#countDeposition").html() - 1);
                    $("#depositionFormDiv").show();
                    if ($(".post")[0] === undefined) {
                        $("#section").append("<center>Não há mais depoimentos!</center>");
                    }
                } else {
                    errorDeleteDeposition(depositionId);
                }
            });
}

/**
 * Exibe a mensagem de erro em deletar o post
 * @param {int} postId O id do post
 * @returns {undefined}
 */
function errorDeletePost(postId) {
    errorMessageToPost(postId, "Não foi possível deletar o post.",
            confirmDeletePost);
}

/**
 * Exibe o erro em deletar o depoimento
 * @param {int} depositionId Id do depoimento
 * @returns {undefined}
 */
function errorDeleteDeposition(depositionId) {
    errorMessageToDeposition(depositionId, "Não foi possível deletar o depoimento.",
            confirmDeleteDeposition);
}

/**
 * Exibe mensagem de erro relacionado com post
 * @param {int} postId O id do post
 * @param {string} textMessage A mensagem de erro
 * @param {string} callable_try A função para tentar de novo (ação que tentava mas que deu erro)
 * @returns {errorMessageToPost}
 */
function errorMessageToPost(postId, textMessage, callable_try) {
    message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
    message.setAutoClose(true);
    message.getButton(0).setClass("btn-warning");
    message.addButton(new Button("Ver postagem", "", function (postId) {
        window.open("/post/" + postId, "_blank");
    }.bind(this, postId)));
    message.addButton(new Button("Tentar novamente", "btn-primary", function (postId) {
        callable_try(postId);
    }.bind(this, postId)));
    message.setContent(textMessage);
    message.setTitle("Erro");
    message.show();
}

/**
 * Exibe mensagem de erro relacionado com depoimento
 * @param {int} depositionId O id do post
 * @param {string} textMessage A mensagem de erro
 * @param {string} callable_try A função para tentar de novo (ação que tentava mas que deu erro)
 * @returns {errorMessageToPost}
 */
function errorMessageToDeposition(depositionId, textMessage, callable_try) {
    message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
    message.getButton(0).setClass("btn-warning");
    message.addButton(new Button("Tentar novamente", "btn-primary", function (depositionId) {
        callable_try(depositionId);
    }.bind(this, depositionId)));
    message.setContent(textMessage);
    message.setTitle("Erro");
    message.show();
}

/**
 * Abre o dialog para editar o post
 * @param {int} postId O id do post
 * @returns {undefined}
 */
function editPost(postId) {
    html = $("#postEditDiv").html();
    message = new MessageDialog(MessageDialog.MESSAGE_CUSTOM);
    message.setContent(html);
    message.setTitle("Editar post");
    message.setAutoClose(true);
    message.addButton(new Button("Salvar", "btn-primary", function () {
        text = $(".modal-body .post-edition-text").val();
        submitEditionPost(postId, text);
    }));
    message.addButton(new Button("Fechar"));
    message.show();
    $(".post-edition-id").val(postId);
    text = getPostText(postId);
    $(".post-edition-text").html(text);
}

/**
 * Abre um dialog para editar o depoimento
 * @param {int} depositionId O id do depoimento
 * @returns {undefined}
 */
function editDeposition(depositionId) {
    html = $("#postEditDiv").html();
    message = new MessageDialog(MessageDialog.MESSAGE_CUSTOM);
    message.setContent(html);
    message.setTitle("Editar depoimento");
    message.setAutoClose(true);
    message.addButton(new Button("Salvar", "btn-primary", function () {
        text = $(".modal-body .post-edition-text").val();
        submitEditionDeposition(depositionId, text);
    }));
    message.addButton(new Button("Fechar"));
    message.show();
    $(".post-edition-id").val(depositionId);
    text = getPostText(depositionId);
    $(".post-edition-text").html(text);
}

/**
 * Pega o texto de um post e retorna ele sem tags
 * @param {int} postId O id número do post
 * @returns {string} Texto livre de tags
 */
function getPostText(postId){
    text = $("#post" + postId + " .postText").html();
    video = $("#post"+postId+" .video")[0];
    hashtags = $("#post" + postId + " .postText .hashtag-span");
    text = replaceAll(text,"</span></a>", "");
    for (i=0;i<hashtags.size();i++){
        id = $(hashtags[i]).attr("hash");
        console.log('<span class="label label-primary hashtag-span hash'+id+'" hash="'+id+'">');
        console.log('<span class="label label-primary hashtag-span hash'+id+' label-warning">');
        text = replaceAll(text,'<span class="label label-primary hashtag-span hash'+id+'" hash="'+id+'">','');
        text = replaceAll(text,'<span class="label label-primary hashtag-span hash'+id+' label-warning" hash="'+id+'">','');
    }
    text = replaceAll(text,"<a onclick='searchTag(this)' class='click hashtag'>", "");
    text = replaceAll(text,'<a onclick="searchTag(this)" class="click hashtag">', "");
    text = replaceAll(text,"<br>", "\r\n");
    if (video!==undefined){
        video = ($(video).attr("src")+"").split("/")[4];
        text = text.substr(0,text.indexOf('<iframe class="video"'));
        console.log(text);
        text = "http://www.youtube.com/watch?v="+video+" "+text;
    }
    return text;
}

/**
 * Substituir um texto
 * @param {string} texto A string
 * @param {string} achar O texto a achar
 * @param {string} substituir O texto que vai substituir
 * @returns {string} resultado
 */
function replaceAll(texto,achar,substituir){
    while(text.indexOf(achar)>=0){
        text = text.replace(achar,substituir);
    }
    return text;
}

/**
 * Faz o checkIn (pega a posição de latitude e longitude)
 * @returns {undefined}
 */
function checkIn() {
    $("#location").html("Aguarde...");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showPositionError);
    } else {
        msg = new MessageDialog(MessageDialog.MESSAGE_ERROR);
        msg.setContent("Não é possível fazer o check-in!");
        msg.show();
    }
}

/**
 * Limpa os dados de localização do post
 * @returns {undefined}
 */
function clearLocation() {
    $("#inputLocal").val("");
    $("#inputLatitude").val("");
    $("#inputLongitude").val("");
    $("#location").html(locationHTML);
    $("#clearLocation").hide();
    $("#location").attr("title", "");
}

/**
 * Define o localização de onde a postagem está sendo feita
 * @param {double|string} lat Latitude
 * @param {double|string} lng Longitude
 * @param {string} local O endereço do local
 * @returns {undefined}
 */
function setLocation(lat, lng, local) {
    $("#inputLocal").val(local);
    $("#inputLatitude").val(lat);
    $("#inputLongitude").val(lng);
    $("#location").html(local.substr(0, 51) + "...");
    $("#location").attr("title", local);
    $("#clearLocation").show();
}

/**
 * Com os dados de posição, busca no google o endereço do local e os define na tela
 * @param {mixed} position
 * @returns {undefined}
 */
function showPosition(position) {
    $.ajax("https://maps.googleapis.com/maps/api/geocode/json?latlng="
            + position.coords.latitude + "," + position.coords.longitude).done(
            function (data) {
                local = data.results[0].formatted_address;
                console.log(data);
                setLocation(position.coords.latitude, position.coords.longitude, local);
            });
}

/**
 * Mensagem de erro caso não seja possivel encontrar a localização
 * @param {string} error
 * @returns {undefined}
 */
function showPositionError(error) {
    console.error(error);
    message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
    message.setContent("Não foi possível usar a geolocalização!");
    message.show();
    clearLocation();
}

/**
 * Marca uma notificação como lida ou não lida
 * @param {string} notification O html da notificação
 * @returns {undefined}
 */
function markNotification(notification) {
    note = $(notification);
    link = $("#notification"+note.attr("number-id")+" a").attr("href");
    $.post(link,
            {alreadyRead: (note.is(':checked') ? '1' : '0')});
    setTimeout(function () {
        $("#notification").dropdown("toggle");
    }, 0);
    noteDiv = $("#notification" + note.attr('number-id'));
    if (note.is(':checked')) {
        noteDiv.removeClass('alert-info');
        $("#notificationBadge").html($("#notificationBadge").html() - 1);
    } else {
        noteDiv.addClass('alert-info');
        $("#notificationBadge").html($("#notificationBadge").html() - 1 + 2);
    }
    $("#notificationBadge").html($("#notificationBadge").html() > 0 ? $("#notificationBadge").html() : '');
}

/**
 * Faz o upload de um arquivo sem interferir na página atual
 * @param {string} action_url O url para onde enviar
 * @param {function} event_done A função que deseja executar para quando o 
 *  o envio terminar
 * @returns {undefined}
 */
function fileUpload(action_url, event_done) {
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "upload_iframe");
    iframe.setAttribute("name", "upload_iframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0; height: 0; border: none;");
    $("body").append(iframe);
    window.frames['upload_iframe'].name = "upload_iframe";
    iframeId = document.getElementById("upload_iframe");
    var eventHandler = function () {
        if (iframeId.detachEvent)
            iframeId.detachEvent("onload", eventHandler);
        else
            iframeId.removeEventListener("load", eventHandler, false);
        if (iframeId.contentDocument) {
            content = iframeId.contentDocument.body.innerHTML;
        } else if (iframeId.contentWindow) {
            content = iframeId.contentWindow.document.body.innerHTML;
        } else if (iframeId.document) {
            content = iframeId.document.body.innerHTML;
        }
        event_done(content);
        setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
    };
    if (iframeId.addEventListener)
        iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent)
        iframeId.attachEvent("onload", eventHandler);
    form = $("#formUploadFile");
    form.attr("target", "upload_iframe");
    form.attr("action", action_url);
    form.attr("method", "post");
    form.attr("enctype", "multipart/form-data");
    form.attr("encoding", "multipart/form-data");
    console.info("Upload file:", $("#uploadFile").val());
    form.submit();
    $("#userPhoto").attr("src", "/img/load.gif");
}

/**
 * Editar senha do usuário;
 *  Abre um dialog para o usuário preenchar a senha atual e a nova
 * @returns {undefined}
 */
function editPassword() {
    msg = new MessageDialog(MessageDialog.MESSAGE_CUSTOM);
    msg.addButton(new Button("Salvar", "btn-info", function () {
        saveNewPassword();
    }));
    msg.addButton(new Button("Cancelar", "btn-warning", MessageDialog.FUNCTION_CLOSE));
    msg.setTitle("Alterar a senha");
    msg.setContent($("#divPasswordForm").html());
    msg.show();
    setTimeout(function () {
        $('.modal-body #passwordForm').bootstrapValidator({
            message: 'Esse valor n&ati lde;o é válido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                password: {
                    group: '.form-group',
                    validators: {
                        notEmpty: {
                            message: field_empty
                        },
                        stringLength: {
                            min: 6,
                            max: 12,
                            message: 'A senha deve ter pelo menos 6 à 12 caracteres.'
                        },
                        identical: {
                            field: 'confirmPass',
                            message: 'A senha não combina com o campo de confirmação.'
                        }
                    }
                },
                confirmPass: {
                    group: '.form-group',
                    validators: {
                        notEmpty: {
                            message: field_empty
                        },
                        identical: {
                            field: 'password',
                            message: 'As senhas não combinam!'
                        }
                    }
                },
                currentPass: {
                    group: '.form-group',
                    validators: {
                        notEmpty: {
                            message: field_empty
                        },
                        stringLength: {
                            min: 6,
                            max: 12,
                            message: 'A senha deve ter pelo menos 6 à 12 caracteres.'
                        }
                    }
                }
            }
        });
        $(".modal-body #inputCurrentPass").focus();
    }, 1000);
}

/**
 * Envia para o servidor a nova senha
 * @returns {undefined}
 */
function saveNewPassword() {
    root = ".modal-body ";
    currentPass = $(root + "#inputCurrentPass").val();
    password = $(root + "#inputPassword").val();
    confirmPass = $(root + "#inputConfirmPass").val();
    messageWait("Salvando...");
    $.post("",
            {action: "edit_password", currentPass: currentPass, password: password, confirmPass: confirmPass}
    ).always(function (data) {
        if (isResponseValid(data)) {
            msg = new MessageDialog(MessageDialog.MESSAGE_ALERT);
            msg.setTitle("Senha modificada");
            msg.setContent("Sua senha foi modificada com sucesso!");
            msg.show();
        } else if (isResponseValid(data, "error")) {
            json = $.parseJSON(data);
            msg = new MessageDialog(MessageDialog.MESSAGE_ERROR);
            msg.setContent("Não foi possível modificar a senha: " + json.error);
            msg.show();
        } else {
            msg = new MessageDialog(MessageDialog.MESSAGE_ERROR);
            msg.setContent("Não foi possível modificar a senha! Tente novamente mais tarde.");
            msg.show();
        }
    });
}

/**
 * Faz a validação de um atributo (legal,confiavel,sexy)
 * @param {int} value O valor do atributo
 * @returns {boolean}
 */
function validateAttribute(value) {
    return isFinite(value) && value >= 0 && value <= 100;
}

/**
 * Abre um dialog para o usuário informar os atributos e válida
 * @param {int} reliable O valor atual de confiável
 * @param {int} cool O valor atual de legal
 * @param {int} sexy O valor atual de sexy
 * @returns {undefined}
 */
function editAttributes(reliable, cool, sexy) {
    dialog = new MessageDialog();
    dialog.addButton(new Button("Salvar", "btn-primary", function () {
        reliable = $(".modal-body #inputReliable").val();
        cool = $(".modal-body #inputCool").val();
        sexy = $(".modal-body #inputSexy").val();
        if (validateAttribute(reliable) &&
                validateAttribute(cool) &&
                validateAttribute(sexy)) {
            sendAttributes(reliable, cool, sexy);
        } else {
            error = new MessageDialog(MessageDialog.MESSAGE_ERROR);
            error.setContent("Entre com valores de 0 a 100.");
            error.getButton(0).setCallable(function (dialog) {
                editAttributes(reliable, cool, sexy);
            }).setLabel("Ok");
            error.show();
        }
    }));
    dialog.addButton(new Button("Cancelar", "", MessageDialog.FUNCTION_CLOSE));
    dialog.setTitle("Definir atributos");
    dialog.setContent($("#attributesFormDiv").html());
    dialog.setClass("modal-sm");
    dialog.show();
    if (reliable !== undefined) {
        $(".modal-body #inputReliable").val(reliable);
        $(".modal-body #inputCool").val(cool);
        $(".modal-body #inputSexy").val(sexy);
    }
}

/**
 * Envia os atributos informados para o servidor
 * @param {int} reliable O valor de confiável
 * @param {int} cool O valor de legal
 * @param {int} sexy O valor de sexy
 * @returns {undefined}
 */
function sendAttributes(reliable, cool, sexy) {
    console.log("Enviado atributos:", cool, reliable, sexy);
    wait = new MessageDialog(MessageDialog.MESSAGE_ALERT);
    wait.setTitle("Aguarde...");
    wait.setCloseVisible(false);
    wait.setContent("<img alt='Salvando...' src='/img/load.gif'>");
    wait.show();
    $.post("", {action: "edit_attributes", reliable: reliable,
        cool: cool, sexy: sexy})
            .always(function (data) {
                if (isResponseValid(data)) {
                    window.location.href = window.location.href + "";
                } else if (isResponseValid(data, "error")) {
                    json = $.parseJSON(data);
                    dialog = new MessageDialog(MessageDialog.MESSAGE_ERROR);
                    dialog.setContent("Não foi possível definir os atributos: " +
                            lowerFirstLetter(json.error));
                    dialog.addButton(new Button("Tentar novamente", "btn-primary",
                            function (dialog) {
                                editAttributes(reliable, cool, sexy);
                            }
                    ));
                    dialog.show();
                } else {
                    dialog = new MessageDialog(MessageDialog.MESSAGE_ERROR);
                    dialog.setContent("Não foi possível definir os atributos.");
                    dialog.addButton(new Button("Tentar novamente", "btn-primary",
                            function (dialog) {
                                editAttributes(reliable, cool, sexy);
                            }
                    ));
                    dialog.show();
                }
            });
}

