$(function(){
  //YouCurte.addResizeEvent(Search.resize);
  //Search.resize();
});

function Search(){};

/**
 * Atualiza o formulário de resultados da search
 * @returns {undefined}
 */
Search.update = function(){
    $(".b-more").css('margin-left',($(".div-user").width()-$(".user-img").width()-114)+"px"); 
};

/**
 * Se a pagina redimensionar ele atualiza o search
 * @returns {undefined}
 */
Search.resize = function(){
    Search.update();
};
