var renamedImage = false, photoId = -1;
$(function () {
    /*
     * Verifica quando um foto é selecionado e se o formato está okay, 
     *  envia a foto para o servidor. 
     */
    $('#uploadFile').on("change", function (e) {
        file = $("#uploadFile").val();
        if (validateExtensionImage(file, "#uploadFile")) {
            messageWait("Enviando...");
            fileUpload("", function (data) {
                location.reload();
            });
        }
    });
    /**
     * Quando o álbum selecionado muda, recarrega a página
     */
    $("#selectAlbum").on("change", function (e) {
        location.href = "?albumId=" + $("#selectAlbum").val();
    });
    /**
     * Verifica se alguma foto está selecionada e libera os botões mover e deletar
     */
    setInterval(function () {
        selected = listSelectedCheckboxes();
        count = selected.length;
        if (count < 1) {
            $("#deletePhotoButton").addClass("disabled");
            $("#movePhotoButton").addClass("disabled");
        } else {
            $("#deletePhotoButton").removeClass("disabled");
            $("#movePhotoButton").removeClass("disabled");
        }
        if (renamedImage&&$("#messageDialog").css("display")==='none'){
            location.reload();
        }
    }, 500);
});

/**
 * Abre a janela para selecionar e enviar uma foto
 */
function uploadPhoto() {
    $('#uploadFile').click();
}

/**
 * Seleciona uma foto do album
 * @param {int} id O id da foto
 */
function selectPhoto(id) {
    $("#checkPhoto" + id).prop("checked", !$("#checkPhoto" + id).prop("checked"));
}

/**
 * Retorna as fotos selecionadas 
 * @returns {$} html dos checkboxs
 */
function listSelectedCheckboxes() {
    return $("input:checked");
}

/**
 *  Verifica se tem algum foto selecionada e manda para o servidor quais deletar
 */
function deletePhotos() {
    var selected = listSelectedCheckboxes();
    if (selected.size() <= 0) {
        message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
        message.setContent("Nenhuma foto selecionada para deletar.");
        message.setTitle("Aviso");
        message.show();
    } else {
        message = new MessageDialog(MessageDialog.MESSAGE_CONFIRM);
        message.setContent("Tem certeza que deseja deletar a(s) foto(s) selecionada(s)?");
        message.setTitle("Deletar foto(s)");
        message.getButton("Sim").setCallable(function () {
            var ids = "";
            for (i = 0; i < selected.size(); i++) {
                if (ids !== "") {
                    ids += ",";
                }
                ids += $(selected[i]).attr("photo-id");
            }
            submitForm({ids: ids, action: "delete_photos"});
        });
        message.show();
    }
}

/**
 * Envia para o servidor o comando para deletar o álbum atual junto com suas
 *  fotos;
 */
function deleteAlbum() {
    message = new MessageDialog(MessageDialog.MESSAGE_CONFIRM);
    message.setTitle("Deletar álbum");
    content = "Tem certeza que deseja deletar o álbum selecionado?";
    content += " Todas as fotos do álbum também serão deletadas!";
    message.setContent(content);
    message.getButton("Sim").setCallable(function () {
        id = $("#selectAlbum").val();
        submitForm({albumId: id, action: 'delete_album'}, id);
    });
    message.show();
}

/**
 * Solicita um nome para novo álbum e manda o comando para o servidor para 
 *  criar esse novo álbum
 */
function newAlbum() {
    message = new MessageDialog();
    message.setTitle("Criar novo álbum");
    message.setContent($("#albumForm").html());
    message.addButton(new Button("Criar", "btn-primary", function () {
        albumName = $(".modal-body #inputName").val();
        if (albumName === undefined || albumName.trim().length === 0) {
            message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
            message.setTitle("Nome inválido!");
            message.setContent("O nome do albúm não pode ficar em branco, entre com um nome válido!");
            button = message.getButton(0);
            button.setLabel("Ok");
            button.setCallable(function () {
                newAlbum();
            });
            message.show();
        }
        submitForm({albumName: albumName, action: 'new_album'})
    }));
    message.addButton(new Button("Fechar", "", MessageDialog.FUNCTION_CLOSE));
    message.show();
}

/**
 * Faz o submit de form que cria por si mesmo.
 * @param {Object} inputs Objeto com os inputs do form. Exemplo:
 *      submitForm({action:"deletar_album",id:10});
 */
function submitForm(inputs) {
    var form = document.createElement("form");
    $(form).attr({class: "hide"});
    for (var key in inputs) {
        input = document.createElement("input");
        form.method = "post";
        input.name = key;
        input.value = inputs[key];
        form.appendChild(input);
    }
    document.body.appendChild(form);
    form.submit();
}

/**
 * Solicita o novo nome do álbum e manda para o servidor, reatualizando a pagina
 */
function renameAlbum() {
    var albumId = $("#selectAlbum").val();
    message = new MessageDialog();
    message.setTitle("Renomear álbum");
    message.setContent($("#albumForm").html());
    message.addButton(new Button("Renomear", "btn-primary", function () {
        albumName = $(".modal-body #inputName").val();
        if (albumName === undefined || albumName.trim().length === 0) {
            message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
            message.setTitle("Nome inválido!");
            message.setContent("O nome do albúm não pode ficar em branco, entre com um nome válido!");
            button = message.getButton(0);
            button.setLabel("Ok");
            button.setCallable(function () {
                renameAlbum();
            });
            message.show();
        }
        submitForm({albumName: albumName, action: 'rename_album'});
    }));
    message.addButton(new Button("Fechar", "", MessageDialog.FUNCTION_CLOSE));
    message.show();
    $(".modal-body #inputName").val($("#option" + albumId).html());
}

/**
 * Define a foto do perfil
 * @param {int} id [opcional]O id da foto 
 * @returns {undefined}
 */
function setProfilePhoto(id) {
    if (id === undefined) {
        id = photoId;
    }
    title = $(".photo-title").html();
    image = $("#msgImage").attr("src");
    message = new MessageDialog(MessageDialog.MESSAGE_CONFIRM);
    message.setTitle("Foto para perfil");
    content = "Tem certeza que deseja alterar sua foto de perfil para essa?";
    message.setContent(content);
    message.getButton("Sim").setCallable(function () {
        $.post("", {photoId: id, action: "set_profile"})
                .always(function (data) {
                    if (!isResponseValid(data)) {
                        message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
                        message.setContent("Não foi possível alterar a foto de perfil!");
                        message.getButton(0).setCallable(function () {
                            showAlbumPhoto2(id, image, title);
                        });
                        message.show();
                    } else {
                        renamedImage = true;
                        showAlbumPhoto2(id, image, title);
                    }
                });
    });
    message.show();
}

/**
 * Exibe a foto num dialog
 * @param {int} id O id da foto 
 * @returns {undefined}
 */
function showAlbumPhoto(id) {
    image = $("#image" + id).attr("src");
    title = $("#title" + id).html();
    showAlbumPhoto2(id, image, title);
}

/**
 * Exibe a foto num dialog (com parametros diferentes)
 * @param {int} id O id da foto
 * @param {string} image O link da foto
 * @param {string} title    O titulo da foto
 * @returns {undefined}
 */
function showAlbumPhoto2(id, image, title) {
    message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
    message.setContent("<div class='load-img'><img id='msgImage' src='" + image + "'></div>");
    message.setTitle("<div class='inline-block photo-title'>" + title + "</div>&nbsp;<button class='btn btn-xs btn-warning show-at-photo' onclick='renamePhoto();'>Renomear</button>");
    message.setClass("modal-lg");
    message.addButton(new Button("< Anterior", "btn-primary float-left", function () {
        prevPhoto();
    }));
    message.setClosingEvent(function (modal) {
        if (renamedImage) {
            location.reload();
            return false;
        } else {
            return true;
        }
    });
    message.setSpecialClose(false);
    message.addButton(new Button("Usar no perfil", "btn-warning if-logged-in", function () {
        setProfilePhoto();
    }));
    message.addButton(new Button("Postar", "btn-warning if-logged-in", function () {
        openLink("/posts?photoId=" + photoId, true);
    }));
    /*
     message.addButton(new Button("Renomear", "btn-warning", function () {
     renamePhoto(photoId);
     }));*/

    message.addButton(new Button("Próximo >", "btn-primary", function () {
        nextPhoto();
    }));
    message.show();
    if ($("#deletePhotoButton")[0] === undefined) {
        $(".show-at-photo").hide();
    } else {
        $(".show-at-photo").show();
    }
    photoId = id;
    YouCurte.update();
}

/**
 * Renomeia uma foto. (Abre um dialog pedindo o novo nome da foto)
 * @param {int} id [opcional]O id da foto
 */
function renamePhoto(id) {
    if (id === undefined) {
        id = photoId;
    }
    title = $(".photo-title").html();
    image = $("#msgImage").attr("src");
    message = new MessageDialog();
    message.setTitle("Renomear photo");
    message.setContent($("#albumForm").html());
    message.addButton(new Button("Renomear", "btn-primary", function () {
        photoName = $(".modal-body #inputName").val();
        if (photoName === undefined || photoName.trim().length === 0) {
            message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
            message.setTitle("Nome inválido!");
            message.setContent("O nome da foto não pode ficar em branco, entre com um nome válido!");
            button = message.getButton(0);
            button.setLabel("Ok");
            button.setCallable(function () {
                renamePhoto(id);
            });
            message.show();
        }
        $.post("", {photoName: photoName, photoId: id, action: "rename_photo"})
                .always(function (data) {
                    if (!isResponseValid(data)) {
                        message = new MessageDialog(MessageDialog.MESSAGE_ERROR);
                        message.setContent("Não foi possível renomear a foto!");
                        message.getButton(0).setCallable(function () {
                            showAlbumPhoto2(id, image, photoName);
                        });
                        message.show();
                    }
                });
        renamedImage = true;
        showAlbumPhoto2(id, image, photoName);
    }));
    message.addButton(new Button("Fechar", "", function (dialog) {
        showAlbumPhoto2(id, image, title);
    }));
    message.show();
    $(".modal-body #labelName").html("Nome da foto:");
    $(".modal-body #inputName").val(title);
}

/**
 * Exibe a próxima foto no dialog
 * @param {int} result Parametro enviado para o servidor
 */
function nextPhoto(result) {
    if (result === undefined) {
        result = 0;
    }
    title = $(".photo-title").html();
    image = $("#msgImage").attr("src");
    $("#msgImage").hide();
    $.post("", {action: "set_photo", photoId: photoId, result: result})
            .always(function (data) {
                if (isResponseValid(data)) {
                    json = $.parseJSON(data);
                    photoId = json.id;
                    $("#msgImage").attr("src", json.src).show();
                    $(".photo-title").html(json.name);
                } else {
                    msg = new MessageDialog(MessageDialog.MESSAGE_ERROR);
                    msg.setContent("Não foi possível carregar a próxima foto!");
                    msg.getButton(0).setCallable(function () {
                        showAlbumPhoto2(photoId, image, title);
                    });
                    msg.show();
                }
            });
}

/**
 * Exibe a foto anterior no dialog
 */
function prevPhoto() {
    nextPhoto(1);
}

/**
 * Move as fotos selecionadas para outro álbum
 * @returns {undefined}
 */
function movePhotos() {
    var selected = listSelectedCheckboxes();
    if (selected.size() <= 0) {
        message = new MessageDialog(MessageDialog.MESSAGE_ALERT);
        message.setContent("Nenhuma foto selecionada para mover.");
        message.setTitle("Aviso");
        message.show();
        return;
    }
    message = new MessageDialog();
    message.setTitle("Mover fotos para outro álbum");
    message.setContent($("#selectAlbumDiv").html());
    message.addButton(new Button("Mover", "btn-primary", function () {
        var ids = "";
        id = $(".modal-body .select-album-move").val();
        for (i = 0; i < selected.size(); i++) {
            if (ids !== "") {
                ids += ",";
            }
            ids += $(selected[i]).attr("photo-id");
        }
        submitForm({toAlbumId: id, action: 'move_to', ids: ids});
    }));
    message.addButton(new Button("Cancelar", "", MessageDialog.FUNCTION_CLOSE));
    message.show();
}
